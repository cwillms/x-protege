package de.dfki.x_protege;

import de.dfki.x_protege.model.OntologyCache;
import de.dfki.x_protege.model.X_ModelManager;
import de.dfki.x_protege.model.X_ModelManagerImpl;
import de.dfki.x_protege.model.data.X_Individual;
import de.dfki.x_protege.model.data.X_Property;
import de.dfki.x_protege.model.data.X_Type;
import org.junit.BeforeClass;
import org.junit.Test;

import java.net.URISyntaxException;

import static org.junit.Assert.assertEquals;


/**
 * Created by christian on 13/08/16.
 */
public class MetricsTest {
//    Test_Topic:Test Metrix
//    Case: Add a new Class
//    Subcase: Atom
//    Subcase: Cartesian
//    Case: Remove Class
//    Subcase: Atom
//    Subcase: Cartesian
//    Case: Add a new Prop
//    Subcase: Mixed
//    Subcase: Data
//    Subcase: Obj
//    Case: Remove Prop
//    Subcase: Mixed
//    Subcase: Data
//    Subcase: Obj
//    Case: Add new Instance
//    Subcase: Atom
//    Subcase: Cartesian
//    Case: Remove Instance
//    Subcase: Atom
//    Subcase: Cartesian
//    Setup:
//    Load default onto -> create classes using eventhandler calls -> check metrics (correct number & automatically updated)

    public static X_ModelManager m;

    @BeforeClass
    public static void init() {
        OntologyCache.clear();
        m = new X_ModelManagerImpl(TestUtils.getTestResource("test.nt"),
                TestUtils.getTestResource("test.ns"),
                TestUtils.getTestResource("default.eqred.rdl"));
    }

    @Test
    public void testLoadedEntities() throws URISyntaxException {
        // check number of loaded entities
        int entityCount = OntologyCache.loadedTypes.size() + OntologyCache.loadedProperties.size() + OntologyCache.loadedIndividuals.size();
        assertEquals(62,entityCount);
    }

    @Test
    public void testLoadedClasses() throws URISyntaxException {
        // check overall number of loaded classes
        int classCount = OntologyCache.loadedTypes.size();
        int atomClassCount = 0;
        int cartClassCount = 0;
        for (X_Type t : OntologyCache.loadedTypes.values())
        {
            if(t.isAtomType())
                atomClassCount++;
            else
                cartClassCount++;
        }
        assertEquals(31, classCount);
        // check number of loaded atomic classes
        assertEquals(28, atomClassCount);
        // check number of loaded cartesian classes
        assertEquals(3, cartClassCount);
    }


    @Test
    public void testLoadedProperties() {
        // check overall number of loaded properties
        assertEquals(28, OntologyCache.loadedProperties.size());
        int mixedPropCount = 0;
        int dataPropCount = 0;
        int objectPropCount = 0;
        for (X_Property p : OntologyCache.loadedProperties.values()){
            int i = p.getPropertyType().nextSetBit(0);
            if (i == 0)
                mixedPropCount++;
            else
            if (i == 1)
                dataPropCount++;
            else
                if(i ==2)
                objectPropCount++;
        }
        // check number of mixed properties
        assertEquals(1,mixedPropCount);
        // check number of datatype Props
        assertEquals(2, dataPropCount);
        // check number of object Props
        assertEquals(2, objectPropCount);
    }


    @Test
    public  void testLoadedInstances(){
        // check number of loaded instances
        assertEquals(3, OntologyCache.loadedIndividuals.size());
        // check number of loaded atomic instances
        int numberOfAtomInstances = 0;
        int numberOfCartInstances = 0;
        for (X_Individual i: OntologyCache.loadedIndividuals.values()){
            if(i.isAtomInstance())
                numberOfAtomInstances++;
            else
                numberOfCartInstances++;
        }
        assertEquals(3, numberOfAtomInstances);
        // check number of loaded cartesian instances
        assertEquals(0, numberOfCartInstances);
    }



}
