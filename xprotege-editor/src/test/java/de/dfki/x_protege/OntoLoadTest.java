package de.dfki.x_protege;

import de.dfki.x_protege.model.OntologyCache;
import de.dfki.x_protege.model.X_ModelManagerImpl;
import de.dfki.x_protege.model.data.*;
import org.junit.BeforeClass;
import org.junit.Test;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.BitSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


/**
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 09.07.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class OntoLoadTest {

    @BeforeClass
    public static void init() {
        OntologyCache.clear();
        X_ModelManagerImpl m = new X_ModelManagerImpl(TestUtils.getTestResource("test.nt"),
                TestUtils.getTestResource("test.ns"),
                TestUtils.getTestResource("default.eqred.rdl"));
    }

    @Test
    public void testLoadedEntities() throws URISyntaxException {
        // check number of loaded entities
        int entityCount = OntologyCache.loadedTypes.size() + OntologyCache.loadedProperties.size() + OntologyCache.loadedIndividuals.size();
        assertEquals(62,entityCount);
    }

    @Test
    public void testLoadedClasses() throws URISyntaxException {
        // check overall number of loaded classes
        int classCount = OntologyCache.loadedTypes.size();
        int atomClassCount = 0;
        int cartClassCount = 0;
        for (X_Type t : OntologyCache.loadedTypes.values())
        {
            if(t.isAtomType())
                atomClassCount++;
            else
                cartClassCount++;
        }
        assertEquals(31, classCount);
        // check number of loaded atomic classes
        assertEquals(28, atomClassCount);
        // check number of loaded cartesian classes
        assertEquals(3, cartClassCount);
    }

    @Test
    public  void testLoadedCartClasses() {
        ArrayList<String> cartDate = new ArrayList<>();
        cartDate.add("<xsd:date>");
        cartDate.add("<xsd:date>");
        // check whether <xsd:gyear> x <xsd:gyear> is loaded
        assertTrue(OntologyCache.containsType(cartDate));
    }

    @Test
    public void testLoadedProperties() {
        // check overall number of loaded properties
        assertEquals(28, OntologyCache.loadedProperties.size());
        int mixedPropCount = 0;
        int dataPropCount = 0;
        int objectPropCount = 0;
        for (X_Property p : OntologyCache.loadedProperties.values()){
            int i = p.getPropertyType().nextSetBit(0);
            if (i == 0)
                mixedPropCount++;
            else
                if (i == 1)
                    dataPropCount++;
                else
                    objectPropCount++;
        }
        // check number of mixed properties
        assertEquals(1,mixedPropCount);
        // check number of datatype Props
        assertEquals(2, dataPropCount);
        // check number of object Props
        assertEquals(25, objectPropCount);
    }

    @Test
    public void testComplexProperties_marriedTo() {
        // check whether marriedTo was loaded
        ArrayList<String> marriedTo = new ArrayList<>();
        marriedTo.add("<bio:marriedTo>");
        assertTrue(OntologyCache.containsProperty(marriedTo));
        // check whether marriedTo was populated correctly
        X_Property marriedToProp = OntologyCache.getProperty(marriedTo);
        // check type: must be ObjectProperty
        BitSet type = marriedToProp.getPropertyType();
        assertEquals(2, type.nextSetBit(0));
        // check domain
        assertNotNull(marriedToProp.getExplicitDomain());
        ArrayList<String> bioPerson = new ArrayList<>();
        bioPerson.add("<bio:Person>");
        ArrayList<String> bioMan = new ArrayList<>();
        bioMan.add("<bio:Man>");
        ArrayList<String> bioWoman = new ArrayList<>();
        bioWoman.add("<bio:Woman>");
        assertTrue(marriedToProp.getExplicitDomain().contains(OntologyCache.getType(bioPerson)));
        assertTrue(marriedToProp.getImplicitDomain().contains(OntologyCache.getType(bioMan)));
        assertTrue(marriedToProp.getImplicitDomain().contains(OntologyCache.getType(bioWoman)));
        // check range
        assertTrue(marriedToProp.getExplicitRange().contains(OntologyCache.getType(bioPerson)));
        assertTrue(marriedToProp.getImplicitRange().contains(OntologyCache.getType(bioMan)));
        assertTrue(marriedToProp.getImplicitRange().contains(OntologyCache.getType(bioWoman)));
        // check extra
        ArrayList<String> cartDate = new ArrayList<>();
        cartDate.add("<xsd:date>");
        cartDate.add("<xsd:date>");
        assertTrue(marriedToProp.getExplicitExtra().contains(OntologyCache.getType(cartDate)));
    }
    @Test
    public void testComplexProperties_hasAge() {
        // check whether hasAge was loaded
        ArrayList<String> hasAge = new ArrayList<>();
        hasAge.add("<bio:hasAge>");
        assertTrue(OntologyCache.containsProperty(hasAge));
        // check whether hasAge was populated correctly
        X_Property hasAgeProp = OntologyCache.getProperty(hasAge);
        // check type: must be ObjectProperty
        BitSet type = hasAgeProp.getPropertyType();
        assertEquals(1, type.nextSetBit(0));
        // check domain
        assertTrue(hasAgeProp.getExplicitDomain() != null);
        ArrayList<String> bioPerson = new ArrayList<>();
        bioPerson.add("<bio:Person>");
        ArrayList<String> bioMan = new ArrayList<>();
        bioMan.add("<bio:Man>");
        ArrayList<String> bioWoman = new ArrayList<>();
        bioWoman.add("<bio:Woman>");
        assertTrue(hasAgeProp.getExplicitDomain().contains(OntologyCache.getType(bioPerson)));
        assertTrue(hasAgeProp.getImplicitDomain().contains(OntologyCache.getType(bioMan)));
        assertTrue(hasAgeProp.getImplicitDomain().contains(OntologyCache.getType(bioWoman)));
        // check range
        ArrayList<String> xsdInt = new ArrayList<>();
        xsdInt.add("<xsd:int>");
        assertTrue(hasAgeProp.getExplicitRange().contains(OntologyCache.getType(xsdInt)));
        // check extra
        ArrayList<String> xsdDate = new ArrayList<>();
        xsdDate.add("<xsd:date>");
        assertTrue(hasAgeProp.getExplicitExtra().contains(OntologyCache.getType(xsdDate)));
    }

    @Test
    public  void testLoadedInstances(){
        // check number of loaded instances
        assertEquals(3, OntologyCache.loadedIndividuals.size());
        // check number of loaded atomic instances
        int numberOfAtomInstances = 0;
        int numberOfCartInstances = 0;
        for (X_Individual i: OntologyCache.loadedIndividuals.values()){
            if(i.isAtomInstance())
                numberOfAtomInstances++;
            else
                numberOfCartInstances++;
        }
        assertEquals(3, numberOfAtomInstances);
        // check number of loaded cartesian instances
        assertEquals(0, numberOfCartInstances);
    }

    @Test
    public void testPopulatedInstances(){
        // check whether peter is populated correctly
        ArrayList<String> peter = new ArrayList<>();
        peter.add("<Peter>");
        X_Individual peterInst = OntologyCache.getIndividual(peter);
        ArrayList<String> marriedTo = new ArrayList<>();
        marriedTo.add("<bio:marriedTo>");
        assertNotNull(peterInst);
        assertNotNull(peterInst.getPopulatedProperties(2));
        assertTrue(peterInst.getPopulatedProperties(2).containsKey(OntologyCache.getProperty(marriedTo)));
        // peter hasAge
        ArrayList<String> hasAge = new ArrayList<>();
        hasAge.add("<bio:hasAge>");
        assertTrue(peterInst.getPopulatedProperties(1).containsKey(OntologyCache.getProperty(hasAge)));
    }
}
