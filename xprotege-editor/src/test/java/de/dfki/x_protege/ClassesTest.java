/**
 * 
 */
package de.dfki.x_protege;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.net.URISyntaxException;
import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;

import de.dfki.x_protege.model.OntologyCache;
import de.dfki.x_protege.model.X_ModelManagerImpl;
import de.dfki.x_protege.model.data.AtomicX_Type;
import de.dfki.x_protege.model.data.CartesianX_Type;

/**
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 04.05.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class ClassesTest {

	@BeforeClass
	public static void init() {
		X_ModelManagerImpl m = new X_ModelManagerImpl(TestUtils.getTestResource("pre.default.eqred.nt"),
				TestUtils.getTestResource("default.ns"),
				TestUtils.getTestResource("default.eqred.rdl"));
	}

	@Test
	public void testConstructorAtom() throws URISyntaxException {
		ArrayList<String> name = new ArrayList<>();
		name.add("testAtom");
		AtomicX_Type t = new AtomicX_Type(name);
		assertNotNull(t);
		assertEquals("<testAtom>", t.getShortName().get(0));
	}

	@Test
	public void testConstructorCartesian() throws URISyntaxException {
		ArrayList<String> name = new ArrayList<>();
		name.add("testAtom1");
		name.add("testAtom2");
		CartesianX_Type t = new CartesianX_Type(name);
		assertNotNull(t);
		assertEquals("<testAtom1>", t.getShortName().get(0));
		assertEquals("<testAtom2>", t.getShortName().get(1));
		assertEquals(2, t.getSubElements().size());
		ArrayList<AtomicX_Type> subs = t.getSubElements();
		assertEquals("<testAtom1>", subs.get(0).getShortName().get(0));
		assertEquals("<testAtom2>", subs.get(1).getShortName().get(0));
		assertTrue(OntologyCache.containsType(subs.get(0).getShortName()));
	}

}
