package de.dfki.x_protege;

import java.io.File;

/**
 * Created by christian on 02/07/16.
 */
public class TestUtils {
    private static final File testResourceDir = new File("src/test/resources/");
    private static final File resourceDir = new File("src/main/resources/");


    public static String getResource(String name) {
        // System.out.println(new File(".").getAbsolutePath());
        return new File(resourceDir, name).getPath();
    }

    public static String getTestResource(String name) {
        return new File(testResourceDir, name).getPath();
    }

}
