package de.dfki.x_protege;

import de.dfki.x_protege.model.OntologyCache;
import de.dfki.x_protege.model.X_ModelManager;
import de.dfki.x_protege.model.X_ModelManagerImpl;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.data.X_Type;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.Type.ModelChangeType;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 *
 * Created by christian on 13/08/16.
 */
public class NamespaceTest {
//    Test_Topic: Test Namespace
//    Case: Add new Namespace (test#)
//    Case: Edit Namespace	(rename bio to biograph)
//    Case: Remove Namespace	(remove test, after adding some entities with this namespace)
//    Setup: Load Peter Onto -> manipulate onto as stated in the braces behind the cases -> check whether (1) Ontocache, (2) HFC, and effected entities of other namespaces are correctly represented.

    public static X_ModelManager m;
    public static String test1 = "test:foo"; // class
    public static String test2 = "test:foo2"; //class
    public static String test3 = "test:bar"; //prob

    @BeforeClass
    public static void init() {
        OntologyCache.clear();
         m = new X_ModelManagerImpl(TestUtils.getTestResource("test.nt"),
                TestUtils.getTestResource("test.ns"),
                TestUtils.getTestResource("default.eqred.rdl"));
    }

    @Test
    public void testNamespaceAdd(){

    }

    @Test
    public void testNamespaceEdit(){


    }

    @Test
    public void testNamespaceRemove(){
//        ArrayList<String> name = new ArrayList<>();
//        name.add(test1);
//        Set<? extends ArrayList<String>> testClass1Tuples = OntologyCache.getType(name).getTupleRep();
//        name.clear(); name.add(test2);
//        Set<? extends ArrayList<String>> testClass2 = OntologyCache.getType(name).getTupleRep();
//        name.clear();name.add(test3);
//        Set<? extends ArrayList<String>> testProp = OntologyCache.getProperty(name).getTupleRep();
//        Set<ArrayList<String>> tupleReps = new HashSet<>();
//        tupleReps.addAll(testClass1Tuples); tupleReps.addAll(testClass2);tupleReps.addAll(testProp);
//        m.handleChange(new ModelChange(ModelChangeType.NAMESPACEREMOVE, "test"));
//        //Check whether namespace is correctly updated
//        assertTrue(!OntologyCache.getNamespace().contains("test"));
//        //check whether effected entities where removed from ontocache
//        ArrayList<ArrayList<X_Entity>> results = OntologyCache.search("test");
//        assertEquals(0, results.get(0).size());
//        assertEquals(0, results.get(1).size());
//        assertEquals(0, results.get(2).size());
//        //check whether effected entities where removed from hfc
//        for(ArrayList<String> t: tupleReps){
//            assertTrue(! m.getActiveModel().getHfc().tupleStore.ask(t));
//        }

    }
}