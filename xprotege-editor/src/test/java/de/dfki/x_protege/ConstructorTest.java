/**
 * 
 */
package de.dfki.x_protege;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;

import org.junit.Test;

import de.dfki.x_protege.model.OntologyCache;
import de.dfki.x_protege.model.X_ModelManagerImpl;
import de.dfki.x_protege.model.data.Ontology;;

/**
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 30.04.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class ConstructorTest {

	@Test
	public void testConstructorOnto_NotNull() throws URISyntaxException {
		URI loadedUri = new URI("test");
		Ontology o = new Ontology("test", loadedUri, TestUtils.getTestResource("pre.default.eqred.nt"),
				TestUtils.getTestResource("default.ns"),TestUtils.getTestResource("default.eqred.rdl"));
		assertNotNull(o);
	}

	@Test
	public void testConstructorModel_NotNull() throws URISyntaxException {
		X_ModelManagerImpl m = new X_ModelManagerImpl(TestUtils.getTestResource("pre.default.eqred.nt"),
				TestUtils.getTestResource("default.ns"),
				TestUtils.getTestResource("default.eqred.rdl"));
		assertNotNull(m);
	}

	@Test
	public void testConstructorModel_Cache() throws URISyntaxException {
		OntologyCache.clear();
		X_ModelManagerImpl m = new X_ModelManagerImpl(TestUtils.getTestResource("pre.default.eqred.nt"),
				TestUtils.getTestResource("default.ns"),
				TestUtils.getTestResource("default.eqred.rdl"));
		assertEquals(23, OntologyCache.loadedTypes.size());
		assertEquals(26, OntologyCache.loadedProperties.size());
		assertEquals(0, OntologyCache.loadedIndividuals.size());

	}

}
