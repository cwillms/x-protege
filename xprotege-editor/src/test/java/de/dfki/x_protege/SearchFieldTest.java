package de.dfki.x_protege;

import de.dfki.x_protege.model.OntologyCache;
import de.dfki.x_protege.model.X_ModelManagerImpl;
import de.dfki.x_protege.model.data.X_Entity;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import static org.junit.Assert.assertEquals;


/**
 * Created by christian on 13/08/16.
 */
public class SearchFieldTest {
    //Test_Topic: Test Search Field
//    // Setup: Load Peter onto and simply run search for a bunch of more or less correct strings. Check whether presented results are correct, what happens if there is no result.
    @BeforeClass
    public static void init() {
        OntologyCache.clear();
        X_ModelManagerImpl m = new X_ModelManagerImpl(TestUtils.getTestResource("test.nt"),
                TestUtils.getTestResource("test.ns"),
                TestUtils.getTestResource("default.eqred.rdl"));
    }

    @Test
    public void testCorrectInput() {
        ArrayList<ArrayList<X_Entity>> result = OntologyCache.search("Lisa");
        assertEquals(0,result.get(0).size());
        assertEquals(0,result.get(1).size());
        assertEquals(1,result.get(2).size());
        assertEquals("<Lisa>", result.get(2).get(0).getShortName().get(0));
        result = OntologyCache.search("Person");
        assertEquals(1,result.get(0).size());
        assertEquals("<bio:Person>", result.get(0).get(0).getShortName().get(0));
        assertEquals(0,result.get(1).size());
        assertEquals(0,result.get(2).size());
        result = OntologyCache.search("hasAge");
        assertEquals(0,result.get(0).size());
        assertEquals(1,result.get(1).size());
        assertEquals("<bio:hasAge>", result.get(1).get(0).getShortName().get(0));
        assertEquals(0,result.get(2).size());
    }

    @Test
    public void testPartialInput() {
        ArrayList<ArrayList<X_Entity>> result = OntologyCache.search("Li");
        assertEquals(1,result.get(0).size());
        assertEquals(0,result.get(1).size());
        assertEquals(2,result.get(2).size());
        result = OntologyCache.search("Per");
        assertEquals(1,result.get(0).size());
        assertEquals("<bio:Person>", result.get(0).get(0).getShortName().get(0));
        assertEquals(14,result.get(1).size());
        assertEquals(0,result.get(2).size());
        result = OntologyCache.search("has");
        assertEquals(0,result.get(0).size());
        assertEquals(1,result.get(1).size());
        assertEquals("<bio:hasAge>", result.get(1).get(0).getShortName().get(0));
        assertEquals(0,result.get(2).size());

    }

    @Test
    public void testNotMatchingInput(){
        ArrayList<ArrayList<X_Entity>> result = OntologyCache.search("Bob");
        assertEquals(0,result.get(0).size());
        assertEquals(0,result.get(1).size());
        assertEquals(0,result.get(2).size());
        result = OntologyCache.search("Tea");
        assertEquals(0,result.get(0).size());
        assertEquals(0,result.get(1).size());
        assertEquals(0,result.get(2).size());
        result = OntologyCache.search("drink");
        assertEquals(0,result.get(0).size());
        assertEquals(0,result.get(1).size());
        assertEquals(0,result.get(2).size());
    }


}
