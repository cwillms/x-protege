package de.dfki.x_protege;

import java.io.File;
import java.net.URI;
import java.util.Arrays;
import java.util.List;

import org.protege.editor.core.editorkit.EditorKit;
import org.protege.editor.core.editorkit.EditorKitDescriptor;
import org.protege.editor.core.editorkit.EditorKitFactory;
import org.protege.editor.core.ui.util.UIUtil;

/**
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 17.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class X_EditorKitFactory implements EditorKitFactory {

	public static final String ID = "de.dfki.x_protege.X_EditorKitFactory";

	public static final List<String> EXTENSIONS = Arrays.asList("nt");

	@Override
	public String getId() {
		return ID;
	}

	@Override
	public boolean canLoad(URI uri) {
		String s = uri.toString();
		for (String ext : EXTENSIONS) {
			if (s.endsWith(ext)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public EditorKit createEditorKit() throws Exception {
		return new X_EditorKit(this);
	}

	@Override
	public boolean isValidDescriptor(EditorKitDescriptor descriptor) {
		URI uri = descriptor.getURI(X_EditorKit.URI_KEY);
		if (uri == null || uri.getScheme() == null) {
			return false;
		}
		if (UIUtil.isLocalFile(uri)) {
			File file = new File(uri);
			return file.exists();
		}
		return true;
	}

}
