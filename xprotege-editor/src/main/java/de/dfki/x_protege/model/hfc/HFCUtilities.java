package de.dfki.x_protege.model.hfc;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import de.dfki.lt.hfc.ForwardChainer;
import de.dfki.x_protege.model.data.Ontology;
import de.dfki.x_protege.model.data.X_Entity;

/**
 * This class is used to transform the hfc data structure into a more suitable format for X-Protege.
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 22.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class HFCUtilities {

	private ForwardChainer hfc;

	private HashSet<int[]> simpleTuples = new HashSet<int[]>();
	private HashSet<int[]> complexTuples = new HashSet<int[]>();
	private HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> properties = new HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>>();
	private final boolean verbose = false;

	public HFCUtilities() {
	}

	/**
	 * Parses the hfc datastructure ( a set of tuples) by <br>
	 *     first splitting the set into tuples of length 3 (standard and easy to parse) and those of length greater than 3 (tricky to parse, as structure unknown)<br>.
	 * 	   then the simple tuples are "parsed", i.e. domain, property and range extracted.
	 * 	   finally the complex ones are parsed with respect to the intel we get from parsing the simple ones (e.g. we know the properties etc.)
	 * @param hfc reference to the hfc backend.
	 */
	public X_Entity[] parse(ForwardChainer hfc) {
		clear();
		this.hfc = hfc;
		// sort HFC tuplestore
		sortTuples(hfc.tupleStore.getAllTuples());
		// iterate simple tuples
		populateSimpleProperties(hfc);
		// iterate complex tuples
		populateComplexProperties(hfc);
		// use the data stored in the cache to populate the TreeModel
		HFC_TreeFactory f = new HFC_TreeFactory(properties);
		f.createTrees();
		// populate instances { iterate trees top down and compute all necessary
		// information for each node}
		// clear the cache
		return f.getTreeModels();
	}

	/**
	 *
	 * @param ontology reference to the currently active ontology.
	 * @return An {@link HashMap} containing all tuples connected to the given ontology. The keys of the map are properties, whereas the values are  domain -> range + extra mappings.
	 */
	public HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> getConnectedTuples(
			Ontology ontology) {
		return new HFC_TreeFactory(properties).getConnectedTuples(ontology);
	}

	/**
	 * TODO: is there any way to speed this up?
	 *
	 * Transforms an ontology stored in prefix notation into one stored in infix notation, by using a similiar approach to the original parsing.
	 * Transforms the tuplefile  by <br>
	 *     first splitting the set into tuples of length 3 (standard and easy to parse) and those of length greater than 3 (tricky to parse, as structure unknown)<br>.
	 * 	   then the simple tuples are rearranged, i.e. domain, property and range extracted.
	 * 	   finally the complex ones are rearranged with respect to some the intel we get while processing the simple ones (e.g. we know the properties etc.)
	 *
	 * @param tupleStorePath the path to the tuplefile (.nt) to be transformed
	 * @param usedIOFormat the used I/O format: true ->infix, false -> prefix
	 * @return the path to the rearranged file.
	 * @throws IOException
	 */
	public String parseInput(String tupleStorePath, boolean usedIOFormat)
			throws IOException {
		clear();
		if (usedIOFormat) { // infix used
			return tupleStorePath;
		} else { // prefix used
			// read in tuples and sort tuples
			Set<String[]> restructuredTuples = new HashSet<>();
			Set<String[]> readStringsS = new HashSet<>();
			Set<String[]> readStringsC = new HashSet<>();
			File parsedFile = readIn(tupleStorePath, readStringsS, readStringsC,
					true);
			// restructure
			for (String[] tuple : readStringsS) {
				String pred = getStringRep(hfc, tuple[0]);
				if (properties.containsKey(pred)) {
					ArrayList<String> instance = new ArrayList<>();
					instance.add(getStringRep(hfc, tuple[2]));
					ArrayList<String> domain = new ArrayList<>();
					domain.add(getStringRep(hfc, tuple[1])); // get
																// domain
					if (properties.get(pred).containsKey(domain))
						properties.get(pred).get(domain).add(instance);
					else {
						Set<ArrayList<String>> value = new HashSet<>();
						value.add(instance);
						properties.get(pred).put(domain, value);
					}
				} else {
					HashMap<ArrayList<String>, Set<ArrayList<String>>> nmap = new HashMap<>();
					Set<ArrayList<String>> instances = new HashSet<>();
					ArrayList<String> instance = new ArrayList<>();
					instance.add(getStringRep(hfc, tuple[2]));
					instances.add(instance);
					ArrayList<String> domain = new ArrayList<>();
					domain.add(getStringRep(hfc, tuple[1])); // get
																// domain
					nmap.put(domain, instances);
					properties.put(pred, nmap);
				}
				String[] rstrTuple = {tuple[1], tuple[0], tuple[2], tuple[3]};
				restructuredTuples.add(rstrTuple);
			}
			for (String[] ctuple : readStringsC) {
				if (verbose) {
					System.err.println();
					prettyprintTuple(ctuple);
				}
				String pred = getStringRep(hfc, ctuple[0]);
				int[] arity = parseObject(pred); // TODO: This method could be
													// *much* faster
				String[] rstrTuple = new String[ctuple.length];
				int c = 0;
				if (arity[0] != 0 || arity[1] != 0 || arity[2] != 0
						|| hasExtra(pred)) {

					// write subject to the first position
					if (arity[0] == 0)
						arity[0] = 1;
					for (int i = 1; i <= arity[0]; i++) {
						rstrTuple[c] = ctuple[i];
						c++;
					}
					// write predicate
					rstrTuple[c] = ctuple[0];
					c++;
					// write object and extras
					for (int i = arity[0] + 1; i < ctuple.length; i++) {
						rstrTuple[c] = ctuple[i];
						c++;
					}
					restructuredTuples.add(rstrTuple);
				} else {

					int predPos;
					if (pred.equals(
							"<http://www.w3.org/2000/01/rdf-schema#range>")
							|| pred.equals(
									"<http://www.w3.org/2000/01/rdf-schema#domain>")
							|| pred.equals(
									"<http://de.dfki.xprotege.2015/03/nary#extra>")) {

						rstrTuple[0] = ctuple[1];
						rstrTuple[1] = ctuple[0];
						for (int i = 2; i < ctuple.length; i++)
							rstrTuple[i] = ctuple[i];
					} else {

						if (rstrTuple.length % 2 == 1) {
							predPos = rstrTuple.length - 3;
						} else {
							predPos = (rstrTuple.length / 2) - 1;
						}
						for (int i = 1; i <= predPos; i++) {
							rstrTuple[c] = ctuple[i];
							c++;
						}
						// write predicate
						rstrTuple[c] = ctuple[0];
						c++;
						// write object and extras
						for (int i = predPos + 1; i < ctuple.length; i++) {
							rstrTuple[c] = ctuple[i];
							c++;
						}
					}
					restructuredTuples.add(rstrTuple);
				}

			}
			// write restructured tuples to file
			FileOutputStream fos = new FileOutputStream(parsedFile);

			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
			StringBuilder stbr;
			for (String[] tup : restructuredTuples) {
				stbr = new StringBuilder();
				for (String e : tup) {
					stbr.append(e);
					stbr.append(" ");
				}
				bw.write(stbr.toString());
				bw.newLine();
			}

			bw.close();
			return parsedFile.getPath();
		}
	}

	private boolean hasExtra(String pred) {
		ArrayList<String> predArray = new ArrayList<>();
		predArray.add(pred);
		if (properties.containsKey("<nary:extra>") || properties
				.containsKey("<http://de.dfki.xprotege.2015/03/nary#extra>")) {
			if (properties.containsKey("<nary:extra>")) {
				HashMap<ArrayList<String>, Set<ArrayList<String>>> findings = properties
						.get("<nary:domainArity>");

				if (findings.containsKey(predArray)) {
					return true;
				}
			} else {
				HashMap<ArrayList<String>, Set<ArrayList<String>>> findings = properties
						.get("<http://de.dfki.xprotege.2015/03/nary#extra>");

				if (findings.containsKey(predArray)) {
					return true;
				}
			}
		}
		return false;

	}

	/**
	 * TODO: is there any way to speed this up?
	 *
	 * Transforms an ontology stored in infix notation into one stored in prefix notation, by using a similiar approach to the original parsing.
	 * Transforms the tuplefile  by <br>
	 *     first splitting the set into tuples of length 3 (standard and easy to parse) and those of length greater than 3 (tricky to parse, as structure unknown)<br>.
	 * 	   then the simple tuples are rearranged, i.e. domain, property and range extracted.
	 * 	   finally the complex ones are rearranged with respect to some the intel we get while processing the simple ones (e.g. we know the properties etc.)
	 *
	 * @param tupleStorePath the path to the tuplefile (.nt) to be transformed
	 * @param usedIOFormat the used I/O format: true ->infix, false -> prefix
	 * @return the path to the rearranged file.
	 * @throws IOException
	 */
	public void restructure(String tupleUri2) throws IOException {
		clear();
		Set<String[]> restructuredTuples = new HashSet<>();
		Set<String[]> readStringsS = new HashSet<>();
		Set<String[]> readStringsC = new HashSet<>();
		ArrayList<String> foo;
		boolean found, predFound;
		// READ THE FILE
		File parsedFile = readIn(tupleUri2, readStringsS, readStringsC, false);
		// Restructure tuples
		// parse and restructure simple tuples
		for (String[] tuple : readStringsS) {
			String pred = getStringRep(hfc, tuple[1]);
			if (properties.containsKey(pred)) {
				ArrayList<String> instance = new ArrayList<String>();
				instance.add(getStringRep(hfc, tuple[2]));
				ArrayList<String> domain = new ArrayList<String>();
				domain.add(getStringRep(hfc, tuple[0])); // get
															// domain
				if (properties.get(pred).containsKey(domain))
					properties.get(pred).get(domain).add(instance);
				else {
					Set<ArrayList<String>> value = new HashSet<>();
					value.add(instance);
					properties.get(pred).put(domain, value);
				}
			} else {
				HashMap<ArrayList<String>, Set<ArrayList<String>>> nmap = new HashMap<ArrayList<String>, Set<ArrayList<String>>>();
				Set<ArrayList<String>> instances = new HashSet<>();
				ArrayList<String> instance = new ArrayList<String>();
				instance.add(getStringRep(hfc, tuple[2]));
				instances.add(instance);
				ArrayList<String> domain = new ArrayList<String>();
				domain.add(getStringRep(hfc, tuple[0])); // get
															// domain
				nmap.put(domain, instances);
				properties.put(pred, nmap);
			}
			String[] rstrTuple = {tuple[1], tuple[0], tuple[2], tuple[3]};
			restructuredTuples.add(rstrTuple);
		}
		// parse and restructure complex tuples
		for (String[] tuple : readStringsC) {
			prettyprintTuple(tuple);
			found = false;
			predFound = false;
			foo = new ArrayList<>();
			// prefix format used as I/O-Format
			ArrayList<String> domain = new ArrayList<String>();
			ArrayList<String> instance = new ArrayList<>(); // range and
			String pred = "";
			int predIndex = 0;
			// how to guess the right predicate
			for (int i = 1; i < tuple.length; i++) {
				String s = tuple[i];
				pred = getStringRep(hfc, s);
				if (isProperty(pred)) {
					found = true;
					predIndex = i;
					break;
				} else {
					switch (pred) {
						case "<http://www.w3.org/2000/01/rdf-schema#range>" :
							found = true;
							predIndex = i;
							break;
						case "<http://www.w3.org/2000/01/rdf-schema#domain>" :
							found = true;
							predIndex = i;
							break;
						case "<http://de.dfki.xprotege.2015/03/nary#extra>" :
							found = true;
							predIndex = i;
							break;
						default :
							continue;
					}
					if (found)
						break;
				}
			}
			if (found) {
				for (int i = 0; i < predIndex; i++) {
					domain.add(tuple[i]);
				}
				for (int i = predIndex + 1; i < tuple.length; i++) {
					instance.add(tuple[i]);
				}
			} else {
				for (int i = 0; i < tuple.length; i++) {
					String name = tuple[i];
					if (!predFound) {
						if (properties.containsKey(name)) {
							predFound = true;
							pred = name;
						} else {
							domain.add(name);
						}
					} else {
						instance.add(name);
					}
				}
			}
			foo.add(pred);
			foo.addAll(domain);
			foo.addAll(instance);
			String[] restructuredTuple = foo.toArray(new String[foo.size()]);
			if (properties.get(pred) != null) {
				if (properties.get(pred).containsKey(domain))
					properties.get(pred).get(domain).add(instance);
				else {
					Set<ArrayList<String>> value = new HashSet<>();
					value.add(instance);
					properties.get(pred).put(domain, value);
				}
			} else {
				Set<ArrayList<String>> value = new HashSet<>();
				value.add(instance);
				HashMap<ArrayList<String>, Set<ArrayList<String>>> map = new HashMap<>();
				properties.put(pred, map);
				properties.get(pred).put(domain, value);
			}
			prettyprintTuple(restructuredTuple);
			restructuredTuples.add(restructuredTuple);
		}

		// write restructured tuples to file
		FileOutputStream fos = new FileOutputStream(parsedFile);

		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
		StringBuilder stbr;
		for (String[] tup : restructuredTuples)

		{
			stbr = new StringBuilder();
			for (String e : tup) {
				stbr.append(e);
				stbr.append(" ");
			}
			bw.write(stbr.toString());
			bw.newLine();
		}

		bw.close();
	}

	// ################################## Private Methods

	private void populateComplexProperties(ForwardChainer hfc) {
		for (int[] tuple : complexTuples) {
			if (verbose)
				prettyPrintTupleInt(tuple);
			for (int index = 1; index < tuple.length; index++) {
				String stringRep = getStringRep(hfc, tuple[index]);
				if (isProperty(stringRep)) {
					addComplexTuple(hfc, stringRep, tuple, index);
					break;
				}
			}
		}
	}

	private boolean isProperty(String stringRep) {
		boolean isProp = false;
		if (properties.containsKey(stringRep)
				|| stringRep
						.equals("<http://www.w3.org/2000/01/rdf-schema#domain>")
				|| stringRep
						.equals("<http://www.w3.org/2000/01/rdf-schema#range>")
				|| stringRep
						.equals("<http://de.dfki.xprotege.2015/03/nary#extra>")
				|| stringRep.equals("<rdfs:domain>")
				|| stringRep.equals("rdfs:range>")
				|| stringRep.equals("<nary:extra>")) {
			isProp = true;
		}
		ArrayList<String> name = new ArrayList<>();
		name.add(stringRep);
		if (properties.containsKey(
				"<http://www.w3.org/2000/01/rdf-schema#domain>") && !isProp) {
			isProp = properties
					.get("<http://www.w3.org/2000/01/rdf-schema#domain>")
					.containsKey(name);
		}
		if (properties.containsKey("<rdfs:domain>") && !isProp) {
			isProp = properties.get("<rdfs:domain>").containsKey(name);
		}
		if (properties.containsKey(
				"<http://www.w3.org/2000/01/rdf-schema#range>") && !isProp) {
			isProp = properties
					.get("<http://www.w3.org/2000/01/rdf-schema#range>")
					.containsKey(name);
		}
		if (properties.containsKey("<rdfs:range>") && !isProp) {
			isProp = properties.get("<rdfs:range>").containsKey(name);
		}
		if (properties.containsKey(
				"<http://de.dfki.xprotege.2015/03/nary#rangeArity>")
				&& !isProp) {
			isProp = properties
					.get("<http://de.dfki.xprotege.2015/03/nary#rangeArity>")
					.containsKey(name);
		}
		if (properties.containsKey("<nary:domainArity>") && !isProp) {
			isProp = properties.get("<nary:domainArity>").containsKey(name);
		}
		if (properties.containsKey(
				"<http://de.dfki.xprotege.2015/03/nary#domainArity>")
				&& !isProp) {
			isProp = properties
					.get("<http://de.dfki.xprotege.2015/03/nary#domainArity>")
					.containsKey(name);
		}
		if (properties.containsKey("<nary:rangeArity>") && !isProp) {
			isProp = properties.get("<nary:rangeArity>").containsKey(name);
		}
		if (properties.containsKey(
				"<http://de.dfki.xprotege.2015/03/nary#extra>") && !isProp) {
			isProp = properties
					.get("<http://de.dfki.xprotege.2015/03/nary#extra>")
					.containsKey(name);
		}
		if (properties.containsKey("<nary:extra>") && !isProp) {
			isProp = properties.get("<nary:extra>").containsKey(name);
		}
		if (properties.containsKey(
				"<http://de.dfki.xprotege.2015/03/nary#extraArity>")
				&& !isProp) {
			isProp = properties
					.get("<http://de.dfki.xprotege.2015/03/nary#extraArity>")
					.containsKey(name);
		}
		if (properties.containsKey("<nary:extraArity>") && !isProp) {
			isProp = properties.get("<nary:extraArity>").containsKey(name);
		}
		return isProp;
	}

	private void addComplexTuple(ForwardChainer hfc, String pred, int[] tuple,
			int predPos) {

		if (predPos == -1) {
			if (!properties.containsKey(pred)) {
				HashMap<ArrayList<String>, Set<ArrayList<String>>> instances = new HashMap<ArrayList<String>, Set<ArrayList<String>>>();
				Set<ArrayList<String>> set = new HashSet<>();
				ArrayList<String> instance = new ArrayList<String>();
				ArrayList<String> domain = new ArrayList<String>();
				domain.add(getStringRep(hfc, tuple[0]));
				for (int index = 2; index < tuple.length; index++) {
					instance.add(getStringRep(hfc, tuple[index]));
				}
				set.add(instance);
				instances.put(domain, set);
				properties.put(pred, instances);
			} else {
				ArrayList<String> instance = new ArrayList<String>();
				ArrayList<String> domain = new ArrayList<String>();
				domain.add(getStringRep(hfc, tuple[0]));
				for (int index = 2; index < tuple.length; index++) {
					instance.add(getStringRep(hfc, tuple[index]));
				}
				if (properties.get(pred).containsKey(domain))
					properties.get(pred).get(domain).add(instance);
				else {
					Set<ArrayList<String>> value = new HashSet<>();
					value.add(instance);
					properties.get(pred).put(domain, value);
				}
			}
		} else {
			if (!properties.containsKey(pred)) {

				HashMap<ArrayList<String>, Set<ArrayList<String>>> instances = new HashMap<ArrayList<String>, Set<ArrayList<String>>>();
				Set<ArrayList<String>> set = new HashSet<>();
				ArrayList<String> instance = new ArrayList<String>();
				ArrayList<String> domain = new ArrayList<String>();
				for (int index = 0; index < predPos; index++) {
					domain.add(getStringRep(hfc, tuple[index]));
				}
				for (int index = predPos + 1; index < tuple.length; index++) {
					instance.add(getStringRep(hfc, tuple[index]));
				}
				set.add(instance);

				instances.put(domain, set);
				properties.put(pred, instances);
			} else {

				ArrayList<String> instance = new ArrayList<String>();
				ArrayList<String> domain = new ArrayList<String>();
				for (int index = 0; index < predPos; index++) {
					domain.add(getStringRep(hfc, tuple[index]));
				}
				for (int index = predPos + 1; index < tuple.length; index++) {
					instance.add(getStringRep(hfc, tuple[index]));
				}

				if (properties.get(pred).containsKey(domain))
					properties.get(pred).get(domain).add(instance);
				else {
					Set<ArrayList<String>> value = new HashSet<>();
					value.add(instance);
					properties.get(pred).put(domain, value);
				}
			}
		}
	}

	private void populateSimpleProperties(ForwardChainer hfc) {
		for (int[] tuple : simpleTuples) {
			// if (print) {
			// prettyPrintTupleInt(tuple);
			// }
			String pred = getStringRep(hfc, tuple[1]);
			if (!properties.containsKey(pred)) {
				HashMap<ArrayList<String>, Set<ArrayList<String>>> instances = new HashMap<ArrayList<String>, Set<ArrayList<String>>>();
				Set<ArrayList<String>> set = new HashSet<>();
				ArrayList<String> instance = new ArrayList<String>();
				ArrayList<String> domain = new ArrayList<String>();
				domain.add(getStringRep(hfc, tuple[0])); // get
															// domain
				instance.add(getStringRep(hfc, tuple[2])); // add
															// range
				set.add(instance);
				instances.put(domain, set);
				properties.put(pred, instances);
			} else {
				ArrayList<String> instance = new ArrayList<String>();
				ArrayList<String> domain = new ArrayList<String>();
				domain.add(getStringRep(hfc, tuple[0])); // get
															// domain
				instance.add(getStringRep(hfc, tuple[2])); // add
															// range
				if (properties.get(pred).containsKey(domain))
					properties.get(pred).get(domain).add(instance);
				else {
					Set<ArrayList<String>> value = new HashSet<>();
					value.add(instance);
					properties.get(pred).put(domain, value);
				}
			}

		}
	}

	private void clear() {
		simpleTuples.clear();
		complexTuples.clear();
		properties.clear();
	}

	// '''''''''''''''''''''''''''''''''''' PRIVATE
	// '''''''''''''''''''''''''''''''''''''''''''

	private void sortTuples(Set<int[]> tuples) {
		for (int[] tuple : tuples) {
			if (tuple.length == 3) {
				simpleTuples.add(tuple);
			} else {
				complexTuples.add(tuple);
			}
		}
	}

	private int[] parseObject(String pred) {
		int[] result = new int[]{0, 0, 0};
		ArrayList<String> predArray = new ArrayList<>();
		predArray.add(pred);
		if (properties.containsKey("<nary:domainArity>")
				|| properties.containsKey("<nary:rangeArity>")
				|| properties.containsKey("<nary:extraArity>")) {
			if (properties.containsKey("<nary:domainArity>")) {
				HashMap<ArrayList<String>, Set<ArrayList<String>>> findings = properties
						.get("<nary:domainArity>");

				if (findings.containsKey(predArray)) {
					int dArity = parseXSDInt(findings.get(predArray));
					result[0] = dArity;
				}
			} else {
				if (properties.containsKey("<nary:rangeArity>")) {
					HashMap<ArrayList<String>, Set<ArrayList<String>>> findings = properties
							.get("<nary:rangeArity>");
					if (findings.containsKey(predArray)) {
						int rArity = parseXSDInt(findings.get(predArray));
						result[1] = rArity;
					}
				} else {
					HashMap<ArrayList<String>, Set<ArrayList<String>>> findings = properties
							.get("<nary:extraArity>");
					if (findings.containsKey(predArray)) {
						int rArity = parseXSDInt(findings.get(predArray));
						result[2] = rArity;
					}
				}
			}
		}
		if (properties.containsKey(
				"<http://de.dfki.xprotege.2015/03/nary#domainArity>")
				|| properties.containsKey(
						"<http://de.dfki.xprotege.2015/03/nary#rangeArity>")
				|| properties.containsKey(
						"<http://de.dfki.xprotege.2015/03/nary#extraArity>")) {
			if (properties.containsKey(
					"<http://de.dfki.xprotege.2015/03/nary#domainArity>")) {
				HashMap<ArrayList<String>, Set<ArrayList<String>>> findings = properties
						.get("<http://de.dfki.xprotege.2015/03/nary#domainArity>");
				if (findings.containsKey(predArray)) {
					int dArity = parseXSDInt(findings.get(predArray));
					result[0] = dArity;
				}
			}
			if (properties.containsKey(
					"<http://de.dfki.xprotege.2015/03/nary#rangeArity>")) {
				HashMap<ArrayList<String>, Set<ArrayList<String>>> findings = properties
						.get("<http://de.dfki.xprotege.2015/03/nary#rangeArity>");
				if (findings.containsKey(predArray)) {
					int rArity = parseXSDInt(findings.get(predArray));
					result[1] = rArity;
				}
			}
			if (properties.containsKey(
					"<http://de.dfki.xprotege.2015/03/nary#extraArity>")) {
				HashMap<ArrayList<String>, Set<ArrayList<String>>> findings = properties
						.get("<http://de.dfki.xprotege.2015/03/nary#extraArity>");
				if (findings.containsKey(predArray)) {
					int rArity = parseXSDInt(findings.get(predArray));
					result[2] = rArity;
				}
			}
		}
		return result;
	}

	private String getStringRep(ForwardChainer hfc, Object o) {
		if (o instanceof Integer) {
			return hfc.tupleStore.getObject((Integer) o);
		} else {
			return (String) o;
		}
	}

	private int parseXSDInt(Set<ArrayList<String>> set) {
		if (set.size() > 1) {
			throw new IllegalArgumentException(
					"There must not be more then 1 nary:Arity Tuple for a property");
		} else {
			String[] splittedString = new String[3];
			for (ArrayList<String> v : set) {
				splittedString = v.get(0).split("\"");
			}
			return Integer.parseInt(splittedString[1]);
		}
	}

	private File readIn(String tupleStorePath, Set<String[]> readStringsS,
			Set<String[]> readStringsC, boolean b)
					throws FileNotFoundException, IOException {
		File parsedFile;
		if (b) {
			parsedFile = new File(tupleStorePath.replace(".nt", "_parsed.nt"));
		} else {
			parsedFile = new File(tupleStorePath);
		}
		BufferedReader bfr = new BufferedReader(new FileReader(tupleStorePath));
		String line;
		String[] t;
		while ((line = bfr.readLine()) != null) {
			// strip of spaces at begin and end of line
			line = line.trim();
			// empty lines are NOT recognized as tuples of length 0
			if (line.length() == 0)
				continue;
			// skip comments
			if (line.startsWith("#"))
				continue;
			t = line.split(" ");
			if (t.length > 4) {
				readStringsC.add(t);
			} else {
				readStringsS.add(t);
			}
		}
		bfr.close();
		return parsedFile;
	}

	private void prettyprintTuple(String[] tuple) {
		StringBuilder strb = new StringBuilder();
		for (String s : tuple) {
			strb.append(s);
			strb.append(" ");
		}
		System.err.println(strb.toString());
	}

	private void prettyPrintTupleInt(int[] t) {
		StringBuilder strb = new StringBuilder();
		for (int i : t) {
			strb.append(hfc.tupleStore.getObject(i));
			strb.append(" ");
		}
		System.err.println(strb.toString());
	}

}
