package de.dfki.x_protege.model.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * This class represents simple atomic individuals/instances, e.g. \<bio:Peter\>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 22.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class AtomicX_Indivdual extends X_Individual {

	private final HashSet<CartesianX_Individual> partOf;
	private boolean xsd;

	/**
	 * Creates a new instance of {@link AtomicX_Indivdual}.
	 * @param uri the uri/name of the individual/instance.
     */
	public AtomicX_Indivdual(List<String> uri) {
		super(uri);
		if (uri.get(0).contains("<xsd")) {
			this.xsd = true;
		}
		this.partOf = new HashSet<>();
	}

	@Override
	protected void performExtraPopulation(
			HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> connectedTuples) {
		// Nothing to do

	}

	@Override
	protected Collection<? extends ArrayList<String>> getStructuralTuples() {
		Set<ArrayList<String>> structuralTuples = new HashSet<ArrayList<String>>();
		for (X_Type x : getClazz()) {
			ArrayList<String> t = new ArrayList<String>();
			t.addAll(this.getShortName());
			t.add("<rdf:type>");
			t.addAll(x.getShortName());
			structuralTuples.add(t);
		}
		return structuralTuples;
	}

	/**
	 *
	 * @return All {@link CartesianX_Individual}s this one is part of.
     */
	public Set<CartesianX_Individual> getPartOf() {
		return this.partOf;
	}

	/**
	 *
	 * @return true if instance of xsd type, false otherwise.
     */
	public boolean isXSD() {
		return this.xsd;
	}

	@Override
	protected String getHtmlIcon() {
		if (isXSD())
			return "<font  size=\"4\" color=#808080> &diams;</font> ";
		return "<font  size=\"4\" color=#5F04B4> &diams;</font> ";

	}

	/**
	 * Add the given individual to the set of those this one is part of.
	 * @param newInd the {@link X_Individual} to be added.
     */
	public void addPartOf(X_Individual newInd) {
		if (!this.partOf.contains(newInd)) {
			this.partOf.add((CartesianX_Individual) newInd);
		}
	}

	/**
	 * Remove the given individual from the set of those this one is part of.
	 * @param individual the {@link X_Individual} to be removed.
	 */
	public void removePartOf(X_Individual individual) {
		this.partOf.remove(individual);

	}

	/**
	 * Mark this instance as instance of an xsd type.
	 */
	public void setXSD() {
		this.xsd = true;

	}

	@Override
	public String getPrettyPrint(boolean shortNameSpace) {
		return this.getShortName().get(0);

	}

	@Override
	public boolean isAtomInstance() {
		return true;
	}



}
