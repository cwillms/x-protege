package de.dfki.x_protege.model.data;

import java.util.ArrayList;

/**
 * This class creates the following {@link AnnotationProperty}s: <br>
 *       rdfs:comment>, owl:deprecated, rdfs:isDefinedBy, rdfs:label, rdfs:seeAlso, owl:versionInfo>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 22.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class AnnotationPropertyFactory {

	private final ArrayList<String> comment = new ArrayList<String>();
	private final ArrayList<String> deprecated = new ArrayList<String>();
	private final ArrayList<String> isDefinedBy = new ArrayList<String>();
	private final ArrayList<String> label = new ArrayList<String>();
	private final ArrayList<String> seeAlso = new ArrayList<String>();
	private final ArrayList<String> versionInfo = new ArrayList<String>();
	private ArrayList<AnnotationProperty> annotations;

	/**
	 * Creates a new instance of {@link AnnotationPropertyFactory}.
	 */
	public AnnotationPropertyFactory() {
		comment.add("<rdfs:comment>");
		deprecated.add("<owl:deprecated>");
		isDefinedBy.add("<rdfs:isDefinedBy>");
		label.add("<rdfs:label>");
		seeAlso.add("<rdfs:seeAlso>");
		versionInfo.add("<owl:versionInfo>");
	}

	/**
	 * Creates an {@link AnnotationProperty} of type rdfs:comment.
	 * @return {@link AnnotationProperty} of type rdfs:comment
     */
	public AnnotationProperty createComment() {
		return new AnnotationProperty(comment);
	}

	/**
	 * Creates an {@link AnnotationProperty} of type owl:deprecated.
	 * @return {@link AnnotationProperty} of type owl:deprecated
	 */
	public AnnotationProperty createDeprecated() {
		return new AnnotationProperty(deprecated);
	}

	/**
	 * Creates an {@link AnnotationProperty} of type rdfs:isDefinedBy.
	 * @return {@link AnnotationProperty} of type rdfs:isDefinedBy
	 */
	public AnnotationProperty createIsDefinedBy() {
		return new AnnotationProperty(isDefinedBy);
	}

	/**
	 * Creates an {@link AnnotationProperty} of type rdfs:label.
	 * @return {@link AnnotationProperty} of type rdfs:label
	 */
	public AnnotationProperty createLabel() {
		return new AnnotationProperty(label);
	}

	/**
	 * Creates an {@link AnnotationProperty} of type rdfs:seeAlso.
	 * @return {@link AnnotationProperty} of type rdfs:seeAlso
	 */
	public AnnotationProperty createSeeAlso() {
		return new AnnotationProperty(seeAlso);
	}

	/**
	 * Creates an {@link AnnotationProperty} of type owl:versionInfo.
	 * @return {@link AnnotationProperty} of type owl:versionInfo
	 */
	public AnnotationProperty createVersionInfo() {
		return new AnnotationProperty(versionInfo);
	}


	/**
	 *
	 * @return An {@link ArrayList} containing an instance of all available annotations.
     */
	public ArrayList<AnnotationProperty> getPossibleAnnotations() {
		if (annotations == null) {
			this.annotations = new ArrayList<>();
			this.annotations.add(createComment());
			this.annotations.add(createDeprecated());
			this.annotations.add(createIsDefinedBy());
			this.annotations.add(createLabel());
			this.annotations.add(createSeeAlso());
			this.annotations.add(createVersionInfo());
		}
		return annotations;
	}
}
