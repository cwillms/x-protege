package de.dfki.x_protege.model.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.PriorityQueue;
import java.util.Set;

import de.dfki.x_protege.model.OntologyCache;

/**
 * The {@link X_Type} is a general representation of classes/types connected to
 * a loaded property. There exist two kinds of types within an ontology<br>
 * 1. the well known atomic types, e.g., owl:Thing and its subclasses<br>
 * 2. the cartesian types intorduced by x-protégé. This types consist of at
 * least two atomic types. <br>
 * 
 * @see {@link AtomicX_Type}, {@link CartesianX_Type}<br>
 *      <br>
 *      Author: Christian Willms<br>
 *      German Research Center for Artificial Intelligence (DFKI)<br>
 *      Date: 21.02.2016<br>
 *      <br>
 *      christian.willms@dfki.de<br>
 *      <br>
 */
public abstract class X_Type extends AbstractX_Entity
		implements
			Comparable<X_Type> {

	/**
	 * The {@link X_Type}s which are disjoint to this {@link X_Type}.
	 */
	protected Set<X_Type> disjoints;

	/**
	 * The {@link X_Property}s connected to this {@link X_Type}.
	 */
	protected Set<X_Property> properties;

	/**
	 * The {@link X_Property}s implicitly assigned to this {@link X_Type}.
	 */
	protected Set<X_Property> imProperties;

	/**
	 * The {@link X_Individual}s which have this {@link X_Type}.
	 */
	protected PriorityQueue<X_Individual> individuals;

	/**
	 * The {@link X_Property}s which have this {@link X_Type} as range.
	 */
	protected Set<X_Property> rangeOfProperties;

	protected Set<X_Property> imRangeOfProperties;

	/**
	 * The {@link X_Property}s which have this {@link X_Type} as extra argument.
	 */
	protected Set<X_Property> extraOf;

	protected Set<X_Property> imExtraOf;

	protected boolean disjointsComputed, propertiesComputed,
			individualsComputed = false;


	private final Comparator<X_Individual> nameComparator = new Comparator<X_Individual>() {

		@Override
		public int compare(X_Individual o1, X_Individual o2) {
			return o1.toString().compareTo(o2.toString());
		}

	};

	/**
	 * Creates a new instance of {@link X_Type}.
	 * 
	 * @param list
	 *            The name of this instance of {@link X_Type} represented as a
	 *            {@link List} of String.
	 */
	public X_Type(List<String> list) {
		super(list);
		disjoints = new HashSet<>();
		properties = new HashSet<>();
		imProperties = new HashSet<>();
		individuals = new PriorityQueue<>(nameComparator);
		rangeOfProperties = new HashSet<>();
		imRangeOfProperties = new HashSet<>();
		extraOf = new HashSet<>();
		imExtraOf = new HashSet<>();
	}

	@Override
	public int compareTo(X_Type o) {
		return 0;
	}

	/**
	 * Return the disjoint {@link X_Type}s of this class.
	 * 
	 * @return  the disjoint {@link X_Type}s
	 */
	public Set<X_Type> getDisjoints() {
		return this.disjoints;
	}

	/**
	 * Returns the {@link X_Property}s assigned to this {@link X_Type}.
	 *
	 * @return A {@link Set} of {@link X_Property}s assigned to this {@link X_Type}.
	 */
	public Set<X_Property> getConnectedProperties() {
		return this.properties;
	}

	/**
	 * Returns all {@link X_Individual}s of this class in alphabetic order.
	 * 
	 * @return A {@link PriorityQueue} of all {@link X_Individual}s of this class in alphabetic order.
	 */
	public PriorityQueue<X_Individual> getConnectedIndividualsSorted() {
		return this.individuals;
	}

	/**
	 * Returns all {@link X_Individual}s of this class and all its subclasses in alphabetic order.
	 *
	 * @return A {@link PriorityQueue} of all {@link X_Individual}s of this class and all its subclasses in alphabetic order.
	 */
	public PriorityQueue<X_Individual> getSubIndividualsSorted() {
		PriorityQueue<X_Individual> subIndividuals = new PriorityQueue<>(
				nameComparator);
		for (X_Entity sub : this.subclasses) {
			subIndividuals.addAll(((X_Type) sub).getSubIndividualsSorted());
			subIndividuals
					.addAll(((X_Type) sub).getConnectedIndividualsSorted());
		}
		return subIndividuals;
	}

	@Override
	public void populateConnections(
			HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> connectedTuples) {
		if (this.generated)
			return;
		if (!this.disjointsComputed) {

			computeDisjoints(connectedTuples);

		}
		if (!this.annotationComputed) {

			computeAnnotations(connectedTuples);

		}
		if (!this.propertiesComputed) {

			computeConnectedProperties(connectedTuples);
			computeRangeOfProperties(connectedTuples);

			this.propertiesComputed = true;
		}
		if (!this.individualsComputed) {

			computeIndividuals(connectedTuples);

		}
		performExtraPopulation(connectedTuples);
	}

	private void computeRangeOfProperties(
			HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> connectedTuples) {
		if (connectedTuples.containsKey("<rdfs:range>")) {
			for (Entry<ArrayList<String>, Set<ArrayList<String>>> e : connectedTuples
					.get("<rdfs:range>").entrySet()) {
				this.rangeOfProperties.add(
						(X_Property) OntologyCache.getProperty(e.getKey()));
			}
		}
	}

    /**
     * Computes the additional class specific values based on the tuple representation of the class. Called whenever the class is loaded from from file.
     *
     * @param connectedTuples An {@link HashMap} containing all tuples connected to this class. The keys of the map are properties, whereas the values are  domain -> range + extra mappings.
     */
	protected abstract void performExtraPopulation(
			HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> connectedTuples);

	/**
	 * This method is called after loading an ontology from file to compute the classes disjoint to this one.
	 * @param connectedTuples An {@link HashMap} containing all tuples connected to this entity. The keys of the map are properties, whereas the values are  domain -> range + extra mappings.
	 */
	protected void computeDisjoints(
			HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> connectedTuples) {
		if (connectedTuples.containsKey("<owl:disjointWith>")) {
			for (Entry<ArrayList<String>, Set<ArrayList<String>>> e : connectedTuples
					.get("<owl:disjointWith>").entrySet()) {
				X_Type type;
				for (ArrayList<String> e1 : e.getValue()) {
					if (e1 == this.getShortName()) {
						type = OntologyCache.getType(e.getKey());
					} else {
						type = OntologyCache.getType(e1);
					}
					this.addDisjoint(type);
					type.addDisjoint(this);
				}
			}
		}
		this.disjointsComputed = true;
	}

	/**
	 * This method is called after loading an ontology from file to compute the properties connected to this class.
	 * @param connectedTuples An {@link HashMap} containing all tuples connected to this entity. The keys of the map are properties, whereas the values are  domain -> range + extra mappings.
	 */
	protected void computeConnectedProperties(
			HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> connectedTuples) {
		if (connectedTuples.containsKey("<rdfs:domain>")) {
			for (Entry<ArrayList<String>, Set<ArrayList<String>>> e : connectedTuples
					.get("<rdfs:domain>").entrySet()) {
				X_Property p = (X_Property) OntologyCache
						.getProperty(e.getKey());
				this.properties.add(p);
				p.addExplicitDomain(this);
			}
		}
	}

    /**
     * This method is called after loading an ontology from file to compute the individuals/instances connected to this class.
     * @param connectedTuples An {@link HashMap} containing all tuples connected to this entity. The keys of the map are properties, whereas the values are  domain -> range + extra mappings.
     */
	protected void computeIndividuals(
			HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> connectedTuples) {
		if (connectedTuples.containsKey("<rdf:type>")) {
			for (Entry<ArrayList<String>, Set<ArrayList<String>>> e : connectedTuples
					.get("<rdf:type>").entrySet()) {
				if (!OntologyCache.containsType(e.getKey())) {
					X_Individual i;
					if (OntologyCache.containsIndividual(e.getKey())) {
						i = OntologyCache.getIndividual(e.getKey());
						this.individuals.add(i);
						i.addClazz(this);
					} else {
						if (this.isAtomType()) {
							i = new AtomicX_Indivdual(e.getKey());
						} else {
							i = new CartesianX_Individual(e.getKey());
						}
						OntologyCache.addIndividual(i);
						this.individuals.add(i);
						i.addClazz(this);
					}
				}
			}
		}
		this.individualsComputed = true;
	}

	public void addExplicitProperty(X_Property propertyElement) {
		this.properties.add(propertyElement);
	}

	public void removeExplicitProperty(AbstractX_Property p) {
		this.properties.remove(p);
	}

	public void addImplicitProperty(X_Property propertyElement) {
		this.imProperties.add(propertyElement);
	}

	public void removeImplicitProperty(AbstractX_Property p) {
		this.imProperties.remove(p);
	}

	public void addExplicitRangeOF(X_Property propertyElement) {
		this.rangeOfProperties.add(propertyElement);
	}

	public void addImplicitRangeOf(X_Property propertyElement) {
		this.imRangeOfProperties.add(propertyElement);
	}

	public void addExplicitExtraOf(X_Property p) {
		this.extraOf.add(p);
	}

	public void removeExplicitExtraOf(AbstractX_Property p) {
		this.extraOf.remove(p);
	}

	public void addImplicitExtraOf(X_Property p) {
		this.imExtraOf.add(p);
	}

	public void removeImplicitExtraOf(X_Property p) {
		this.imExtraOf.remove(p);
	}

	public void addIndividual(X_Individual ind) {
		this.individuals.add(ind);
	}

	public void removeIndividual(X_Individual individual) {
		this.individuals.remove(individual);

	}

	/**
	 * Returns the "arity" of this type.
	 * 
	 * @return  If this {@link X_Type} is atomic it is 1
     * else the arity is the number of subelements (e.i., those classes the
     * cartesian type consists of).
	 */
	public abstract int getArity();

    /**
     * Adds the given {@link X_Type} to the set of classes to which this class is disjoint.
     * @param disjointClass the class to be added
     */
	public void addDisjoint(X_Type disjointClass) {
		if (disjointClass != this) {
			this.disjoints.add(disjointClass);
		}
	}

    /**
     * Removes the given {@link X_Type}  from the set of classes to which this class is disjoint.
     * @param disjointClass the class to be removed
     */
	public void removeDisjoint(X_Type disjointClass) {
		this.disjoints.remove(disjointClass);
	}

	@Override
	public Collection<? extends ArrayList<String>> getTuples(boolean rename) {
		Set<ArrayList<String>> tuples = new HashSet<ArrayList<String>>();
		if (rename)
			tuples.addAll(this.getRenameTuples());
		tuples.addAll(this.getDisjointTuples());
		tuples.addAll(this.getIndividualTuples());
		tuples.addAll(this.getStructuralTuples());
		return tuples;
	}

    /**
     * @return Set of tuples ({@link ArrayList} of {@link String}) modeling the subclass hierarchy related to this class.
     */
	protected abstract Collection<? extends ArrayList<String>> getStructuralTuples();

    /**
     * @return Set of tuples ({@link ArrayList} of {@link String}) representing the hfc entries to be replaced after this property was renamed.
     */
	protected abstract Collection<? extends ArrayList<String>> getRenameTuples();


	private Collection<? extends ArrayList<String>> getIndividualTuples() {
		HashSet<ArrayList<String>> individualTuples = new HashSet<>();
		for (X_Individual individual : this.individuals) {
			ArrayList<String> temp = new ArrayList<>();
			temp.addAll(individual.getShortName());
			temp.add("<rdf:type>");
			temp.addAll(this.getShortName());
			individualTuples.add(temp);
		}
		return individualTuples;
	}


	private Collection<? extends ArrayList<String>> getDisjointTuples() {
		HashSet<ArrayList<String>> disjointTuples = new HashSet<ArrayList<String>>();
		for (X_Type disjoint : this.disjoints) {
			ArrayList<String> temp = new ArrayList<String>();
			temp.addAll(this.getShortName());
			temp.add("<owl:disjointWith>");
			temp.addAll(disjoint.getShortName());
			disjointTuples.add(temp);
		}
		return disjointTuples;
	}

    /**
     * Removes the given {@link X_Property} p from the set of properties to which this class is explicitly assigned as range.
     * @param p the property to be removed
     */
	public void removeRangeOf(AbstractX_Property p) {
		this.rangeOfProperties.remove(p);
	}

	public void clearIndividual() {
		this.individuals = new PriorityQueue<X_Individual>(nameComparator);

	}

	/**
     * @return those {@link AbstractX_Property}s that contain this
     * {@link X_Type} as range.
	 */
	public Set<X_Property> getRangeProperties() {
		return this.rangeOfProperties;
	}

	/**
	 *
	 * @return those {@link AbstractX_Property}s that contain this
     * {@link X_Type} as extra argument.
	 */
	public Set<X_Property> getExtraProperties() {
		return this.extraOf;
	}

	/**
	 *
	 * @return true if this instance of {@link X_Type} is of type xsd:AnyType or
     * any of its subclasses.
	 */
	public abstract boolean isXSDType();

	@Override
	public boolean isCompatible(X_Entity p) {
		return false;
	}

	@Override
	public boolean isType() {
		return true;
	}


	@Override
	public String getDescription() {
		StringBuilder strb = new StringBuilder("<html>");
		strb.append("<u><b>URI:</b></u> " + this.getLongName() + "<br>");
		strb.append("<u><b>Annotations:</b></u> <br>");
		for (Annotation a : this.annotations) {
			strb.append(a.render().replace("</html>", ""));
			strb.append("<br>");
		}
		strb.append("<u><b>Properties:</b></u> <br>");
		for (X_Property p : this.properties) {
			strb.append(p.render(true));
			strb.append("<br>");
		}
		strb.append("<u><b>Disjoints:</b></u> <br>");
		for (X_Type d : this.disjoints) {
			strb.append(d.render(true));
			strb.append("<br>");
		}
		strb.append("</html>");
		return strb.toString();
	}

	/**
     * Removes the given {@link X_Property} p from the set of properties to which this class is implicitly assigned as range.
	 * @param p the property to be removed
	 */
	public void removeImplicitRangeOf(AbstractX_Property p) {
		this.imRangeOfProperties.remove(p);
	}

	/**
	 * @return Those properties where this class is assigned as domain.
	 */
	public Set<X_Property> getConnectedImProperties() {
		return this.imProperties;
	}

	/**
	 * @return Those properties where this class is assigned as range.
	 */
	public Set<X_Property> getRangeImProperties() {
		return this.imRangeOfProperties;
	}

	/**
	 * @return Those properties where this class is assigned as extra argument.
	 */
	public Set<X_Property> getExtraImProperties() {
		return this.imExtraOf;
	}

	/**
	 * @return A {@link Set} containing the superclasses of this one.
	 */
	public Set<X_Entity> getSuperTypes() {
		return this.superClasses;
	}

}
