package de.dfki.x_protege.model.data;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * This class represents an annotation property, one of: <br>
 *       rdfs:comment>, owl:deprecated, rdfs:isDefinedBy, rdfs:label, rdfs:seeAlso, owl:versionInfo>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 22.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class AnnotationProperty extends AbstractX_Property {

	/**
	 * Creates a new instance of {@link AnnotationProperty} specified by the given type.
	 * @param type the type of the annotation property.
     */
	public AnnotationProperty(List<String> type) {
		super(type);
	}

	@Override
	protected void computeCharacteristics(
			HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> connectedTuples) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void performExtraPopulation(
			HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> connectedTuples) {
		// TODO Auto-generated method stub

	}

	@Override
	protected Collection<? extends ArrayList<String>> getStructuralTuples() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BitSet getSupportedCharacteristics() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BitSet getPropertyType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void computeInverse(
			HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> connectedTuples) {
		// TODO Auto-generated method stub

	}

	@Override
	protected String getHtmlIcon() {
		// TODO Auto-generated method stub
		return null;
	}

	protected void changeShortName(String localNs) {
		for (String s : shortName) {
			backupShortName.add(s);
		}
	}


	@Override
	protected Collection<? extends ArrayList<String>> getRenameTuples() {
		// Nothing to do here, as annotation properties can not be renamed.
		return null;
	}

}
