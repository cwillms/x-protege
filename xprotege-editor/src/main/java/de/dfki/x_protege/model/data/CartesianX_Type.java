package de.dfki.x_protege.model.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.dfki.x_protege.model.OntologyCache;

/**
 * This class represents more complex, cartesian class/type, e.g. \<xsd:year\> x \<xsd:year\>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 22.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class CartesianX_Type extends X_Type {

	private final ArrayList<AtomicX_Type> subElements;

	private boolean subElementsComputed = false;

    /**
     * Creates a new instance of {@link CartesianX_Type}.
     * @param uri the uri/name of the class/type.
     */
	public CartesianX_Type(List<String> uri) {
		super(uri);
		this.subElements = new ArrayList<>();
		ArrayList<String> name = new ArrayList<>();
		if (!uri.contains("<nary:Thing+>") && !uri.contains("<nary:Class>"))
			for (String subName : uri) {
				name.add(subName);
				AtomicX_Type subElement = (AtomicX_Type) OntologyCache
						.getType(name);
				if (subElement != null) {
					this.subElements.add(subElement);
					subElement.addCartType(this);
				} else {
					subElement = new AtomicX_Type(name);
					OntologyCache.addType(subElement);
					this.subElements.add(subElement);
					subElement.addCartType(this);
				}
				name.clear();
			}
	}

	@Override
	protected void performExtraPopulation(
			HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> connectedTuples) {
		// Nothing to do here

	}

    /**
     *
     * @return All {@link AtomicX_Type}s that are contained in this cartesian type.
     */
	public ArrayList<AtomicX_Type> getSubElements() {
		return this.subElements;
	}

	@Override
	public int getArity() {
		return getShortName().size();
	}

	public void addSubelements(ArrayList<AtomicX_Type> arrayList) {
		for (AtomicX_Type s : arrayList) {
			this.subElements.add(s);
		}
	}


	public void addSubelement(AtomicX_Type string) {
		this.subElements.add(string);
	}

	public void removeSubelement(int index) {
		this.subElements.remove(index);

	}

	public void setSubelement(int selectedElementIndex,
			AtomicX_Type lastPathComponent) {
		this.subElements.set(selectedElementIndex, lastPathComponent);

	}

    /**
     * Recomputes the uri and shortname of the class after one of its subelements changed, e.g. by renaming it.
     */
	public void revalidateName() {
		ArrayList<String> newName = new ArrayList<>();
		for (X_Entity se : subElements) {
			newName.add(se.getShortName().get(0));
		}
		this.setShortName(newName);
	}

	@Override
	protected Collection<? extends ArrayList<String>> getStructuralTuples() {
		Set<ArrayList<String>> structuralTuples = new HashSet<>();
		for (X_Entity sup : this.superClasses) {
			ArrayList<String> t = new ArrayList<>();
			t.addAll(this.getShortName());
			t.add("<rdfs:subClassOf>");
			t.addAll(sup.getShortName());
			structuralTuples.add(t);
		}
		return structuralTuples;
	}


	@Override
	protected Collection<? extends ArrayList<String>> getRenameTuples() {
		// the computed set should contain the tuples off subclasses
		Set<ArrayList<String>> renameTuples = new HashSet<>();
		for (X_Entity sub : this.subclasses) {
			ArrayList<String> t = new ArrayList<>();
			t.addAll(sub.getShortName());
			t.add("<rdfs:subClassOf>");
			t.addAll(this.getShortName());
			renameTuples.add(t);
		}
		for (X_Entity sub : this.individuals) {
			ArrayList<String> t = new ArrayList<>();
			t.addAll(sub.getShortName());
			t.add("<rdf:type>");
			t.addAll(this.getShortName());
			renameTuples.add(t);
		}
		return renameTuples;
	}

	@Override
	protected String getHtmlIcon() {
		return "<font  size=\"4\" color=#FF0000> &#9679;</font> ";
	}

	@Override
	public boolean isXSDType() {
		for (AtomicX_Type sub : this.getSubElements()) {
			if (sub.isXSDType())
				return true;
		}
		return false;
	}

	@Override
	public boolean isCartesianType() {
		return true;
	}
}
