package de.dfki.x_protege.model.utils;

import java.util.List;

import de.dfki.x_protege.model.OntologyCache;
import de.dfki.x_protege.model.data.AtomicX_Type;
import de.dfki.x_protege.model.data.CartesianX_Type;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.data.X_Type;

/**
 * Implementation of the {@link ElementProvider} interface specialized on the creation of {@link X_Type}s.
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 22.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class TypeElementProvider extends ElementProvider {

	private boolean check = true;

	public TypeElementProvider() {

	}

	public TypeElementProvider(boolean check) {
		this.check = check;
	}

	@Override
	public X_Entity createNewEntity(List<String> list, X_Entity parent) {
		X_Entity result;
		if (parent.isAtomType()) {
			result = new AtomicX_Type(list);
		} else {
			result = new CartesianX_Type(list);
		}
		parent.addSub(result);
		result.setGenerated(true);
		if (check) {
			OntologyCache.addType((X_Type) result);
		}
		return result;
	}

}
