package de.dfki.x_protege.model.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.dfki.x_protege.model.OntologyCache;
/**
 * This class represents more complex, cartesian instance/individual, e.g. \"1992"^^<xsd:year\> x "1999"^^\<xsd:year\>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 22.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class CartesianX_Individual extends X_Individual {

	private final List<AtomicX_Indivdual> contains;

	/**
	 * Creates a new instance of {@link CartesianX_Individual}.
	 * @param uri the uri/name of the class/type.
	 */
	public CartesianX_Individual(List<String> uri) {
		super(uri);
		this.contains = new ArrayList<>();
		this.computeSubElements(uri);
	}

	private void computeSubElements(List<String> list) {
		contains.clear();
		ArrayList<String> sname = new ArrayList<>();
		for (String s : list) {
			sname.add(s);
			if (s.contains("<xsd:")) {
				AtomicX_Indivdual si = new AtomicX_Indivdual(sname);
				si.setXSD();
				contains.add(si);
				si.addPartOf(this);
			} else {
				if (OntologyCache.getIndividual(sname) != null) {
					contains.add((AtomicX_Indivdual) OntologyCache
							.getIndividual(sname));
					((AtomicX_Indivdual) OntologyCache.getIndividual(sname))
							.addPartOf(this);
				}
			}
			sname.clear();
		}
	}

	@Override
	protected void performExtraPopulation(
			HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> connectedTuples) {
		// Nothing to do

	}

	@Override
	protected Collection<? extends ArrayList<String>> getStructuralTuples() {
		Set<ArrayList<String>> structuralTuples = new HashSet<ArrayList<String>>();
		for (X_Type x : getClazz()) {
			ArrayList<String> t = new ArrayList<String>();
			for (AtomicX_Indivdual si : contains) {
				t.add(si.getPrettyPrint(false));
			}
			t.add("<rdf:type>");
			t.addAll(x.getShortName());
			structuralTuples.add(t);
		}
		return structuralTuples;
	}

	/**
	 *
	 * @return All {@link AtomicX_Indivdual}s that are contained in this cartesian individual.
     */
	public List<AtomicX_Indivdual> getContains() {
		if (contains.contains(null) || contains.isEmpty()) {
			computeSubElements(this.getShortName());
		}
		return contains;
	}

	@Override
	protected String getHtmlIcon() {
		return "<font size=\"4\" color=#E10909> &diams;</font> ";
	}

	@Override
	public String getPrettyPrint(boolean shortNameSpace) {
		StringBuilder strb = new StringBuilder();
		for (AtomicX_Indivdual i : this.contains) {
			strb.append(i.getPrettyPrint(shortNameSpace));
			strb.append(" ");
		}
		return strb.toString().trim();
	}

	@Override
	public boolean isCartesianInstance() {
		return true;
	}

	public void recomputeShortName() {
		this.shortName = new ArrayList<>();
		for (X_Entity e : contains)
			shortName.add(e.getShortName().get(0));
	}
}
