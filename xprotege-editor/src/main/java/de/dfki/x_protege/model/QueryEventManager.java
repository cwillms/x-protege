/**
 * 
 */
package de.dfki.x_protege.model;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;

import de.dfki.lt.hfc.BindingTable;
import de.dfki.lt.hfc.Query;
import de.dfki.lt.hfc.QueryParseException;
import de.dfki.x_protege.model.data.Ontology;
import de.dfki.x_protege.model.data.X_BindingTableModel;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.ui.logic.EventManager;
import de.dfki.x_protege.ui.logic.Event.Event;
import de.dfki.x_protege.ui.logic.Event.QueryEvent;
import de.dfki.x_protege.ui.logic.Event.Type.QueryEventType;
import de.dfki.x_protege.ui.logic.listener.EventListener;
import de.dfki.x_protege.ui.logic.listener.QueryEventListener;

/**
 * The {@link QueryEventManager} is used to connect thee query tab and the model by using so called {@link QueryEvent}s. Those events are handled and hfc and/or the model are searched accordingly.
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 22.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class QueryEventManager extends EventManager {

	/**
	 * Creates a new instance of the {@link QueryEventManager} for the given ontology.
	 * @param ontology the currently loaded ontology.
	 */
	public QueryEventManager(Ontology ontology) {
		super(ontology);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void handle(Event event) {
		QueryEvent change = null;
		QueryEvent c = (QueryEvent) event;
		switch (c.getType()) {
			case SUGGESTION : {
				// TODO compute suggestions
				// change = new ;
				break;
			}
			default :
				// forward the query to hfc, send the resulting X_bindingtable
				// back
				Set<int[]> allTuples = relatedOntology.getHfc().tupleStore
						.getAllTuples();
				for (int[] t : allTuples) {
					StringBuilder strb = new StringBuilder();
					for (int id : t) {
						strb.append(relatedOntology.getHfc().tupleStore
								.getJavaObject(id));
					}
				}
				String queryString = (String) c.getValue();
				Query q = new Query(relatedOntology.getHfc());
				System.err.println((String) c.getValue());
				try {
					BindingTable bt = q.query((String) c.getValue());
					String[] vars = bt.getVars();
					Arrays.sort(bt.getVars(), (a, b) -> bt.obtainPosition(a)
							- bt.obtainPosition(b));
					X_BindingTableModel xbtm = new X_BindingTableModel(vars,
							transform(bt), (String) c.getValue());
					change = new QueryEvent(xbtm, QueryEventType.QUERY, null);
				} catch (QueryParseException e) {
					change = new QueryEvent(e.getMessage(),
							QueryEventType.NOTVALID, null);
					break;
				}

				break;
		}
		for (EventListener qel : this.listener) {
			((QueryEventListener) qel).queryEvent(change);
		}
	}

	private X_Entity[][] transform(BindingTable bt) {
		X_Entity[][] transformedTable = new X_Entity[bt.table
				.size()][bt.getVars().length];
		Iterator hfcTableIterator = bt.iterator();
		int column = 0;
		String[] vars = bt.getVars();
		while (hfcTableIterator.hasNext()) {
			int[] tuple = (int[]) hfcTableIterator.next();
			String[] stringTuple = relatedOntology.getHfc().tupleStore
					.toString(tuple).split(" ");
			X_Entity[] row = new X_Entity[vars.length];
			for (int i = 0; i < row.length; i++)
				row[i] = OntologyCache
						.getEntity(stringTuple[bt.obtainPosition(vars[i])]);
			transformedTable[column++] = row;
		}
		return transformedTable;
	}

}
