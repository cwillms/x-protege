package de.dfki.x_protege.model.data;

import java.util.ArrayList;

import de.dfki.x_protege.model.OntologyCache;

/**
 * This class represents an annotation and mainly consists of an {@link X_Entity} (domain), {@link AnnotationProperty} and a value (range).
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 22.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class Annotation {

	private AnnotationProperty type;

	private X_Entity domain;

	private String value;

	/**
	 * Creates a new instance of {@link Annotation}.
	 * @param domain the {@link X_Entity} the annotation belongs to.
	 * @param t the {@link AnnotationProperty}
	 * @param comment the value of the annotation.
     */
	public Annotation(X_Entity domain, AnnotationProperty t, String comment) {
		this.setDomain(domain);
		this.setType(t);
		this.setValue(comment);
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(String value) {
		if (!(value.startsWith("\"") && value.contains("\"^^")))
			value = "\"" + value.replace("^^", "\"^^");
		this.value = value;
	}

	/**
	 * @return the domain
	 */
	public X_Entity getDomain() {
		return domain;
	}

	/**
	 * @param domain
	 *            the domain to set
	 */
	public void setDomain(X_Entity domain) {
		this.domain = domain;
	}

	/**
	 * @return the t
	 */
	public AnnotationProperty getType() {
		return type;
	}

	/**
	 * @param t
	 *            the t to set
	 */
	public void setType(AnnotationProperty t) {
		this.type = t;
	}

	/**
	 * This method returns a formatted, human readable representation of the annotation.
	 * @return A html based string, formatting and  highlighting the content of the annotation.
     */
	public String render() {
		StringBuilder strb = new StringBuilder();
		strb.append("<html><font color=blue><b>");
		strb.append(type.render());
		strb.append("</b></font><br> &emsp; &emsp; &emsp; &emsp; &emsp;");
		strb.append(this.value.replace("<", "&lt;").replace(">", "&gt;"));
		strb.append("</html>");
		return strb.toString();
	}

	/**
	 *
	 * @return A tuple ({@link ArrayList}) modeling the annotation.
     */
	public ArrayList<String> getTuple() {
		ArrayList<String> tuple = new ArrayList<>();
		tuple.addAll(domain.getShortName());
		tuple.addAll(this.type.getShortName());
		tuple.add(this.value.replace(" ", "_").replace("xsd:",
				OntologyCache.getNamespace().getLongForShort("xsd")));

		return tuple;
	}

}
