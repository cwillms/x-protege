package de.dfki.x_protege.model.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * The {@link X_Entity} represents the entities of an ontology.<br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 21.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public interface X_Entity {

	/**
	 *
	 * @return The direct sub entities of this {@link X_Entity}.
     */
	Set<X_Entity> getSubs();

	/**
	 *
	 * @return The sub entities of this {@link X_Entity}.
	 */
	Set<X_Entity> getAllSubs();

	/**
	 * Adds the given entity to the set of sub entities
	 * @param entity the {@link X_Entity} to be added.
     */
	void addSub(X_Entity entity);

	/**
	 * Removes the given entity from the set of sub entities
	 * @param entity the {@link X_Entity} to be removed
     */
	void removeSub(X_Entity entity);

	/**
	 * 
	 * @return the name of an entity represented as {@link ArrayList} of
     * {@link String}s.
	 */
	ArrayList<String> getShortName();

	/**
	 *
	 * @return the namespace connected to this entity.
	 */
	String getNamespace();

	/**
	 *
	 * @return a pretty print string representation of this entity used to
     * represent it in the UI.
	 */
	String render();

	/**
	 * 
	 * @param HTMLICON
	 *            Use an html environment for the String representation.
	 * @return a pretty print string representation of this entity used to
     * represent it in the UI.
	 */
	String render(boolean HTMLICON);

	/**
	 * Compute the entities that are connected to this one.
	 * 
	 * @param connectedTuples An {@link HashMap} containing all tuples connected to this {@link X_Entity}. The keys of the map are properties, whereas the values are  domain -> range + extra mappings.
	 */
	 void populateConnections(
			HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> connectedTuples);

	/**
	 *
	 * @param b boolean flag indicating whether this entity was generated in this very session of x-Protege
	 */
	 void setGenerated(boolean b);

	/**
	 * @param rename flag indicating whether this method was triggered by an rename event.
	 * 
	 * @return a tuple representation of this entity. A tuple representation
     * contains all attributes of this entity (e.g. subclasses, connected
     * properties, etc)
	 */
	Set<? extends ArrayList<String>> getTupleRep(boolean rename);

    /**
     *
     * @return a tuple representation of this entity. A tuple representation
     * contains all attributes of this entity (e.g. subclasses, connected
     * properties, etc)
     */
	 Set<? extends ArrayList<String>> getTupleRep();

	/**
	 *
	 * @return a tuple representation of this entity. A tuple representation
     * contains all attributes of this entity (e.g. subclasses, connected
     * properties, etc)
	 */
	 Collection<? extends ArrayList<String>> getTuples(boolean rename);

	/**
	 * 
	 * @return the {@link Annotation}s connected to this entity.
	 */
    Set<Annotation> getAnnotations();

	/**
	 * Adds an {@link Annotation} to this entity.
	 * 
	 * @param anno the annotation to be added.
	 */
	void addAnnotation(Annotation anno);

	/**
	 * Remove the given {@link Annotation} from this entity.
	 * 
	 * @param annotation the annotation to be removed
	 */
	void removeAnnotation(Annotation annotation);

	/**
	 * Change the value of the given {@link Annotation} to the given
	 * {@link String}
	 * 
	 * @param anno the {@link Annotation} to be updated
	 * @param value the new value for the given annotation
	 */
	void editAnnotation(Annotation anno, String value);

	/**
	 * 
	 * @return the short namespace of this entity.
	 */
	 String getPrefix();

	/**
	 * Change the uri of this entity to the given one.
	 * 
	 * @param uri the new uri
	 */
	void setShortName(List<String> uri);

	/**
	 * Returns true if this entity can be compared to the given one.
	 *
	 * @param p the {@link X_Entity} to compare this one to
	 * @return true if this entity can be compared to the given one, false otherwise
	 */
	 boolean isCompatible(X_Entity p);

	/**
	 * 
	 * @return true if this entity represents a Type/class within an ontology, false otherwise.
	 */
	boolean isType();

	/**
	 * 
	 * @return true if this entity represents a property within an ontology, false otherwise.
	 */
	boolean isProperty();

	/**
	 * 
	 * @return true if this entity represents a instance/individual within an
     * ontology, false otherwise.
	 */
	boolean isInstance();

	/**
	 * 
	 * @return true if this entity is atomic, false otherwise
	 */
	boolean isAtomType();

	/**
	 * 
	 * @return true if this entity is cartesian, false otherwise
	 */
	boolean isCartesianType();

	/**
	 * 
	 * @return true if this entity is an atomic instance/individual.
	 */
	boolean isAtomInstance();

	/**
	 * 
	 * @return true if this entity is cartesian instance/individual.
	 */
	boolean isCartesianInstance();

    /**
     * removes the given entity to the set of super entities
     * @param entity the {@link X_Entity} to be removed.
     */
	void removeSuper(X_Entity entity);

    /**
     * Adds the given entity to the set of super entities
     * @param entity the {@link X_Entity} to be added.
     */
	void addSuper(X_Entity entity);

    /**
     *
     * @return a short html based description of the entity featuring the shortname, its kind(instance, type or property) and annotations
     */
    String getDescription();

    /**
     * @return A non html pretty print representation of this entity
     */
    String toStringPretty();

    /**
     *
     * @return String representation of this entity using its long namespace
     */
	String getLongName();

	/**
	 * @return the {@link X_Entity}s superior to this one in the subclass hierarchy
	 */
	Collection<?> getSuper();

}
