package de.dfki.x_protege.model;

import java.util.*;
import java.util.Map.Entry;

import de.dfki.x_protege.model.data.AtomicX_Type;
import de.dfki.x_protege.model.data.CartesianX_Type;
import de.dfki.x_protege.model.data.Ontology;
import de.dfki.x_protege.model.data.PrefixModel;
import de.dfki.x_protege.model.data.UnknownEntity;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.data.X_Individual;
import de.dfki.x_protege.model.data.X_Property;
import de.dfki.x_protege.model.data.X_Type;


/**
 * This class is used as a Cache containing all classes, instances and properties loaded/contained in the currently loaded ontology. It is mainly used to speed up every kind of lookup in the actual model by referring from the string representation of an entity to the corresponding java class. <br>
 * Further, this class consists of various field connected to the Ontology, such as namespaces, a list of already used names (used to avoid collisions), and so on.<br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 17.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class OntologyCache {

	/**
	 * An {@link PrefixModel} representing the namespace of the currently loaded ontology.
	 */
	private static PrefixModel namespace;

	/**
	 * {@link HashMap} mapping a type's unique name ({@link String}) to its java representation ({@link X_Type}).
	 */
	public static HashMap<List<String>, X_Type> loadedTypes = new HashMap<>();

	/**
	 * {@link HashMap} mapping a property's unique name ({@link String}) to its java representation ({@link X_Property}).
	 */
	public static HashMap<List<String>, X_Property> loadedProperties = new HashMap<>();

	/**
	 * {@link HashMap} mapping an  instance's unique name ({@link String}) to its java representation ({@link X_Individual}).
	 */
	public static HashMap<List<String>, X_Individual> loadedIndividuals = new HashMap<>();

	/**
	 * {@link HashMap} mapping an unique name ({@link String}) of unknown entities to their java representation ({@link UnknownEntity}).
	 * Here, unknown means, that we can not determine whether the entity is a class, property or instance.
	 */
	private static HashMap<List<String>, UnknownEntity> unknownEntities = new HashMap<>();

	/**
	* {@link HashMap} matching an unique prefix ( compare {@link PrefixModel}) to all entities using this prefix.
	*/
	private static HashMap<String, Set<X_Entity>> prefixMapping = new HashMap<>();

	/**
	 * {@link Set} of all used names. This set is used to avoid collisions of unique names.
	 */
	private static Set<String> usedNames = new HashSet<>();


	private static int counter = 0;

	private static AtomicX_Type[] dataTypes;

	private static boolean dataTypesChanged = false;

	private static HashMap<Integer, String> index2Characteristic = new HashMap<>();

	private static HashMap<String, Integer> characteristic2Integer = new HashMap<>();

	public static Ontology relatedOntology;

	private static Set<X_Entity> standards = new HashSet<>();

	private static Set<String> annoProps = new HashSet<>();

	private static X_Type allTypes;

	/**
	 *
	 * @return  Set containing the java representation of all default xsd-types.
     */
	public static Set<X_Entity> getStandards() {
		if (standards.isEmpty()) {
			standards.add(OntologyCache
					.getType(Collections.singletonList("<xsd:AnyType>")));
			standards.add(OntologyCache
					.getType(Collections.singletonList("<xsd:dateTime>")));
			standards.add(OntologyCache
					.getType(Collections.singletonList("<xsd:anyURI>")));
			standards.add(OntologyCache
					.getType(Collections.singletonList("<xsd:monetary>")));
			standards.add(OntologyCache
					.getType(Collections.singletonList("<xsd:long>")));
			standards.add(OntologyCache
					.getType(Collections.singletonList("<xsd:date>")));
			standards.add(OntologyCache
					.getType(Collections.singletonList("<xsd:int>")));
			standards.add(OntologyCache
					.getType(Collections.singletonList("<xsd:string>")));
			standards.add(OntologyCache
					.getType(Collections.singletonList("<xsd:float>")));
			standards.add(OntologyCache
					.getType(Collections.singletonList("<xsd:gYear>")));
			standards.add(OntologyCache
					.getType(Collections.singletonList("<xsd:double>")));
			standards.add(OntologyCache
					.getType(Collections.singletonList("<xsd:uDateTime>")));
			standards.add(OntologyCache
					.getType(Collections.singletonList("<xsd:gMonthYear>")));
			standards.add(OntologyCache
					.getType(Collections.singletonList("<xsd:boolean>")));
		}
		return standards;
	}


	private static void initCharacteristicsMap() {
		index2Characteristic.put(0, "<owl:FunctionalProperty>");
		index2Characteristic.put(1, "<owl:InverseFunctionalProperty>");
		index2Characteristic.put(2, "<owl:TransitiveProperty>");
		index2Characteristic.put(3, "<owl:SymmetricProperty>");
		index2Characteristic.put(4, "<owl:AsymmetricProperty>");
		index2Characteristic.put(5, "<owl:ReflexiveProperty>");
		index2Characteristic.put(6, "<owl:IrreflexiveProperty>");

	}

	private static void initCharacteristicsIntMap1() {
		characteristic2Integer.put("<owl:FunctionalProperty>", 0);
		characteristic2Integer.put("<owl:InverseFunctionalProperty>", 1);
		characteristic2Integer.put("<owl:TransitiveProperty>", 2);
		characteristic2Integer.put("<owl:SymmetricProperty>", 3);
		characteristic2Integer.put("<owl:AsymmetricProperty>", 4);
		characteristic2Integer.put("<owl:ReflexiveProperty>", 5);
		characteristic2Integer.put("<owl:IrreflexiveProperty>", 6);
	}

	// ***************************** TYPE *************************************

	/**
	 * Adds {@link X_Type} t to the mapping of loaded classes.
	 * @param t the {@link X_Type} to be added.
	 */
	public static void addType(X_Type t) {
		if (t.isCartesianType()) {
			CartesianX_Type ct = (CartesianX_Type) t;
			for (AtomicX_Type se : ct.getSubElements()) {
				se.addCartType(ct);
			}
		}
		loadedTypes.put(t.getShortName(), t);
		addToPrefixMapping(t);
		usedNames.add(t.render());
	}

	/**
	 * Removes {@link X_Type} t from the mapping of loaded classes.
	 * @param t the {@link X_Type} to be removed.
	 */
	public static void removeType(X_Entity t) {
		loadedTypes.remove(t.getShortName());
		usedNames.remove(t.render());
		prefixMapping.get(t.getPrefix()).remove(t);
	}

	/**
	 * Checks whether a class with the given name exists in the classes mapping.
	 * @param name The name of the class to be ckecked.
	 * @return true if a class with the given name exists <br> false otherwise.
	 */
	public static boolean containsType(List<String> name) {
		return loadedTypes.containsKey(name);
	}

	/**
	 * Returns  the java representation of a class with the given name.
	 * @param name The name of the class to be ckecked.
	 * @return X_Type representing the class, if a class with the given name exists <br> null otherwise.
	 */
	public static X_Type getType(List<String> list) {
		return loadedTypes.get(list);

	}

	/**
	 * This method returns a {@link Set} containing the java representations ({@link X_Type}) of all classes specified in the input, if they exist.
	 * @param names A {@link Set} of names representing classes.
	 * @return Set containing the {@link X_Type}s connected to the classes.
	 */
	public static Set<X_Type> getTypeSet(Set<ArrayList<String>> names) {
		Set<X_Type> result = new HashSet<>();
		for (ArrayList<String> uri : names) {
			result.add(loadedTypes.get(uri));
		}
		return result;
	}

	/**
	 * This method returns an {@link X_Type}, which may be used as a placeholder for all other {@link X_Type}s.
	 * @return The all-{@link X_Type}
     */
	public static X_Type getAllType() {
		if (allTypes == null) {
			ArrayList<String> name = new ArrayList<>();
			name.add("All");
			allTypes = new AtomicX_Type(name);
		}
		return allTypes;
	}

	// ***************************** PROPERTY
	// *************************************

	/**
	 * Adds {@link X_Property} p to the mapping of loaded properties.
	 * @param p the {@link X_Property} to be added.
	 */
	public static void addProperty(X_Property p) {
		loadedProperties.put(p.getShortName(), p);
		addToPrefixMapping(p);
		usedNames.add(p.render());
	}

	/**
	 * Removes {@link X_Property} p from the mapping of loaded properties.
	 * @param p the {@link X_Property} to be removed.
	 */
	public static void removeProperties(X_Entity p) {
		loadedProperties.remove(p.getShortName());
		usedNames.remove(p.render());
		prefixMapping.get(p.getPrefix()).remove(p);
	}

	/**
	 * Checks whether a property with the given name exists in the property mapping.
	 * @param name The name of the property to be ckecked.
	 * @return true if a property with the given name exists <br> false otherwise.
	 */
	public static boolean containsProperty(List<String> name) {
		return loadedProperties.containsKey(name);
	}

	/**
	 * Returns  the java representation of a property with the given name.
	 * @param name The name of the property to be ckecked.
	 * @return X_Property representing the property, if a property with the given name exists <br> null otherwise.
	 */
	public static X_Property getProperty(List<String> name) {
		return loadedProperties.get(name);
	}

	/**
	 * This method returns a {@link Set} containing the java representations ({@link X_Property}) of all properties specified in the input, if they exist.
	 * @param names A {@link Set} of names representing properties.
	 * @return Set containing the {@link X_Property} connected to the properties.
	 */
	public static Set<X_Property> getPropertySet(
			Set<ArrayList<String>> names) {
		Set<X_Property> result = new HashSet<>();
		for (ArrayList<String> uri : names) {
			result.add(loadedProperties.get(uri));
		}
		return result;
	}

	// ***************************** Individual *******************

	/**
	 * Adds {@link X_Individual} i to the mapping of loaded individuals.
	 * @param i the {@link X_Individual} to be added.
	 */
	public static void addIndividual(X_Individual i) {
		if (i != null) {
			if (!i.getShortName().get(0).contains("^^<xsd:")) {
				loadedIndividuals.put(i.getShortName(), i);
				addToPrefixMapping(i);
				usedNames.add(i.render());
			}
		}
	}

	/**
	 * Removes {@link X_Individual} i from the mapping of loaded individuals.
	 * @param i the {@link X_Individual} to be removed.
	 */
	public static void removeIndividual(X_Entity i) {
		loadedIndividuals.remove(i);
		usedNames.remove(i.render());
		prefixMapping.get(i.getPrefix()).remove(i);
	}

	/**
	 * Checks whether a individual with the given name exists in the individuals mapping.
	 * @param name The name of the individual to be ckecked.
	 * @return true if a individual with the given name exists <br> false otherwise.
	 */
	public static boolean containsIndividual(List<String> name) {
		return loadedIndividuals.containsKey(name);
	}

	/**
	 * Returns  the java representation of the individual with the given name.
	 * @param name The name of the individual to be ckecked.
	 * @return X_Individual representing the individual, if one with the given name exists <br> null otherwise.
	 */
	public static X_Individual getIndividual(List<String> name) {
		return loadedIndividuals.get(name);
	}

	/**
	 * This method returns a {@link Set} containing the java representations ({@link X_Individual}) of all instances specified in the input, if they exist.
	 * @param names A {@link Set} of names representing individuals.
	 * @return Set containing the {@link X_Individual} connected to the properties.
	 */
	public static Set<X_Individual> getIndividualSet(
			Set<ArrayList<String>> names) {
		Set<X_Individual> result = new HashSet<>();
		for (ArrayList<String> uri : names) {
			result.add(loadedIndividuals.get(uri));
		}
		return result;
	}

	// ################################# OTHER

	/**
	 * Simple method increasing the counter used to create unique default names for new entities.
	 * @return Integer representing the current value of the counter.
	 */
	public static int getDefaultCounter() {
		counter++;
		ArrayList<ArrayList<X_Entity>> searchResults = search(
				"default" + counter);
		if (searchResults.get(0).isEmpty() && searchResults.get(1).isEmpty()
				&& searchResults.get(2).isEmpty()) {
			return counter;
		} else {
			return getDefaultCounter();
		}
	}

	/**
	 * 
	 * @deprecated FOR TEST Purposes only
	 * @return a {@link String} representation of the OntologyCache including loaded classes, properties and instances.
	 */
	@Deprecated
	public static String printCache() {
		StringBuilder strb = new StringBuilder("##### CACHE ######\n");
		strb.append("Classes:");
		strb.append(loadedTypes.toString());
		strb.append("\n");
		strb.append("Properties:");
		for (Entry<List<String>, X_Property> e : loadedProperties
				.entrySet()) {
			strb.append(
					e.getKey() + "domain: " + e.getValue().getExplicitDomain());
		}
		strb.append("\n");
		strb.append("Individuals:");
		strb.append(loadedIndividuals.toString());
		strb.append("\n");
		return strb.toString();
	}


	/**
	 * This method returns the string representation of all entities containing the given searchTerm.
	 * @param value the search term.
	 * @return Set containing all names of entities matching the search term  .
	 */
	public static Set<ArrayList<String>> findMatches(String value) {
		Set<X_Entity> matchingElements = populateMatchingElements(value);
		Set<ArrayList<String>> matchingTuples = new HashSet<>();
		for (X_Entity e : matchingElements) {
			matchingTuples.addAll(e.getTupleRep());
		}
		return matchingTuples;
	}


	private static Set<X_Entity> populateMatchingElements(String match) {
		Set<X_Entity> res = new HashSet<>();
		for (Entry<List<String>, X_Type> t : loadedTypes.entrySet()) {
			if (t.getKey().contains(match)) {
				res.add(t.getValue());
				res.addAll(t.getValue().getSubs());
				res.addAll(t.getValue().getDisjoints());
			}
		}
		for (Entry<List<String>, X_Property> p : loadedProperties.entrySet()) {
			if (p.getKey().contains(match)) {
				res.addAll(p.getValue().getSubs());
				res.add(p.getValue());
			}
		}
		for (Entry<List<String>, X_Individual> i : loadedIndividuals
				.entrySet()) {
			if (i.getKey().contains(match))
				res.add(i.getValue());
		}
		return res;
	}

	/**
	 * 
	 * @return TODO: replace with getStandards
	 */
	public static AtomicX_Type[] getDataTypes() {
		if (dataTypes == null || dataTypesChanged) {
			ArrayList<String> xsdAnyType = new ArrayList<String>();
			xsdAnyType.add("<xsd:AnyType>");
			X_Type anyType = loadedTypes.get(xsdAnyType);
			int numberOfDataTypes = anyType.getSubs().size();
			dataTypes = new AtomicX_Type[numberOfDataTypes];
			int i = 0;
			for (X_Entity sub : anyType.getSubs()) {
				dataTypes[i++] = (AtomicX_Type) sub;
			}
		}
		return dataTypes;
	}

	private static void addToPrefixMapping(X_Entity t) {
		if (!(t.isCartesianInstance() && t.isCartesianType())) {
			String prefix = t.getPrefix();
			if (prefixMapping.containsKey(prefix)) {
				prefixMapping.get(prefix).add(t);
			} else {
				HashSet<X_Entity> set = new HashSet<X_Entity>();
				set.add(t);
				prefixMapping.put(prefix, set);
			}
		}
	}

	/**
	 * 
	 * @param prefix
	 *            The short form of the prefix should be used
	 * @return A {@link Set} containing all entities using the given prefix.
	 */
	public static Set<X_Entity> getPrefixMapping(String prefix) {
		return prefixMapping.get(prefix);
	}

	/**
	 * Updates the prefixmapping by replacing the old name with the new one. Takes also care of possible side-effect, i.e. renames/updates effected {@link X_Entity}s.
	 * @param oldName
	 * @param newName
	 */
	public static void updatePrefixMapping(String oldName, String newName) {
		Set<X_Entity> effected = prefixMapping.get(oldName);
		// update cache
		for (X_Entity e : effected) {
			ArrayList<String> name = new ArrayList<>();
			name.add(e.getShortName().get(0).replaceFirst(oldName, newName));
			if (e.isAtomType()) {
				loadedTypes.remove(e.getShortName());
				e.setShortName(name);
				loadedTypes.put(e.getShortName(), (X_Type) e);
			}
			if (e.isCartesianType()) {
				loadedTypes.remove(e.getShortName());
				((CartesianX_Type) e).revalidateName();
				loadedTypes.put(e.getShortName(), (CartesianX_Type) e);
			}
			if (e.isProperty()) {
				loadedProperties.remove(e.getShortName());
				e.setShortName(name);
				loadedProperties.put(e.getShortName(), (X_Property) e);
			}
			if (e.isAtomInstance()) {
				loadedIndividuals.remove(e.getShortName());
				e.setShortName(name);
				loadedIndividuals.put(e.getShortName(), (X_Individual) e);
			}
		}
		prefixMapping.remove(oldName);
		prefixMapping.put(newName, effected);
	}

	/**
	 * This method is called when ever the searchField in the Gui is used. It returns the string representation of all entities containing the given searchTerm.
	 * @param searchTerm the search term.
	 * @return An {@link ArrayList}  consisting of 3 {@link ArrayList}s where the first one contains java representation of all classes matching the searchterm, the second one the representations of all properties and the third one all instances.
	 */
	public static ArrayList<ArrayList<X_Entity>> search(String searchTerm) {
		ArrayList<ArrayList<X_Entity>> result = new ArrayList<>();
		ArrayList<X_Entity> tBox = search(loadedTypes,
				searchTerm.toLowerCase());
		result.add(tBox);
		ArrayList<X_Entity> rBox = search(loadedProperties,
				searchTerm.toLowerCase());
		result.add(rBox);
		ArrayList<X_Entity> aBox = search(loadedIndividuals,
				searchTerm.toLowerCase());
		result.add(aBox);
		return result;
	}

	private static ArrayList<X_Entity> search(
			HashMap<List<String>, ? extends X_Entity> map, String searchTerm) {
		ArrayList<X_Entity> result = new ArrayList<>();
		for (List<String> sn : map.keySet()) {
			for (String n : sn) {
				if (n.toLowerCase().contains(searchTerm)) {
					result.add(map.get(sn));
					break;
				}
			}
		}
		return result;
	}

	/**
	 * Clears the whole cache. Must be called before a new ontology is loaded.
	 */
	public static void clear() {
		prefixMapping = new HashMap<>();
		loadedIndividuals = new HashMap<>();
		loadedProperties = new HashMap<>();
		loadedTypes = new HashMap<>();
		counter = 0;
		dataTypes = null;
		dataTypesChanged = false;
		index2Characteristic = new HashMap<>();
		characteristic2Integer = new HashMap<>();
		standards = new HashSet<>();
		namespace = null;
		allTypes = null;
	}

	/**
	 * This method provides access to the local NameSpace of the currently used ontology. This namespace is used whenever no explicit namespace is stated for an {@link X_Entity}.<br>
	 * E.g. The currently loaded ontology has the uri ...#onto42 then onto42 is used as short prefix and the full uri as long prefix. A entity with uri Pizza, is internally represented using the short default prefix onto42:Pizza.
	 * @return the default/local namespace of the ontology.
     */
	public String getLocalNameSpace() {
		if (relatedOntology != null)
			return relatedOntology.getLocalNameSpace();
		else
			throw new IllegalStateException(
					"OntologyCache was not initiated correctly");
	}

	public static HashMap<Integer, String> getIndex2Characteristic() {
		if (index2Characteristic.isEmpty()) {
			initCharacteristicsMap();
		}
		return index2Characteristic;
	}

	public static HashMap<String, Integer> getCharacteristic2Integer() {
		if (characteristic2Integer.isEmpty()) {
			initCharacteristicsIntMap1();
		}
		return characteristic2Integer;
	}

	/**
	 *
	 * @param name The name to be checked.
	 * @return true, if the name was not used already, false otherwise.
     */
	public static boolean isUniqueName(String name) {
		return !usedNames.contains(name);
	}

	/**
	 * Annptation properties are:<br>
	 *     rdfs:comment>, owl:deprecated, rdfs:isDefinedBy, rdfs:label, rdfs:seeAlso, owl:versionInfo>
	 * @return A {@link Set} containing the string representation of all available annotation properties.
	 */
	public static Set<String> getAnnoProperties() {
		if (annoProps.isEmpty()) {
			annoProps.add("<rdfs:comment>");
			annoProps.add("<owl:deprecated>");
			annoProps.add("<rdfs:isDefinedBy>");
			annoProps.add("<rdfs:label>");
			annoProps.add("<rdfs:seeAlso>");
			annoProps.add("<owl:versionInfo>");
		}
		return annoProps;
	}

	public static PrefixModel getNamespace() {
		return namespace;
	}

	/**
	 * @param ns
	 *            the namespace to set
	 */
	public static void setNamespace(PrefixModel ns) {
		namespace = ns;
	}

	/**
	 * @param name The name of the entity
	 * @return The java representation {@link X_Entity} of the entity connected to the given name. If no such entity exists null is returned.
	 */
	public static X_Entity getEntity(String name) {
		List<String> e = Collections.singletonList(name);
		return getEntity(e);

	}

	/**
	 * @param name The name of the entity
	 * @return The java representation {@link X_Entity} of the entity connected to the given name. If no such entity exists null is returned.
	 */
	public static X_Entity getEntity(List<String> name) {
		if (loadedTypes.containsKey(name))
			return loadedTypes.get(name);
		if (loadedProperties.containsKey(name))
			return loadedProperties.get(name);
		if (loadedIndividuals.containsKey(name))
			return loadedIndividuals.get(name);
		if (unknownEntities.containsKey(name)) {
			return unknownEntities.get(name);
		}
		return createNewUnknownEntity(name);
	}

	private static X_Entity createNewUnknownEntity(List<String> name) {
		UnknownEntity unknownEntity = new UnknownEntity(name);
		unknownEntities.put(name, unknownEntity);
		return unknownEntity;
	}

	/**
	 * @return all currently loaded properties.
	 */
	public static Collection<X_Property> getLoadedProperties() {
		return loadedProperties.values();
	}

	/**
	 * @param prefix the prefix to be removed from the namespace
	 */
	public static void removePrefixMapping(String prefix) {
		prefixMapping.remove(prefix);
	}

	public static boolean isBasicProperty(X_Property sup) {
		Set basicProperties = new HashSet();
		basicProperties.add(loadedProperties.get(
				Collections.singletonList("<nary:MixedProperty>")));
		basicProperties.add(loadedProperties.get(
				Collections.singletonList("<owl:ObjectProperty>")));
		basicProperties.add(loadedProperties.get(
				Collections.singletonList("<owl:DatatypeProperty>")));
		return basicProperties.contains(sup);

	}
}
