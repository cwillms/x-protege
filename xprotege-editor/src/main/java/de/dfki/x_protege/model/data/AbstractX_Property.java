package de.dfki.x_protege.model.data;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;

import de.dfki.lt.hfc.QueryParseException;
import de.dfki.x_protege.model.OntologyCache;

/**
 * Basic implementation of the {@link AbstractX_Entity} representing Properties.
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 22.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public abstract class AbstractX_Property extends AbstractX_Entity
        implements
        Comparable<AbstractX_Property> {

    /**
     * 000 -> default <br>
     * 100 -> mixed <br>
     * 010 -> data <br>
     * 001 -> object
     */
    protected BitSet type = new BitSet(3);

    private final Set<X_Type> explicitDomain;
    private final Set<X_Type> implicitDomain;

    private final Set<X_Type> explicitRange;
    private final Set<X_Type> implicitRange;

    private final Set<X_Type> explicitExtra;
    private final Set<X_Type> implicitExtra;

    /**
     * The arity of the domain of this property.
     */
    protected int domainArity;

    /**
     * The arity of the range of this property.
     */
    protected int rangeArity;

    /**
     * The arity of the extra arguments of this property.
     */
    protected int extraArity;

    private AbstractX_Property inverse;

    /**
     * Simple flag representing whether the named value was already computed.
     */
    protected boolean rangeComputed, domainComputed, argumentsComputed,
            characteristicsComputed, inverseComputed = false;

    /**
     * {@link BitSet} representing the characteristics of the property.<br>
     * 0 -> owl:FunctionalProperty<br>
     * 1 -> owl:InverseFunctionalProperty<br>
     * 2 -> owl:TransitiveProperty<br>
     * 3 -> owl:SymmetricProperty<br>
     * 4 -> owl:AsymmetricProperty<br>
     * 5 -> owl:ReflexiveProperty<br>
     * 6 -> owl:IrreflexiveProperty
     */
    protected BitSet characteristics = new BitSet(7);

    /**
     * Creates a new instance of {@link AbstractX_Property}.
     *
     * @param uri the uri of this Property.
     */
    public AbstractX_Property(List<String> uri) {
        super(uri);
        this.explicitDomain = new HashSet<>();
        this.implicitDomain = new HashSet<>();
        this.domainArity = 0;
        this.explicitRange = new HashSet<>();
        this.implicitRange = new HashSet<>();
        this.rangeArity = 0;
        this.explicitExtra = new HashSet<>();
        this.implicitExtra = new HashSet<>();
        this.extraArity = 0;
    }

    @Override
    public int compareTo(AbstractX_Property o) {
        return 0;
    }

    /**
     * Add the given {@link X_Type} d to the set of explicit assigned domains.
     *
     * @param d the domain to be assigned.
     */
    public void addExplicitDomain(X_Type d) {
        this.explicitDomain.add(d);
        this.domainArity = d.getArity();
    }

    /**
     * Add the given {@link X_Type} d to the set of implicit assigned domains.
     *
     * @param d the domain to be assigned.
     */
    public void addImplicitDomain(X_Type d) {
        this.implicitDomain.add(d);
    }

    /**
     * Add the given {@link X_Type} r to the set of explicit assigned range.
     *
     * @param r the range to be assigned.
     */
    public void addExplicitRange(X_Type r) {
        this.explicitRange.add(r);
        this.rangeArity = r.getArity();
    }

    /**
     * Add the given {@link X_Type} r to the set of implicit assigned range.
     *
     * @param r the range to be assigned.
     */
    public void addImplicitRange(X_Type r) {
        this.implicitRange.add(r);
    }

    /**
     * Add the given {@link X_Type} a to the set of explicit assigned extra arguments.
     *
     * @param a the extra argument to be assigned.
     */
    public void addExplicitExtra(X_Type a) {
        this.explicitExtra.add(a);
        this.extraArity = a.getArity();
    }

    /**
     * Add the given {@link X_Type} a to the set of implicit assigned extra arguments.
     *
     * @param a the extra argument to be assigned.
     */
    public void addImplicitExtra(X_Type a) {
        this.implicitExtra.add(a);
    }

    /**
     * Removes the given {@link X_Type} d from the set of explicit assigned domains.
     *
     * @param d the domain to be removed.
     * @return true, if the given type was in the set, false otherwise
     */
    public boolean removeExplicitDomain(X_Type d) {
        if (this.explicitDomain.size() == 1)
            this.domainArity = 0;
        return this.explicitDomain.remove(d);
    }

    /**
     * Removes the given {@link X_Type} d from the set of implicit assigned domains.
     *
     * @param d the domain to be removed.
     * @return true, if the given type was in the set, false otherwise
     */
    public boolean removeImplicitDomain(X_Type d) {
        return this.implicitDomain.remove(d);
    }

    /**
     * Removes the given {@link X_Type} r from the set of explicit assigned range.
     *
     * @param r the range to be removed.
     * @return true, if the given type was in the set, false otherwise
     */
    public boolean removeExplicitRange(X_Type r) {
        if (this.explicitRange.size() == 1)
            this.rangeArity = 0;
        return this.explicitRange.remove(r);
    }

    /**
     * Removes the given {@link X_Type} r from the set of implicit assigned range.
     *
     * @param r the range to be removed.
     * @return true, if the given type was in the set, false otherwise
     */
    public boolean removeImplicitRange(X_Type r) {
        return this.implicitRange.remove(r);
    }

    /**
     * Removes the given {@link X_Type} a from the set of explicit assigned extra arguments.
     *
     * @param a the extra argument to be removed.
     * @return true, if the given type was in the set, false otherwise
     */
    public boolean removeExplicitExtra(X_Type a) {
        if (this.explicitExtra.size() == 1)
            this.extraArity = 0;
        return this.explicitExtra.remove(a);
    }

    /**
     * Removes the given {@link X_Type} a from the set of implicit assigned extra arguments.
     *
     * @param a the extra argument to be removed.
     * @return true, if the given type was in the set, false otherwise
     */
    public boolean removeImplicitExtra(X_Type a) {
        return this.implicitExtra.remove(a);
    }

    // ############## GETTER #################

    public Set<X_Type> getExplicitDomain() {
        return explicitDomain;
    }

    public int getDomainArity() {
        return domainArity;
    }

    public Set<X_Type> getExplicitRange() {
        return explicitRange;
    }

    public int getRangeArity() {
        return rangeArity;
    }

    public Set<X_Type> getExplicitExtra() {
        return explicitExtra;
    }

    public int getExtraArity() {
        return extraArity;
    }

    public boolean isDataTypeProp() {
        return type.get(1);
    }

    @Override
    public void populateConnections(
            HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> connectedTuples) {
        if (this.generated)
            return;
        if (!domainComputed) {
            try {
                computeDomains(connectedTuples);
            } catch (QueryParseException e) {
                e.printStackTrace();
            }
        }
        if (!rangeComputed) {
            try {
                computeRange(connectedTuples);
            } catch (QueryParseException e) {
                e.printStackTrace();
            }
        }
        if (!argumentsComputed) {
            try {
                computeArguments(connectedTuples);
            } catch (QueryParseException e) {
                e.printStackTrace();
            }
        }
        if (!annotationComputed) {
            computeAnnotations(connectedTuples);
        }
        if (!characteristicsComputed) {

            computeCharacteristics(connectedTuples);

        }
        if (!inverseComputed) {
            computeInverse(connectedTuples);
        }
        performExtraPopulation(connectedTuples);
    }

    /**
     * Computes the inverse property based on the tuple representation of the property. Called whenever the property is loaded from from file.
     *
     * @param connectedTuples An {@link HashMap} containing all tuples connected to this Property. The keys of the map are properties, whereas the values are  domain -> range + extra mappings.
     */
    protected abstract void computeInverse(
            HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> connectedTuples);

    /**
     * Computes the characteristics of the property based on the tuple representation of the property. Called whenever the property is loaded from from file.
     *
     * @param connectedTuples An {@link HashMap} containing all tuples connected to this Property. The keys of the map are properties, whereas the values are  domain -> range + extra mappings.
     */
    protected abstract void computeCharacteristics(
            HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> connectedTuples);

    /**
     * Computes the additional property specific values based on the tuple representation of the property. Called whenever the property is loaded from from file.
     *
     * @param connectedTuples An {@link HashMap} containing all tuples connected to this Property. The keys of the map are properties, whereas the values are  domain -> range + extra mappings.
     */
    protected abstract void performExtraPopulation(
            HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> connectedTuples);

    private void computeArguments(
            HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> connectedTuples)
            throws QueryParseException {
        if (connectedTuples.containsKey("<nary:extra>")) {
            for (Entry<ArrayList<String>, Set<ArrayList<String>>> e : connectedTuples
                    .get("<nary:extra>").entrySet()) {
                for (ArrayList<String> e1 : e.getValue()) {
                    X_Type x_type = OntologyCache.getType(e1);
                    this.addExplicitExtra(x_type);
                }
            }
        }
        this.argumentsComputed = true;
    }

    private void computeRange(
            HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> connectedTuples)
            throws QueryParseException {
        if (connectedTuples.containsKey("<rdfs:range>")) {
            for (Entry<ArrayList<String>, Set<ArrayList<String>>> e : connectedTuples
                    .get("<rdfs:range>").entrySet()) {
                for (ArrayList<String> e1 : e.getValue()) {
                    X_Type x_type = OntologyCache.getType(e1);
                    this.addExplicitRange(x_type);
                }
            }
        }
        this.rangeComputed = true;
    }

    private void computeDomains(
            HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> connectedTuples)
            throws QueryParseException {
        // <rdfs:domain>
        if (connectedTuples.containsKey("<rdfs:domain>")) {
            for (Entry<ArrayList<String>, Set<ArrayList<String>>> e : connectedTuples
                    .get("<rdfs:domain>").entrySet()) {
                for (ArrayList<String> e1 : e.getValue()) {
                    X_Type x_type = OntologyCache.getType(e1);
                    this.addExplicitDomain(x_type);
                    x_type.addExplicitProperty((X_Property) this);
                }
            }
        }
        this.domainComputed = true;
    }

    /**
     * 0 -> owl:FunctionalProperty<br>
     * 1 -> owl:InverseFunctionalProperty<br>
     * 2 -> owl:TransitiveProperty<br>
     * 3 -> owl:SymmetricProperty<br>
     * 4 -> owl:AsymmetricProperty<br>
     * 5 -> owl:ReflexiveProperty<br>
     * 6 -> owl:IrreflexiveProperty
     *
     * @return A {@link BitSet} representing the characteristics of the property.
     */
    public BitSet getCharacteristics() {
        return this.characteristics;
    }

    @Override
    public Collection<? extends ArrayList<String>> getTuples(boolean rename) {
        Set<ArrayList<String>> tuples = new HashSet<>();
        if (rename)
            tuples.addAll(this.getRenameTuples());
        tuples.addAll(this.getDomainTuples());
        tuples.addAll(this.getRangeTuples());
        tuples.addAll(this.getStructuralTuples());
        tuples.addAll(this.getExtraTuples());
        tuples.addAll(this.getArityTuples());
        return tuples;
    }

    private Collection<? extends ArrayList<String>> getArityTuples() {
        Set<ArrayList<String>> tuples = new HashSet<>();
        int[] arities = getArities();
        if (arities[0] > 1) {
            ArrayList<String> tuple = new ArrayList<>();
            tuple.addAll(this.getShortName());
            tuple.add("<nary:domainArity>");
            tuple.add(getXSDInt(arities[0]));
            tuples.add(tuple);
        }
        if (arities[1] > 1) {
            ArrayList<String> tuple = new ArrayList<>();
            tuple.addAll(this.getShortName());
            tuple.add("<nary:rangeArity>");
            tuple.add(getXSDInt(arities[1]));
            tuples.add(tuple);
        }
        if (arities[2] > 1) {
            ArrayList<String> tuple = new ArrayList<>();
            tuple.addAll(this.getShortName());
            tuple.add("<nary:extraArity>");
            tuple.add(getXSDInt(arities[2]));
            tuples.add(tuple);
        }
        return tuples;
    }

    /**
     * @return Array containing the arity of domain, range and extra argument
     */
    public int[] getArities() {
        int[] results = new int[3];
        results[0] = getArity(this.explicitDomain);
        results[1] = getArity(this.explicitRange);
        results[2] = getArity(this.explicitExtra);
        return results;
    }

    private int getArity(Set<X_Type> set) {
        int arity = 0;
        for (X_Type e : set) {
            arity = e.getArity();
        }
        return arity;
    }

    private String getXSDInt(int integer) {
        StringBuilder strb = new StringBuilder("\"");
        strb.append(integer);
        strb.append("\"^^<xsd:int>");
        return strb.toString();
    }

    /**
     * @return Set of tuples ({@link ArrayList} of {@link String}) modeling the subclass and subproperty hierarchy related to this property.
     */
    protected abstract Collection<? extends ArrayList<String>> getStructuralTuples();

    /**
     * @return Set of tuples ({@link ArrayList} of {@link String}) representing the hfc entries to be replaced after this property was renamed.
     */
    protected abstract Collection<? extends ArrayList<String>> getRenameTuples();

    private Set<? extends ArrayList<String>> getRangeTuples() {
        Set<ArrayList<String>> rangeTuples = new HashSet<>();
        for (X_Type range : this.explicitRange) {
            ArrayList<String> t = new ArrayList<>();
            t.addAll(this.getShortName());
            t.add("<rdfs:range>");
            t.addAll(range.getShortName());
            rangeTuples.add(t);
        }
        for (X_Type range : this.implicitRange) {
            ArrayList<String> t = new ArrayList<>();
            t.addAll(this.getShortName());
            t.add("<rdfs:range>");
            t.addAll(range.getShortName());
            rangeTuples.add(t);
        }
        return rangeTuples;
    }

    private Collection<? extends ArrayList<String>> getDomainTuples() {
        Set<ArrayList<String>> domainTuples = new HashSet<>();
        for (X_Type domain : this.explicitDomain) {
            ArrayList<String> t = new ArrayList<>();
            t.addAll(this.getShortName());
            t.add("<rdfs:domain>");
            t.addAll(domain.getShortName());
            domainTuples.add(t);
        }
        for (X_Type domain : this.implicitDomain) {
            ArrayList<String> t = new ArrayList<>();
            t.addAll(this.getShortName());
            t.add("<rdfs:domain>");
            t.addAll(domain.getShortName());
            domainTuples.add(t);
        }
        return domainTuples;
    }

    private Collection<? extends ArrayList<String>> getExtraTuples() {
        Set<ArrayList<String>> extraTuples = new HashSet<>();
        for (X_Type extra : this.explicitExtra) {
            ArrayList<String> t = new ArrayList<>();
            t.addAll(this.getShortName());
            t.add("<nary:extra>");
            t.addAll(extra.getShortName());
            extraTuples.add(t);
        }
        for (X_Type extra : this.implicitExtra) {
            ArrayList<String> t = new ArrayList<>();
            t.addAll(this.getShortName());
            t.add("<nary:extra>");
            t.addAll(extra.getShortName());
            extraTuples.add(t);
        }
        return extraTuples;
    }

    /**
     * A property supports different characteristics depending on its type.
     *
     * @return A {@link BitSet} depicting which characteristics are supported by the property.
     */
    public abstract BitSet getSupportedCharacteristics();

    /**
     * 000 -> default <br>
     * 100 -> mixed <br>
     * 010 -> data <br>
     * 001 -> object
     *
     * @return A {@link BitSet} indicating the type of the property.
     */
    public abstract BitSet getPropertyType();

    public AbstractX_Property getInverse() {
        return inverse;
    }

    public void setInverse(AbstractX_Property inverse) {
        this.inverse = inverse;
    }

    public X_Type getDataType() {
        if (this.type.nextSetBit(0) == 1 && this.explicitRange.size() == 1) {
            X_Type t = (X_Type) this.explicitRange.toArray()[0];
            return t;
        } else {
            return null;
        }
    }

    public ComboBoxModel<X_Type> getRangeComboBoxModel() {
        HashSet<X_Type> range = new HashSet<>();
        range.addAll(explicitRange);
        range.addAll(implicitRange);
        X_Type[] items = new X_Type[range.size() + 1];
        items[0] = OntologyCache.getAllType();
        int i = 1;
        for (X_Type t : range) {
            items[i] = t;
            i++;
        }
        ComboBoxModel<X_Type> result = new DefaultComboBoxModel<>(items);
        return result;
    }

    public ComboBoxModel<X_Type> getExtraComboBoxModel() {
        HashSet<X_Type> extras = new HashSet<>();
        extras.addAll(explicitExtra);
        extras.addAll(implicitExtra);
        X_Type[] items = new X_Type[extras.size() + 1];
        items[0] = OntologyCache.getAllType();
        int i = 1;
        for (X_Type t : extras) {
            items[i] = t;
            i++;
        }
        ComboBoxModel<X_Type> result = new DefaultComboBoxModel<>(items);
        return result;
    }

    @Override
    public boolean isCompatible(X_Entity p) {
        if (p.isProperty()) {
            boolean result = false;
            int type = ((X_Property) p).getPropertyType().nextSetBit(0);
            switch (type) {
                case 0: { // mixed property
                    result = true;
                    break;
                }
                case 1: { // data type property
                    result = true;
                    for (X_Type t : explicitRange) {
                        if (!t.isXSDType()) {
                            result = false;
                            break;
                        }
                    }
                    break;
                }
                case 2: { // object property
                    result = true;
                    for (X_Type t : explicitRange) {
                        if (t.isXSDType()) {
                            result = false;
                            break;
                        }
                    }
                    break;
                }
                default:
                    // a basic type isn't compatible with any other property
                    break;
            }
            return result;
        } else {
            return false;
        }
    }

    @Override
    public boolean isProperty() {
        return true;
    }


    @Override
    public String getDescription() {
        StringBuilder strb = new StringBuilder("<html>");
        strb.append("<u><b>URI:</b></u> " + this.getLongName() + "<br>");
        strb.append("<u><b>Annotations:</b></u> <br>");
        for (Annotation a : this.annotations) {
            strb.append(a.render().replace("</html>", ""));
            strb.append("<br>");
        }
        strb.append("<u><b>Domain:</b></u> <br>");
        for (X_Type t : this.explicitDomain) {
            strb.append(t.render(true));
            strb.append("<br>");
        }
        strb.append("<u><b>Range:</b></u> <br>");
        for (X_Type t : this.explicitRange) {
            strb.append(t.render(true));
            strb.append("<br>");
        }
        strb.append("<u><b>Extra:</b></u> <br>");
        for (X_Type t : this.explicitExtra) {
            strb.append(t.render(true));
            strb.append("<br>");
        }
        strb.append("</html>");
        return strb.toString();
    }


    public Collection<? extends X_Type> getImplicitDomain() {
        return implicitDomain;
    }

    public Set<X_Type> getImplicitRange() {
        return implicitRange;
    }

    public Set<X_Type> getImplicitExtra() {
        return implicitExtra;
    }
}
