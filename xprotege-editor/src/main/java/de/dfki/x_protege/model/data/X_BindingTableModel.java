/**
 * 
 */
package de.dfki.x_protege.model.data;

import javax.swing.table.AbstractTableModel;

/**
 * This table model is used to populate the BindingTable used within the
 * X_QueryTab <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 19.03.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class X_BindingTableModel extends AbstractTableModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private X_Entity[][] table;

	private String[] header;

	private String query;

	/**
	 * Creates a new instance of {@link X_BindingTableModel}.
	 */
	public X_BindingTableModel(String[] vars, X_Entity[][] table, String query) {
		this.table = table;
		this.header = vars;
		this.query = query;
	}

	@Override
	public int getRowCount() {
		return table.length;
	}

	@Override
	public int getColumnCount() {
		return table[0].length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return table[rowIndex][columnIndex];
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return X_Entity.class;
	}


	public String[] getHeader() {
		return this.header;
	}

	public String getQuery() {
		return this.query;
	}

}
