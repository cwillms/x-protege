package de.dfki.x_protege.model.utils;

import java.util.List;

import de.dfki.x_protege.model.data.X_Entity;

/**
 * this class is designed as a factory offering {@link X_Entity}s for a given
 * URI.
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 22.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
abstract class ElementProvider {

	ElementProvider() {
	}

	/**
	 * Creates a new {@link X_Entity} with the given name and the given superclass.
	 * @param name the unique name/uri of the Entity to be created
	 * @param parent the superclass of the {@link X_Entity} to be created.
     * @return The new {@link X_Entity}
     */
	public abstract X_Entity createNewEntity(List<String> name, X_Entity parent);

}
