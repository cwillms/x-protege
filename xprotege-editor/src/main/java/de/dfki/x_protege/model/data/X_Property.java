package de.dfki.x_protege.model.data;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import de.dfki.x_protege.model.OntologyCache;

/**
 * Implementation of the {@link AbstractX_Property} representing Properties.
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 22.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class X_Property extends AbstractX_Property {

	private BitSet supportedCharacteristics = new BitSet(7);

	/**
	 * Creates a new instance of {@link AbstractX_Property}.
	 *
	 * @param uri the uri of this Property.
	 * @param i integer decoding whether the property is mixed, datatype or object property
	 */
	public X_Property(List<String> uri, int i) {
		super(uri);
		if (i != -1)
			type.set(i);
		switch (i) {
			case 0 :
				supportedCharacteristics.flip(0, 7);
				supportedCharacteristics.flip(2);
				break;
			case 1 :
				supportedCharacteristics.flip(0);
				break;
			case 2 :
				supportedCharacteristics.flip(0, 7);
				break;
			default :
				break;
		}
	}

	@Override
	protected void performExtraPopulation(
			HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> connectedTuples) {
	}

	@Override
	public BitSet getSupportedCharacteristics() {
		return this.supportedCharacteristics;
	}

	@Override
	protected void computeInverse(
			HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> connectedTuples) {
		if (connectedTuples.containsKey("<owl:inverseOf>")) {
			for (ArrayList<String> r : connectedTuples.get("<owl:inverseOf>")
					.get(this.getShortName()))
				this.setInverse(OntologyCache.getProperty(r));
		}
		this.inverseComputed = true;
	}

	@Override
	protected void computeCharacteristics(
			HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> connectedTuples) {
		if (connectedTuples.containsKey("<rdf:type>")) {
			for (Entry<ArrayList<String>, Set<ArrayList<String>>> e : connectedTuples
					.get("<rdf:type>").entrySet()) {
				for (ArrayList<String> e1 : e.getValue()) {
					if (OntologyCache.getIndex2Characteristic().values()
							.contains(e1.get(0))) {
						characteristics.flip(OntologyCache
								.getCharacteristic2Integer().get(e1.get(0)));
					}
				}
			}
		}
		this.characteristicsComputed = true;

	}

	/**
	 * 000 default 100 mixed 010 data 001 object
	 */
	@Override
	public BitSet getPropertyType() {
		return this.type;
	}

	@Override
	protected Collection<? extends ArrayList<String>> getStructuralTuples() {

		Set<ArrayList<String>> structuralTuples = new HashSet<>();
		//structuralTuples.addAll(getHierarchyTuples());
		structuralTuples.add(getTypeTuple());
		structuralTuples.addAll(getSubPropertyTuples());
		structuralTuples.addAll(getCharacteristicTuple());
		if (this.getInverse() != null) {
			structuralTuples.add(getInverseTuple());
		}
		return structuralTuples;
	}

	private ArrayList<String> getInverseTuple() {
		ArrayList<String> t = new ArrayList<>();
		t.addAll(this.getShortName());
		t.add("<owl:inverseOf>");
		t.addAll(this.getInverse().getShortName());
		return t;
	}

    /**
     * @return Set of tuples ({@link ArrayList} of {@link String}) modeling the characteristics of this property, e.g. functional etc..
     */
	public Collection<? extends ArrayList<String>> getCharacteristicTuple() {
		Set<ArrayList<String>> characteristicTupels = new HashSet<>();
		for (int i = 0; i < characteristics.length(); i++) {
			if (characteristics.get(i)) {
				ArrayList<String> t = new ArrayList<>();
				t.addAll(this.getShortName());
				t.add("<rdf:type>");
				t.add(OntologyCache.getIndex2Characteristic().get(i));
				characteristicTupels.add(t);
			}

		}

		return characteristicTupels;
	}

//	private Set<ArrayList<String>> getHierarchyTuples() {
//		Set<ArrayList<String>> tuples = new HashSet<>();
//		for (X_Entity sub : this.superClasses) {
//			ArrayList<String> t = new ArrayList<>();
//			t.addAll(this.getShortName());
//			t.add("<rdf:type>");
//			t.addAll(sub.getShortName());
//			tuples.add(t);
//		}
//		return tuples;
//	}

	private Set<ArrayList<String>> getSubPropertyTuples() {
		Set<ArrayList<String>> tuples = new HashSet<>();
		for (X_Entity sup : this.superClasses) {
			if(OntologyCache.isBasicProperty((X_Property) sup) && !OntologyCache.isBasicProperty(this)) {
				//do nothing
			} else {
			ArrayList<String> t = new ArrayList<>();
			t.addAll(this.getShortName());
			t.add("<rdfs:subPropertyOf>");
			t.addAll(sup.getShortName());
			tuples.add(t);
			}
		}
		return tuples;
	}




	@Override
	protected Collection<? extends ArrayList<String>> getRenameTuples() {
		Set<ArrayList<String>> tuples = new HashSet<>();
		for (X_Entity sub : this.subclasses) {
			// We don't need to check whether the property is a basic one, as those can't be renamed.
			ArrayList<String> t = new ArrayList<>();
			t.addAll(sub.getShortName());
			t.add("<rdfs:subPropertyOf>");
			t.addAll(this.getShortName());
			tuples.add(t);

		}
		for (X_Entity sub : this.subclasses) {
			ArrayList<String> t = new ArrayList<>();
			t.addAll(sub.getShortName());
			t.add("<rdf:type>");
			t.addAll(this.getShortName());
			tuples.add(t);
		}
		return tuples;
	}

	private ArrayList<String> getTypeTuple() {
		ArrayList<String> t = new ArrayList<>();
		switch (type.nextSetBit(0)) {
			case 0 :
				t.addAll(this.getShortName());
				t.add("<rdf:type>");
				t.add("<nary:MixedProperty>");
				break;
			case 1 :
				t.addAll(this.getShortName());
				t.add("<rdf:type>");
				t.add("<owl:DatatypeProperty>");
				break;
			case 2 :
				t.addAll(this.getShortName());
				t.add("<rdf:type>");
				t.add("<owl:ObjectProperty>");
				break;
			default :
				t.addAll(this.getShortName());
				t.add("<rdf:type>");
				t.add("<rdf:Property>");
				break;
		}
		return t;
	}

	@Override
	protected String getHtmlIcon() {
		String result;
		switch (this.type.nextSetBit(0)) {
			case 0 :
				result = "<font size=\"4\" color=#E10909> &#x2585;</font> ";
				break;
			case 1 :
				result = "<font size=\"4\" color=#01DF01> &#x2585;</font> ";
				break;
			case 2 :
				result = "<font size=\"4\" color=#0040FF> &#x2585;</font> ";
				break;
			default :
				result = "<font size=\"4\" color=#D5CFCF> &#x2585;</font> ";
				break;
		}
		return result;
	}

    /**
     * This method is used to check whether a given type can be assigned to this property as domain, range or extra argument.
     * @param assignmentType {@link Integer} encoding the assignment type ( 0 = domain, 1= range and 2 = extra arg)
     * @param t the type to be checked
     * @return true if the assignment is valid, false otherwise
     */
	public boolean checkAssignment(int assignmentType, X_Type t) {
		switch (assignmentType) {
			case 0 :
				if ((this.getDomainArity() == 0
						|| this.getDomainArity() == t.getArity()))
					return true;
				return false;
			case 1 :
				if (this.getRangeArity() == 0
						|| this.getRangeArity() == t.getArity()) {
					return true;
				}
				return false;
			case 2 :
				if (this.getExtraArity() == 0
						|| this.getExtraArity() == t.getArity()) {
					return true;
				}
				return false;
			default :
				return false;
		}
	}

    /**
     * This method is used to check whether a given type can be assigned to this property as domain, range or extra argument.
     * @param assignmentType {@link Integer} encoding the assignment type ( 0 = domain, 1= range and 2 = extra arg)
     * @param t the type to be checked
     * @return true if the assignment is valid, false otherwise
     */
	public boolean checkAssignment2(int assignmentType, X_Type t) {
		switch (assignmentType) {
			case 0 :
				if (t.isXSDType() && t.isAtomType())
					return false;
				return true;
			case 1 :
				if (t.isAtomType() && t.isXSDType()) {
					if (containsXSD(getExplicitRange()))
						return true;
					else
						return false;
				}
				return true;
			case 2 :
				if (t.isAtomType() && t.isXSDType()) {
					if (containsXSD(getExplicitExtra()))
						return true;
					else
						return false;
				}
				return true;
			default :
				return false;
		}
	}

	private boolean containsXSD(Set<X_Type> types) {
		for (X_Type t : types)
			if (!t.isXSDType())
				return false;
		return true;
	}

}
