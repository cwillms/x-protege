package de.dfki.x_protege.model.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.dfki.x_protege.model.OntologyCache;

/**
 * This class represents simple atomic class/type, e.g. \<bio:Person\>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 22.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class AtomicX_Type extends X_Type {

	private final Set<CartesianX_Type> cartproducts = new HashSet<CartesianX_Type>();

	/**
	 * Creates a new instance of {@link AtomicX_Type}.
	 * @param uri the uri/name of the class/type.
	 */
	public AtomicX_Type(List<String> uri) {
		super(uri);
	}

	@Override
	protected void performExtraPopulation(
			HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> connectedTuples) {
		// Nothing to do here
	}

	/**
	 *
	 * @return All {@link CartesianX_Type}s this one is part of.
     */
	public Set<CartesianX_Type> getComplexClasses() {
		return cartproducts;
	}

	/**
	 * Add the given {@link CartesianX_Type} to the set of those this on is part of.
	 * @param ct the {@link CartesianX_Type} to be added.
     */
	public void addCartType(CartesianX_Type ct) {
		this.cartproducts.add(ct);
	}

	/**
	 * Remove the given {@link CartesianX_Type} from the set of those this on is part of.
	 * @param ct the {@link CartesianX_Type} to be removed.
	 */
	public void removeCartType(CartesianX_Type ct) {
		this.cartproducts.remove(ct);
	}

	@Override
	protected Collection<? extends ArrayList<String>> getStructuralTuples() {
		Set<ArrayList<String>> structuralTuples = new HashSet<ArrayList<String>>();
		for (X_Entity sup : this.superClasses) {
			ArrayList<String> t = new ArrayList<>();
			t.addAll(this.getShortName());
			t.add("<rdfs:subClassOf>");
			t.addAll(sup.getShortName());
			structuralTuples.add(t);
		}
		return structuralTuples;
	}

	@Override
	protected String getHtmlIcon() {
		return "<font  size=\"4\" color=#FFBF00> &#9679;</font> ";
	}

	@Override
	public int getArity() {
		return 1;
	}

	@Override
	public boolean isXSDType() {
		return OntologyCache.getStandards().contains(this)
				|| OntologyCache.getStandards()
						.contains(OntologyCache.getType(this.getShortName()));
	}

	@Override
	public boolean isAtomType() {
		return true;
	}


	@Override
	protected Collection<? extends ArrayList<String>> getRenameTuples() {
		// the computed set should contain the tuples of subclasses
		Set<ArrayList<String>> renameTuples = new HashSet<>();
		for (X_Entity sub : this.subclasses) {
			ArrayList<String> t = new ArrayList<>();
			t.addAll(sub.getShortName());
			t.add("<rdfs:subClassOf>");
			t.addAll(this.getShortName());
			renameTuples.add(t);
		}
		return renameTuples;
	}

}
