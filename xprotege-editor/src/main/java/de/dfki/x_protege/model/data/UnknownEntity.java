/**
 * 
 */
package de.dfki.x_protege.model.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * {@link X_Entity} representing an entity we do not exactly know from the tuplefile whether it is a class/type, a property or an instance/individual.<br>
 *     A question mark is used as icon. Instances of this class are only used to visualize the above described fact in the GUI and thus don't need any functionality.
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 20.03.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class UnknownEntity extends AbstractX_Entity {

	/**
	 * Creates a new instance of {@link UnknownEntity} with the given uri.
	 * @param uri the uri of the entity
	 */
	public UnknownEntity(List<String> uri) {
		super(uri);
	}


	@Override
	public void populateConnections(
			HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> connectedTuples) {
	}


	@Override
	protected String getHtmlIcon() {
		return null;
	}

	public Collection<? extends ArrayList<String>> getTuples() {
		return new HashSet<>();
	}


	@Override
	public String getDescription() {
		StringBuilder strb = new StringBuilder("<html>");
		strb.append("URI: " + this.render() + "<br>");
		strb.append("</html>");
		return strb.toString();
	}


	@Override
	public Collection<? extends ArrayList<String>> getTuples(boolean rename) {
		return new HashSet<>();
	}

}
