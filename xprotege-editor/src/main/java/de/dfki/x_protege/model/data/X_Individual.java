package de.dfki.x_protege.model.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import de.dfki.x_protege.model.OntologyCache;

/**
 * Implementation of the {@link AbstractX_Entity} representing Instances/Individual.
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 20.03.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public abstract class X_Individual extends AbstractX_Entity
		implements
			Comparable<X_Individual> {

	private HashSet<X_Type> clazz;
	private boolean propertiesComputed = false;
	private final HashMap<Integer, HashSet<AbstractX_Property>> connectedProperties = new HashMap<>();

	private final Map<Integer, Map<AbstractX_Property, Set<ArrayList<X_Individual>>>> populatedProperties = new HashMap<>();

	/**
	 * Create a new instance of {@link X_Individual}.
	 * @param uri the uri/name of the individual/instance
     */
	public X_Individual(List<String> uri) {
		super(uri);
		this.clazz = new HashSet<>();
		initMap();
	}

	/**
	 * Create a new instance of {@link X_Individual}.
	 * @param uri the uri/name of the individual/instance
	 */
	public X_Individual(ArrayList<String> uri) {
		super(uri);
		this.clazz = new HashSet<>();
		initMap();
	}

	private void initMap() {
		this.connectedProperties.put(0, new HashSet<>());
		this.connectedProperties.put(1, new HashSet<>());
		this.connectedProperties.put(2, new HashSet<>());
		this.populatedProperties.put(0,
				new HashMap<>());
		this.populatedProperties.put(1,
				new HashMap<>());
		this.populatedProperties.put(2,
				new HashMap<>());
	}

	@Override
	public int compareTo(X_Individual o) {
		return 0;
	}

	/**
	 *
	 * @return the classes this {@link X_Individual} is a instance of.
     */
	public Set<X_Type> getClazz() {
		return this.clazz;
	}

	@Override
	public void populateConnections(
			HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> connectedTuples) {
		if (this.generated)
			return;
		if (!this.annotationComputed) {
			computeAnnotations(connectedTuples);
		}
		if (!this.propertiesComputed) {
			computeProperties(connectedTuples);
		}
		performExtraPopulation(connectedTuples);
	}

	private void computeProperties(
			HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> connectedTuples) {
		HashSet<AbstractX_Property> allConnectedProperties = new HashSet<>();
		allConnectedProperties.addAll(this.connectedProperties.get(0));
		allConnectedProperties.addAll(this.connectedProperties.get(1));
		allConnectedProperties.addAll(this.connectedProperties.get(2));
		for (AbstractX_Property p : allConnectedProperties) {
			String propertyName = p.getShortName().get(0);
			if (connectedTuples.containsKey(propertyName)) {
				Map<ArrayList<String>, Set<ArrayList<String>>> matches = connectedTuples
						.get(propertyName);
				Set<ArrayList<String>> rangeAndExtra = matches
						.get(this.getShortName());
				if (rangeAndExtra != null) {
					for (ArrayList<String> indis : rangeAndExtra) {
						addPopulatedProperty(p, getIndividualFor(p, indis));
					}
				}
			}
		}
	}

	private ArrayList<X_Individual> getIndividualFor(AbstractX_Property p,
			ArrayList<String> indis) {
		ArrayList<X_Individual> result = new ArrayList<>();
		int rangeArity = p.getArities()[1];
		int extraArity = p.getArities()[2];
		// create Range
		ArrayList<String> ra = new ArrayList<>();
		for (int i = 0; i < rangeArity; i++) {
			ra.add(indis.get(i));
		}
		if (rangeArity == 1) {
			if (ra.get(0).contains("^^<xsd:")) {
				result.add(new AtomicX_Indivdual(ra));
			} else {
				result.add(OntologyCache.getIndividual(ra));
			}
		} else {
			if (OntologyCache.getIndividual(ra) == null) {
				CartesianX_Individual ci = new CartesianX_Individual(ra);
				OntologyCache.addIndividual(ci);
				result.add(ci);
			} else {
				result.add(OntologyCache.getIndividual(ra));
			}
		}
		if (extraArity != 0 && rangeArity != indis.size()) {
			ArrayList<String> ex = new ArrayList<>();
			for (int i = rangeArity; i < indis.size(); i++) {
				ex.add(indis.get(i));
			}
			if (extraArity == 1) {
				if (ex.get(0).contains("^^<xsd:")) {
					result.add(new AtomicX_Indivdual(ex));
				} else {
					result.add(OntologyCache.getIndividual(ex));
				}
			} else {
				if (OntologyCache.getIndividual(ex) == null) {
					CartesianX_Individual ci = new CartesianX_Individual(ex);
					OntologyCache.addIndividual(ci);
					result.add(ci);
				} else {
					result.add(OntologyCache.getIndividual(ex));
				}
			}
		}
		return result;
	}

    /**
     * Computes the additional instance specific values based on the tuple representation of the individual. Called whenever the property is loaded from from file.
     *
     * @param connectedTuples An {@link HashMap} containing all tuples connected to this Property. The keys of the map are properties, whereas the values are  domain -> range + extra mappings.
     */
	protected abstract void performExtraPopulation(
			HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> connectedTuples);

    /**
     * Add the given type/class to the one this individual is an instance of
     * @param type the type to be added.
     */
	public void addClazz(X_Type type) {
		this.clazz.add(type);
		addConnectedProperties(type.getConnectedProperties());
		addConnectedProperties(OntologyCache.getType(type.getShortName())
				.getConnectedProperties());
	}

	@Override
	public Collection<? extends ArrayList<String>> getTuples(boolean rename) {
		Set<ArrayList<String>> tuples = new HashSet<ArrayList<String>>();
		tuples.addAll(this.getStructuralTuples());
		tuples.addAll(this.getPropertyTuples());
		return tuples;
	}



	public Collection<? extends ArrayList<String>> getTuples() {
		return getTuples(false);
	}

	protected abstract Collection<? extends ArrayList<String>> getStructuralTuples();

	private Collection<? extends ArrayList<String>> getPropertyTuples() {
		Set<ArrayList<String>> propertyTuples = new HashSet<ArrayList<String>>();
		for (int key : populatedProperties.keySet()) {
			for (AbstractX_Property prop : populatedProperties.get(key)
					.keySet()) {
				for (ArrayList<X_Individual> i : populatedProperties.get(key)
						.get(prop)) {
					ArrayList<String> tuple = new ArrayList<>();
					tuple.addAll(this.getShortName());
					tuple.addAll(prop.getShortName());
					for (X_Individual indi : i) {
						if (indi.isAtomInstance()) {
							tuple.add(indi.getPrettyPrint(false));
						} else {
							for (AtomicX_Indivdual si : ((CartesianX_Individual) indi)
									.getContains()) {
								tuple.add(si.getPrettyPrint(false));
							}
						}
					}
					propertyTuples.add(tuple);
				}
			}
		}
		return propertyTuples;
	}

    /**
     * Add the given {@link X_Property}s to the set of properties which may be populated by this {@link X_Individual}.
     * @param connectedProperties the {@link X_Property}s to be added
     */
	public void addConnectedProperties(Set<X_Property> connectedProperties) {
		for (X_Property p : connectedProperties) {
			addConnectedProperty(p);
		}
	}

    /**
     *
     * @return the set of properties which may be populated by this {@link X_Individual}.
     */
	public HashMap<Integer, HashSet<AbstractX_Property>> getConnectedProperties() {
		return this.connectedProperties;
	}

    /**
     * Add the given {@link X_Property} with the given values to the mapping of properties populated by this {@link X_Individual}.
     * @param p the {@link X_Property}s to be added
     * @param values {@link ArrayList} of {@link X_Individual}s describing the range and extra argument of the {@link X_Property} populated by this {@link X_Individual}.
     */
	public void addPopulatedProperty(AbstractX_Property p,
			ArrayList<X_Individual> values) {
		for (int i : connectedProperties.keySet()) {
			if (connectedProperties.get(i).contains(p)) {
				if (populatedProperties.get(i).containsKey(p)) {
					this.populatedProperties.get(i).get(p).add(values);
				} else {
					Set<ArrayList<X_Individual>> valueSet = new HashSet<>();
					valueSet.add(values);
					this.populatedProperties.get(i).put(p, valueSet);
				}
			}
		}
	}

    /**
     *
     * @return the mapping of properties populated by this {@link X_Individual} to their value.
     */
	public Map<AbstractX_Property, Set<ArrayList<X_Individual>>> getPopulatedProperties(
			int mode) {

		return populatedProperties.get(mode);
	}

    /**
     * Remove the property-value mapping for the given values.
     * @param property the property to be updated/removed
     * @param values the values to be removed from populated property mapping
     */
	public void removePopulatedProperty(AbstractX_Property property,
			ArrayList<X_Individual> values) {
		for (int i : connectedProperties.keySet()) {
			if (populatedProperties.get(i).containsKey(property)) {
				populatedProperties.get(i).get(property).remove(values);
			}
		}
	}

	public void removeClazz(X_Type value) {
		this.clazz.remove(value.getShortName());
	}

	@Override
	public String toString() {
		StringBuilder strb = new StringBuilder("<html>");
		strb.append(render(true));
		strb.append("</html>");
		return strb.toString();
	}

    /**
     * Adds the given property to the set of properties which may be populated by this individual.
     * @param p the property to be added.
     */
	public void addConnectedProperty(AbstractX_Property p) {
		this.connectedProperties.get(p.getPropertyType().nextSetBit(0)).add(p);
	}

	/**
	 * removes the given {@link AbstractX_Property} from the {@link Map} of
	 * connected Properties and if necessary from the set of populated
	 * properties.
	 * 
	 * @param p
	 *            The {@link AbstractX_Property} to be removed.
	 */
	public void removeConnectedProperty(AbstractX_Property p) {
		int type = p.getPropertyType().nextSetBit(0);
		this.connectedProperties.get(type).remove(p);
		if (this.populatedProperties.get(type).containsKey(p)) {
			this.populatedProperties.get(type).remove(p);
		}
	}

    /**
     *
     * @param shortNamespace boolean flag indicating whether the short or long namespace should be used.
     * @return A formatted human readable version of the individual.
     */
	public abstract String getPrettyPrint(boolean shortNamespace);

	@Override
	public boolean isInstance() {
		return true;
	}

	@Override
	public boolean isAtomType() {
		return false;
	}

	@Override
	public boolean isCartesianType() {
		return false;
	}


	@Override
	public String getDescription() {
		StringBuilder strb = new StringBuilder("<html>");
		strb.append("<u><b>URI:</b></u> " + this.getLongName() + "<br>");
		strb.append("<u><b>Annotations:</b></u> <br>");
		for (Annotation a : this.annotations) {
			strb.append(a.render().replace("</html>", ""));
			strb.append("<br>");
		}
		strb.append("<u><b>Mixed Properties:</b></u> <br>");
		for (Entry<AbstractX_Property, Set<ArrayList<X_Individual>>> e : this.populatedProperties
				.get(0).entrySet())
			for (ArrayList<X_Individual> inst : e.getValue()) {
				strb.append(render(e.getKey(), inst).replace("</html>", ""));
				strb.append("<br>");
			}
		strb.append("<u><b>Datatype Properties:</b></u> <br>");
		for (Entry<AbstractX_Property, Set<ArrayList<X_Individual>>> e : this.populatedProperties
				.get(1).entrySet())
			for (ArrayList<X_Individual> inst : e.getValue()) {
				strb.append(render(e.getKey(), inst).replace("</html>", ""));
				strb.append("<br>");
			}
		strb.append("<u><b>Object Properties:</b></u> <br>");
		for (Entry<AbstractX_Property, Set<ArrayList<X_Individual>>> e : this.populatedProperties
				.get(2).entrySet())
			for (ArrayList<X_Individual> inst : e.getValue()) {
				strb.append(render(e.getKey(), inst).replace("</html>", ""));
				strb.append("<br>");
			}
		strb.append("<u><b>Types:</b></u> <br>");
		for (X_Type t : this.clazz) {
			strb.append(t.render(true).replace("</html>", ""));
			strb.append("<br>");
		}
		strb.append("</html>");
		return strb.toString();
	}

	private String render(AbstractX_Property p,
			ArrayList<X_Individual> values) {
		StringBuilder strb = new StringBuilder("<html>");
		strb.append(p.render(true));
		strb.append("<p>&nbsp;&nbsp;&nbsp;&nbsp;");
		// html range
		for (X_Individual i : values) {
			if (values.indexOf(i) < values.size() - 1) {
				if (i != null) {
					strb.append(i.render(true));
					strb.append("</p><p>&nbsp;&nbsp;&nbsp;&nbsp;");
				}
			} else {
				strb.append(i.render(true));
			}
		}
		// html extra
		strb.append("</p></html>");
		return strb.toString().trim();
	}

	/**
	 *
	 * @return all classes which are disjoint to those this individual instantiates.
	 */
	public Set<X_Type> getDisjoints() {
		HashSet<X_Type> disjoints = new HashSet<>();
		for (X_Type t : this.clazz) {
			disjoints.addAll(t.getDisjoints());
		}
		return disjoints;
	}

	/**
	 * This method is used to updates a property connected to this instance ( the ones that may be used as well as the populated ones) after the type of it has been changed, e.g. from mixed to data
	 * @param p the property to be updates
	 * @param nodeType the old type
	 * @param parentType the new type
	 */
	public void updateProperties(AbstractX_Property p, int nodeType,
			int parentType) {
		Set<ArrayList<X_Individual>> values = this.populatedProperties
				.get(nodeType).get(p);
		this.connectedProperties.get(nodeType).remove(p);
		if (this.populatedProperties.get(nodeType).containsKey(p)) {
			this.populatedProperties.get(nodeType).remove(p);
		}
		this.connectedProperties.get(parentType).add(p);
		this.populatedProperties.get(parentType).put(p, values);

	}
}
