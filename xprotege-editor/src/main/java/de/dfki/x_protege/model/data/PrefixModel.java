package de.dfki.x_protege.model.data;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.table.AbstractTableModel;

import org.apache.log4j.Logger;

import de.dfki.lt.hfc.Namespace;
import de.dfki.x_protege.model.OntologyCache;
import de.dfki.x_protege.ui.components.table.PrefixMapperTable;

/**
 * This table model is used to populate the NamespaceTable used within the
 * Ontology tab <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 19.03.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class PrefixModel extends AbstractTableModel {
	private static final long serialVersionUID = -5098097390890500539L;

	private static final Logger LOGGER = Logger.getLogger(PrefixModel.class);

	private final static String NARY_SHORT = "nary";

	private static final String NARY_LONG = "http://de.dfki.xprotege.2015/03/nary#";

	public enum Column {
		PREFIX_NAME, PREFIX;
	}

	private List<String> prefixes;

	private Namespace ns;

	private boolean changed = false;

	private Set<String> standards;

	private PrefixMapperTable connectedTable;

	private String localNS_short = "";

    /**
     * Creates a new instance of {@link PrefixModel} based on the given {@link Namespace}
     * @param namespace the namespace the model is based on
     */
	public PrefixModel(Namespace namespace) {
		super();
		this.standards = new HashSet<>();
		this.standards.add(Namespace.RDF_SHORT);
		this.standards.add(Namespace.OWL_SHORT);
		this.standards.add(Namespace.XSD_SHORT);
		this.standards.add(NARY_SHORT);
		this.standards.add(Namespace.RDFS_SHORT);
		this.prefixes = new LinkedList<>();
		this.ns = namespace;
		populateModel();
	}

	private void populateModel() {
		for (String prefixName : ns.shortToLong.keySet()) {
			if (!prefixName
					.equals(OntologyCache.relatedOntology.getLocalNameSpace()))
				prefixes.add(prefixName);
		}
	}

    /**
     *
     * @param prefix the prefix to check
     * @return the index of the prefix in the tablemodel
     */
	public int getIndexOfPrefix(String prefix) {
		return prefixes.indexOf(prefix);
	}

    /**
     * Add a new prefix mapping to the underlying namespace
     * @param prefix the prefix to be added
     * @param value the prefix' value
     */
    void addFormToNameSpace(String prefix, String value) {
		ns.putForm(prefix, value);
	}

	private void addLocalNameSpace(String prefix, String value) {
		setLocalNS_short(prefix);
		addFormToNameSpace(prefix, value);
	}

    /**
     * Add a new prefix mapping to the model
     * @param prefix the prefix to be added
     * @param value the prefix' value
     */
	public int addMapping(String prefix, String value) {
		changed = changed || (value != null && value.length() != 0);
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("adding mapping " + prefix + " -> " + value
					+ " changed = " + changed);
		}
		prefixes.add(prefix);
		Collections.sort(prefixes);
		ns.putForm(prefix, value);
		fireTableDataChanged();
		return prefixes.indexOf(prefix);
	}

    /**
     * Remove the given mapping from the model
     * @param prefix the prefix to be removed
     * @param value the prefix' value
     */
	public void removeMapping(String prefix) {
		prefixes.remove(prefix);
		ns.longToShort.remove(ns.getLongForm(prefix));
		String prefixValue = ns.shortToLong.remove(prefix);
		changed = changed || (prefixValue != null & prefixValue.length() != 0);
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("removing mapping " + prefix + " -> " + prefixValue
					+ " changed = " + changed);
		}
		fireTableDataChanged();
	}

    /**
     * updates the underlying namespace with the current state of the model. This is done before an ontology is saved to ensure that everything is up-to-date.
     * @return true if there was a difference between namespace and model, false otherwise
     */
	public boolean commitPrefixes() {
		if (changed) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(
						"committing prefix changes and clearing changed flag");
			}
			for (Entry<String, String> prefixName2PrefixEntry : ns.shortToLong
					.entrySet()) {
				String prefixName = prefixName2PrefixEntry.getKey();
				String prefix = prefixName2PrefixEntry.getValue();
				if (prefix != null && prefix.length() != 0) {
					// tailing : automatically added in here
					this.ns.putForm(prefixName, prefix);
				}
			}
			changed = false;
			return true;
		}
		return false;
	}

	/*
	 * Table Model interfaces
	 */

	@Override
	public String getColumnName(int column) {
		switch (Column.values()[column]) {
			case PREFIX_NAME :
				return "Prefix";
			case PREFIX :
				return "Value";
			default :
				throw new UnsupportedOperationException(
						"Programmer error: missed a case");
		}
	}

	@Override
	public int getRowCount() {
		return prefixes.size();
	}

	@Override
	public int getColumnCount() {
		return Column.values().length;
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return !isStandardPrefix(prefixes.get(rowIndex));

	}

    /**
     * Standard prefixes are : owl, rdf, rdfs, xsd and nary
     * @param prefix the prefix to be checked
     * @return true, if the given prefix is a standard one, false otherwise
     */
	public boolean isStandardPrefix(String prefix) {
		return this.standards.contains(prefix);

	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		String prefix = prefixes.get(rowIndex);
		switch (Column.values()[columnIndex]) {
			case PREFIX_NAME :
				return prefix;
			case PREFIX :
				return ns.shortToLong.get(prefix);
			default :
				throw new UnsupportedOperationException(
						"Programmer error: missed a case");
		}
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		String currentPrefixName = (String) getValueAt(rowIndex,
				Column.PREFIX_NAME.ordinal());

		switch (Column.values()[columnIndex]) {
			case PREFIX_NAME :
				// Replacing prefix name
				String newPrefix = aValue.toString();
				if (!prefixes.contains(newPrefix)) {
					prefixes.remove(currentPrefixName);
					prefixes.add(newPrefix);
					Collections.sort(prefixes);
					String prefixValue = ns.shortToLong
							.remove(currentPrefixName);
					ns.longToShort.remove(prefixValue);
					ns.putForm(newPrefix, prefixValue);
					changed = changed || (prefixValue != null
							&& prefixValue.length() != 0);
				}
				break;
			case PREFIX :
				// Replacing value
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug(
							"Changing the value associated with the prefix "
									+ currentPrefixName
									+ " with a delete and an add.");
				}
				removeMapping(currentPrefixName);
				addMapping(currentPrefixName, aValue.toString());
				break;
			default :
				throw new UnsupportedOperationException(
						"Programmer error: missed a case");
		}
		this.connectedTable.prefixChanged(currentPrefixName, aValue.toString());
	}

    /**
     * Writes the underlying namespace to the given location
     * @param namespaceUri2 the location where to store the onto
     * @return true if the write process was successful, false otherwise
     */
    boolean write(String namespaceUri2) {
		StringBuilder strb = new StringBuilder();
		for (Entry<String, String> entry : this.ns.shortToLong.entrySet()) {
			strb.append(entry.getKey());
			strb.append(" ");
			strb.append(entry.getValue());
			strb.append("\n");
		}
		try {
			return Files.write(Paths.get(namespaceUri2),
					strb.toString().getBytes()) != null;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

    /**
     *
     * @param namespace the namespace to check
     * @return true if the given namespace is part of the model, false otherwise
     */
	public boolean contains(String namespace) {
		return ns.shortToLong.keySet().contains(namespace)
				|| ns.longToShort.keySet().contains(namespace);
	}

    /**
     * connects the model to the given table
     * @param prefixMapperTable the {@link PrefixMapperTable} to be set
     */
	public void setConnectedTable(PrefixMapperTable prefixMapperTable) {
		this.connectedTable = prefixMapperTable;
	}

    /**
     * updates the local namespace after the ontology was renamed
     * @param oldPrefix the old local namespace
     * @param lastBitFromUrl the last part of the new namespace (short namespace),
     * @param string the full namespace
     */
	public void localNamespaceChanged(String oldPrefix, String lastBitFromUrl,
			String string) {
		if (contains(oldPrefix))
			removeMapping(oldPrefix);
		addLocalNameSpace(lastBitFromUrl, string);

	}


	public void setLocalNS_short(String localNS_short) {
		this.localNS_short = localNS_short;
	}

	public String getShortForLong(String longPrefix) {
		String result = longPrefix;
		if (longPrefix.contains(Namespace.RDFS_LONG)) {
			result = longPrefix.replace(Namespace.RDFS_LONG,
					Namespace.RDFS_SHORT);
		}
		if (longPrefix.contains(Namespace.RDF_LONG)) {
			result = longPrefix.replace(Namespace.RDF_LONG,
					Namespace.RDF_SHORT);
		}
		if (longPrefix.contains(Namespace.XSD_LONG)) {
			result = longPrefix.replace(Namespace.XSD_LONG,
					Namespace.XSD_SHORT);
		}
		if (longPrefix.contains(Namespace.OWL_LONG)) {
			result = longPrefix.replace(Namespace.OWL_LONG,
					Namespace.OWL_SHORT);
		}
		if (longPrefix.contains(NARY_LONG)) {
			result = longPrefix.replace(NARY_LONG, NARY_SHORT);
		}
		return result;
	}

	public String getLongForShort(String shortPrefix) {
		String result = shortPrefix;
		if (shortPrefix.contains(Namespace.RDFS_SHORT)) {
			result = shortPrefix.replace(Namespace.RDFS_SHORT,
					Namespace.RDFS_LONG);
		}
		if (shortPrefix.contains(Namespace.RDF_SHORT)) {
			result = shortPrefix.replace(Namespace.RDF_SHORT,
					Namespace.RDF_LONG);
		}
		if (shortPrefix.contains(Namespace.XSD_SHORT)) {
			result = shortPrefix.replace(Namespace.XSD_SHORT,
					Namespace.XSD_LONG);
		}
		if (shortPrefix.contains(Namespace.OWL_SHORT)) {
			result = shortPrefix.replace(Namespace.OWL_SHORT,
					Namespace.OWL_LONG);
		}
		if (shortPrefix.contains(NARY_SHORT)) {
			result = shortPrefix.replace(NARY_SHORT, NARY_LONG);
		}
		return result;
	}

	public ArrayList<String> search(String searchTerm) {
		ArrayList<String> result = new ArrayList<>();
		for (Entry<String, String> e : ns.shortToLong.entrySet()) {
			if (e.getValue().contains(searchTerm))
				result.add("<" + e.getValue());
			if (e.getKey().contains(searchTerm))
				result.add("<" + e.getKey() + ":");
		}
		return result;
	}

	public String getNameSpace(String uri) {
		String result = uri;
		if (uri.contains(Namespace.RDFS_SHORT)
				|| uri.contains(Namespace.RDFS_LONG)) {
			result = Namespace.RDFS_LONG;
		}
		if (uri.contains(Namespace.RDF_SHORT)
				|| uri.contains(Namespace.RDF_LONG)) {
			result = Namespace.RDF_LONG;
		}
		if (uri.contains(Namespace.XSD_SHORT)
				|| uri.contains(Namespace.XSD_LONG)) {
			result = Namespace.XSD_LONG;
		}
		if (uri.contains(Namespace.OWL_SHORT)
				|| uri.contains(Namespace.OWL_LONG)) {
			result = Namespace.OWL_LONG;
		}
		if (uri.contains(NARY_SHORT) || uri.contains(NARY_LONG)) {
			result = NARY_LONG;
		}
		return result;
	}

}
