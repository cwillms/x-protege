package de.dfki.x_protege.model.hfc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import de.dfki.x_protege.model.OntologyCache;
import de.dfki.x_protege.model.data.AbstractX_Property;
import de.dfki.x_protege.model.data.AtomicX_Type;
import de.dfki.x_protege.model.data.CartesianX_Type;
import de.dfki.x_protege.model.data.Ontology;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.data.X_Individual;
import de.dfki.x_protege.model.data.X_Property;
import de.dfki.x_protege.model.data.X_Type;

/**
 * This class is used to extract the subclass/subproperty trees from the hfc data structure.
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 22.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
class HFC_TreeFactory {

	/**
	 * 0 property tree 1 class tree 2 property tree - down from mixed property
	 */
	private X_Entity[] treeModels = new X_Entity[3];
	private HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> properties;

	private HashMap<ArrayList<String>, ArrayList<ArrayList<String>>> parent2Child = new HashMap<>();

	private HashSet<ArrayList<String>> objectProp = new HashSet<>();

	private HashSet<ArrayList<String>> dataProp = new HashSet<>();
	private HashSet<ArrayList<String>> mixedProp = new HashSet<>();

	/**
	 * Creates a new instance of {@link HFC_TreeFactory}
	 * @param properties Mapping from property to domain and range + extra arguments.
     */
	public HFC_TreeFactory(
			HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> properties) {
		this.properties = properties;
	}

	/**
	 * Creates the subclass and subproperty trees.
	 */
	public void createTrees() {
		// populate dependency structure (type2paren, property2Parent, cache)
		computeDependencies();
		buildPropertyStructure();
		// create trees by resolving dependencies
		populateTrees();
	}

	private void populateTrees() {
		// first resolve properties
		populatePropertiesTree();
		// second resolve types
		populateTypeTree();

		populateConnections();
	}

	private void populateConnections() {
		// populate properties
		populateConnection(treeModels[0]);
		// populate classes -> includes creation of individuals
		populateConnection(treeModels[1]);
		// populate individuals created by populateConnections()
		populateStandards();
		populateIndividualConnections();
		// compute implicit dependencies
		computeImplicitDependencies();
	}

	private void populateStandards() {
		HashMap<ArrayList<String>, Set<ArrayList<String>>> domain2Instance = properties
				.get("<rdf:type>");
		// iterate domains
		for (ArrayList<String> domain : domain2Instance.keySet()) {
			for (ArrayList<String> instance : domain2Instance.get(domain)) {
				if (OntologyCache.containsProperty(instance)
						&& !OntologyCache.containsProperty(domain)) {
					OntologyCache.addProperty(new X_Property(domain, -1));
					break;
				} else {
					if (!OntologyCache.containsType(instance)
							&& !OntologyCache.containsIndividual(instance)
							&& !OntologyCache.containsProperty(instance)) {
						OntologyCache.addType(new AtomicX_Type(instance));
						if (!OntologyCache.containsType(domain))
							OntologyCache.addType(new AtomicX_Type(domain));
					}
				}
			}
		}
	}

	private void computeImplicitDependencies() {
		// compute implicit stuff for properties
		extractImplicitDomains();
		// compute implicit stuff for types
		extractImplicitProperties();
	}

	private void extractImplicitProperties() {
		Set<X_Entity> subProperties;
		for (X_Type t : OntologyCache.loadedTypes.values()) {
			Set<X_Property> properties = t.getConnectedProperties();
			// the actual computation is not very effective but who cares... TODO
			// replace this by a stream and lambda expression
			for (X_Property p : properties) {
				subProperties = p.getAllSubs();
				for (X_Entity sp : subProperties) {
					t.addImplicitProperty((X_Property) sp);
				}
			}
			// remove dublicates
			for (X_Property dp : t.getConnectedImProperties()) {
				if (t.getConnectedProperties().contains(dp))
					t.removeExplicitProperty(dp);
			}
		}
	}

	/**
	 * 
	 */
	private void extractImplicitDomains() {
		Set<X_Entity> subTypes;
		for (AbstractX_Property p : OntologyCache.getLoadedProperties()) {
			HashSet<X_Type> domain = (HashSet<X_Type>) p.getExplicitDomain();
			// domain
			for (X_Type d : domain) {
				subTypes = d.getAllSubs();
				for (X_Entity e : subTypes) {
					p.addImplicitDomain((X_Type) e);
					for (X_Individual i : ((X_Type) e)
							.getConnectedIndividualsSorted()) {
						i.addConnectedProperty(p);
					}
				}
			}
			// remove duplicates
			for (X_Type dt : p.getImplicitDomain()) {
				if (p.getExplicitDomain().contains(dt)) {
					p.removeExplicitDomain(dt);
				}
			}
			// range
			for (X_Type r : p.getExplicitRange()) {
				subTypes = r.getAllSubs();
				for (X_Entity e : subTypes) {
					p.addImplicitRange((X_Type) e);
					((X_Type) e).addImplicitRangeOf((X_Property) p);
				}
			}
			// remove duplicates
			for (X_Type rt : p.getImplicitRange()) {
				if (p.getExplicitRange().contains(rt))
					p.removeExplicitRange(rt);
			}
			// extra
			for (X_Type ex : p.getExplicitExtra()) {
				subTypes = ex.getAllSubs();
				for (X_Entity e : subTypes) {
					p.addImplicitExtra((X_Type) e);
					((X_Type) e).addImplicitExtraOf((X_Property) p);
				}
			}
			// remove duplicates
			for (X_Type et : p.getImplicitExtra()) {
				if (p.getExplicitExtra().contains(et))
					p.removeExplicitExtra(et);
			}
		}
	}

	private void populateIndividualConnections() {
		for (Entry<List<String>, X_Individual> e : OntologyCache.loadedIndividuals
				.entrySet()) {
			populateConnection(e.getValue());
		}

	}

	private void populateConnection(X_Entity node) {
		// find the connected tuples
		HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> connectedTuples = computeConnectedTuples(
				node);
		// populate the element using the collected information
		node.populateConnections(connectedTuples);
		for (X_Entity sub : node.getSubs()) {
			populateConnection(sub);
		}
	}

	private HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> computeConnectedTuples(
			X_Entity node) {
		HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> result = new HashMap<>();
		for (Entry<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> e : properties
				.entrySet()) {
			for (Entry<ArrayList<String>, Set<ArrayList<String>>> e1 : e
					.getValue().entrySet()) {
				if (e1.getKey().equals(node.getShortName())) {
					for (ArrayList<String> r : e1.getValue()) {
						addTo(result, e.getKey(), e1.getKey(), r);
					}
				}
				if (e1.getValue().contains(node.getShortName())) {
					for (ArrayList<String> r : e1.getValue()) {
						if (r.equals(node.getShortName()))
							addTo(result, e.getKey(), e1.getKey(), r);
					}
				}
			}
			if (e.getKey().equals(node.getShortName().get(0))) {
				addTo(result, e.getKey(), e.getValue());
			}
		}
		return result;

	}

	private void addTo(
			HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> result,
			String key,
			HashMap<ArrayList<String>, Set<ArrayList<String>>> value) {
		for (Entry<ArrayList<String>, Set<ArrayList<String>>> e : value
				.entrySet()) {
			for (ArrayList<String> r : e.getValue())
				addTo(result, key, e.getKey(), r);
		}

	}

	private void addTo(
			HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> result,
			String key, ArrayList<String> key2, ArrayList<String> value) {
		if (result.containsKey(key)) {
			if (result.get(key).containsKey(key2))
				result.get(key).get(key2).add(value);
			else {
				Set<ArrayList<String>> ranges = new HashSet<>();
				ranges.add(value);
				result.get(key).put(key2, ranges);
			}
		} else {
			HashMap<ArrayList<String>, Set<ArrayList<String>>> newMap = new HashMap<>();
			Set<ArrayList<String>> ranges = new HashSet<>();
			ranges.add(value);
			newMap.put(key2, ranges);
			result.put(key, newMap);
		}
	}

	private void populateTypeTree() {
		ArrayList<String> name = new ArrayList<>();
		name.add("<owl:Class>");
		OntologyCache.addType(new AtomicX_Type(name));
		name.clear();
		name.add("<nary:Class>");
		OntologyCache.addType(new CartesianX_Type(name));
		name.clear();
		name.add("<nary:Thing+>");
		X_Type root = new CartesianX_Type(name);
		OntologyCache.addType(root);
		appendChildsTo(root);
		treeModels[1] = root;
	}

	private void populatePropertiesTree() {
		// init the root of the whole tree
		ArrayList<String> name = new ArrayList<>();
		name.add("<rdf:Property>");
		X_Property root = new X_Property(name, -1);
		OntologyCache.addProperty(root);
		appendChildsTo(root);
		treeModels[0] = root;
	}

	private void populatePropertyStructure(X_Entity entity, HashSet<ArrayList<String>> props, int type){
		HashSet<X_Property> notAssigned = new HashSet<>();
		for (ArrayList<String> propName : props){
			X_Property p = new X_Property(propName, type);
			OntologyCache.addProperty(p);
			if (properties.get("<rdfs:subPropertyOf>").containsKey(propName)){
				// do nothing in this step but mark the property
				notAssigned.add(p);
			} else {
				entity.addSub(p);
			}
		}
		// recursively solve problematic assignments
		solveAssignmentProblem(notAssigned,type);
	}

	private void solveAssignmentProblem( HashSet<X_Property> notAssigned, int type) {
		HashSet<X_Property> toBeAssigned = new HashSet<>();
		for(X_Property p : notAssigned){
			// Check whether we can assign it to the already existing properties
			Set<ArrayList<String>> parents = properties.get("<rdfs:subPropertyOf>").get(p.getShortName());
			for (ArrayList<String> parent : parents){
				OntologyCache.getProperty(parent).addSub(p);
			}
		}
	}

	private void buildPropertyStructure(){
		HashMap<ArrayList<String>, Set<ArrayList<String>>> domain2Instance = properties
				.get("<rdf:type>");
		// iterate Entries
		for (Entry<ArrayList<String>, Set<ArrayList<String>>> entry : domain2Instance.entrySet()) {
			if (entry.getValue().contains(Stream.of("<owl:ObjectProperty>").collect(Collectors.toList()))){
				this.objectProp.add(entry.getKey());
			}
			else {
				if (entry.getValue().contains(Stream.of("<owl:DatatypeProperty>").collect(Collectors.toList()))){
					this.dataProp.add(entry.getKey());
				}
				else {
					if (entry.getValue().contains(Stream.of("<nary:MixedProperty>").collect(Collectors.toList()))) {
						this.mixedProp.add(entry.getKey());
					}
				}
			}
		}
	}

	private void appendChildsTo(X_Entity root) {
		ArrayList<ArrayList<String>> childs = parent2Child
				.get(root.getShortName());
		if (childs != null) {
			for (int i = 0; i < childs.size(); i++) {

				if (compare(root.getShortName(), childs.get(i)))
					continue;
				X_Entity child;
				if (root.isProperty()) {
					ArrayList<String> childName = childs.get(i);
					// special cases
					if (childName.get(0).contains("<owl:ObjectProperty>")){
						child = new X_Property(childName, 2);
						root.addSub(child);
						OntologyCache.addProperty((X_Property) child);
						populatePropertyStructure(child, this.objectProp, 2);
					}
					else if (childName.get(0)
							.contains("<owl:DatatypeProperty>")){
						child = new X_Property(childName, 1);
						root.addSub(child);
						OntologyCache.addProperty((X_Property) child);
						populatePropertyStructure(child, this.dataProp,1);
					}
					else if (childName.get(0)
							.contains("<nary:MixedProperty>")) {
						child = new X_Property(childName, 0);
						this.treeModels[2] = child;
						root.addSub(child);
						OntologyCache.addProperty((X_Property) child);
						populatePropertyStructure(child, this.mixedProp,0);
					} else {

								child = new X_Property(childName, -1);
						OntologyCache.addProperty((X_Property) child);
						root.addSub(child);

						}


				} else {
					if (root.isCartesianType()) {
						if (childs.get(i).size() > 1) {
							child = new CartesianX_Type(childs.get(i));
						} else {
							child = new AtomicX_Type(childs.get(i));
						}
					} else {
						if (!OntologyCache.containsType(childs.get(i)))
							child = new AtomicX_Type(childs.get(i));
						else
							child = OntologyCache.getType(childs.get(i));
					}
					OntologyCache.addType((X_Type) child);
					root.addSub(child);
				}
				appendChildsTo(child);
			}
		}
	}

	private void computeDependencies() {
		HashMap<ArrayList<String>, Set<ArrayList<String>>> domain2Instance = properties
				.get("<rdfs:subClassOf>");
		// iterate domains
		for (ArrayList<String> domain : domain2Instance.keySet()) {
			// disambiguate whether the domain is a type or a property itself
			Set<ArrayList<String>> parents = domain2Instance.get(domain);
			for (ArrayList<String> parent : parents) {
				if (parent2Child.containsKey(parent)) {
					parent2Child.get(parent).add(domain);
				} else {
					ArrayList<ArrayList<String>> instances = new ArrayList<>();
					instances.add(domain);
					parent2Child.put(parent, instances);
				}
			}
		}
	}

	/**
	 *
	 * @return Array containing the root node of each of the trees. The root node is sufficient to extract the model.
     */
	public X_Entity[] getTreeModels() {
		return this.treeModels;
	}

	/**
	 *
	 * @param ontology reference to the currently active ontology.
	 * @return An {@link HashMap} containing all tuples connected to the given ontology. The keys of the map are properties, whereas the values are  domain -> range + extra mappings.
     */
	public HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> getConnectedTuples(
			Ontology ontology) {
		return computeConnectedTuples(ontology);
	}

	private boolean compare(ArrayList<String> r, ArrayList<String> c) {
		if (r == null && c == null) {
			return true;
		}

		if ((r == null && c != null) || r != null && c == null
				|| r.size() != c.size()) {
			return false;
		}

		r = new ArrayList<>(r);
		c = new ArrayList<>(c);

		Collections.sort(r);
		Collections.sort(c);
		return r.equals(c);
	}

}
