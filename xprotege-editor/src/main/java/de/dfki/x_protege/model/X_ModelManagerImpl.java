package de.dfki.x_protege.model;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.protege.editor.core.AbstractModelManager;
import org.protege.editor.core.Disposable;
import org.protege.editor.core.ui.util.UIUtil;

import de.dfki.x_protege.model.data.AnnotationProperty;
import de.dfki.x_protege.model.data.Ontology;
import de.dfki.x_protege.model.data.OntologyDescriptor;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.utils.OntDescriptorUtil;
import de.dfki.x_protege.ui.logic.ModelEventManager;
import de.dfki.x_protege.ui.logic.OntologyEventManager;
import de.dfki.x_protege.ui.logic.SelectionEventManager;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.OntoChange;
import de.dfki.x_protege.ui.logic.Event.QueryEvent;
import de.dfki.x_protege.ui.logic.Event.SelectionChange;
import de.dfki.x_protege.ui.logic.Event.Type.ModelChangeType;
import de.dfki.x_protege.ui.logic.Event.Type.OntoChangeTypes;
import de.dfki.x_protege.ui.logic.listener.InformEventListener;
import de.dfki.x_protege.ui.logic.listener.ModelChangeListener;
import de.dfki.x_protege.ui.logic.listener.OntologyChangeListener;
import de.dfki.x_protege.ui.logic.listener.QueryEventListener;
import de.dfki.x_protege.ui.logic.listener.SelectionChangeListener;

/**
 * Implementation of the {@link X_ModelManager} interface based on protege's {@link AbstractModelManager}.<br>
 * This class builds the most upper level of X-Protege's model, it wraps up the currently loaded {@link Ontology}, connected Namespaces as well as the tree structures of classes and properties.
 * It also contains references to the several {@link de.dfki.x_protege.ui.logic.EventManager} used to update the model according to gui actions.
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 22.08.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class X_ModelManagerImpl extends AbstractModelManager
        implements
        X_ModelManager {

    private static final Logger logger = Logger
            .getLogger(X_ModelManagerImpl.class);

    private static final File resourceDir = new File(System.getProperty("user.dir") + "/resources/");

    public static String getResource(String name) {
        return new File(resourceDir, name).getPath();
    }

    private String TUPLEFILE = getResource("pre.default.eqred.nt");
    private String NAMESPACE = getResource("default.ns");
    private String RULES = getResource("default.eqred.rdl");
    private final Set<Ontology> loadedOntologies;
    private final String uriCorpus = "http://www.xprotege.dfki.de/";
    private Ontology activeModel;
    private SelectionEventManager selectionEventManager;

    private OntologyEventManager ontoEventHandler;

    private ModelEventManager modelEventManager;

    private QueryEventManager queryEventManager;


    /**
     * Creates a new instance of {@link X_ModelManager}.
     */
    public X_ModelManagerImpl() {
        super();
        this.loadedOntologies = new HashSet<>();
        selectionEventManager = new SelectionEventManager(getActiveModel());
        ontoEventHandler = new OntologyEventManager(getActiveModel());
        modelEventManager = new ModelEventManager(getActiveModel());
        queryEventManager = new QueryEventManager(getActiveModel());
    }

    /**
     * @deprecated For test only
     */
    @Deprecated
    public X_ModelManagerImpl(String tuple, String name, String rule) {
        super();
        TUPLEFILE = tuple;
        NAMESPACE = name;
        RULES = rule;
        this.loadedOntologies = new HashSet<>();
        selectionEventManager = new SelectionEventManager(getActiveModel());
        ontoEventHandler = new OntologyEventManager(getActiveModel());
        modelEventManager = new ModelEventManager(getActiveModel());
        queryEventManager = new QueryEventManager(getActiveModel());
    }

    // ################# PROTEGE PLUGIN SYSTEM #############################
    @Override
    public <T extends Disposable> void put(Object key, T object) {
        // TODO Auto-generated method stub

    }

    // PROTEGE ORIGNINAL
    @Override
    public <T extends Disposable> T get(Object key) {
        // TODO Auto-generated method stub
        return null;
    }


    // #################### SAVE ##########################################

    @Override
    public boolean save() {
        this.modelEventManager
                .handle(new ModelChange(ModelChangeType.ACCEPT, this));
        if (this.activeModel.getFileUri() != null) {
            this.activeModel.setDirty(false);
            return this.activeModel.store(this.activeModel.getFileUri());
        }
        this.activeModel.setDirty(false);
        return this.activeModel.store(null);
    }

    @Override
    public boolean save(Ontology model) {
        this.modelEventManager
                .handle(new ModelChange(ModelChangeType.ACCEPT, this));
        if (model.store(null)) {
            this.activeModel.setDirty(false);
            return true;
        }
        return false;
    }

    @Override
    public boolean saveAs(URI uri) {
        this.modelEventManager
                .handle(new ModelChange(ModelChangeType.ACCEPT, this));
        if (this.activeModel.store(uri)) {
            this.activeModel.setDirty(false);
            return true;
        }
        return false;

    }

    // ###################### ONTOLOGYCHANGES #############################

    @Override
    public void handleChange(OntoChange ontoChanged) {
        this.ontoEventHandler.handle(ontoChanged);
    }

    @Override
    public void addOntologyChangeListener(
            OntologyChangeListener ontologyChangeListener) {
        this.ontoEventHandler.addListener(ontologyChangeListener);
    }

    // ###################### SELECTION CHANGES #############################

    @Override
    public void handleSelectionChanged(SelectionChange selectionChange) {
        this.selectionEventManager.handle(selectionChange);

    }

    @Override
    public void addSelectionChangeListener(
            SelectionChangeListener selChangeListener) {
        this.selectionEventManager.addListener(selChangeListener);
    }

    @Override
    public void removeSelectionChangeListener(
            SelectionChangeListener selChangeListener) {
        this.selectionEventManager.removeListener(selChangeListener);
    }

    // ###################### MODELCHANGES #################################

    @Override
    public void handleChange(ModelChange modelChange) {
        this.modelEventManager.handle(modelChange);
    }

    @Override
    public void addModelChangeListener(ModelChangeListener modChangeListener) {
        this.modelEventManager.addListener(modChangeListener);
    }

    @Override
    public void removeModelChangeListener(
            ModelChangeListener modelChangeListener) {
        this.modelEventManager.removeListener(modelChangeListener);

    }

    // ################## HELPER################################################

    /**
     * This method loads an {@link Ontology} from the location specified by the given URI. The loaded {@link Ontology} is the active ontology maintained by this instance of {@link X_ModelManagerImpl}.
     *
     * @param uri {@link URI} specifying the location of the {@link Ontology} to be loaded
     * @return true if the loading of {@link Ontology} was successful, false otherwise
     * @throws URISyntaxException
     */
    public boolean loadOntologyFromPhysicalURI(URI uri)
            throws URISyntaxException {
        if (UIUtil.isLocalFile(uri)) {
            // Load the URIs of other ontologies that are contained in the same
            // folder.
            File parentFile = new File(uri).getParentFile();
            logger.info("Adding root folder: " + parentFile + " ...");
            // addRootFolder(parentFile);
            logger.info("\t...done");
        }
        Ontology model = null;
        model = this.loadOntologyFromOntologyDocument(uri);
        this.setActiveOntology(activeModel);
        return model != null;
    }

    private Ontology loadOntologyFromOntologyDocument(URI uri)
            throws URISyntaxException {
        OntologyCache.clear();
        OntologyDescriptor od = OntDescriptorUtil.parse(uri);
        // parse the .ont file connected to the ontology
        // OntologyDescriptor od = parsed file
        Ontology onto;
        URI loadedUri = new URI(od.getName());

        onto = createLoadedOntology(uri.getFragment(), loadedUri, TUPLEFILE, od,
                NAMESPACE, RULES);

        this.handleChange(new OntoChange(OntoChangeTypes.IDCHANGED,
                loadedUri.toString()));
        return onto;
    }

    @Override
    public Ontology createLoadedOntology(String id, URI uri, String tuplefile2,
                                         OntologyDescriptor od, String nameSpace2, String rules2) {
        Ontology o = new Ontology(id, uri, tuplefile2, od, NAMESPACE, RULES);
        this.activeModel = o;
        this.loadedOntologies.add(o);
        return o;
    }

    @Override
    public X_Entity getOWLThing() {
        ArrayList<String> owlT = new ArrayList<>();
        owlT.add("<owl:Thing>");
        return OntologyCache.getType(owlT);
    }

    /**
     * @return An {@link X_Entity} representing the owl:DatatypeProperty.
     */
    public X_Entity getDataTypeProp() {
        ArrayList<String> data = new ArrayList<>();
        data.add("<owl:DatatypeProperty>");
        return OntologyCache.getType(data);
    }

    @Override
    public X_Entity getXSDAnyType() {
        ArrayList<String> owlT = new ArrayList<>();
        owlT.add("<xsd:AnyType>");
        return OntologyCache.getType(owlT);
    }

    @Override
    public X_Entity getNaryThing() {
        ArrayList<String> owlT = new ArrayList<>();
        owlT.add("<nary:Thing+>");
        return OntologyCache.getType(owlT);
    }

    @Override
    public boolean isDefaultOntololgy() {
        return !(this.activeModel.isLoaded()
                || this.activeModel.isManipulated());
    }

    @Override
    public Ontology getActiveModel() {
        if (this.activeModel == null) {
            this.activeModel = createDefaultOntology(getDefaultID(), TUPLEFILE,
                    NAMESPACE, RULES);
        }
        return this.activeModel;
    }

    private String getDefaultID() {
        return "onto42";
    }

    @Override
    public Ontology createDefaultOntology(String ontologyID, String tuplefile,
                                          String nameSpace, String rules) {
        URI physicalURI;
        try {
            physicalURI = new URI(uriCorpus + ontologyID);
            Ontology o = new Ontology(ontologyID, physicalURI, tuplefile,
                    nameSpace, rules);
            this.activeModel = o;
            this.loadedOntologies.add(o);
            return o;
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        throw new IllegalArgumentException(
                "Was not able to create ontology for " + tuplefile);
    }

    @Override
    public boolean removeModel(Ontology ont) {
        if (loadedOntologies.size() > 1) {
            this.loadedOntologies.remove(ont);
            if (this.activeModel == ont) {
                this.activeModel = this.loadedOntologies.iterator().next();
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean isDirty() {
        return this.activeModel.isDirty();
    }

    @Override
    public boolean isDirty(Ontology model) {
        return model.isDirty();
    }


    @Override
    public void setDirty(Ontology model) {
        if (model == this.activeModel) {
            model.setDirty(true);
        }
    }

    @Override
    public void setActiveOntology(Ontology activeModel) {
        this.activeModel = activeModel;
        this.selectionEventManager = new SelectionEventManager(activeModel);
        this.modelEventManager = new ModelEventManager(activeModel);
        this.ontoEventHandler = new OntologyEventManager(activeModel);
        this.queryEventManager = new QueryEventManager(activeModel);
        OntologyCache.relatedOntology = activeModel;
    }

    @Override
    public ArrayList<AnnotationProperty> getAnnotationProperties() {
        return this.activeModel.getAnnotationProperties();
    }

    @Override
    public void addInformEventListener(
            InformEventListener informEventListener) {
        this.modelEventManager.setInformEventListener(informEventListener);

    }

    @Override
    public void removeOntologyChangeListener(
            OntologyChangeListener ontologyChangeListener) {
        this.ontoEventHandler.removeListener(ontologyChangeListener);

    }

    @Override
    public void addQueryEvenetListener(QueryEventListener qListener) {
        this.queryEventManager.addListener(qListener);

    }

    @Override
    public void removeQueryEvenetListener(QueryEventListener qListener) {
        this.queryEventManager.removeListener(qListener);

    }

    @Override
    public void handleQueryEvent(QueryEvent e) {
        this.queryEventManager.handle(e);
    }

}
