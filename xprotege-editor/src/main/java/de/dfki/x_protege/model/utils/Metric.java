package de.dfki.x_protege.model.utils;

import javax.swing.table.AbstractTableModel;

/**
 * This class provides a simple table model containing general information on the currently loaded onto, e.g. overall number of entities.
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 03.07.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class Metric extends AbstractTableModel{



    private String[] metricsNames;

    private int[] values;

    /**
     * Creates a new instance of {@link Metric}.
     * @param headers the names of the metrics
     * @param values the values of the metrics
     */
    public Metric(String[] headers, int[] values){
        this.metricsNames = headers;
        this.values = values;
    }


    public int getRowCount() {
        return metricsNames.length;
    }


    public int getColumnCount() {
        return 2;
    }


    public Object getValueAt(int rowIndex, int columnIndex) {
        if (columnIndex == 0) {
            return metricsNames[rowIndex];
        }
        else {
            return values[rowIndex];
        }
    }


    public String getColumnName(int column) {
        if (column == 0) {
            return "Metric";
        }
        else {
            return "Value";
        }
    }

    /**
     * Updates the table values after changes in the ontology model and informs the connected table, such that the table is rendered properly.
     * @param values the new values for the metrics.
     */
    public void update(int[] values) {
        this.values = values;
        fireTableDataChanged();
    }
}
