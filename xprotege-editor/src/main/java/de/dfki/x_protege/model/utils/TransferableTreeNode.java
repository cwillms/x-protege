package de.dfki.x_protege.model.utils;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import javax.swing.tree.TreePath;

import de.dfki.x_protege.model.data.X_Entity;

/**
 * Implementation of the {@link Transferable} interface to allow the .
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 22.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class TransferableTreeNode implements Transferable {

	private static DataFlavor X_Element_FLAVOR = new DataFlavor(X_Entity.class, "X_Element");

	private DataFlavor flavors[] = { X_Element_FLAVOR };

	private TreePath path;

	public TransferableTreeNode(TreePath tp) {
		path = tp;
	}

	@Override
	public DataFlavor[] getTransferDataFlavors() {
		return flavors;
	}

	@Override
	public boolean isDataFlavorSupported(DataFlavor flavor) {
		return (flavor.getRepresentationClass() == X_Entity.class);
	}

	@Override
	public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
		if (isDataFlavorSupported(flavor)) {
			return path;
		} else {
			throw new UnsupportedFlavorException(flavor);
		}
	}
}
