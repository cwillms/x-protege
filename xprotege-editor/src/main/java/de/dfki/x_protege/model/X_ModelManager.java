package de.dfki.x_protege.model;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Comparator;

import org.protege.editor.core.ModelManager;

import de.dfki.lt.hfc.WrongFormatException;
import de.dfki.x_protege.model.data.AnnotationProperty;
import de.dfki.x_protege.model.data.Ontology;
import de.dfki.x_protege.model.data.OntologyDescriptor;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.OntoChange;
import de.dfki.x_protege.ui.logic.Event.QueryEvent;
import de.dfki.x_protege.ui.logic.Event.SelectionChange;
import de.dfki.x_protege.ui.logic.listener.InformEventListener;
import de.dfki.x_protege.ui.logic.listener.ModelChangeListener;
import de.dfki.x_protege.ui.logic.listener.OntologyChangeListener;
import de.dfki.x_protege.ui.logic.listener.QueryEventListener;
import de.dfki.x_protege.ui.logic.listener.SelectionChangeListener;


/**
 * Extends the default protege {@link ModelManager}.
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 22.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public interface X_ModelManager extends ModelManager {



	/**
	 * Creates a new, empty ontology that has the specified ontology ID - i.e.
	 * the name of the ontology.
	 */
	Ontology createDefaultOntology(String ontologyID, String tuplefile,
								   String nameSpace, String rules);

	/**
	 * Creates an instance of {@link Ontology} from a given
	 * {@link OntologyDescriptor}
	 * 
	 * @throws IOException
	 * @throws WrongFormatException
	 * @throws FileNotFoundException
	 */
	Ontology createLoadedOntology(String ontologyID, URI uri, String tuplefile,
								  OntologyDescriptor ont, String nameSpace, String rules)
					throws FileNotFoundException, WrongFormatException,
					IOException;

	/**
	 * Remove the given ontology from the model manager. Cannot remove the last
	 * ontology from the model manager.
	 * 
	 * @param ont
	 *            the ontology to remove
	 * @return false if the ontology cannot be removed (eg if it does not exist
	 *         or is the last open ontology)
	 */
	boolean removeModel(Ontology ont);

	/**
	 * Performs a save operation. The behaviour of this is implementation
	 * specific. For example, some implementations may choose to save the active
	 * ontology, other implementations may choose to save all open ontologies
	 * etc.
	 */
	boolean save();

	/**
	 * Save only the ontology specified
	 * 
	 * @param ont
	 *            the ontology to save
	 * @throws org.semanticweb.owlapi.model.OntologyStorageException
	 *             if a problem occurs during the save
	 */
	boolean save(Ontology ont);

	/**
	 * @throws OntologyStorageException
	 *             if a problem occurs during the save
	 */
	boolean saveAs(URI uri);

	/**
	 * Checks if the ontology has been changed since it was loaded or last
	 * saved.
	 * 
	 * @param model
	 */
	boolean isDirty(Ontology model);

	/**
	 * This call is generally not recommended but there are occasions where a
	 * plugin knows that an ontology has been saved even though it has not been
	 * saved through the OWLModelManager save interface.
	 *
	 * @param ontology
	 *
	void setClean(Ontology model);

	/**
	 * Forces the system to believe that an ontology has been modified.
	 *
	 * @param model
	 *            The ontology to be made dirty.
	 */
	void setDirty(Ontology model);

	/**
	 * Gets the active ontology. This is the ontology that edits that cause
	 * information to be added to an ontology usually take place in.
	 * 
	 */
	Ontology getActiveModel();


	/**
	 *
	 * @return An {@link X_Entity} representing  owl:Thing.
     */
	X_Entity getOWLThing();

	/**
	 *
	 * @return An {@link X_Entity} representing  nary:Thing+.
	 */
	X_Entity getNaryThing();

	/**
	 *
	 * @return An {@link X_Entity} representing  xsd:AnyType.
	 */
	X_Entity getXSDAnyType();


    /**
     * Adds the given {@link OntologyChangeListener} from the set of active listeners
     * @param ontologyChangeListener the {@link OntologyChangeListener} to be added.
     */
	void addOntologyChangeListener(
			OntologyChangeListener ontologyChangeListener);

    /**
     * Processes the given {@link OntoChange}.
     * @param ontoChanged the {@link OntoChange} to be handled.
     */
	void handleChange(OntoChange ontoChanged);

    /**
     * Processes the given {@link SelectionChange}.
     * @param selectionChange the {@link SelectionChange} to be handled.
     */
	void handleSelectionChanged(SelectionChange selectionChange);

    /**
     * Adds the given {@link SelectionChangeListener} from the set of active listeners
     * @param selChangeListener the {@link SelectionChangeListener} to be added.
     */
	void addSelectionChangeListener(SelectionChangeListener selChangeListener);

    /**
     * Processes the given {@link ModelChange}.
     * @param modelChanged the {@link ModelChange} to be handled.
     */
	void handleChange(ModelChange modelChange);

	/**
	 * Adds the given {@link ModelChangeListener} from the set of active listeners
	 * @param modChangeListener the {@link ModelChangeListener} to be added.
	 */
	void addModelChangeListener(ModelChangeListener modChangeListener);

	/**
	 * removes the given {@link SelectionChangeListener} from the set of active listeners
	 * @param selChangeListener the {@link SelectionChangeListener} to be removed
	 */
	void removeSelectionChangeListener(
			SelectionChangeListener selChangeListener);

	/**
	 * This method is designed as an approach to disambiguate whether the the
	 * currently active ontology is in the default state
	 * 
	 * @return true - is in default state, false otherwise
	 */
	boolean isDefaultOntololgy();

	/**
	 *
	 * @return A {@link ArrayList} containing all available {@link AnnotationProperty}s.
     */
	ArrayList<AnnotationProperty> getAnnotationProperties();

	/**
	 * removes the given {@link ModelChangeListener} from the set of active listeners
	 * @param modelChangeListener the {@link ModelChangeListener} to be removed
	 */
	void removeModelChangeListener(ModelChangeListener modelChangeListener);

	/**
	 * adds the given {@link InformEventListener} to the set of active listeners
	 * @param informEventListener the {@link InformEventListener} to be added
	 */
	void addInformEventListener(InformEventListener informEventListener);

	/**
	 * removes the given {@link OntologyChangeListener} from the set of active listeners
	 * @param ontologyChangeListener the {@link OntologyChangeListener} to be removed
	 */
	void removeOntologyChangeListener(
			OntologyChangeListener ontologyChangeListener);

	/**
	 * adds the given {@link QueryEventListener} to the set of active listeners
	 * @param qListener the {@link QueryEventListener} to be added
	 */
	void addQueryEvenetListener(QueryEventListener qListener);

	/**
	 * removes the given {@link QueryEventListener} from the set of active listeners
	 * @param qListener the {@link QueryEventListener} to be removed
	 */
	void removeQueryEvenetListener(QueryEventListener qListener);

	/**
	 * delegates the given queryEvent to the {@link QueryEventManager} connected to the currently loaded ontology.
	 * @param queryEvent  the query to be processed
	 */
	void handleQueryEvent(QueryEvent queryEvent);
	/**
	 * Set the activeOntology field to the given {@link Ontology}
	 * @param activeModel the new active {@link Ontology}
	 */
	void setActiveOntology(Ontology activeModel);

}
