package de.dfki.x_protege.model.utils;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.StringTokenizer;

import de.dfki.x_protege.model.data.OntologyDescriptor;

/**
 * This class provides a simple parser for .ont files. <br>
 *     Such files have the following layout.<br>
 *          Name: foo //Name of the Ontology <br>
 * 			Version: 1.1 // OPTIONAL: in case not set, 1.0.0 is used<br>
 *			TupleFile: bar.nt<br>
 *			TupleFile_maxCartArity: 42 //maximale size allowed for cartesian types<br>
 *			Namespace: bar.ns // OPTIONAL: in case not set: default.ns is used <br>
 * 			Rules: bar.rdl //OPTIONAL: in case not set: default.rdl is used <br>
 * 			IO: Prefix     //OPTIONAL: in case not set: Infix is used <br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 03.07.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class OntDescriptorUtil {

	/**
	 * Generates a valid {@link OntologyDescriptor} by parsing the .ont file stored at the given location.
	 * @param uri the location of the .ont file
	 * @return the resulting {@link OntologyDescriptor}
     */
	public static OntologyDescriptor parse(URI uri) {
		String path = uri.getPath();
		String name = null;
		String version = null;
		String ioformat = null;
		String tuplefile = null;
		String namespace = null;
		String rules = null;
		try {
			FileInputStream fis = new FileInputStream(path);

			// Construct BufferedReader from InputStreamReader
			BufferedReader br = new BufferedReader(new InputStreamReader(fis));

			String line = null;
			while (!((line = br.readLine()) == null)) {
				if (line.isEmpty())
					continue;
				StringTokenizer tk = new StringTokenizer(line);
				String t = tk.nextToken();
				switch (t) {
					case "Name:" : {
						name = tk.nextToken();
						break;
					}
					case "Version:" : {
						version = tk.nextToken();
						break;
					}
					case "TupleFile:" : {
						tuplefile = tk.nextToken();
						break;
					}
					case "Namespace:" : {
						namespace = tk.nextToken();
						break;
					}
					case "I/O-Format:" : {
						ioformat = tk.nextToken();
						break;
					}
						// case "Rules:" : {
						// rules = tk.nextToken(); // TODO fix it
						// break;
						// }
					default :
						break;
				}
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new OntologyDescriptor(uri, name, version, ioformat, tuplefile,
				namespace, rules);

	}

}
