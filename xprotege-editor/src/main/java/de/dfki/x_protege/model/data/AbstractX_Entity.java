package de.dfki.x_protege.model.data;

import java.util.*;
import java.util.Map.Entry;

import de.dfki.x_protege.model.OntologyCache;

/**
 * Basic implementation of the {@link X_Entity} interface.
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 22.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public abstract class AbstractX_Entity implements X_Entity {

	/**
	 * The subclasses of this {@link X_Entity}.
	 */
	protected final Set<X_Entity> subclasses;

	/**
	 * The superclasses of this {@link X_Entity}.
	 */
	protected final Set<X_Entity> superClasses;

	/**
	 * The unique uri of this {@link X_Entity} using long namespaces.
	 */
	protected final List<String> uri;

	/**
	 * The unique uri of this {@link X_Entity} using Short namespaces.
	 */
	protected ArrayList<String> shortName = new ArrayList<>();

	/**
	 * This field is only used in case the {@link X_Entity} was renamed. It contains the shortname before renaming.
	 * This simplifies undoing renaming.
	 */
	protected ArrayList<String> backupShortName = new ArrayList<>();

	private String namespace;

	/**
	 * The {@link Annotation}s connected to this {@link X_Entity}.
	 */
	protected final Set<Annotation> annotations;

	/**
	 * Simple boolean flag indicating whether the annotations are already loaded.
	 */
	protected boolean annotationComputed = false;

	/**
	 * This simple flag indicates whether this {@link X_Entity} was generated during this very session of X-Protege.
	 */
	protected boolean generated;

	private String prefix;

	/**
	 * indicates whether the namespace of this {@link X_Entity} changed.
	 */
	protected boolean nsChanged;

	/**
	 * Creates a new instance of {@link AbstractX_Entity} with respect to the given uri.
	 * This constructor is used for atom {@link X_Entity}.
	 * @param uri An {@link String} representing the unique uri of the {@link X_Entity}.
     */
	public AbstractX_Entity(String uri) {
		if ((uri.startsWith("<") || uri.startsWith("\""))
				&& uri.endsWith(">")) {
			this.uri = Collections.singletonList(uri);
		} else {
			this.uri = Collections.singletonList("<" + uri + ">");
		}
		this.subclasses = new HashSet<>();
		this.superClasses = new HashSet<>();
		this.annotations = new HashSet<>();
	}

	/**
	 * Creates a new instance of {@link AbstractX_Entity} with respect to the given uri.
	 * This constructor is used for cartesian {@link X_Entity}.
	 * @param uri2 An {@link List} of {@link String} representing the unique uri of the {@link X_Entity}.
	 */
	public AbstractX_Entity(List<String> uri2) {
		ArrayList<String> temp = new ArrayList<>(uri2.size());
		for (int it = 0; it < uri2.size(); it++) {
			if ((uri2.get(it).startsWith("<") || uri2.get(it).startsWith("\""))
					&& uri2.get(it).endsWith(">")) {
				temp.add(it, uri2.get(it));
			} else {
				temp.add(it, "<" + uri2.get(it) + ">");
			}
		}
		this.uri = temp;
		this.subclasses = new HashSet<>();
		this.superClasses = new HashSet<>();
		this.annotations = new HashSet<>();
	}

	@Override
	public void setShortName(List<String> list) {
		this.shortName = (ArrayList<String>) list;
		this.prefix = shortName.get(0).split(":")[0].replace("<", "");
	}

	@Override
	public ArrayList<String> getShortName() {
		if (this.shortName.isEmpty()) {
			shortName = new ArrayList<String>(uri.size());
			for (int i = 0; i < uri.size(); i++) {
				this.shortName.add(i, OntologyCache.getNamespace()
						.getShortForLong(uri.get(i)));
			}
		}
		return shortName;
	}

	@Override
	public String getNamespace() {
		if (this.namespace == null || this.nsChanged) {
			this.nsChanged = false;
			this.namespace = OntologyCache.getNamespace()
					.getNameSpace(this.uri.get(0));
		}
		return namespace;
	}

	@Override
	public Set<Annotation> getAnnotations() {
		return this.annotations;
	}

	@Override
	public String render() {
		return render(false);
	}

	@Override
	public String render(boolean provideHTMLIcon) {

		StringBuilder strb = new StringBuilder();
		if (provideHTMLIcon)
			strb.append(getHtmlIcon());
		for (int i = 0; i < this.getShortName().size(); i++) {

			String localNs = OntologyCache.relatedOntology.getLocalNameSpace()
					+ ":";
			String shortNameString = this.getShortName().get(i);
			if (shortNameString.contains(localNs)) {
				shortNameString = shortNameString.replaceFirst(localNs, "");
			}
			strb.append(shortNameString.replace("<", "").replace(">", ""));

			if (i != this.shortName.size() - 1) {
				strb.append(" x ");
			}
		}
		return strb.toString();
	}

	/**
	 *
	 * @return An HTML string representation of this {@link X_Entity} including an html version of the entities specific icon.
     */
	protected abstract String getHtmlIcon();

	/**
	 * This method is called after loading an ontology from file to compute the annotations connected to the represented entity.
	 * @param connectedTuples An {@link HashMap} containing all tuples connected to this entity. The keys of the map are properties, whereas the values are  domain -> range + extra mappings.
     */
	protected void computeAnnotations(
			HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> connectedTuples) {
		for (String annoProp : OntologyCache.getAnnoProperties()) {
			computeAnnotation(connectedTuples, annoProp);
		}
		this.annotationComputed = true;
	}

	private void computeAnnotation(
			HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> connectedTuples,
			String searchterm) {
		if (connectedTuples.containsKey(searchterm)) {
			for (Entry<ArrayList<String>, Set<ArrayList<String>>> e : connectedTuples
					.get(searchterm).entrySet()) {
				for (ArrayList<String> e1 : e.getValue()) {
					ArrayList<String> list = new ArrayList<>();
					list.add(searchterm);
					this.annotations.add(new Annotation(this,
							new AnnotationProperty(list), e1.get(0)));
				}
			}
		}
	}

	@Override
	public void setGenerated(boolean generated) {
		this.generated = generated;
	}

	@Override
	public Set<? extends ArrayList<String>> getTupleRep() {
		return getTupleRep(false);
	}

	@Override
	public Set<? extends ArrayList<String>> getTupleRep(boolean rename) {
		Set<ArrayList<String>> result = new HashSet<ArrayList<String>>();
		result.addAll(this.getAnnotationTuples());
		result.addAll(this.getTuples(rename));
		return result;
	}

	private Collection<? extends ArrayList<String>> getAnnotationTuples() {
		Set<ArrayList<String>> comments = new HashSet<ArrayList<String>>();
		for (Annotation a : this.annotations) {
			comments.add(a.getTuple());
		}
		return comments;
	}

	@Override
	public abstract Collection<? extends ArrayList<String>> getTuples(
			boolean rename);

	@Override
	public void addAnnotation(Annotation anno) {
		this.annotations.add(anno);
	}

	@Override
	public void removeAnnotation(Annotation anno) {
		this.annotations.remove(anno);
	}

	@Override
	public void editAnnotation(Annotation anno, String value) {
		this.annotations.remove(anno);
		anno.setValue(value);
		this.annotations.add(anno);
	}

	@Override
	public String getPrefix() {
		if (this.prefix == null) {
			this.prefix = getShortName().get(0).split(":")[0].replace("<", "");
		}
		return prefix;
	}

	@Override
	public boolean isType() {
		return false;
	}

	@Override
	public boolean isAtomType() {
		return false;
	}

	@Override
	public boolean isCartesianType() {
		return false;
	}

	@Override
	public boolean isProperty() {
		return false;
	}

	@Override
	public boolean isInstance() {
		return false;
	}

	@Override
	public boolean isAtomInstance() {
		return false;
	}

	@Override
	public boolean isCartesianInstance() {
		return false;
	}

	@Override
	public boolean isCompatible(X_Entity p) {
		return false;
	}

	@Override
	public Set<X_Entity> getSubs() {
		return this.subclasses;
	}

	@Override
	public void addSub(X_Entity entity) {
		this.subclasses.add(entity);
		entity.addSuper(this);
	}

	@Override
	public void removeSub(X_Entity entity) {
		this.subclasses.remove(entity);
		entity.removeSuper(this);
	}

	@Override
	@Deprecated
	public void removeSuper(X_Entity entity) {
		this.superClasses.remove(entity);
	}

	@Override
	@Deprecated
	public void addSuper(X_Entity entity) {
		this.superClasses.add(entity);
	}

	@Override
	public String toStringPretty() {
		return this.getShortName().toString().replace("[", "").replace("]", "");
	}

	@Override
	public String getLongName() {
		return OntologyCache.getNamespace().getLongForShort(this
				.toStringPretty().replace("<", "&lt;").replace(">", "&gt;"));
	}

	@Override
	public Set<X_Entity> getAllSubs() {
		HashSet<X_Entity> subs = new HashSet<>();
		for (X_Entity e : this.subclasses) {
			subs.add(e);
			subs.addAll(e.getAllSubs());
		}
		return subs;
	}

	@Override
	public Set<X_Entity> getSuper() {
		return this.superClasses;
	}
}
