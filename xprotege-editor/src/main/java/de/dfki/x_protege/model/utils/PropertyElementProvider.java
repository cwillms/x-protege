package de.dfki.x_protege.model.utils;

import java.util.ArrayList;
import java.util.List;

import de.dfki.x_protege.model.OntologyCache;
import de.dfki.x_protege.model.data.AbstractX_Property;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.data.X_Property;

/**
 * Implementation of the {@link ElementProvider} interface specialized on the creation of {@link X_Property}s.
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 22.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class PropertyElementProvider extends ElementProvider {

	public PropertyElementProvider() {

	}

	/**
	 * Creates a new {@link X_Entity} with the given name and the given superclass.
	 * @param name the unique name/uri of the Entity to be created
     * @param i unused TODO fix this
	 * @param parent the superclass of the {@link X_Entity} to be created.
	 * @return The new {@link X_Entity}
	 */
	public X_Property create(ArrayList<String> name, int i, X_Entity parent) {
		X_Property result = null;
		if (OntologyCache.containsProperty(name)) {
			result = OntologyCache.getProperty(name);
		}
		if (parent != null) {
			int parentCategorie = ((AbstractX_Property) parent).getPropertyType().nextSetBit(0);
			switch (parentCategorie) {
			case -1: {
				result = new X_Property(name, 0);
				OntologyCache.addProperty(result);
				break;
			}
			case 2: {
				result = new X_Property(name, 1);
				OntologyCache.addProperty(result);
				break;
			}
			case 3: {
				result = new X_Property(name, 2);
				OntologyCache.addProperty(result);
				break;
			}
			default:
				break;
			}
		}
		if (name.get(0).contains("nary:MixedProperty")) {
			result = new X_Property(name, 0);
			OntologyCache.addProperty(result);
		}
		if (name.get(0).contains("owl:DatatypeProperty")) {
			result = new X_Property(name, 1);
			OntologyCache.addProperty(result);
		}
		if (name.get(0).contains("owl:ObjectProperty")) {
			result = new X_Property(name, 2);
			OntologyCache.addProperty(result);
		}
		return result;
	}

	@Override
	public X_Entity createNewEntity(List<String> list, X_Entity parent) {
		X_Property result;

		if (((X_Property) parent).getPropertyType().nextSetBit(0) == 2) {
			result = new X_Property(list, 2);

		} else {
			if (((X_Property) parent).getPropertyType().nextSetBit(0) == 1) {
				result = new X_Property(list, 1);
			} else {
				result = new X_Property(list, 0);
			}
		}
		parent.addSub(result);
		OntologyCache.addProperty(result);
		result.setGenerated(true);
		return result;
	}

}
