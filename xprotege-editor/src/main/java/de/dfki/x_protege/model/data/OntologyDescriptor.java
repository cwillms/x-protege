package de.dfki.x_protege.model.data;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Simple java representation of an .ont file including paths to tuplefile, namespace etc
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 20.03.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class OntologyDescriptor {

	private URI uri;
	private String name;
	private String version;
	private String NameSpace;
	private String TupleStore;
	private String RuleStore;
	private String IOFormat;

	/**
	 * Creates a new instance of {@link OntologyDescriptor}
	 * @param uri uri of the represented ontology
	 * @param name name of the represented ontology
	 * @param version version number of the represented ontology
	 * @param ioFormat the I/O format used to store the ontology ( prefix or infix)
	 * @param tuplefile the location of the tuplefile
	 * @param namespace2 the location of the namespace file
     * @param rules the location of the rule file
     */
	public OntologyDescriptor(URI uri, String name, String version, String ioFormat, String tuplefile,
			String namespace2, String rules) {
		this.uri = uri;
		this.name = name;
		this.version = (version != null) ? version.trim() : null;
		this.IOFormat = ioFormat;
		this.TupleStore = (tuplefile != null) ? tuplefile.trim() : null;
		this.NameSpace = (namespace2 != null) ? namespace2.trim() : null;
		this.RuleStore = (rules != null) ? rules.trim() : null;
	}

	public String getNamespacePath() {
		if (NameSpace == null) {
			return null;
		}
		if (NameSpace.endsWith("default.ns")) {
			return null;
		}
		return NameSpace;
	}

	public String getTupleStorePath() {
		if (TupleStore == null) {
			return null;
		}
		if (TupleStore.endsWith("default.nt")) {
			return null;
		}
		return TupleStore;

	}

	public String getRuleStorePath() {
		if (RuleStore == null) {
			return null;
		}
		if (RuleStore.endsWith("default.eqred.rdl")) {
			return null;
		}
		return RuleStore;

	}

	/**
	 * Generates an .ont file at the ontologies uri
	 * @return true if the process was successful, false otherwise
     */
	public boolean write() {
		StringBuilder strb = new StringBuilder();
		// add name
		strb.append("Name: ");
		strb.append(this.name);
		strb.append("\n");
		strb.append("Version: ");
		strb.append(this.version);
		strb.append("\n");
		strb.append("I/O-Format: ");
		if (this.IOFormat == null) {
			this.IOFormat = "infix";
		}
		strb.append(this.IOFormat);
		strb.append("\n");
		// add tuplestore
		strb.append("TupleFile: ");
		strb.append(this.TupleStore);
		strb.append("\n");
		// add namespace
		strb.append("Namespace: ");
		if (isDefaultNameSpace()) {
			strb.append("default.ns");
		} else {
			strb.append(this.NameSpace);
		}
		strb.append("\n");
		// add rulestore
		strb.append("Rules: ");
		if (RuleStore == null) {
			strb.append("default.eqred.rdl");
		} else {
			if (RuleStore.endsWith("default.eqred.rdl")) {
				strb.append("default.eqred.rdl");
			} else {
				strb.append(this.RuleStore);
			}
		}
		strb.append("\n");
		try {
			return Files.write(Paths.get(uri), strb.toString().getBytes()) != null;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Generates an .ont file at the given location
	 * @return true if the process was successful, false otherwise
	 */
	public boolean write(URI uri) {
		this.uri = uri;
		File f = new File(uri);
		this.TupleStore = f.getPath().replace(".ont", ".nt");
		this.NameSpace = f.getPath().replace(".ont", ".ns");
		this.RuleStore = f.getPath().replace(".ont", ".rdl");
		return write();
	}

	/**
	 *
	 * @return true if the default namespace is used , false otherwise
     */
	public boolean isDefaultNameSpace() {
		if (NameSpace == null) {
			return true;
		}
		if (NameSpace.endsWith("default.ns")) {
			return true;
		}
		return false;
	}


	public void setID(String name) {
		this.name = name;

	}

	public String getVersion() {
		return this.version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getName() {
		return this.name;
	}

	public boolean getIOFormat() {
		if (IOFormat == null) {
			return true;
		}
		if (IOFormat.endsWith("infix")) {
			return true;
		}
		return false;
	}

	public void setIOFormat(boolean usedIOFormat) {
		if (usedIOFormat) {
			this.IOFormat = "infix";
		} else {
			this.IOFormat = "prefix";
		}

	}
}
