package de.dfki.x_protege.model.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.*;

import javax.swing.tree.DefaultTreeModel;

import de.dfki.lt.hfc.*;
import de.dfki.x_protege.model.OntologyCache;
import de.dfki.x_protege.model.hfc.HFCUtilities;
import de.dfki.x_protege.model.utils.Metric;
import de.dfki.x_protege.ui.components.tree.TreeNodeGenerator;
import de.dfki.x_protege.ui.components.tree.X_EntityTreeNode;
import de.dfki.x_protege.ui.logic.Event.QueryEvent;
import de.dfki.x_protege.ui.logic.Event.Type.QueryEventType;
import de.dfki.x_protege.ui.logic.X_Rule;
import de.dfki.x_protege.ui.logic.Event.Type.SelectionType;

/**
 * The {@link Ontology} class is designed as a data structure allowing access to
 * a {@link ForwardChainer} representing a specific ontology.
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 22.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class Ontology extends AbstractX_Entity {


	private String id;

	private URI uri;

	private String tupleUri;

	private String version;

	private String namespaceUri;

	private String rulesUri;

	private ForwardChainer hfc;

	private final HashMap<SelectionType, X_Entity> selectionType2Entity = new HashMap<SelectionType, X_Entity>();

	private final HashMap<DefaultTreeModel, SelectionType> model2SelectionType = new HashMap<DefaultTreeModel, SelectionType>();

	private final Set<X_Entity> standards = new HashSet<X_Entity>();

	private boolean dirty = false;

	private CartesianX_Type typeInCreation;

	private OntologyDescriptor ontoDescriptor;

	private boolean manipulated = false;

	private final boolean loaded;

	private ArrayList<AnnotationProperty> annotationProperties;

	private AnnotationPropertyFactory factory = new AnnotationPropertyFactory();

	private String[] ioFormats = {"Prefix", "Infix"};

	/**
	 * simple boolean flag indicating whether infix (true) or prefix (false) is
	 * used
	 */
	private boolean usedIOFormat;

	private File fileUri = null;

	private HFCUtilities ut;

	private ArrayList<X_Type> tempClasses = new ArrayList<>();

	private ArrayList<AbstractX_Property> tempProperties = new ArrayList<>();

	private ArrayList<X_Rule> rules = new ArrayList<>();

	private ArrayList<X_Individual> tempInstances = new ArrayList<>();

	private Metric metric;

	/**
	 * creates a new instance of {@link Ontology}
	 * 
	 * @param ontologyID String representing the unique id of the ontology @deprecated (never used)
	 * @param uri   The Uri of the ontology
	 * @param tuplefile the location of the tuplefile (.nt) connected to this ontology
	 * @param nameSpace the location of the namespace (.ns) connected to this ontology
	 * @param rules the location of the rules (.rls) connected to this ontology
	 */
	public Ontology(String ontologyID, URI uri, String tuplefile,
			String nameSpace, String rules) {
		super(uri.toString());
		this.uri = uri;
		this.version = null;
		OntologyCache.relatedOntology = this;
		this.id = uri.getPath();
		this.setTupleUri(tuplefile);
		this.namespaceUri = nameSpace;
		this.rulesUri = rules;
		this.loaded = false;
		this.usedIOFormat = true;

		System.err.println("Directory " +System.getProperty("user.dir"));
		this.setOntoDescriptor(new OntologyDescriptor(null, this.id, null, null,
				tuplefile, nameSpace, rules));
		try {
			this.hfc = new ForwardChainer(tuplefile, rules, nameSpace);
		} catch (Exception e) {
			System.err.println("Problems creating the Forwardchainer for "
					+ tuplefile + ", " + rulesUri + ", " + namespaceUri);
		}
		OntologyCache.setNamespace(new PrefixModel(hfc.tupleStore.namespace));
		OntologyCache.getNamespace().addFormToNameSpace(
				getLastBitFromUrl(this.getUri().toString()),
				this.getUri().toString() + "#");
		this.ut = new HFCUtilities();
		initTreeModels();
//		try {
//			initRules();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		populateStandarts();
		computeMetrics();
		this.annotationProperties = this.factory.getPossibleAnnotations();
	}

    /**
     * creates a new instance of {@link Ontology}
     *
     * @param ontologyID String representing the unique id of the ontology
     * @param uri   The Uri of the ontology
     * @param tuplefile the location of the tuplefile (.nt) connected to this ontology
     * @param nameSpace the location of the namespace (.ns) connected to this ontology
     * @param rules the location of the rules (.rls) connected to this ontology
     * @param od reference to the {@link OntologyDescriptor} connected to this ontology
     */
	public Ontology(String ontologyID, URI uri, String tuplefile,
			OntologyDescriptor od, String nameSpace, String rules) {
		super(od.getName());
		OntologyCache.clear();
		OntologyCache.relatedOntology = this;
		this.id = od.getName();
		this.uri = uri;
		this.version = od.getVersion();
		this.loaded = true;
		this.setOntoDescriptor(od);
		this.usedIOFormat = od.getIOFormat();
		this.ut = new HFCUtilities();
		System.err.println("Directory " +System.getProperty("user.dir"));
		try {
			if (od.getNamespacePath() != null) {
				this.namespaceUri = od.getNamespacePath();
			} else {
				this.namespaceUri = nameSpace;
			}
			if (od.getTupleStorePath() != null) {
				this.tupleUri = this.ut.parseInput(od.getTupleStorePath(),
						usedIOFormat);
			} else {

			}
			if (od.getRuleStorePath() != null) {
				this.rulesUri = od.getRuleStorePath();
			} else {
				this.rulesUri = rules;
			}
			this.hfc = new ForwardChainer(this.tupleUri, this.rulesUri,
					this.namespaceUri);
		} catch (Exception e) {
			System.err.println("Problems creating the Forwardchainer for "
					+ tuplefile + ", " + rulesUri + ", " + namespaceUri);
			System.exit(0);
		}
		OntologyCache.setNamespace(new PrefixModel(hfc.tupleStore.namespace));
		OntologyCache.getNamespace().addFormToNameSpace(
				getLastBitFromUrl(this.getUri().toString()),
				this.getUri().toString() + "#");

		initTreeModels();
		populateStandarts();
		computeAnnotations(this.ut.getConnectedTuples(this));
		computeMetrics();
		this.annotationProperties = this.factory.getPossibleAnnotations();
	}

	private void populateStandarts() {
		this.standards.addAll(OntologyCache.getStandards());
		this.standards.add(OntologyCache
				.getType(Collections.singletonList("<nary:Thing+>")));
		this.standards.add(OntologyCache
				.getType(Collections.singletonList("<owl:Thing>")));
		this.standards.add(OntologyCache
				.getType(Collections.singletonList("<nary:Nothing+>")));
		this.standards.add(OntologyCache
				.getType(Collections.singletonList("<owl:Nothing>")));
		this.standards.add(OntologyCache.getProperty(
                Collections.singletonList("<nary:MixedProperty>")));
		this.standards.add(OntologyCache.getProperty(
                Collections.singletonList("<owl:ObjectProperty>")));
		this.standards.add(OntologyCache.getProperty(
                Collections.singletonList("<owl:DatatypeProperty>")));
	}

	private void initTreeModels() {
		X_Entity[] nodes = ut.parse(hfc);
		TreeNodeGenerator.computeModel(SelectionType.PROPERTYHIERARCHY,
				nodes[2]);
		TreeNodeGenerator.computeModel(SelectionType.TEMPPROPERTYHIERARCHY,
				nodes[2]);
		TreeNodeGenerator.computeModel(SelectionType.CLASSHIERARCHY, nodes[1]);
		TreeNodeGenerator.computeModel(SelectionType.TEMPCLASSHIERARCHY,
				nodes[1]);
		TreeNodeGenerator.computeModel(SelectionType.CARTCREATIONHIERARCHY,
				nodes[1]);
		TreeNodeGenerator.computeModel(SelectionType.SUPERCLASSHIERARCHY,
				nodes[1]);
		TreeNodeGenerator.computeModel(SelectionType.ICLASSHIERARCHY, nodes[1]);
		TreeNodeGenerator.computeModel(SelectionType.TEMPICLASSHIERARCHY,
				nodes[1]);
		TreeNodeGenerator.computeModel(SelectionType.XSDONLY, nodes[1]);
		TreeNodeGenerator.computeModel(SelectionType.OBJECTONLY, nodes[1]);
	}

	@Override
	public Set<Annotation> getAnnotations() {
		return this.annotations;
	}

	/**
	 *
	 * @return the uri of this ontology
     */
	public URI getUri() {
		return uri;
	}

	/**
	 *
	 * @return the location of the tuplefile
     */
	public String getTupleUri() {
		return tupleUri;
	}

	/**
	 * Sets the location of the tuplefile to the given value
	 * @param tupleUri the location to be set
     */
	public void setTupleUri(String tupleUri) {
		this.tupleUri = tupleUri;
	}

	/**
	 *
	 * @return the unique id/name of the ontology
     */
	public String getOntologyID() {
		return this.id;
	}

	/**
	 *
	 * @return the ontology's uri including the version number
     */
	public String getVersionUri() {
		if (this.version == null) {
			this.version = "1.0.0";
		}
		return this.uri.toString() + "/" + this.version;
	}

	/**
	 * {@link SelectionType} are used to decode the several tabs used in x-protege's frontend
	 *
	 * @param t the {@link SelectionType} of interest
	 * @return The selected {@link X_Entity} with respect to the given {@link SelectionType}
     */
	public X_Entity getSelection(SelectionType t) {
		if (!selectionType2Entity.containsKey(t)
				&& t != SelectionType.INDIVIDUALHIERARCHY) {
			updateSelections(t,
					((X_EntityTreeNode) TreeNodeGenerator.getModel(t).getRoot())
							.getValue());
		}
		return this.selectionType2Entity.get(t);

	}

	/**
	 * Set the ontology's uri to the given value
	 * @param uri the uri to be used
     */
	public void setID(ArrayList<String> uri) {
		StringBuilder builder = new StringBuilder();
		for (String s : uri) {
			builder.append(s);
		}
		this.id = builder.toString();
		this.ontoDescriptor.setID(builder.toString());
		try {
			this.uri = new URI(builder.toString());
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		dirty = true;
	}

	private String createDefaultUri(X_Entity value) {
		StringBuilder strb = new StringBuilder();
		if (value.getShortName().get(0).contains("nary:Thing+")) {
			strb.append(value.getPrefix());
		}
		if (value.getShortName().get(0).contains("xsd:AnyType")) {
			strb.append(value.getPrefix());
		}
		if (value.getShortName().get(0).contains("owl:Thing")) {
			strb.append(value.getPrefix());
		} else {
			strb.append(value.getPrefix());
		}
		strb.append(":default");
		strb.append(OntologyCache.getDefaultCounter());
		return strb.toString();
	}

    /**
     *
     * @param selectedClass the class to be checked
     * @return true, if the given class is a standard/default class, (meaning this class can not be changed as it is an essential part of every ontology's framework) <br>
     *     false otherwise
     */
	public boolean isStandard(X_Entity selectedClass) {
		return this.standards.contains(selectedClass);
	}

	@Override
	public void populateConnections(
			HashMap<String, HashMap<ArrayList<String>, Set<ArrayList<String>>>> connectedTuples) {
		// nothing to do here, yet
	}

    /**
     *
     * @return true if there are unsaved changes, false otherwise
     */
	public boolean isDirty() {
		return dirty;
	}

    /**
     *
     * @return the hfc {@link ForwardChainer} connected with this ontology
     */
	public ForwardChainer getHfc() {
		return hfc;
	}

    /**
     * @deprecated need to be changed, no good practice
     * @param newType foo
     */
    @Deprecated
	public void setTypeInCreation(CartesianX_Type newType) {
		this.typeInCreation = newType;

	}

    /**
     * @deprecated need to be changed, no good practice
     */
    @Deprecated
	public CartesianX_Type getTypeInCreation() {
		return this.typeInCreation;
	}


	public void setDirty(boolean b) {
		this.dirty = b;
	}

	/**
	 * @return the ontoDescriptor
	 */
	public OntologyDescriptor getOntoDescriptor() {
		return ontoDescriptor;
	}

	/**
	 * @param ontoDescriptor
	 *            the ontoDescriptor to set
	 */
	public void setOntoDescriptor(OntologyDescriptor ontoDescriptor) {
		this.ontoDescriptor = ontoDescriptor;
	}

    /**
     *
     * @return true if any value of the ontology was changed, false otherwise
     */
	public boolean isManipulated() {
		return this.manipulated;
	}

    /**
     *
     * @return true if the ontology has been loaded, false if the ontology was newly created in this very session of x-protege
     */
	public boolean isLoaded() {
		return this.loaded;
	}

    /**
     * Writes  namespace, tuplefile and ontodescriptor to files at the given location
     * @param uri the location the data has to be stored
     * @return true if the process was successful, false otherwise
     */
	public boolean store(URI uri) {
		boolean step1, step2, step3;
		if (uri == null) {
			// 1) write ont file
			this.ontoDescriptor.setIOFormat(this.usedIOFormat);
			this.ontoDescriptor.setID(getUri().toString());
			this.ontoDescriptor.setVersion(this.version);
			step1 = this.ontoDescriptor.write();
			// 2) write tuplestore
			step2 = this.writeTupleStore(tupleUri);
			// 3) write namespace
			step3 = this.writeNameSpace(namespaceUri);
			// // 4) write rulestore
			// step4 = this.writeRuleStore(rulesUri);
		} else {
			File f;
			if (uri.toString().endsWith(".ont"))
				f = new File(uri);
			else
				f = new File(uri.toString() + ".ont");
			this.fileUri = f;
			// 1) write ont file
			this.ontoDescriptor.setIOFormat(this.usedIOFormat);
			this.ontoDescriptor.setID(getUri().toString());
			this.ontoDescriptor.setVersion(this.version);
			step1 = this.ontoDescriptor.write(uri);
			// 2) write tuplestore
			step2 = this.writeTupleStore(f.getPath().replace(".ont", ".nt"));
			// 3) write namespace
			step3 = this.writeNameSpace(f.getPath().replace(".ont", ".ns"));
			// // 4) write rulestore
			// step4 = this.writeRuleStore(uri.toString().replace(".ont",
			// ".rdl"));
		}
		// OntologyCache.ioFormat = this.usedIOFormat;
		if (!this.loaded)
			this.manipulated = true;
		return (step1 && step2 && step3); // && step4);
	}

    /**
     * adds the given tuple to the hfc {@link TupleStore}
     * @param tuple the tuple to be added
     */
	public void addNewTupleToTupleStore(ArrayList<String> tuple) {
		String[] t = tuple.toArray(new String[tuple.size()]);
		StringBuilder strb = new StringBuilder();
		for (String s : t) {
			strb.append(s);
		}
		if (!this.hfc.tupleStore.ask(tuple))
			this.hfc.tupleStore.addTuple(t);
	}

    /**
     * removes the given tuple from the hfc {@link TupleStore}
     * @param tuple the tuple to be removed
     * @return true if the process was successful, false otherwise
     */
	public boolean removeTupleFromTupleStore(ArrayList<String> tuple) {
		int[] internalizedTuple = hfc.tupleStore.internalizeTuple(tuple);
		return hfc.tupleStore.removeTuple(internalizedTuple);
	}


	private boolean writeTupleStore(String tupleUri2) {
		hfc.tupleStore.writeExpandedTuples(tupleUri2);
		if (!usedIOFormat) {
			try {
				new HFCUtilities().restructure(tupleUri2);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}

	private boolean writeNameSpace(String namespaceUri2) {
		if (!this.ontoDescriptor.isDefaultNameSpace()) {
			return OntologyCache.getNamespace().write(namespaceUri2);
		}
		return true;
	}

    /**
     * Returns the last significant part of an given uri. In general this method is used to extract the short local namespace for the ontology.
     * @param url the uri to be parsed
     * @return the last part of the given url
     */
	public String getLastBitFromUrl(String url) {
		return url.replaceFirst(".*/([^/?]+).*", "$1");
	}

    /**
     *
     * @param ontologyIRIString the name to be checked
     * @return true if the name is valid (correct formatting, used namespace exists and it is unique), false otherwise
     */
	public boolean isValidName(String ontologyIRIString) {
		return isValidName(ontologyIRIString, true);
	}

    /**
     *
     * @param ontologyIRIString the name to be checked
     * @return true if the name is valid (correct formatting, used namespace exists and it is unique), false otherwise
     */
	public boolean isValidIndiName(String ontologyIRIString) {
		return isValidName(ontologyIRIString, true);
	}

	private boolean isValidName(String ontologyIRIString, boolean output) {
		if (ontologyIRIString.contains(":")) {
			String[] splitted = ontologyIRIString.split(":");
			String namespace = splitted[0];
			if (namespace.startsWith("<")) {
				namespace = namespace.substring(1);
			}
			return OntologyCache.getNamespace().contains(namespace)
					&& OntologyCache.isUniqueName(ontologyIRIString);
		} else {
			if (output) {
				return output;
			}
			return OntologyCache.isUniqueName(ontologyIRIString);
		}
	}

    /**
     *
     * @return a tuple representation of this entity. A tuple representation
     * contains all attributes of this entity (e.g. subclasses, connected
     * properties, etc)
     */
	public Collection<? extends ArrayList<String>> getTuples() {
		// nothing to do here at the moment
		return new HashSet<ArrayList<String>>();
	}

    /**
     *
     * @return the {@link AnnotationProperty}s which may be used in the context of this ontology.
     */
	public ArrayList<AnnotationProperty> getAnnotationProperties() {
		return this.annotationProperties;
	}

    /**
     *
     * @return the supported I/O formats (normally prefix and infix)
     */
	public String[] getIOFormats() {
		return this.ioFormats;
	}

    /**
     * Changed the version number to the given value
     * @param version the new version number
     */
	public void setVersion(String version) {
		this.version = version;
	}

    /**
     * changes the used I/O format according to the input.
     * @param ioFormat the I/O format to be set ( true -> infix. false -> prefix)
     */
	public void setIOFormat(boolean ioFormat) {
		if (this.usedIOFormat != ioFormat) {

			this.usedIOFormat = ioFormat;
			this.dirty = true;
			hfc.tupleStore.cleanUpTupleStore();
		}
	}

	public boolean getUsedIO() {
		return this.usedIOFormat;
	}

	/**
	 * @return the fileUri
	 */
	public URI getFileUri() {
		if (fileUri != null)
			return fileUri.toURI();
		return null;
	}

	@Override
	protected String getHtmlIcon() {
		return null;
	}

    /**
     *
     * @return the classes which are already created but not yet integrated into hfc, as they may be removed if a dialogue is cancelled
     */
	public ArrayList<X_Type> getTempClasses() {
		return this.tempClasses;
	}

    /**
     * The local namespace is used whenever a entity was created without specifying a distinct namespace.
     * @return the local namespace, i.e. the namespace derived from the ontologie's uri.
     */
	public String getLocalNameSpace() {
		return getLastBitFromUrl(this.getUri().toString());
	}

    /**
     *
     * @return the properties which are already created but not yet integrated into hfc, as they may be removed if a dialogue is cancelled
     */
	public ArrayList<AbstractX_Property> getTempProperty() {
		return this.tempProperties;
	}

	/**
	 * Updates the mapping from {@link SelectionType} to {@link X_Entity} representing which entity is selected in the several tabs of the frontend
	 * 
	 * @param type     the {@link SelectionType} to be updated
	 * @param value the selected {@link X_Entity}
	 */
	public void updateSelections(SelectionType type, X_Entity value) {
		// this may happen after drag and drop actions
		if (value == null) {
			this.selectionType2Entity.remove(type);
			return;
		}
		this.selectionType2Entity.put(type, value);
	}

    /**
     * creates a new default uri representing a subclass of the given entity
     * @param value the parent entity
     * @return new default uri, e.g. bio:default2
     */
	public String getNewUri(X_Entity value) {
		return createDefaultUri(value);
	}

	public void setNamespaceChanged(boolean b) {
		this.nsChanged = b;
	}

	/**
	 * TODO move into new Class or X_Modelmanager
	 * 
	 * @param model
	 * @return
	 */
	public SelectionType getTypeForModel(DefaultTreeModel model) {
		return this.model2SelectionType.get(model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.de.dfki.x_protege.model.data.X_Entity#getDescription()
	 */
	@Override
	public String getDescription() {
		return "";
	}

	/**
	 * TODO move to rule-Manager
	 * 
	 * @return the set of loaded/active rules
	 */
    @Deprecated
	public ArrayList<X_Rule> getRules() {
		return this.rules;
	}

	/**
	 * TODO move to rule-Manager
	 * 
	 * @throws IOException
	 */
    @Deprecated
	private void initRules() throws IOException {
		ArrayList<Rule> hfcRules = this.hfc.ruleStore.allRules;
		HashMap<String, String> allRuleNames = new HashMap<>();
		boolean isNew = true;
		BufferedReader br = Files
				.newBufferedReader(new File(this.rulesUri).toPath());
		String line, name = "";
		StringBuilder strb = new StringBuilder();
		while ((line = br.readLine()) != null) {
			line = line.trim();
			if (line.startsWith("#") || line.isEmpty())
				continue;
			// new rule
			if (line.startsWith("$")) {
				if (allRuleNames.containsKey(line)) {
					isNew = false;
				} else {
					allRuleNames.put(name, strb.toString());
					strb = new StringBuilder();
				}
				name = line;
			} else {
				if (isNew) {
					strb.append(line);
					strb.append("\n");
				}
			}
		}
		allRuleNames.put(name, strb.toString());
		for (Rule r : hfcRules) {
			this.rules.add(new X_Rule(r, allRuleNames.get(r.getName())));
		}
	}

	@Override
	public Collection<? extends ArrayList<String>> getTuples(boolean rename) {
		return null;
	}

    /**
     *
     * @return the instances which are already created but not yet integrated into hfc, as they may be removed if a dialogue is cancelled
     */
	public ArrayList<X_Individual> getTempInstances() {
		return this.tempInstances;
	}

    /**
     *
     * @return the {@link Metric} for the current state of the ontology
     */
	public Metric getMetric() {
		if(dirty)
			computeMetrics();
		return this.metric;
	}

	private void computeMetrics() {
		String[] names = {"Entity Count","Class count", "Atomic Types","Cartesian Types", "Property Count" , "Mixed Property Count", "DataTypeProperty Count", "ObjectProperty Count","Instance Count", "Atomic Instances", "Cartesian Instances"};
		int cc = 0;
		int atc = 0;
		int ctc = 0;
		for(X_Type t: OntologyCache.loadedTypes.values()){
			cc++;
			if (t.isAtomType())
				atc++;
			else
				ctc++;
		}
		int mpc = 0;
		int dpc = 0;
		int opc = 0;
		int pc = 0;
		for (X_Property p : OntologyCache.getLoadedProperties()){
			/**
			 * 000 default 100 mixed 010 data 001 object
			 */
			int t = p.getPropertyType().nextSetBit(0);
			switch (t){
				case 0: mpc++;pc++; break;
				case 1: dpc++;pc++; break;
				case 2: opc++;pc++; break;
				default:
					pc++;
			}
		}
		int inst = 0;
		int ain =0;
		int cin = 0;
		for (X_Individual i :OntologyCache.loadedIndividuals.values()){
			inst++;
			if (i.isCartesianInstance()){
				cin++;
			} else {
				ain++;
			}
		}

		int[] values = {(cc+pc+inst),cc, atc,ctc, pc,mpc, dpc,opc, inst, ain,cin };
		if(this.metric == null)
			this.metric = new Metric(names, values);
		else {
			this.metric.update(values);
		}
	}


}
