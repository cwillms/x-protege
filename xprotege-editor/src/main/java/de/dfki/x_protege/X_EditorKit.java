package de.dfki.x_protege;

import java.io.File;
import java.net.URI;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;

import org.osgi.framework.ServiceRegistration;
import org.protege.editor.core.BookMarkedURIManager;
import org.protege.editor.core.ModelManager;
import org.protege.editor.core.editorkit.AbstractEditorKit;
import org.protege.editor.core.editorkit.EditorKit;
import org.protege.editor.core.editorkit.EditorKitDescriptor;
import org.protege.editor.core.editorkit.RecentEditorKitManager;
import org.protege.editor.core.ui.workspace.Workspace;

import de.dfki.x_protege.model.X_ModelManager;
import de.dfki.x_protege.model.X_ModelManagerImpl;
import de.dfki.x_protege.ui.X_Workspace;
import de.dfki.x_protege.ui.utilities.UIHelper;

/**
 * This is the x-protege specific implementation of the protege editorKit. It is the upper most layer of the x-protege plugin, connecting it to the main protege framework.<br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 17.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */

public class X_EditorKit extends AbstractEditorKit<X_EditorKitFactory> {

	public static final String ID = "X_EditorKit";

	public static final String URI_KEY = "URI";

	public static final String FILE_URI_SCHEME = "file";

	private X_Workspace workspace;

	private X_ModelManager modelManager;

	@SuppressWarnings("unused")
	private Set<URI> newPhysicalURIs;

	@SuppressWarnings({"rawtypes", "unused"})
	private ServiceRegistration registration;

	private boolean modifiedDocument = false;

	public X_EditorKit(X_EditorKitFactory editorKitFactory) {
		super(editorKitFactory);

	}

	@Override
	protected void initialise() {
		this.newPhysicalURIs = new HashSet<URI>();
		modelManager = new X_ModelManagerImpl();
		registration = X_Protege.getBundleContext().registerService(
				EditorKit.class.getCanonicalName(), this,
				new Hashtable<String, Object>());
		getX_Workspace().refreshComponents();
	}

	@Override
	public String getId() {
		return ID;
	}

	@Override
	public Workspace getWorkspace() {
		if (workspace == null) {
			workspace = new X_Workspace();
			workspace.setup(this);
			workspace.initialise();
		}
		return this.workspace;
	}

	@Override
	public ModelManager getModelManager() {
		return modelManager;
	}

	@Override
	public boolean handleNewRequest() throws Exception {
		// initialise();
		return true;
	}

	@Override
	public boolean handleLoadRequest() throws Exception {
		File f = new UIHelper(this).chooseOntFile("Select an Ontology file");
		return f != null && handleLoadFrom(f.toURI());
	}

	@Override
	public boolean handleLoadFrom(URI uri) throws Exception {
		boolean success = ((X_ModelManagerImpl) getModelManager())
				.loadOntologyFromPhysicalURI(uri);

		if (success) {
			addRecent(uri);
		}
		modifiedDocument = true;
		workspace.registerListener();
		return success;
	}

	private void addRecent(URI uri) {
		String label = uri.toString();
		if (FILE_URI_SCHEME.equals(uri.getScheme())) {
			label = new File(uri).getPath();
		} else {
			// also add to the URI bookmarks
			BookMarkedURIManager.getInstance().add(uri);
		}
		EditorKitDescriptor descriptor = new EditorKitDescriptor(label,
				getEditorKitFactory());
		descriptor.setURI(URI_KEY, uri);
		RecentEditorKitManager.getInstance().add(descriptor);
	}

	@Override
	public boolean handleLoadRecentRequest(EditorKitDescriptor descriptor)
			throws Exception {
		boolean success = ((X_ModelManagerImpl) getModelManager())
				.loadOntologyFromPhysicalURI(
						new URI("file:" + descriptor.toString()));

		modifiedDocument = true;
		workspace.registerListener();
		return success;
	}

	@Override
	public void handleSave() throws Exception {
		if (getX_ModelManager().isDefaultOntololgy()) {
			handleSaveAs();
		} else {
			getX_ModelManager().save();
		}
	}

	@Override
	public void handleSaveAs() throws Exception {
		File f = new UIHelper(this).saveOntFile("Select an Ontology file");
		if (f != null) {
			if (!f.getPath().endsWith(".ont")) {
				String path = f.getPath();
				f = new File(path + ".ont");
			}
			getX_ModelManager().saveAs(f.toURI());
		}
	}

	@Override
	public boolean hasModifiedDocument() {
		return modifiedDocument;
	}

	public X_ModelManager getX_ModelManager() {
		return this.modelManager;
	}

	public X_Workspace getX_Workspace() {
		return (X_Workspace) getWorkspace();
	}

}
