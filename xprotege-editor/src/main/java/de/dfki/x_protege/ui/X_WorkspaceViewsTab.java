package de.dfki.x_protege.ui;

import javax.swing.JComponent;

import org.protege.editor.core.ui.workspace.WorkspaceViewsTab;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.model.X_ModelManager;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.ui.components.ElementDisplayProvider;

/**
 * Represents a workspace tab that hosts a views panel.<br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 17.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class X_WorkspaceViewsTab extends WorkspaceViewsTab {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7256357035112058780L;
	@SuppressWarnings("unused")
	private ElementDisplayProvider provider = new ElementDisplayProvider() {
		@Override
		public boolean canDisplay(X_Entity element) {
			return false;
		}

		@Override
		public JComponent getDisplayComponent() {
			return X_WorkspaceViewsTab.this;
		}
	};

	@Override
	public void initialise() {
		super.initialise();
	}

	@Override
	public void dispose() {
		super.dispose();
	}

	/**
	 *
	 * @return The {@link X_ModelManager} this {@link X_Workspace} belongs to.
	 */
	public X_ModelManager getX_ModelManager() {
		return (X_ModelManager) getWorkspace().getEditorKit().getModelManager();
	}

	/**
	 *
	 * @return The {@link X_EditorKit} this {@link X_Workspace} belongs to.
	 */
	public X_EditorKit getX_EditorKit() {
		return (X_EditorKit) getWorkspace().getEditorKit();
	}

}
