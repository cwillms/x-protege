package de.dfki.x_protege.ui.components.lists;

import org.protege.editor.core.ui.list.MListItem;

import de.dfki.x_protege.model.data.AtomicX_Type;
import de.dfki.x_protege.model.data.X_Type;

/**
 * The {@link SelectionListItem} represents an instance of {@link X_Type}. It is
 * used by {@link SelectionList}s.<br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 17.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class SelectionListItem implements MListItem {

	private X_Type value;
	private SelectionList list;
	private int index;
	private boolean editModeActive = false;

	public SelectionListItem(AtomicX_Type clazz, SelectionList selectionList, int i) {
		this.value = clazz;
		this.list = selectionList;
		this.index = i;
	}

	@Override
	public boolean isEditable() {
		return true;
	}

	@Override
	public void handleEdit() {
		this.editModeActive = !this.editModeActive;
		this.list.handleItemEdit(this);

	}

	@Override
	public boolean isDeleteable() {
		return true;
	}

	@Override
	public boolean handleDelete() {
		return this.list.handleItemDelete(this);
	}

	@Override
	public String getTooltip() {
		// TODO Auto-generated method stub
		return null;
	}

	public X_Type getValue() {
		// TODO Auto-generated method stub
		return this.value;
	}

	public int getIndex() {
		return index;
	}

	/**
	 * @return the editModeActive
	 */
	public boolean isEditModeActive() {
		return editModeActive;
	}

	/**
	 * @param editModeActive
	 *            the editModeActive to set
	 */
	public void setEditModeActive(boolean editModeActive) {
		this.editModeActive = editModeActive;
	}

}
