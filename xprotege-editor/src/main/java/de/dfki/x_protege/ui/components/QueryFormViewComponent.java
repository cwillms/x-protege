/**
 * 
 */
package de.dfki.x_protege.ui.components;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.util.HashSet;
import java.util.Set;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import org.protege.editor.core.PropertyUtil;
import org.protege.editor.core.ProtegeProperties;

import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.QueryEvent;
import de.dfki.x_protege.ui.logic.Event.SelectionChange;
import de.dfki.x_protege.ui.logic.Event.Type.QueryEventType;
import de.dfki.x_protege.ui.logic.listener.ModelChangeListener;
import de.dfki.x_protege.ui.logic.listener.QueryEventListener;
import de.dfki.x_protege.ui.utilities.UIHelper;
import de.dfki.x_protege.ui.utilities.X_Icons;

/**
 * This {@link QueryFormViewComponent} is used to represent the Form used in the
 * QueryTab to formulate an SQL-like query.<br>
 * <query> ::= <select> <where> [<filter>] [<aggregate>] | ASK
 * <groundtuple> <select> ::= {"SELECT" | "SELECTALL"} ["DISTINCT"] {"*" |
 * <var>^+} <var> ::= "?"{a-zA-Z0-9}^+ | "?_" <nwchar> ::= any NON-whitespace
 * character <where> ::= "WHERE" <tuple> {"&" <tuple>}^* <tuple> ::= <literal>^+
 * <gtuple> ::= <constant>^+ <literal> ::= <var> | <constant> <constant> ::=
 * <uri> | <atom> <uri> ::= "<" <nwchar>^+ ">" <atom> ::= "\"" <char>^* "\"" [
 * "@" <langtag> | "^^" <xsdtype> ] <char> ::= any character, incl. whitespaces,
 * numbers, even '\"' <langtag> ::= "de" | "en" | ... <xsdtype> ::= "<xsd:int>"
 * | "<xsd:long>" | "<xsd:float>" | "<xsd:double>" | "<xsd:dateTime>" |
 * "<xsd:string>" | "<xsd:boolean>" | "<xsd:date>" | "<xsd:gYear>" |
 * "<xsd:gMonthDay>" | "<xsd:gDay>" | "<xsd:gMonth>" | "<xsd:gYearMonth>" |
 * "<xsd:duration>" | "<xsd:anyURI>" | ... <filter> ::= "FILTER" <constr> {"&"
 * <constr>}^* <constr> ::= <ineq> | <predcall> <ineq> ::= <var> "!="
 * <literal> <predcall> ::= <predicate> <literal>^* <predicate> ::= <nwchar>^+
 * <aggregate> ::= "AGGREGATE" <funcall> {"&" <funcall>}^* <funcall> ::= <var>^+
 * "=" <function> <literal>^* <function> ::= <nwchar>^+ <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 22.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class QueryFormViewComponent extends AbstractX_QueryViewComponent {

	private static final long serialVersionUID = -3966885117291952246L;

	protected Color background = PropertyUtil.getColor(ProtegeProperties
			.getInstance().getProperty(ProtegeProperties.ONTOLOGY_COLOR_KEY),
			Color.GRAY);

	private final String[] FORMFIELDS = {"SELECT", "WHERE", "FILTER",
			"AGGREGATE"};

	private final QueryFormInputBox[] FORMBOXES = new QueryFormInputBox[4];

	/**
	 * This {@link Set} contains the {@link ModelChangeListener}s of the
	 * {@link QueryFormInputBox}es contained in this component.
	 */
	private Set<ModelChangeListener> modelListeners = new HashSet<>();

	/**
	 * This {@link Set} contains the {@link QueryEventListener}s of the
	 * {@link QueryFormInputBox}es contained in this component.
	 */
	private Set<QueryEventListener> queryListeners = new HashSet<>();

	private Action searchAction = new AbstractAction("Search", X_Icons.search) {

		/**
		 * 
		 */
		private static final long serialVersionUID = -5692656643912820314L;

		@Override
		public void actionPerformed(ActionEvent e) {
			String value = getQuery();
			if (value.isEmpty()) {
				String message = "Your Query is empty!";
				new UIHelper(getX_EditorKit()).showError("NO Valid Query",
						message);
			}
			getX_ModelManager().handleQueryEvent(
					new QueryEvent(value, QueryEventType.QUERY, null));
		}

	};

	private Action synCheckAction = new AbstractAction("Syntax Check") {

		/**
		 * 
		 */
		private static final long serialVersionUID = -6291816706696977220L;

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			System.out.println("Not yet implemented");
		}
	};

	@Override
	protected void handleQueryEvent(final QueryEvent queryEvent) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {
				if (queryEvent.getType() == QueryEventType.SUGGESTION) {
					updateView(queryEvent.getValue());
				}
				if (queryEvent.getType() == QueryEventType.NOTVALID) {
					new UIHelper(getX_EditorKit()).showError("NO Valid Query",
							(String) queryEvent.getValue());
				}
			}
		};
		SwingUtilities.invokeLater(doHighlight);
	}

	private void updateView(Object value) {
		// TODO
	}

	@Override
	protected void handleModelChanged(ModelChange modelChanged) {
		// only used to forward rename events to the QueryFormIntputBoxes
		// nothing more to do here.
		// TODO Auto-generated method stub
	}

	@Override
	protected void handleSelectionChanges(SelectionChange selectionchanges) {
		// Absolutely nothing to do

	}

	@Override
	public void initialiseView() throws Exception {
		this.setLayout(new BorderLayout());
		// Arrange components --------------------------------------------------
		this.setLayout(new BorderLayout());
		// add Header containing checkbox and searchbutton
		this.add(createHeader(), BorderLayout.NORTH);
		this.add(new JSeparator());
		// add inpuptboxes
		JPanel formContainer = new JPanel();
		formContainer.setLayout(new BoxLayout(formContainer, BoxLayout.Y_AXIS));
		formContainer.setBorder(new EmptyBorder(0, 0, 0, 0));
		for (int i = 0; i < FORMFIELDS.length; i++) {
			String type = FORMFIELDS[i];
			QueryFormInputBox inBox = createInputBox(this, type);
			FORMBOXES[i] = inBox;
			formContainer.add(inBox);
		}
		this.add(new JScrollPane(formContainer));
		this.add(createFooter(), BorderLayout.SOUTH);
		// Init Listener -------------------------------------------------------
		// we do not need the Selection change listener in this component, so we
		// have to remove it from the modelmanager
		getX_ModelManager().removeSelectionChangeListener(selChangeListener);
		getX_ModelManager().addQueryEvenetListener(qListener);
	}

	private QueryFormInputBox createInputBox(QueryFormViewComponent t,
			String type) {
		if (type.equals("SELECT"))
			return new SelectionQueryInputBox(t, type);
		else if (type.equals("WHERE"))
			return new WhereQueryFormInputBox(t, type);
		else if (type.equals("FILTER"))
			return new FilterQueryFormInputBox(t, type);
		else
			return new AggregateQueryFormInputBox(t, type);

	}

	private JPanel createHeader() {
		JPanel header = new JPanel();
		header.setLayout(new BorderLayout());
		JButton searchButton = new JButton(this.searchAction);
		searchButton.setBackground(background);
		searchButton.setForeground(Color.WHITE);
		searchButton.setBorder(BorderFactory.createRaisedBevelBorder());
		header.add(searchButton, BorderLayout.NORTH);
		header.add(new JLabel(" "), BorderLayout.SOUTH);

		return header;
	}

	private JPanel createFooter() {
		JPanel footer = new JPanel();
		JCheckBox syntaxCheck = new JCheckBox(this.synCheckAction);
		footer.add(syntaxCheck, BorderLayout.EAST);
		return footer;
	}

	@Override
	public void disposeView() {
		getX_ModelManager().removeQueryEvenetListener(qListener);
		getX_ModelManager().removeModelChangeListener(modelChangeListener);
	}

	@Override
	@Deprecated
	protected X_Entity updateView() {
		// This method should never be called
		return null;
	}

	public void addModelChangeListener(ModelChangeListener mListener) {
		this.modelListeners.add(mListener);
	}

	public void addQueryEventListener(QueryEventListener qListener) {
		this.queryListeners.add(qListener);
	}

	public void removeModelChangeListener(ModelChangeListener mListener) {
		this.modelListeners.remove(mListener);
	}

	public void removeQueryEventListener(QueryEventListener qListener) {
		this.queryListeners.remove(qListener);
	}

	private String getQuery() {
		StringBuilder strb = new StringBuilder();
		for (QueryFormInputBox ib : this.FORMBOXES) {
			strb.append(ib.getValue());
			strb.append(" ");
		}
		return strb.toString();
	}

}
