/**
 * 
 */
package de.dfki.x_protege.ui.components;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.text.BadLocationException;

import org.protege.editor.core.PropertyUtil;
import org.protege.editor.core.ProtegeProperties;
import org.protege.editor.core.ui.util.AugmentedJTextField;

import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.QueryEvent;
import de.dfki.x_protege.ui.logic.Event.Type.ModelChangeType;
import de.dfki.x_protege.ui.logic.Event.Type.QueryEventType;
import de.dfki.x_protege.ui.logic.listener.ModelChangeListener;
import de.dfki.x_protege.ui.logic.listener.QueryEventListener;
import de.dfki.x_protege.ui.utilities.SuggestionResearcher;

/**
 * The {@link QueryFormInputBox} is a central part of the
 * {@link QueryFormViewComponent}. It is connected to an part of an SQL-Query
 * (e.g. WHERE) and allows the specification of this part using a
 * {@link JTextField}.<br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 22.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public abstract class QueryFormInputBox extends JPanel {

	protected Color background = PropertyUtil.getColor(ProtegeProperties
			.getInstance().getProperty(ProtegeProperties.ONTOLOGY_COLOR_KEY),
			Color.GRAY);

	private static final long serialVersionUID = 1L;

	protected final String header;

	protected final QueryFormViewComponent queryForm;

	protected final AugmentedJTextField inputField;

	private String searchTerm;

	private final ActionListener alistener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			String suggestion = ((JMenuItem) e.getSource()).getText();
			try {
				inputField.getDocument().remove(
						inputField.getCaretPosition() - searchTerm.length(),
						searchTerm.length());
				inputField.getDocument().insertString(
						inputField.getCaretPosition(), suggestion, null);
			} catch (BadLocationException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}
	};

	private final Action computeSuggestionAction = new AbstractAction() {

		/**
		 * 
		 */
		private static final long serialVersionUID = -6709178767617549936L;

		@Override
		public void actionPerformed(ActionEvent e) {
			int caretPosition = inputField.getCaretPosition();
			searchTerm = computeLastWord(caretPosition, inputField.getText());
			new SuggestionResearcher(inputField, searchTerm, alistener)
					.execute();
		}

		private String computeLastWord(int caretPos, String text) {
			String preText = text.substring(0, caretPos);
			if (preText.indexOf(" ") > 0) {
				String[] words = preText.split(" ");
				return words[words.length - 1]; // return last word
			} else {
				return preText;
			}
		}

	};

	private final ModelChangeListener mListener = new ModelChangeListener() {

		@Override
		public void modelChanged(ModelChange modelChanged) {
			handleModelChanged(modelChanged);
		}
	};

	private final QueryEventListener qListener = new QueryEventListener() {

		@Override
		public void queryEvent(QueryEvent query) {
			handleQueryEvent(query);
		}
	};

	/**
	 * Creates a new instance of {@link QueryFormInputBox}
	 * @param form the form ({@link QueryFormViewComponent} this input box belongs to
	 * @param header the header of this box
     */
	public QueryFormInputBox(QueryFormViewComponent form, String header) {
		this.setLayout(new BorderLayout());
		this.setBorder(new LineBorder(background));
		this.queryForm = form;
		this.header = header;
		// populate textfield --------------------------------------------------
		this.inputField = new AugmentedJTextField(getGhostText());

		// this.inputField.getDocument().addDocumentListener(docListener);
		this.inputField.setOpaque(true);
		this.inputField.setBorder(new EmptyBorder(0, 0, 0, 0));
		this.inputField
				.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT)
				.put(KeyStroke.getKeyStroke("ctrl pressed SPACE"), "sug");
		this.inputField.getActionMap().put("sug", computeSuggestionAction);

		// Init specific Key Listeners -----------------------------------------
		// Arrange components --------------------------------------------------
		JLabel headerLabel = new JLabel(header);
		headerLabel.setBackground(background);
		headerLabel.setOpaque(true);
		headerLabel.setBorder(new EmptyBorder(0, 0, 0, 0));
		this.add(initHeader(), BorderLayout.NORTH);
		JScrollPane scroll = new JScrollPane(inputField);
		scroll.setBorder(new EmptyBorder(0, 0, 0, 0));
		this.add(scroll, BorderLayout.CENTER);
		// Init Listener -------------------------------------------------------
		// we do not need the Selection change listener in this component, so we
		// have to remove it from the modelmanager
		this.queryForm.addModelChangeListener(mListener);
		this.queryForm.addQueryEventListener(qListener);

	}

	protected abstract JComponent initHeader();

	/**
	 * This method is used to handle {@link QueryEvent}s forwarded by the
	 * {@link QueryFormViewComponent}.
	 * 
	 * @param queryEvent
	 *            The {@link QueryEvent} to be handled.
	 */
	private void handleQueryEvent(final QueryEvent queryEvent) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {
				if (queryEvent.getType() == QueryEventType.SUGGESTION) {
					// TODO
				}
			}
		};
		SwingUtilities.invokeLater(doHighlight);

	}

	// only used to handle renames, nothing more to do here.
	private void handleModelChanged(final ModelChange modelChanged) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {
				if (modelChanged.getType() == ModelChangeType.RENAME) {
					// TODO
				}
			}
		};
		SwingUtilities.invokeLater(doHighlight);
	}

	public void disposeView() {
		this.queryForm.removeModelChangeListener(mListener);
		this.queryForm.removeQueryEventListener(qListener);
	}

	/**
	 * 
	 * @return the value of this FormInputBox.
	 */
	public abstract String getValue();

	/**
	 * 
	 * @return the header of this FormInputBox.
	 */
	public String getHeader() {
		return header;
	}

	@Override
	public void setEnabled(boolean b) {
		this.inputField.setEnabled(b);
		if (!b) {
			this.inputField.setBackground(Color.lightGray);
		} else {
			this.inputField.setBackground(Color.WHITE);
		}
	}

	protected abstract String getGhostText();
}
