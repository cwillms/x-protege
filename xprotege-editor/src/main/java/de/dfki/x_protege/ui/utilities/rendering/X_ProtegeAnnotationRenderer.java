package de.dfki.x_protege.ui.utilities.rendering;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import de.dfki.x_protege.ui.components.lists.AnnotationsListItem;
import de.dfki.x_protege.ui.utilities.X_Icons;

/**
 * This distinct renderer is specifically designed to visualize the {@link de.dfki.x_protege.model.data.AnnotationProperty}.
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 13.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class X_ProtegeAnnotationRenderer extends JLabel implements ListCellRenderer<AnnotationsListItem> {

	/**
	* 
	*/
	private static final long serialVersionUID = 5896453832614035646L;

	@Override
	public Component getListCellRendererComponent(JList<? extends AnnotationsListItem> list, AnnotationsListItem value,
			int index, boolean isSelected, boolean cellHasFocus) {
		Icon icon = X_Icons.annoProp;
		setText(value.getAnnotation().render());
		setIcon(icon);
		return this;
	}

}
