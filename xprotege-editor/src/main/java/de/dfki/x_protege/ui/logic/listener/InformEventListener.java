package de.dfki.x_protege.ui.logic.listener;

import org.protege.editor.core.ui.workspace.Workspace;

import de.dfki.x_protege.ui.logic.Event.InfromEvent;

/**
 * The inform {@link EventListener} is used by the {@link Workspace}. It listens
 * to so called {@link InfromEvent}s and creates Dialogues which inform the user
 * according to this Event.
 * 
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 13.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public abstract class InformEventListener implements EventListener {

	public abstract void informEvent(InfromEvent inform);

}