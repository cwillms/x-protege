package de.dfki.x_protege.ui.components;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 * This class is a {@link QueryFormInputBox} specialized towards the SELECTION statements of a SparSQL query.
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 26.03.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class SelectionQueryInputBox extends QueryFormInputBox {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4880306627968392422L;
	protected final String GHOSTTEXT = "?s ?p ?o";
	private JCheckBox selectBox;
	private JCheckBox selectAllBox;
	private JCheckBox distinctBox;

    /**
     * Creates a new instance of {@link SelectionQueryInputBox}
     * @param form the form ({@link QueryFormViewComponent}) this input field belongs to.
     * @param header the header of the input field
     */
	public SelectionQueryInputBox(QueryFormViewComponent form, String header) {
		super(form, header);

	}


	@Override
	protected JComponent initHeader() {
		JPanel header = new JPanel();
		ActionListener listener = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JCheckBox source = (JCheckBox) e.getSource();
				if (source == selectBox)
					selectAllBox.setSelected(!selectAllBox.isSelected());
				else
					selectBox.setSelected(!selectBox.isSelected());
			}
		};
		header.setLayout(new FlowLayout());
		this.selectBox = new JCheckBox("SELECT", true);
		this.selectBox.setBackground(background);
		this.selectBox.setForeground(Color.WHITE);
		header.add(this.selectBox);
		this.selectAllBox = new JCheckBox("SELECTALL", false);
		this.selectAllBox.setBackground(background);
		this.selectAllBox.setForeground(Color.WHITE);
		header.add(this.selectAllBox);
		this.distinctBox = new JCheckBox("DISTINCT", false);
		this.distinctBox.setBackground(background);
		this.distinctBox.setForeground(Color.WHITE);
		header.add(this.distinctBox);
		header.setBackground(this.background);
		header.setOpaque(true);
		this.selectBox.addActionListener(listener);
		this.selectAllBox.addActionListener(listener);
		return header;
	}


	@Override
	public String getValue() {
		if (this.selectBox.isSelected())
			if (this.distinctBox.isSelected())
				return "SELECT DISTINCT " + this.inputField.getText();
			else
				return "SELECT " + this.inputField.getText();
		else if (this.distinctBox.isSelected())
			return "SELECTALL DISTINCT " + this.inputField.getText();
		else
			return "SELECTALL " + this.inputField.getText();
	}

	@Override
	protected String getGhostText() {
		return this.GHOSTTEXT;
	}

}
