package de.dfki.x_protege.ui.components.lists;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import org.protege.editor.core.ui.list.MList;
import org.protege.editor.core.ui.list.MListSectionHeader;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.ui.logic.search.EntitiyFinder;
import de.dfki.x_protege.ui.utilities.rendering.X_ProtegeSearchRenderer;

/**
 * The {@link FindingsList} uses {@link FindingListItem}s to present the search
 * results provided by an {@link EntitiyFinder}.<br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 17.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class FindingsList extends MList {

	/**
	 * 
	 */
	private static final long serialVersionUID = -423886259533743426L;;
	private String HEADER_TEXT_ABox = "ABox";
	private String HEADER_TEXT_RBox = "RBox";
	private String HEADER_TEXT_TBox = "TBox";
	private MListSectionHeader headerA = new MListSectionHeader() {

		@Override
		public String getName() {
			return HEADER_TEXT_ABox;
		}

		@Override
		public boolean canAdd() {
			return false;
		}
	};

	private MListSectionHeader headerR = new MListSectionHeader() {

		@Override
		public String getName() {
			return HEADER_TEXT_RBox;
		}

		@Override
		public boolean canAdd() {
			return false;
		}
	};

	private MListSectionHeader headerT = new MListSectionHeader() {

		@Override
		public String getName() {
			return HEADER_TEXT_TBox;
		}

		@Override
		public boolean canAdd() {
			return false;
		}
	};

	/**
	 * create a new instance of {@link FindingsList}
	 * 
	 * @param x_EditorKit
	 *            The connected {@link X_EditorKit}.
	 */
	public FindingsList(X_EditorKit x_EditorKit, String searchTerm) {
		setCellRenderer(new X_ProtegeSearchRenderer(searchTerm));
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent evt) {
				MList list = (MList) evt.getSource();
				if (evt.getClickCount() == 2) {
					int index = list.locationToIndex(evt.getPoint());
					if (index >= 0) {
						try {
							Robot robot = new Robot();
							robot.keyPress(KeyEvent.VK_ENTER);
							robot.keyRelease(KeyEvent.VK_ENTER);
						} catch (AWTException e) {
							e.printStackTrace();
						}
					}
				}
			}
		});
	}

	@SuppressWarnings("unchecked")
	public void setRootObject(ArrayList<ArrayList<X_Entity>> root) {

		java.util.List<Object> data = new ArrayList<Object>();

		data.add(headerT);
		if (!root.get(0).isEmpty()) {
			for (X_Entity t : root.get(0)) {
				data.add(new FindingListItem(t));
			}
		}
		data.add(headerR);
		if (!root.get(1).isEmpty()) {
			for (X_Entity p : root.get(1)) {
				data.add(new FindingListItem(p));
			}
		}
		data.add(headerA);
		if (!root.get(2).isEmpty()) {
			for (X_Entity i : root.get(2)) {
				data.add(new FindingListItem(i));
			}
		}

		setListData(data.toArray());
		revalidate();
	}

}
