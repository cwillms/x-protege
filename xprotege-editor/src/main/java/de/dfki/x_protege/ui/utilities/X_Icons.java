package de.dfki.x_protege.ui.utilities;

import java.net.URL;
import java.util.HashMap;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import de.dfki.x_protege.model.data.AnnotationProperty;
import de.dfki.x_protege.model.data.AtomicX_Indivdual;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.data.X_Property;

/**
 * This class is used to load the icons used be X-Protege's gui components from
 * the resources.
 * 
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 13.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class X_Icons {
	private static HashMap<String, ImageIcon> iconMap;


	static {
		iconMap = new HashMap<String, ImageIcon>();
	}
	public static final Icon ontologyIcon = getIcon("ontology.png");
	public static final Icon search = getIcon("search.png");
	public static final Icon help = getIcon("help.png");
	public static final Icon class_add_sub = getIcon("class.add.sub.png");
	public static final Icon allTypes = getIcon("allClasses.png");
	public static final Icon cartType = getIcon("datarange.png");
	public static final Icon xsdType = getIcon("xsdType.png");
	public static final Icon owlType = getIcon("Classes.gif");
	public static final Icon simpleIdividual = getIcon("individual.png");
	public static final Icon cartIndividual = getIcon("cartIndividual.gif");
	public static final Icon defaultIcon = getIcon("default.png");
	public static final Icon class_add_sib = getIcon("class.add.sib.png");
	public static final Icon protegeIcon = getIcon("ProtegeLogo.gif");
	public static final Icon prefix_add = getIcon("prefix.add.png");
	public static final Icon prefix_generate = getIcon("prefix.generate.png");
	public static final Icon prefix_remove = getIcon("prefix.remove.png");
	public static final Icon prop_delete = getIcon("property.delete.png");
	public static final Icon inverseOf = getIcon("Annotation.gif");

	public static final String ALTERNATIVE_ICONS_DIRECTORY = "resources/";
	public static final Icon class_delete = getIcon("class.delete.png");
	public static final Icon cartTypePop = getIcon("datarange.png");
	public static final Icon owlTypePop = getIcon("Classes.gif");
	public static final Icon property_mixed_add = getIcon(
			"property.mixed.add.png");
	public static final Icon property_data_add = getIcon(
			"property.data.add.png");
	public static final Icon property_object_add = getIcon(
			"property.object.add.png");
	public static final Icon propertyRemove = getIcon("property.remove.png");
	public static final Icon createIndividual = getIcon("individual.add.png");
	public static final Icon deleteIndividual = getIcon(
			"individual.delete.png");
	public static final Icon defaultProperty = getIcon("property.default.png");
	public static final Icon mixedProperty = getIcon("property.mixed.png");
	public static final Icon dataProperty = getIcon("property.data.png");
	public static final Icon objectProperty = getIcon("property.object.png");
	public static final Icon property_addSub = getIcon(
			"property.mixed.addsub.png");
	public static final Icon property_addSib = getIcon(
			"property.mixed.addsib.png");
	public static final Icon property_delete = getIcon(
			"property.mixed.delete.png");
	public static final Icon disabledcartType = getIcon(
			"disabled.datarange.png");
	public static final Icon disabledcartTypePop = getIcon(
			"disabled.datarange.pop.png");
	public static final Icon disabledowlType = getIcon("disabled.Classes.png");
	public static final Icon disabledowlTypePop = getIcon(
			"disabled.class.defined.png");
	public static final Icon rename = getIcon("NerdSmiling.gif");
	public static final Icon annoProp = getIcon("annotationProperty.gif");

	public static final Icon RuleIcon = getIcon("rule.png");

	/**
	 * Gets the Icon with the specified file name.
	 * 
	 * @param fileName
	 *            The file name. May be {@code null}.
	 * @return The Icon that is depicted by the specified file name, or
	 *         {@code null} if there is no file in the class path with the
	 *         specified file name, of {@code fileName} is {@code null}.
	 */
	public static ImageIcon getIcon(String fileName) {
		if (fileName == null) {
			return null;
		}
		if (iconMap.containsKey(fileName)) {
			return iconMap.get(fileName);
		}
		URL url = getIconURL(fileName);
		if (url == null) {
			return null;
		}
		ImageIcon loadedIcon = new ImageIcon(url);
		iconMap.put(fileName, loadedIcon);
		return loadedIcon;
	}

	/**
	 * Gets the URL of the icon file denoted by the specified file name.
	 * 
	 * @param fileName
	 *            The file name. May be {@code null}.
	 * @return The URL of the specified file name, or {@code null} if
	 *         {@code fileName} is {@code null} , or {@code null} if there is
	 *         not file with the specified file name in the bundle class path.
	 */
	private static URL getIconURL(String fileName) {
		if (fileName == null) {
			return null;
		}
		ClassLoader loader = X_Icons.class.getClassLoader();
		URL url = loader.getResource(fileName);
		if (url == null && !isFileNameAbsolute(fileName)) {
			url = loader.getResource(ALTERNATIVE_ICONS_DIRECTORY + fileName);
		}
		return url;
	}

	/**
	 * Determines if the specified file name is absolute.
	 * 
	 * @param fileName
	 *            The file name.
	 * @return {@code true} if the specified file name is absolute, otherwise
	 *         {@code false}.
	 */
	private static boolean isFileNameAbsolute(String fileName) {
		return fileName.startsWith("/");
	}

	/**
	 * Computes the {@link Icon} to be used to represent the given {@link X_Entity}.
	 * @param e the {@link X_Entity} to be represented
	 * @return the {@link Icon} used to represent the {@link X_Entity}.
     */
	public static Icon selectIcon(X_Entity e) {
		if (e.isAtomType()) {
			return X_Icons.owlType;
		}
		if (e.isCartesianType()) {
			return X_Icons.cartType;
		}
		if (e instanceof AnnotationProperty) {
			return X_Icons.annoProp;
		}
		if (e.isProperty()) {
			X_Property pr = (X_Property) e;
			switch (pr.getPropertyType().nextSetBit(0)) {
				case 0 :
					return X_Icons.mixedProperty;
				case 1 :
					return X_Icons.dataProperty;
				case 2 :
					return X_Icons.objectProperty;
				default :
					return X_Icons.defaultProperty;
			}
		}
		if (e.isAtomInstance()) {
			AtomicX_Indivdual si = (AtomicX_Indivdual) e;
			if (si.isXSD()) {
				return X_Icons.xsdType;
			}
			return X_Icons.simpleIdividual;
		}
		if (e.isCartesianInstance()) {
			return X_Icons.cartIndividual;
		}
		return X_Icons.defaultIcon;
	}
}
