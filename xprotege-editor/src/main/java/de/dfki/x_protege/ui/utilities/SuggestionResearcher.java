/**
 * 
 */
package de.dfki.x_protege.ui.utilities;

import java.awt.Rectangle;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.concurrent.ExecutionException;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingWorker;
import javax.swing.text.BadLocationException;

import org.protege.editor.core.ui.util.AugmentedJTextField;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.model.OntologyCache;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.ui.components.dialogue.SearchResultPresenter;

/**
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 21.03.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class SuggestionResearcher extends SwingWorker<ArrayList<String>, String> {

	/**
	 * A {@link String} representing the searchterm.
	 */
	private String searchTerm;
	private ActionListener actionListener;
	private AugmentedJTextField textfield;

	/**
	 * Creates an instance of {@link SuggestionResearcher}.
	 * 
	 * @param searchTerm
	 *            the search term represented by a {@link String}.
	 * @param editorKit
	 *            the connected {@link X_EditorKit}.
	 */
	public SuggestionResearcher(AugmentedJTextField textfield, String searchTerm, ActionListener actionListener) {
		this.textfield = textfield;
		this.actionListener = actionListener;
		this.searchTerm = searchTerm;
	}


	@Override
	protected ArrayList<String> doInBackground() throws Exception {
		ArrayList<String> result = new ArrayList<>();
		if (isPrefix())
			result.addAll(computePrefixMatches());
		else {
			result.addAll(computePrefixMatches());
			result.addAll(computeEntities());
		}
		return result;
	}

	private boolean isPrefix() {
		if (searchTerm.startsWith("<")) {
			if (searchTerm.contains("http")) {
				if (!searchTerm.contains("#"))
					return true;
			}
			if (!searchTerm.contains(":")) {
				return true;
			}
		}
		return false;
	}

	@Override
	protected void done() {
		try {
			showResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private ArrayList<String> computePrefixMatches() {
		ArrayList<String> results = OntologyCache.getNamespace().search(searchTerm.replace("<", ""));
		Collections.sort(results);
		return results;
	}

	private ArrayList<String> computeEntities() {
		HashSet<String> set = new HashSet<>();
		ArrayList<String> results = new ArrayList<>();
		ArrayList<ArrayList<X_Entity>> matches = OntologyCache.search(searchTerm.replace("<", ""));
		for (ArrayList<X_Entity> l : matches) {
			for (X_Entity e : l) {
				set.add(e.toStringPretty());
			}
		}
		results.addAll(set);
		Collections.sort(results);
		return results;
	}

	/**
	 * When the {@link SwingWorker} is done, present the results using a
	 * {@link SearchResultPresenter} or inform the user if there are no matches.
	 * 
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	private void showResult() throws InterruptedException, ExecutionException {
		JPopupMenu popup = new JPopupMenu();
		JMenuItem menuItem;
		if (!get().isEmpty()) {
			for (String s : get()) {
				menuItem = new JMenuItem(s);
				menuItem.addActionListener(actionListener);
				popup.add(menuItem);
			}
		} else {
			menuItem = new JMenuItem("Sorry, no match found :( ");
		}
		Rectangle rectangle;
		try {
			rectangle = textfield.modelToView(textfield.getCaretPosition());
			popup.show(textfield, rectangle.x, rectangle.y);
		} catch (BadLocationException e) {
		}
	}

}
