package de.dfki.x_protege.ui.logic;

import java.util.ArrayList;

import de.dfki.x_protege.model.OntologyCache;
import de.dfki.x_protege.model.data.Annotation;
import de.dfki.x_protege.model.data.Ontology;
import de.dfki.x_protege.ui.logic.Event.Event;
import de.dfki.x_protege.ui.logic.Event.OntoChange;
import de.dfki.x_protege.ui.logic.listener.EventListener;
import de.dfki.x_protege.ui.logic.listener.OntologyChangeListener;


/**
 * The {@link OntologyEventManager} is used to maintain the model (e.i.,
 * {@link Ontology} and {@link OntologyCache}) of x-protégé according to
 * {@link OntoChange}-Events. It is also used to take account of all side
 * effects (e.g., renaming of entities after a namespace was changed) <br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 16.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class OntologyEventManager extends EventManager {

	/**
	 * Creates a new instance of {@link OntologyEventManager}
	 * 
	 * @param ontology
	 *            The connected instance of {@link Ontology}.
	 */
	public OntologyEventManager(Ontology ontology) {
		super(ontology);

	}

	@Override
	public void handle(Event ontoChanged) {
		switch (((OntoChange) ontoChanged).getType()) {
			case IDCHANGED : {
				ArrayList<String> t = new ArrayList<String>();
				String value = (String) ontoChanged.getOriginalValue();
				t.add(value);
				String oldPrefix = getLastBitFromUrl(
						relatedOntology.getUri().toString());
				for (Annotation a : relatedOntology.getAnnotations()) {
					relatedOntology.removeTupleFromTupleStore(a.getTuple());
				}
				relatedOntology.setID(t);
				OntologyCache.getNamespace().localNamespaceChanged(oldPrefix,
						getLastBitFromUrl(relatedOntology.getUri().toString()),
						relatedOntology.getUri().toString() + "#");
				ArrayList<String> list = new ArrayList<String>();
				for (int i = 0; i < t.size(); i++) {
					list.add(i, "<" + OntologyCache.getNamespace()
							.getShortForLong(t.get(i)) + ">");
				}
				relatedOntology.setShortName(list);
				for (Annotation a : relatedOntology.getAnnotations()) {
					a.setDomain(relatedOntology);
					relatedOntology.addNewTupleToTupleStore(a.getTuple());
				}
				break;
			}
			case VERSIONCHANGED : {
				if ((String) ontoChanged.getOriginalValue() != "") {
					relatedOntology.setVersion(
							(String) ontoChanged.getOriginalValue());
				}
				break;
			}
			case PREFIX : {
				relatedOntology.setIOFormat(false);
				break;
			}
			case INFIX : {
				relatedOntology.setIOFormat(true);
				break;
			}
			default :
				break;

		}
		for (EventListener o : this.listener) {
			((OntologyChangeListener) o)
					.ontologiesChanged((OntoChange) ontoChanged);
		}
	}

	private String getLastBitFromUrl(String url) {
		return url.replaceFirst(".*/([^/?]+).*", "$1");
	}

}
