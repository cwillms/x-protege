package de.dfki.x_protege.ui.components.lists;

import java.util.ArrayList;

import org.protege.editor.core.ui.list.MList;
import org.protege.editor.core.ui.list.MListSectionHeader;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.model.data.AnnotationProperty;
import de.dfki.x_protege.ui.components.AnnotationEditor;

/**
 * The {@link AnnotationPropertyList} is part of the {@link AnnotationEditor}.
 * It is used to visualize the available {@link AnnotationProperty}. <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 17.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class AnnotationPropertyList extends MList {

	private final X_EditorKit x_EditorKit;
	/**
	 * 
	 */
	private static final long serialVersionUID = 3466604237630723163L;

	private ArrayList<AnnotationProperty> root;
	private String HEADER_TEXT = "Annotation Properties";
	private MListSectionHeader header = new MListSectionHeader() {

		@Override
		public String getName() {
			return HEADER_TEXT;
		}

		@Override
		public boolean canAdd() {
			return false;
		}
	};

	/**
	 * Creates a new instance of {@link AnnotationPropertyList}.
	 * 
	 * @param x_EditorKit
	 *            The connected {@link X_EditorKit}.
	 */
	@SuppressWarnings("unchecked")
	public AnnotationPropertyList(X_EditorKit x_EditorKit) {
		this.x_EditorKit = x_EditorKit;
		this.root = this.x_EditorKit.getX_ModelManager()
				.getAnnotationProperties();

		java.util.List<Object> data = new ArrayList<Object>();

		data.add(header);

		if (root != null) {
			for (AnnotationProperty prop : root) {
				data.add(new AnnotationPropertiesListItem(prop, this));
			}
		}

		setListData(data.toArray());
		revalidate();
	}

	/**
	 * Returns the {@link AnnotationProperty} located at the given index.
	 * 
	 * @param i
	 *            An index.
	 * @return The {@link AnnotationProperty} located at the given index.
	 */
	public AnnotationProperty getElement(int i) {
		return root.get(i);
	}

}
