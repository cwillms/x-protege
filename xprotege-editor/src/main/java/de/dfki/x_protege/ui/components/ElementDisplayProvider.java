package de.dfki.x_protege.ui.components;

import javax.swing.JComponent;

import de.dfki.x_protege.model.data.X_Entity;

public interface ElementDisplayProvider {

  public boolean canDisplay(X_Entity element);


  public JComponent getDisplayComponent();

}
