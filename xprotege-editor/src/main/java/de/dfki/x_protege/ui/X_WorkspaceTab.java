package de.dfki.x_protege.ui;

import org.protege.editor.core.ui.workspace.WorkspaceTab;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.model.X_ModelManager;

/**
 * Represents a tab in a TabbedWorkspace. This is a core part of the workspace
 * of x-protégé.<br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 17.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public abstract class X_WorkspaceTab extends WorkspaceTab {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4679328340699805977L;

	/**
	 *
	 * @return The {@link X_EditorKit} this {@link X_Workspace} belongs to.
	 */
	public X_EditorKit getX_EditorKit() {
		return (X_EditorKit) getWorkspace().getEditorKit();
	}

	/**
	 *
	 * @return The {@link X_ModelManager} this {@link X_Workspace} belongs to.
	 */
	public X_ModelManager getX_ModelManager() {
		return getX_EditorKit().getX_ModelManager();
	}
}
