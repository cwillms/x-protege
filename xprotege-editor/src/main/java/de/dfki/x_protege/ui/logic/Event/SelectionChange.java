package de.dfki.x_protege.ui.logic.Event;

import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.data.X_Individual;
import de.dfki.x_protege.ui.components.tree.X_EntityTreeNode;
import de.dfki.x_protege.ui.logic.Event.Type.SelectionType;

/**
 * {@link SelectionChange}s indicate changes in one of the lists or trees of the
 * X-protege components.
 * 
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 13.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class SelectionChange extends Event {

	private final SelectionType type;

	private X_Individual indi;

	/**
	 * Creates a new instance of {@link SelectionChange}.
	 * @param t {@link SelectionType} specifying which selection changed (ClassesTab, PropertiesTab, Instances etc.)
	 * @param value the {@link X_EntityTreeNode} representing the selected {@link X_Entity}.
     */
	public SelectionChange(SelectionType t, X_EntityTreeNode value) {
		super(value);
		this.type = t;
	}

	/**
	 * TODO where is this constructor used?
	 * Creates a new instance of {@link SelectionChange}.
	 * @param value the {@link X_Entity} that was selected.
	 */
	public SelectionChange(X_Entity value) {
		super(value);
		type = SelectionType.TABLE;
	}

	/**
	 * TODO where is this constructor used
	 * @param t {@link SelectionType} specifying which selection changed (ClassesTab, PropertiesTab, Instances etc.)
	 * @param value the currently selected {@link X_Individual}
     */
	public SelectionChange(SelectionType t, X_Individual value) {
		super(value);
		this.type = t;
		this.indi = value;
	}

    /**
     *
      * @return {@link SelectionType} specifying which selection changed (ClassesTab, PropertiesTab, Instances etc.)
     */
	public SelectionType getType() {
		return type;
	}

    /**
     *
     * @return the selected {@link X_Individual} in case the specific constructor was used,  null otherwise
     */
	public X_Individual getIndi() {
		return this.indi;
	}

	@Override
	public X_EntityTreeNode getValue() {
		return (X_EntityTreeNode) value;
	}

	@Override
	public String toString() {
		return super.toString() + " Type: " + this.type;
	}

}
