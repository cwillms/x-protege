package de.dfki.x_protege.ui.components.lists;

import org.protege.editor.core.ui.list.MListItem;

import de.dfki.x_protege.model.data.AnnotationProperty;

/**
 * Part of the {@link AnnotationPropertyList}. Represents the different kinds of
 * annotation properties, e.g., comment. <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 17.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class AnnotationPropertiesListItem implements MListItem {

	private AnnotationProperty value;

	/**
	 * Creates a new instance of {@link AnnotationPropertiesListItem}.
	 * 
	 * @param prop
	 *            The {@link AnnotationProperty} to be represented.
	 * @param annotationPropertyList
	 *            The connected {@link AnnotationPropertyList}.
	 */
	AnnotationPropertiesListItem(AnnotationProperty prop, AnnotationPropertyList annotationPropertyList) {
		this.value = prop;
	}

	@Override
	public boolean isEditable() {
		return false;
	}

	@Override
	public void handleEdit() {
		// Nothing to do here
	}

	@Override
	public boolean isDeleteable() {
		return false;
	}

	@Override
	public boolean handleDelete() {
		// Nothing to do here
		return false;
	}

	@Override
	public String getTooltip() {
		// TODO Auto-generated method stub
		return null;
	}

	public AnnotationProperty getValue() {
		return this.value;
	}

}
