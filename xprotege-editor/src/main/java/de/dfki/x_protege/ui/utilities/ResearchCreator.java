/**
 * 
 */
package de.dfki.x_protege.ui.utilities;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.ui.logic.search.EntitiyFinder;

/**
 * <br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 12.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class ResearchCreator {

	private final X_EditorKit editorKit;

	/**
	 * Creates a new instance of {@link ResearchCreator}.
	 */
	public ResearchCreator(X_EditorKit editorKit) {
		this.editorKit = editorKit;
	}

	/**
	 * Creates a new instance of {@link EntitiyFinder}, which is an extended {@link javax.swing.SwingWorker}, for the given searchTerm.
	 * @param searchTerm the term to be researched by the {@link EntitiyFinder}.
	 * @return a new, searchterm specific, instance of {@link EntitiyFinder}
     */
	public EntitiyFinder getEntitySearcher(String searchTerm) {
		return new EntitiyFinder(searchTerm, editorKit);
	}

}
