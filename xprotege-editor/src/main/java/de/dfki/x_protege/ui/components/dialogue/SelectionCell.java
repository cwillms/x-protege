package de.dfki.x_protege.ui.components.dialogue;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentListener;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.model.OntologyCache;
import de.dfki.x_protege.model.data.AtomicX_Indivdual;
import de.dfki.x_protege.model.data.CartesianX_Individual;
import de.dfki.x_protege.model.data.X_Individual;
import de.dfki.x_protege.model.data.X_Type;
import de.dfki.x_protege.ui.utilities.UIHelper;
import de.dfki.x_protege.ui.utilities.X_Icons;

/**
 * The {@link SelectionCell}s are central components of the
 * {@link ComplexCreatorPanel}. The intention of the {@link SelectionCell} is
 * the selection/creation of an {@link X_Individual} for a given {@link X_Type}.
 * Those {@link X_Individual}s are then used as subelements of a
 * {@link CartesianX_Individual}.<br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 20.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class SelectionCell extends JPanel {

	private static final long serialVersionUID = -1684668143497453364L;
	private X_Type type;
	private JComboBox<X_Individual> comboBox;
	private JTextField textField;
	private final X_EditorKit editorKit;
	private final ItemListener listener;
	private final DocumentListener docListener;
	private final ActionListener buttonListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			ArrayList<String> shortName = new ArrayList<String>();
			String returnValue = getString("Individual",
					"Please enter a name for the new Individual");
			if (returnValue != null && !returnValue.equals("")) {
				if (returnValue.contains(":")) {
					String[] splitted = returnValue.split(":");
					String namespace = splitted[0];
					if (!OntologyCache.getNamespace().contains(namespace)) {
						new UIHelper(editorKit).showError("Warning",
								"No valid namespace: " + namespace);
						shortName = new ArrayList<>();
					} else {
						shortName.add("<" + returnValue + ">");
					}
				} else {
					shortName.add("<" + returnValue + ">");
				}
			}
			if (!shortName.isEmpty())
				updateComboboxModel(shortName);
		}
	};

	/**
	 * Creates a new instance if {@link SelectionCell}.
	 * 
	 * @param editorKit
	 *            The connected {@link X_EditorKit}.
	 * @param t
	 *            The {@link X_Type} connected to the {@link SelectionCell},
	 *            i.e. the selected/created {@link X_Individual} must have this
	 *            type.
	 * @param listener
	 *            {@link ItemListener} used to inform the
	 *            {@link ComplexCreatorPanel} about the selected
	 *            {@link X_Individual}, in case the {@link X_Type} is not an
	 *            xsd-Type.
	 * @param docListener
	 *            A {@link DocumentListener} used to inform the
	 *            {@link ComplexCreatorPanel} about the value of the
	 *            {@link X_Individual}, in case the {@link X_Type} is an
	 *            xsd-Type.
	 */
	public SelectionCell(X_EditorKit editorKit, X_Type t, ItemListener listener,
			DocumentListener docListener) {
		this.editorKit = editorKit;
		this.type = t;
		this.listener = listener;
		this.docListener = docListener;
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.setBorder(BorderFactory.createEtchedBorder());
		JPanel label = createLabel();
		this.add(label);
		if (t.isXSDType()) {
			textField = createTextField();
			this.add(textField);
		} else {
			comboBox = createComboBox();
			JButton button = createButton();
			this.add(comboBox);
			this.add(button);
		}
	}

	private String getString(String message, String title) {
		String uriString = JOptionPane.showInputDialog(this, message, title,
				JOptionPane.INFORMATION_MESSAGE);
		if (uriString == null) {
			return null;
		}
		return uriString;
	}

	private void updateComboboxModel(ArrayList<String> shortName) {
		comboBox.addItem(new AtomicX_Indivdual(shortName));
	}

	private JButton createButton() {
		JButton button = new JButton();
		button.setIcon(X_Icons.createIndividual);
		button.setText("New");
		button.setToolTipText("Create a new Instance of " + type.render());
		button.addActionListener(buttonListener);
		return button;
	}

	private JComboBox<X_Individual> createComboBox() {
		X_Individual[] items = type.getConnectedIndividualsSorted().toArray(
				new X_Individual[type.getConnectedIndividualsSorted().size()]);
		JComboBox<X_Individual> comboBox = new JComboBox<>(items);
		comboBox.setBorder(BorderFactory.createEmptyBorder());
		comboBox.addItemListener(this.listener);
		return comboBox;
	}

	private JTextField createTextField() {
		JTextField textField = new JTextField();
		textField.getDocument().addDocumentListener(docListener);
		return textField;
	}

	private JPanel createLabel() {
		JPanel labelPanel = new JPanel();
		JLabel type = new JLabel("Type: ");
		JLabel label = new JLabel();
		label.setText(this.type.render());
		label.setIcon(X_Icons.owlType);
		labelPanel.add(type, BorderLayout.WEST);
		labelPanel.add(label, BorderLayout.CENTER);
		return labelPanel;
	}

	/**
	 * Returns the selected {@link X_Individual}.
	 * 
	 * @return
	 */
	public X_Individual getSelection() {
		X_Individual s;
		if (type.isXSDType()) {
			ArrayList<String> name = new ArrayList<>();
			name.add("\"" + textField.getText() + "\"^^"
					+ type.getShortName().get(0));
			s = new AtomicX_Indivdual(name);
			((AtomicX_Indivdual) s).setXSD();
		} else {
			s = (X_Individual) this.comboBox.getSelectedItem();
		}
		return s;
	}

	/**
	 * Returns the {@link X_Individual} that was created to populate the
	 * {@link X_Individual} connected to this {@link SelectionCell}.
	 * 
	 * @return
	 */
	public X_Individual getNewIndis() {
		if (type.isXSDType()) {
			ArrayList<String> name = new ArrayList<>();
			name.add("\"" + textField.getText() + "\"^^"
					+ type.getShortName().get(0));
			AtomicX_Indivdual newIndi = new AtomicX_Indivdual(name);
			newIndi.addClazz(type);
			newIndi.setXSD();
			return newIndi;
		} else {
			Object indi = comboBox.getSelectedItem();
			if (indi != null) {
				if (OntologyCache.getIndividual(
						((X_Individual) indi).getShortName()) == null)
					return (X_Individual) indi;
			}
		}
		return null;
	}

}
