package de.dfki.x_protege.ui.components;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.tree.TreePath;

import org.protege.editor.core.ui.util.AugmentedJTextField;

import de.dfki.x_protege.model.data.AbstractX_Property;
import de.dfki.x_protege.model.data.X_Property;
import de.dfki.x_protege.ui.components.tree.X_EntityTreeNode;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.SelectionChange;
import de.dfki.x_protege.ui.logic.Event.Type.ModelChangeType;
import de.dfki.x_protege.ui.logic.Event.Type.SelectionType;
import de.dfki.x_protege.ui.logic.listener.CharacteristicsListener;
import de.dfki.x_protege.ui.utilities.UIHelper;
import de.dfki.x_protege.ui.utilities.X_Icons;
import de.dfki.x_protege.ui.utilities.selectionFrames.PropertySelectionFrame;

/**
 * The {@link X_PropertyCharacteristicsViewComponent} allows the assignment of
 * several attributes to the {@link X_Property} currently selected in the
 * {@link MPropertyHierarchyViewComponent}. Such an attribute is for example
 * "symmetric".<br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 20.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class X_PropertyCharacteristicsViewComponent
		extends
			AbstractX_PropertyViewComponent {

	private static final long serialVersionUID = 560907643211092837L;

	private JCheckBox functionalCB;

	private JCheckBox inverseFunctionalCB;

	private JCheckBox transitiveCB;

	private JCheckBox symmetricCB;

	private JCheckBox aSymmetricCB;

	private JCheckBox reflexiveCB;

	private JCheckBox irreflexiveCB;

	private List<JCheckBox> checkBoxes;

	private CharacteristicsListener listener;

	private AbstractX_Property sel;

	@Override
	protected void handleSelectionChanges(
			final SelectionChange selectionchanges) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {

				switch (selectionchanges.getType()) {
					case PROPERTYHIERARCHY : {
						updateView();
						break;
					}

					default :
						break;
				}
			}
		};
		SwingUtilities.invokeLater(doHighlight);

	}

	private ActionListener inverseOfListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			AbstractX_Property p = getSelectedProperty();
			if (p.getCharacteristics().get(3))
				warn(p.getShortName()
						+ " is a symetric property and therefore inverseOf itself.");
			// TODO further criteria for warnings
			else {
				PropertySelectionFrame sel = new PropertySelectionFrame(
						getX_EditorKit(), "inverse", false);
				int returnValue = new UIHelper(getX_EditorKit())
						.showDialog("Please select the inverse Property", sel);
				if (returnValue == 0) {
					getX_ModelManager().handleChange(
							new ModelChange(ModelChangeType.ACCEPT, null));
					TreePath[] selections = sel.getSelections();
					Set<AbstractX_Property> s = new HashSet<>();
					for (TreePath tp : selections) {
						AbstractX_Property property = (AbstractX_Property) ((X_EntityTreeNode) tp
								.getLastPathComponent()).getValue();
						s.add(property);
					}
					if (!s.isEmpty()) {
						for (AbstractX_Property prop : s)
							getX_ModelManager().handleChange(new ModelChange(
									ModelChangeType.INVERSEADD, prop, p));
					}
				} else {
					getX_ModelManager().handleChange(
							new ModelChange(ModelChangeType.CANCEL, null));
				}
				sel.dispose();
			}
		}

	};

	private void warn(String message) {
		new UIHelper(getX_EditorKit()).showError("InverseOf",
				"You must not change the inverseOf property due " + message);
	}

	private ActionListener deleteListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			AbstractX_Property p = getSelectedProperty();
			if (p.getCharacteristics().get(3))
				warn(p.getShortName()
						+ " is a symetric property and therefore inverseOf itself.");
			// TODO further criteria for warnings
			else {
				getX_ModelManager().handleChange(
						new ModelChange(ModelChangeType.INVERSEDEL, p));
			}
		}
	};

	private AugmentedJTextField textBox;

	@Override
	protected void handleModelChanged(final ModelChange modelchanges) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {

				switch (modelchanges.getType()) {
					case CHARCHANGED : {
						updateView();
						break;
					}
					case INVERSEADD : {
						updateView();
						break;
					}
					case INVERSEDEL : {
						updateView();
						break;
					}
					case RENAME : {
						updateView();
						break;
					}
					default :
						break;
				}
			}
		};
		SwingUtilities.invokeLater(doHighlight);

	}

	@Override
	public void initialiseView() throws Exception {
		functionalCB = new JCheckBox("Functional");
		inverseFunctionalCB = new JCheckBox("Inverse functional");
		transitiveCB = new JCheckBox("Transitive");
		symmetricCB = new JCheckBox("Symmetric");
		aSymmetricCB = new JCheckBox("Asymmetric");
		reflexiveCB = new JCheckBox("Reflexive");
		irreflexiveCB = new JCheckBox("Irreflexive");

		checkBoxes = new ArrayList<JCheckBox>();
		checkBoxes.add(functionalCB);
		checkBoxes.add(inverseFunctionalCB);
		checkBoxes.add(transitiveCB);
		checkBoxes.add(symmetricCB);
		checkBoxes.add(aSymmetricCB);
		checkBoxes.add(reflexiveCB);
		checkBoxes.add(irreflexiveCB);

		listener = new CharacteristicsListener(getX_ModelManager());
		for (JCheckBox cb : checkBoxes)
			cb.addActionListener(listener);
		setLayout(new BorderLayout());
		JPanel box = new JPanel();
		box.setLayout(new BoxLayout(box, BoxLayout.Y_AXIS));
		box.setOpaque(false);
		box.add(functionalCB);
		box.add(Box.createVerticalStrut(7));
		box.add(inverseFunctionalCB);
		box.add(Box.createVerticalStrut(7));
		box.add(transitiveCB);
		box.add(Box.createVerticalStrut(7));
		box.add(symmetricCB);
		box.add(Box.createVerticalStrut(7));
		box.add(aSymmetricCB);
		box.add(Box.createVerticalStrut(7));
		box.add(reflexiveCB);
		box.add(Box.createVerticalStrut(7));
		box.add(irreflexiveCB);
		box.add(Box.createVerticalStrut(7));
		//box.add(new JSeparator());
		box.add(createInverseOfBox());
		box.setBorder(new EmptyBorder(0,0,0,0));
		add(new JScrollPane(box));
		add(new JLabel(" "), BorderLayout.SOUTH);
		this.setBorder(new EmptyBorder(0,0,0,0));
		updateView();
	}

	private Component createInverseOfBox() {
		JPanel inversePanel = new JPanel();
		inversePanel.setLayout(new BorderLayout());
		JPanel north = new JPanel();
		north.setLayout(new FlowLayout());
		JLabel label = new JLabel("inverseOf:");
		this.textBox = new AugmentedJTextField("e.g. <owl:test>");
		textBox.setMaximumSize(textBox.getPreferredSize());
		textBox.setFocusable(false);
		inversePanel.add(textBox, BorderLayout.CENTER);
		JButton button = new JButton(X_Icons.mixedProperty);
		button.addActionListener(inverseOfListener);
		JButton delete = new JButton(X_Icons.property_delete);
		delete.addActionListener(deleteListener);
		north.add(label);
		north.add(button);
		north.add(delete);
		inversePanel.add(north, BorderLayout.NORTH);
		return inversePanel;
	}

	@Override
	protected AbstractX_Property updateView(AbstractX_Property property) {
		sel = getSelectedProperty();
		updateHeader(sel);
		enableSupported(sel.getSupportedCharacteristics());
		setCheckBoxesSelected(sel.getCharacteristics());
		setInverse(sel.getInverse());
		return property;
	}

	private void setInverse(AbstractX_Property inverse) {
		if (inverse != null)
			this.textBox.setText(inverse.render());
		else
			this.textBox.setText("");
	}

	private AbstractX_Property getSelectedProperty() {
		return (AbstractX_Property) getX_ModelManager().getActiveModel()
				.getSelection(SelectionType.PROPERTYHIERARCHY);
	}

	private void setCheckBoxesSelected(BitSet characteristics) {
		for (int i = 0; i < checkBoxes.size(); i++) {
			checkBoxes.get(i).setSelected(characteristics.get(i));
		}
	}

	private void enableSupported(BitSet supportedCharacteristics) {
		for (int i = 0; i < checkBoxes.size(); i++) {
			checkBoxes.get(i).setEnabled(supportedCharacteristics.get(i));
		}
	}

}
