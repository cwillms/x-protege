package de.dfki.x_protege.ui.logic;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import de.dfki.x_protege.model.OntologyCache;
import de.dfki.x_protege.model.data.AbstractX_Property;
import de.dfki.x_protege.model.data.Annotation;
import de.dfki.x_protege.model.data.AtomicX_Indivdual;
import de.dfki.x_protege.model.data.AtomicX_Type;
import de.dfki.x_protege.model.data.CartesianX_Individual;
import de.dfki.x_protege.model.data.CartesianX_Type;
import de.dfki.x_protege.model.data.Ontology;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.data.X_Individual;
import de.dfki.x_protege.model.data.X_Property;
import de.dfki.x_protege.model.data.X_Type;
import de.dfki.x_protege.model.utils.PropertyElementProvider;
import de.dfki.x_protege.model.utils.TypeElementProvider;
import de.dfki.x_protege.ui.components.tree.TreeNodeGenerator;
import de.dfki.x_protege.ui.logic.Event.Type.SelectionType;

import static java.util.stream.Collectors.toList;

/**
 * The {@link EventUtilsModel} provides the methods used to maintain the model.
 * It is used by the {@link ModelEventManager}. <br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 16.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class EventUtilsModel {

	private final Ontology relatedOntology;

	private final ModelEventManager modelEventManager;

	private final boolean strictMode = false;

	private boolean accept = false;

	private final HashSet<X_Entity> deletedEntities = new HashSet<>();

	/**
	 * Creates a new instance of {@link EventUtilsModel}.
	 * 
	 * @param ontology
	 *            The connected {@link Ontology}.
	 * @param modelEventManager
	 *            The {@link ModelEventManager} using this instance of
	 *            {@link EventUtilsModel}.
	 */
	public EventUtilsModel(Ontology ontology,
			ModelEventManager modelEventManager) {
		this.relatedOntology = ontology;
		this.modelEventManager = modelEventManager;
	}

	/**
	 * the model and update hfc.
	 * 
	 * @param value
	 *            The class to be removed.
	 * @return true if everything went ok; false if the removing of this class
	 *         would effect other classes.
	 */
	boolean handleClassRemoved(X_Entity value, boolean persistent,
			boolean cancel) { // DONE
		// check whether the removed Class was also part of a Complex element
		if (value.isAtomType()) {
			AtomicX_Type c = (AtomicX_Type) value;
			if (!c.getComplexClasses().isEmpty() && !this.accept) {
				return false;
			} else {
				this.accept = false;
				for (CartesianX_Type ct : c.getComplexClasses()) {
					handleClassRemoved(ct, persistent, cancel);
				}
			}
		}
		relatedOntology.setDirty(true);
		// remove the given class from the hfc tuplestore.
		X_Type t = (X_Type) value;
		if (persistent) {
			removeTuplesFromTupleStore(value.getTupleRep());
		}
		// update parent
		for (X_Entity st : t.getSuperTypes()) {
			st.removeSub(t);
		}
		// update properties
		Set<X_Property> clone = new HashSet<>();
		clone.addAll(t.getConnectedImProperties());
		clone.addAll(t.getConnectedProperties());
		for (X_Property p : clone) {
			removeDependency(p, t, 0, persistent, false);
		}
		clone.clear();
		clone.addAll(t.getRangeImProperties());
		clone.addAll(t.getRangeProperties());
		for (X_Property p : clone) {
			removeDependency(p, t, 1, persistent, false);
		}
		clone.clear();
		clone.addAll(t.getExtraImProperties());
		clone.addAll(t.getExtraProperties());
		for (X_Property p : clone) {
			removeDependency(p, t, 2, persistent, false);
		}
		for (X_Individual i : ((X_Type) value)
				.getConnectedIndividualsSorted()) {
			if (i.getClazz().size() > 1) {
				if (persistent)
					removeTuplesFromTupleStore(i.getTupleRep());
				i.removeClazz((X_Type) value);
				if (persistent)
					addNewTuplesToTupleStore(i.getTupleRep());
			} else {
				if (persistent) {
					removeTuplesFromTupleStore(i.getTupleRep());
				}
				OntologyCache.removeIndividual(i);
			}
		}
		// remove all child nodes of the given node.
		HashSet<X_Entity> subs = new HashSet<>();
		subs.addAll(value.getSubs());
		for (X_Entity sub : subs) {
			handleClassRemoved(sub, persistent, cancel);
		}
		TreeNodeGenerator.removeFromClassModels(value);

		OntologyCache.removeType(value);

		if (!cancel)
			relatedOntology.getTempClasses().remove(value);

		// update subelements if t is a cart type, they should know that they
		// are no longer part of an cart type
		if (t.isCartesianType()) {
			for (AtomicX_Type st : ((CartesianX_Type) t).getSubElements()) {
				st.removeCartType((CartesianX_Type) t);
			}
		}
		deletedEntities.add(value);
		this.accept = false;
		return true;
	}

	/**
	 * Add a default class as subclass of the given {@link X_Entity}.
	 * 
	 * @param value
	 *            The parent node of the new {@link X_Type}.
	 * @param persistent
	 *            true -> persistent, false -> change only temporary
	 * @return
	 */
	X_Entity handleSubClassAdded(X_Entity value, boolean persistent) { // DONE
		if (persistent) {
			relatedOntology.setDirty(true);
			// removeTuplesFromTupleStore(value.getTupleRep());
		}
		TypeElementProvider tp = new TypeElementProvider();
		String defaultUri = relatedOntology.getNewUri(value);
		String[] array = new String[]{defaultUri};
		X_Entity child = tp.createNewEntity(Arrays.asList(array), value);
		// update Models
		TreeNodeGenerator.addToClassModels(child, value);
		addClass((X_Type) child, (X_Type) value);
		if (persistent) {
			addNewTuplesToTupleStore(child.getTupleRep());
		} else
			relatedOntology.getTempClasses().add((X_Type) child);
		return child;
	}

	private void addClass(X_Type newClass, X_Type parent) {
		// sub add explicit domain, range and extra of parent
		for (X_Property t : parent.getConnectedProperties()) {
			newClass.addExplicitProperty(t);
			t.addImplicitDomain(newClass);
		}
		for (X_Property t : parent.getRangeProperties()) {
			newClass.addExplicitRangeOF(t);
			t.addImplicitRange(newClass);
		}
		for (X_Property t : parent.getExtraProperties()) {
			newClass.addExplicitExtraOf(t);
			t.addImplicitExtra(newClass);
		}
		// sub add implicit domain, range and extra of parent
		for (X_Property t : parent.getConnectedImProperties()) {
			newClass.addImplicitProperty(t);
			t.addImplicitDomain(newClass);
		}
		for (X_Property t : parent.getRangeImProperties()) {
			newClass.addImplicitRangeOf(t);
			t.addImplicitRange(newClass);
		}
		for (X_Property t : parent.getExtraImProperties()) {
			newClass.addImplicitExtraOf(t);
			t.addImplicitExtra(newClass);
		}
		// types are implicitely updated

	}

	/**
	 * Add a default class as sibling of the given {@link X_Entity}.
	 * 
	 * @param value
	 *            The sibling node of the new {@link X_Type}.
	 * @param temp
	 *            true -> persistent, false -> change only temporary
	 * @return
	 */
	X_Entity handleSibClassAdded(X_Entity value, boolean persistent) { // DONE
		X_Type parent = (X_Type) TreeNodeGenerator
				.getNode(value, SelectionType.CLASSHIERARCHY).getParentEntity();
		if (persistent) {
			relatedOntology.setDirty(true);
		}
		TypeElementProvider tp = new TypeElementProvider();
		String[] name = new String[]{relatedOntology.getNewUri(value)};
		X_Type child = (X_Type) tp.createNewEntity(Arrays.asList(name), parent);
		TreeNodeGenerator.addToClassModels(child, parent);
		addClass(child, parent);
		if (persistent)
			addNewTuplesToTupleStore(child.getTupleRep());
		else
			relatedOntology.getTempClasses().add(child);
		return child;
	}

	/**
	 * /** Add a cartesian class as subclass of the given {@link X_Entity}.
	 * 
	 * @param value
	 *            The parent node of the new {@link X_Type}.
	 * @param temp
	 *            true -> persistent, false -> change only temporary
	 * @return
	 */
	X_Entity handleCartSubClassAdded(X_Entity value, boolean persistent) { // DONE
		X_Type parent;
		if (persistent) {
			relatedOntology.setDirty(true);
			parent = (X_Type) relatedOntology
					.getSelection(SelectionType.CLASSHIERARCHY);
			// removeTuplesFromTupleStore(parent.getTupleRep());
		} else {
			parent = (X_Type) value;
		}
		X_Type child = relatedOntology.getTypeInCreation();
		OntologyCache.addType(child);
		TreeNodeGenerator.addToClassModels(child, parent);
		addClass(child, parent);
		if (persistent) {
			addNewTuplesToTupleStore(child.getTupleRep());
		} else
			relatedOntology.getTempClasses().add(child);

		return child;
	}

	/**
	 * Assigns the Set of disjointClasses to the given Class.
	 * 
	 * @param value
	 *            The X_Type to be updated.
	 * @param value2
	 *            A set of disjoint classes.
	 */
	void handleDisjointAdded(X_Entity value, Set<X_Type> disjointClasses) { // DONE
		X_Type selectedClass = (X_Type) value;
		this.removeTuplesFromTupleStore(value.getTupleRep());
		for (X_Type disjointClass : disjointClasses) {
			this.removeTuplesFromTupleStore(disjointClass.getTupleRep());
			selectedClass.addDisjoint(disjointClass);
			disjointClass.addDisjoint(selectedClass);
			this.addNewTuplesToTupleStore(disjointClass.getTupleRep());
		}
		this.addNewTuplesToTupleStore(value.getTupleRep());
	}

	// Handle Property Related Events

	/**
	 * Add the given {@link X_Type}s to the given Property.
	 * 
	 * @param value
	 *            The property.
	 * @param value2
	 *            An {@link Object} representing either a set of {@link X_Type}s
	 *            or a single {@link X_Type}.
	 * @param persistent
	 *            true -> persistent, false -> change only temporary
	 */
	void handleDomainAdded(X_Entity value, X_Type value2, boolean persistent) { // DONE
		if (persistent) {
			relatedOntology.setDirty(true);
			this.removeTuplesFromTupleStore(value.getTupleRep());
		}
		// the property the domain is added to
		X_Property p = (X_Property) value;
		X_Type domain = value2;// (OntologyCache.getType(value2.getShortName(),
								// 0));
		if (p.checkAssignment(0, domain)) {
			if (p.checkAssignment2(0, domain))
				addDomain(p, domain, persistent);
			else
				this.modelEventManager.inform2(p, domain, 0);
		} else {
			this.modelEventManager.inform(p, domain, 0);
		}
		// }
		if (persistent)
			this.addNewTuplesToTupleStore(value.getTupleRep());
	}

	/**
	 * Remove the given property from the model.
	 * 
	 * @param value
	 *            The property to be removed.
	 * @param persistent
	 *            true -> persistent, false -> change only temporary
	 */
	void handlePropertyRemoved(X_Entity value, boolean persistent,
			boolean cancel) { // DONE
		OntologyCache.removeProperties(value);
		if (persistent)
			relatedOntology.setDirty(true);
		removeProperty((X_Property) value, persistent);
		TreeNodeGenerator.removeFromPropertyModels(value);
		if (!persistent && !cancel) {
			relatedOntology.getTempProperty().remove(value);
		}
		deletedEntities.add(value);
	}

	/**
	 * Add a default property as sub property of the given {@link X_Entity}.
	 * 
	 * @param value
	 *            The parent node of the new {@link X_Property}.
	 * @param persistent
	 *            true -> persistent, false -> change only temporary
	 * @return The new {@link X_Property}.
	 */
	X_Entity handleSubPropertyAdded(X_Entity value, boolean persistent) { // DONE
		if (persistent) {
			relatedOntology.setDirty(true);
		}
		PropertyElementProvider tp = new PropertyElementProvider();
		String defaultUri = relatedOntology.getNewUri(value);
		X_Property child = (X_Property) tp
				.createNewEntity(Arrays.asList(new String[]{defaultUri}), value);
		TreeNodeGenerator.addToProptertyModels(child, value);
		addProperty(child, (AbstractX_Property) value);
		if (persistent) {
			addNewTuplesToTupleStore(child.getTupleRep());
		} else {
			relatedOntology.getTempProperty().add(child);
		}
		return child;
	}

	/**
	 * Add a default property as sibling of the given {@link X_Entity}.
	 * 
	 * @param value
	 *            The sibling node of the new {@link X_Property}.
	 * @param persistent
	 *            true -> persistent, false -> change only temporary
	 * @return The new {@link X_Property}.
	 */
	X_Entity handleSibPropertyAdded(X_Entity value, boolean persistent) { // DONE
		if (persistent) {
			relatedOntology.setDirty(true);
		}
		X_Entity parent = TreeNodeGenerator
				.getNode(value, SelectionType.PROPERTYHIERARCHY)
				.getParentEntity();
		PropertyElementProvider tp = new PropertyElementProvider();
		String defaultUri = relatedOntology.getNewUri(parent);
		X_Property child = (X_Property) tp
				.createNewEntity(Arrays.asList(new String[]{defaultUri}), parent);
		TreeNodeGenerator.addToProptertyModels(child, parent);
		addProperty(child, (X_Property) parent);
		if (persistent) {
			addNewTuplesToTupleStore(child.getTupleRep());
		} else {
			relatedOntology.getTempProperty().add(child);
		}
		return child;
	}

	/**
	 * Remove the given {@link X_Type}s from the domain of the given property.
	 * 
	 * @param value
	 *            The property.
	 * @param value2
	 *            An {@link Object} representing either a set of {@link X_Type}s
	 *            or a single {@link X_Type}.
	 * @param persistent
	 *            true -> persistent, false -> change only temporary
	 */
	void handleDomainRemoved(X_Entity value, X_Entity value2,
			boolean persistent) { // DONE
		X_Property p = (X_Property) value;
		if (persistent) {
			relatedOntology.setDirty(true);
			this.removeTuplesFromTupleStore(p.getTupleRep());
		}
		X_Type domain = (X_Type) value2;
		for (X_Entity e : domain.getAllSubs())
			removeDependency(p, (X_Type) e, 0, persistent, false);
		removeDependency(p, domain, 0, persistent, false);
		if (persistent) {
			this.addNewTuplesToTupleStore(p.getTupleRep());
		}
	}

	@SuppressWarnings("unchecked")
	/**
	 * Add the given {@link X_Type}s to the Range of the given property.
	 * 
	 * @param value
	 *            The property.
	 * @param value2
	 *            An {@link Object} representing either a set of {@link X_Type}s
	 *            or a single {@link X_Type}.
	 * @param persistent
	 *            true -> persistent, false -> change only temporary
	 */
	void handleRangeAdded(X_Entity value, Object value2, boolean persistent) { // DONE
		X_Property p = (X_Property) value;
		Set<X_Type> range = (Set<X_Type>) value2;
		if (persistent) {
			relatedOntology.setDirty(true);
			this.removeTuplesFromTupleStore(value.getTupleRep());
		}
		for (X_Type t : range) {
			if (p.checkAssignment(1, t)) {
				if (p.checkAssignment2(1, t))
					addRange(p, t, persistent);
				else
					this.modelEventManager.inform2(p, t, 1);
			} else {
				this.modelEventManager.inform(p, t, 1);
			}
			if (p.getCharacteristics().get(3)) {
				if (persistent)
					this.removeTuplesFromTupleStore(t.getTupleRep());
				addDomain(p, t, persistent);
				if (persistent)
					this.addNewTuplesToTupleStore(t.getTupleRep());
			}
		}
		if (persistent)
			this.addNewTuplesToTupleStore(value.getTupleRep());
	}

	/**
	 * Remove the given {@link X_Type}s from the range of the given property.
	 * 
	 * @param value
	 *            The property.
	 * @param value2
	 *            An {@link Object} representing either a set of {@link X_Type}s
	 *            or a single {@link X_Type}.
	 * @param persistent
	 *            true -> persistent, false -> change only temporary
	 */
	void handleRangeRemoved(X_Entity value, X_Entity value2,
			boolean persistent) { // DONE
		X_Property p = (X_Property) value;
		if (persistent) {
			relatedOntology.setDirty(true);
			this.removeTuplesFromTupleStore(p.getTupleRep());
		}

		X_Type range = (X_Type) value2;
		for (X_Entity e : range.getAllSubs())
			removeDependency(p, (X_Type) e, 1, persistent, false);
		removeDependency(p, range, 1, persistent, false);

		if (persistent) {
			this.addNewTuplesToTupleStore(p.getTupleRep());
		}
	}

	@SuppressWarnings("unchecked")
	/**
	 * Add the given {@link X_Type}s to the extra arguments of the given
	 * property.
	 * 
	 * @param value
	 *            The property.
	 * @param value2
	 *            An {@link Object} representing either a set of {@link X_Type}s
	 *            or a single {@link X_Type}.
	 * @param persistent
	 *            true -> persistent, false -> change only temporary
	 */
	void handleExtraAdded(X_Entity value, Object value2, boolean persistent) { // DONE
		X_Property p = (X_Property) value;
		Set<X_Type> extra = (Set<X_Type>) value2;
		if (persistent)
			this.removeTuplesFromTupleStore(value.getTupleRep());
		for (X_Type t : extra) {
			if (p.checkAssignment(2, t)) {
				if (p.checkAssignment2(2, t))
					addExtra(p, t, persistent);
				else
					this.modelEventManager.inform2(p, t, 2);
			} else {
				this.modelEventManager.inform(p, t, 2);
			}
		}
		if (persistent)
			this.addNewTuplesToTupleStore(value.getTupleRep());
	}

	/**
	 * Remove the given {@link X_Type}s from the extra of the given property.
	 * 
	 * @param value
	 *            The property.
	 * @param value2
	 *            An {@link Object} representing either a set of {@link X_Type}s
	 *            or a single {@link X_Type}.
	 * @param persistent
	 *            true -> persistent, false -> change only temporary
	 */
	void handleExtraRemoved(X_Entity value, X_Entity value2,
			boolean persistent) { // DONE
		X_Property p = (X_Property) value;
		if (persistent) {
			relatedOntology.setDirty(true);
			this.removeTuplesFromTupleStore(p.getTupleRep());
		}

		X_Type range = (X_Type) value2;
		for (X_Entity e : range.getAllSubs())
			removeDependency(p, (X_Type) e, 2, persistent, false);
		removeDependency(p, range, 2, persistent, false);

		if (persistent) {
			this.addNewTuplesToTupleStore(p.getTupleRep());
		}
	}

	// Handle Ontology related Events

	/**
	 * Update all {@link X_Entity} that are effected from this namespace change.
	 * Also update the hfc tuplestore.
	 * TODO check if and when this method used.
	 * @param modelChange
	 */
	void handlePrefixChanged(String oldNamespace, String newNamespace) { // DONE
		relatedOntology.setDirty(true);
		Set<X_Entity> effected = new HashSet<>();
		effected.addAll(OntologyCache.getPrefixMapping(oldNamespace));
		if (effected != null) {
			for (X_Entity e : effected) {
				for (ArrayList<String> t : e.getTupleRep()) {
					relatedOntology.removeTupleFromTupleStore(t);
					for (String s : t)
						s.replaceFirst(oldNamespace, newNamespace);
					relatedOntology.addNewTupleToTupleStore(t);
				}
			}
			OntologyCache.updatePrefixMapping(oldNamespace, newNamespace);
		}
	}

	/**
	 * Updates all {@link X_Entity} that are effected by the renaming of the
	 * given {@link X_Entity}.
	 * 
	 * @param value
	 *            The {@link X_Entity} to be renamed.
	 * @param value2
	 *            The new name of the {@link X_Entity}.
	 * @param persistent
	 *            true -> persistent, false -> change only temporary
	 */
	void handleRename(X_Entity value, Object value2, boolean persistent) { // DONE
		Set<X_Entity> effectedEnities = computeEffectedEntities(value);
		if (persistent) {
			relatedOntology.setDirty(true);
			removeTuplesFromTupleStore(value.getTupleRep(true));
			for (X_Entity entity : effectedEnities) {
				removeTuplesFromTupleStore(entity.getTupleRep());
			}
		}
		ArrayList<String> newName = new ArrayList<String>();
		newName.add(value2.toString());
		if (value.isType()) {
			OntologyCache.removeType(value);
			// X_Type type2 = loadedTypesInstances.get(value.getShortName());
			value.setShortName(newName);
			// type2.setShortName(newName);
			// loadedTypes.remove(value.getShortName());
			OntologyCache.addType((X_Type) value);
			// loadedTypesInstances.put(newName, type2);
			if (value.isAtomType()) {
				for (CartesianX_Type cart : ((AtomicX_Type) value)
						.getComplexClasses()) {
					Set<X_Entity> effectedTuples2 = computeEffectedEntities(
							cart);
					if (persistent) {
						removeTuplesFromTupleStore(cart.getTupleRep(true));
						for (X_Entity entity : effectedTuples2) {
							removeTuplesFromTupleStore(entity.getTupleRep());
						}
					}
					OntologyCache.removeType(cart);
					cart.revalidateName();
					OntologyCache.addType(cart);
					if (persistent) {
						addNewTuplesToTupleStore(cart.getTupleRep(true));
						for (X_Entity entity : effectedTuples2) {
							addNewTuplesToTupleStore(entity.getTupleRep());
						}
					}
				}
			}
		}
		if (value.isProperty()) {
			OntologyCache.removeProperties(value);
			value.setShortName(newName);
			OntologyCache.addProperty((X_Property) value);
		}
		if (value.isInstance()) {
			OntologyCache.removeIndividual(value);
			// update connected Types
			for (X_Type parent : ((X_Individual) value).getClazz()) {
				if (persistent) {
					removeTuplesFromTupleStore(parent.getTupleRep());
				}
				parent.removeIndividual((X_Individual) value);
				value.setShortName(newName);
				parent.addIndividual((X_Individual) value);
				if (persistent) {
					addNewTuplesToTupleStore(parent.getTupleRep());
				}
			}
			AtomicX_Indivdual individual = (AtomicX_Indivdual) value;
			for (CartesianX_Individual ci : individual.getPartOf())
				ci.recomputeShortName();
			OntologyCache.addIndividual((X_Individual) value);
		}
		// update all effected entities
		if (persistent) {
			addNewTuplesToTupleStore(value.getTupleRep(true));
			for (X_Entity entity : effectedEnities) {
				addNewTuplesToTupleStore(entity.getTupleRep());

			}
		}
	}

	/**
	 * This method returns the entities that need to be updated after the given
	 * entity was renamed.
	 * 
	 * @param e the {@link X_Entity}, which was renamed
	 * @return the set of entities that were effected by this action
	 */
	private Set<X_Entity> computeEffectedEntities(X_Entity e) {
		HashSet<X_Entity> result = new HashSet<>();
		if (e.isType()) {
			X_Type t = (X_Type) e;
			result.addAll(t.getConnectedIndividualsSorted());
			result.addAll(t.getConnectedImProperties()); // TODO really
															// necessary?
			result.addAll(t.getConnectedProperties());
			result.addAll(t.getRangeImProperties()); // TODO really
														// necessary?
			result.addAll(t.getRangeProperties());
			result.addAll(t.getExtraImProperties()); // TODO really
														// necessary?
			result.addAll(t.getExtraProperties());
			result.addAll(t.getDisjoints());
		}
		if (e.isProperty()) {
			X_Property p = (X_Property) e;
			result.addAll(p.getImplicitDomain());
			result.addAll(p.getExplicitDomain());
			result.addAll(p.getImplicitRange());
			result.addAll(p.getExplicitRange());
			result.addAll(p.getImplicitExtra());
			result.addAll(p.getExplicitExtra());
			result.addAll(OntologyCache.loadedIndividuals.values());
		}
		if (e.isInstance()) {
			AtomicX_Indivdual individual = (AtomicX_Indivdual) e;
			result.addAll(individual.getPartOf());
			for (X_Type d : individual.getClazz()) {
				HashSet<X_Property> properties = new HashSet<>();
				properties.addAll(d.getRangeProperties());
				properties.addAll(d.getRangeImProperties());
				properties.addAll(d.getExtraProperties());
				properties.addAll(d.getExtraImProperties());
				// iterate the properties and find out whether they are
				// actually effected
				for (X_Property p : properties) {
					HashSet<X_Type> domains = new HashSet<>();
					domains.addAll(p.getImplicitDomain());
					domains.addAll(p.getExplicitDomain());
					for (X_Type domain : domains) {
						result.addAll(domain.getConnectedIndividualsSorted());
					}
				}
			}
			for (CartesianX_Individual c : individual.getPartOf()) {
				for (X_Type d : c.getClazz()) {
					HashSet<X_Property> properties = new HashSet<>();
					properties.addAll(d.getRangeProperties());
					properties.addAll(d.getRangeImProperties());
					properties.addAll(d.getExtraProperties());
					properties.addAll(d.getExtraImProperties());
					// iterate the properties and find out whether they are
					// actually effected
					for (X_Property p : properties) {
						HashSet<X_Type> domains = new HashSet<>();
						domains.addAll(p.getImplicitDomain());
						domains.addAll(p.getExplicitDomain());
						for (X_Type domain : domains) {
							result.addAll(
									domain.getConnectedIndividualsSorted());
						}
					}
				}
			}
		}
		return result;
	}

	// Handle Individual Related Events

	/**
	 * 
	 * @param shortName
	 *            The name of the new individual.
	 * @param type
	 *            The {@link X_Type} of the new {@link X_Individual}.
	 * @return a new individual of the given name and the given {@link X_Type}.
	 */
	boolean handleExternIndividualAdd(ArrayList<String> shortName,
			X_Type type) {
		// create the new Individual
		return createNewIndividual(shortName, type, false);
	}

	/**
	 * 
	 * @param shortName
	 *            The name of the new individual.
	 * @return a new individual of the given name.
	 */
	boolean handleIndividualAdd(ArrayList<String> shortName) { // DONE
		// get the selected class, i.e. the parent class
		X_Type c = (X_Type) relatedOntology
				.getSelection(SelectionType.ICLASSHIERARCHY);
		X_Type cl = OntologyCache.getType(c.getShortName());
		// create the new Individual
		return createNewIndividual(shortName, cl, true);
	}

	/**
	 * Create a new individual of the given name and the given type represented.
	 * 
	 * @param shortName
	Create a new individual of the given name and the given type represented.
	 * @param index TODO
	 * 
	 * @return true, if the {@link X_Individual} could be created, false otherwise
	 */
	boolean handleIndividualAdd(ArrayList<String> shortName, int index) { // DONE
		// get the selected class, i.e. the parent class
		X_Type c = (X_Type) relatedOntology
				.getSelection(SelectionType.ICLASSHIERARCHY);
		X_Type cl = OntologyCache.getType(c.getShortName());
		X_Type subElement = ((CartesianX_Type) cl).getSubElements().get(index);
		// create the new Individual
		return createNewIndividual(shortName, subElement, true);
	}

	/**
	 * Create a new individual of the given name and the given type represented.
	 * @param shortName the name of the instance to be created
	 *
	 * @param type the type of the instance to be created
     * @return true,if the {@link X_Individual} could be created, false otherwise
     */
	boolean handleIndividualAdd(ArrayList<String> shortName, X_Type type) {
		return createNewIndividual(shortName, type, true);
	}

	/**
	 * Remove the given {@link X_Individual} element from the model.
	 * 
	 * @param individual
	 *            The {@link X_Individual} to be removed.
	 */
	void handleIndividualRemove(X_Individual individual) { // DONE
		for (X_Type clazz : individual.getClazz()) {
			// update the connected class
			this.removeTuplesFromTupleStore(clazz.getTupleRep());
			clazz.removeIndividual(individual);
			this.addNewTuplesToTupleStore(clazz.getTupleRep());
			// collect the properties that may be effected by the removal of the
			// instance
			HashSet<X_Property> properties = new HashSet<>();
			properties.addAll(clazz.getRangeProperties());
			properties.addAll(clazz.getRangeImProperties());
			properties.addAll(clazz.getExtraProperties());
			properties.addAll(clazz.getExtraImProperties());
			// iterate the properties and find out whether they are actually
			// effected
			for (X_Property p : properties) {
				int mode = p.getPropertyType().nextSetBit(0);
				HashSet<X_Type> domains = new HashSet<>();
				domains.addAll(p.getImplicitDomain());
				domains.addAll(p.getExplicitDomain());
				for (X_Type domain : domains) {
					for (X_Individual i : domain
							.getConnectedIndividualsSorted()) {
						if (i.getPopulatedProperties(mode).containsKey(p)) {
							Set<ArrayList<X_Individual>> values = new HashSet<>();
							values.addAll(
									i.getPopulatedProperties(mode).get(p));
							for (ArrayList<X_Individual> usedInstaces : values) {
								if (usedInstaces.contains(individual)) {
									this.removeTuplesFromTupleStore(
											i.getTupleRep());
									i.removePopulatedProperty(p, usedInstaces);
									this.addNewTuplesToTupleStore(
											i.getTupleRep());
								}
							}
						}
					}
				}
			}
		}
		if (individual.isAtomInstance()) {
			// in case the instance to be removed is an atom, we need to check
			// whether cart instances are effected
			for (X_Individual complex : ((AtomicX_Indivdual) individual)
					.getPartOf()) {
				handleIndividualRemove(complex);
			}
		} else {
			// in case the instance to be removed is cartesian, we need to
			// update its elements
			for (X_Individual sub : ((CartesianX_Individual) individual)
					.getContains()) {
				((AtomicX_Indivdual) sub).removePartOf(individual);
			}
		}
		removeTuplesFromTupleStore(individual.getTupleRep());
		OntologyCache.removeIndividual(individual);
		deletedEntities.add(individual);
	}

	private void removePopulatedProperties(X_Type type, X_Property p, int mode,
			boolean persistent) {
		int propertyType = p.getPropertyType().nextSetBit(0);
		if (mode == 0) {
			// 1) for each instance
			for (X_Individual i : type.getConnectedIndividualsSorted()) {
				if (i.getPopulatedProperties(propertyType).containsKey(p)) {
					// 2.1) remove instance from tuplestore ( if persistent)
					if (persistent) {
						removeTuplesFromTupleStore(i.getTupleRep());
					}
					// 2.2) remove populated prop from the instance
					i.removeConnectedProperty(p);
					// 2.3) add instance to tuplestore ( if persistent)
					if (persistent) {
						addNewTuplesToTupleStore(i.getTupleRep());
					}
				}
			}
		}
		if (mode == 1) {
			int arity = p.getArities()[1];
			HashSet<X_Type> domain = new HashSet<>();
			domain.addAll(p.getExplicitDomain());
			domain.addAll(p.getImplicitDomain());
			for (X_Type t : domain) {
				for (X_Individual i : t.getConnectedIndividualsSorted()) {
					if (i.getPopulatedProperties(propertyType).containsKey(p)) {
						Set<ArrayList<X_Individual>> pops = i
								.getPopulatedProperties(propertyType).get(p);

						for (ArrayList<X_Individual> pop : pops) {
							for (int j = 0; j < arity; j++) {

								if (pop.get(j).getClazz().contains(type)) {

									if (persistent) {
										removeTuplesFromTupleStore(
												i.getTupleRep());
									}
									// 2.2) remove populated prop from the
									// instance
									i.removeConnectedProperty(p);
									// 2.3) add instance to tuplestore ( if
									// persistent)
									if (persistent) {
										addNewTuplesToTupleStore(
												i.getTupleRep());
									}
								}
							}
						}
					}
				}
			}
		}
		if (mode == 2) {
			int arity = p.getArities()[1];

			// get effected instances (all instances of type t used as range in
			// p)
			for (X_Type t : p.getExplicitDomain()) {
				for (X_Individual i : t.getConnectedIndividualsSorted()) {
					if (i.getPopulatedProperties(propertyType).containsKey(p)) {
						Set<ArrayList<X_Individual>> pops = i
								.getPopulatedProperties(propertyType).get(p);
						for (ArrayList<X_Individual> pop : pops) {
							for (int j = arity; j < pop.size(); j++) {
								if (pop.get(j).getClazz().contains(type)) {
									if (persistent) {
										removeTuplesFromTupleStore(
												i.getTupleRep());
									}
									// 2.2) remove populated prop from the
									// instance
									i.removeConnectedProperty(p);
									// 2.3) add instance to tuplestore ( if
									// persistent)
									if (persistent) {
										addNewTuplesToTupleStore(
												i.getTupleRep());
									}
								}
							}
						}
					}
				}
			}
		}

	}

	/**
	 * Add the given {@link Annotation} to the model.
	 * 
	 * @param annotation
	 *            The {@link Annotation} to be added.
	 */
	void handleAnnotationAdded(Annotation annotation, boolean persistent) { // DONE
		if (annotation.getDomain() == relatedOntology) {
			relatedOntology.getAnnotations().add(annotation);
		} else {
			X_Entity effectedElement = null;
			if (annotation.getDomain().isType()) {
				effectedElement = OntologyCache
						.getType(annotation.getDomain().getShortName());
				effectedElement.addAnnotation(annotation);
			}
			if (annotation.getDomain().isProperty()) {
				effectedElement = OntologyCache
						.getProperty(annotation.getDomain().getShortName());
				effectedElement.addAnnotation(annotation);
			}
			if (annotation.getDomain().isInstance()) {
				effectedElement = OntologyCache
						.getIndividual(annotation.getDomain().getShortName());
				effectedElement.addAnnotation(annotation);
			}
		}
		if (persistent) {
			relatedOntology.addNewTupleToTupleStore(annotation.getTuple());
		}
	}

	/**
	 * Remove the given {@link Annotation} from the model.
	 * 
	 * @param annotation
	 *            The annotation to be removed.
	 */
	void handleAnnotationRemoved(Annotation annotation, boolean persistent) { // DONE
		if (annotation.getDomain() == relatedOntology) {
			relatedOntology.getAnnotations().remove(annotation);
		} else {
			X_Entity effectedElement = null;
			if (annotation.getDomain().isType()) {
				effectedElement = OntologyCache
						.getType(annotation.getDomain().getShortName());
				effectedElement.removeAnnotation(annotation);
			}
			if (annotation.getDomain().isProperty()) {
				effectedElement = OntologyCache
						.getProperty(annotation.getDomain().getShortName());
				effectedElement.removeAnnotation(annotation);
			}
			if (annotation.getDomain().isInstance()) {
				effectedElement = OntologyCache
						.getIndividual(annotation.getDomain().getShortName());
				effectedElement.removeAnnotation(annotation);
			}
		}
		if (persistent)
			relatedOntology.removeTupleFromTupleStore(annotation.getTuple());
	}

	/**
	 * This method makes all temporal changes persistent.
	 */
	void handleAccept() { // DONE -
		for (X_Type t : relatedOntology.getTempClasses()) {
			addNewTuplesToTupleStore(t.getTupleRep());
		}
		for (AbstractX_Property p : relatedOntology.getTempProperty()) {
			addNewTuplesToTupleStore(p.getTupleRep());
		}
		for (X_Individual i : relatedOntology.getTempInstances()) {
			addNewTuplesToTupleStore(i.getTupleRep());
		}
		relatedOntology.getTempClasses().clear();
		relatedOntology.getTempProperty().clear();
		relatedOntology.getTempInstances().clear();
	}

	/**
	 * This method undoes all temporal changes of the model.
	 */
	void handleCancel() { // DONE
		for (X_Type t : relatedOntology.getTempClasses()) {
			handleClassRemoved(t, false, true);
		}
		for (AbstractX_Property p : relatedOntology.getTempProperty()) {
			handlePropertyRemoved(p, false, true);
		}
		for (X_Individual i : relatedOntology.getTempInstances()) {
			handleIndividualRemove(i);
		}
		relatedOntology.getTempClasses().clear();
		relatedOntology.getTempProperty().clear();
		relatedOntology.getTempInstances().clear();
	}

	/**
	 * Declares that the given {@link AbstractX_Property}s are inverse.
	 */
	void handleInverseOfAdd(AbstractX_Property value,
			AbstractX_Property value2) { // Done
		removeTuplesFromTupleStore(value.getTupleRep());
		removeTuplesFromTupleStore(value2.getTupleRep());
		value.setInverse(value2);
		value2.setInverse(value);
		addNewTuplesToTupleStore(value.getTupleRep());
		addNewTuplesToTupleStore(value2.getTupleRep());
	}

	/**
	 * Removes the inverse {@link AbstractX_Property} from the given
	 * {@link AbstractX_Property}.
	 *
	 */
	void handleInverseDel(AbstractX_Property value) { // Done
		AbstractX_Property p = value.getInverse();
		removeTuplesFromTupleStore(value.getTupleRep());
		removeTuplesFromTupleStore(p.getTupleRep());
		p.setInverse(null);
		value.setInverse(null);
		addNewTuplesToTupleStore(value.getTupleRep());
		addNewTuplesToTupleStore(p.getTupleRep());
	}

	/**
	 * Add the Values specified by the {@link X_Entity}[] to the populated
	 * properties of the currently selected individual. Also updates the HFC
	 * tuplestore.
	 * 
	 * @param value
	 *            the populated property and its values. The first element is
	 *            the property itself, the remaining entries are the range and
	 *            extra arguments (if defined).
	 */
	void handleIndividualPropAdd(X_Entity[] value) { // DONE
		X_Individual indi = (X_Individual) relatedOntology
				.getSelection(SelectionType.INDIVIDUALHIERARCHY);
		removeTuplesFromTupleStore(indi.getTupleRep());
		ArrayList<X_Individual> values = new ArrayList<X_Individual>();
		AbstractX_Property p = (AbstractX_Property) value[0];
		for (int i = 1; i < value.length; i++) {
			if (value[i] != null) {
				values.add((X_Individual) value[i]);
			}
		}
		indi.addPopulatedProperty(p, values);
		if (p.getCharacteristics().get(3)) {
			X_Individual symIndi = (X_Individual) value[1];
			removeTuplesFromTupleStore(symIndi.getTupleRep());
			ArrayList<X_Individual> symvalues = new ArrayList<>();
			symvalues.add(indi);
			for (int i = 2; i < value.length; i++) {
				if (value[i] != null) {
					symvalues.add((X_Individual) value[i]);
				}
			}
			symIndi.addPopulatedProperty(p, symvalues);
			addNewTuplesToTupleStore(symIndi.getTupleRep());
		}
		addNewTuplesToTupleStore(indi.getTupleRep());
	}

	/**
	 * Removes the given populated properties from the currently selected
	 * individual. Also updates the HFC tuplestore.
	 * 
	 * @param value
	 *            the populated property to be removed.
	 */
	void handleIndividualPropRemove(AbstractX_Property value,
			ArrayList<X_Individual> values) { // DONE
		X_Individual indi = (X_Individual) relatedOntology
				.getSelection(SelectionType.INDIVIDUALHIERARCHY);
		removeTuplesFromTupleStore(indi.getTupleRep());
		indi.removePopulatedProperty(value, values);
		addNewTuplesToTupleStore(indi.getTupleRep());
	}

	/**
	 * Add the given type to the currently selected {@link X_Individual}.
	 * 
	 * @param value the {@link X_Type} to be added as additional type to the currently selected {@link X_Individual}.
	 */
	void handleIndividualTypeAdd(X_Type value) { // DONE
		X_Individual indi = (X_Individual) relatedOntology
				.getSelection(SelectionType.INDIVIDUALHIERARCHY);
		removeTuplesFromTupleStore(indi.getTupleRep());
		removeTuplesFromTupleStore(value.getTupleRep());
		indi.addClazz(value);
		value.addIndividual(indi);
		addNewTuplesToTupleStore(indi.getTupleRep());
		addNewTuplesToTupleStore(value.getTupleRep());
	}

	/**
	 * Remove the given types from the types of the currently selected
	 * {@link X_Individual}.
	 * 
	 * @param value the {@link X_Type} to be removed from the types that currently selected {@link X_Individual} instantiates.
	 */
	void handleIndividualTypeRemove(X_Type value) { // DONE
		X_Individual indi = (X_Individual) relatedOntology
				.getSelection(SelectionType.INDIVIDUALHIERARCHY);
		removeTuplesFromTupleStore(indi.getTupleRep());
		removeTuplesFromTupleStore(value.getTupleRep());
		value.removeIndividual(indi);
		indi.removeClazz(value);
		addNewTuplesToTupleStore(value.getTupleRep());
		addNewTuplesToTupleStore(indi.getTupleRep());
	}

	/**
	 * Changes the given attribute of the currently selected {@link X_Property}.
	 * 
	 * @param value
	 *            An integer value representing the attribute to be flipped.
	 * @return The attributes of the currently selected {@link X_Property}.
	 *
	 *         TODO use hfc rules to get rid side effects.
	 */
	BitSet handleCharChanged(int value) {
		// TODO update the effected individuals too
		X_Property p = (X_Property) relatedOntology
				.getSelection(SelectionType.PROPERTYHIERARCHY);
		removeTuplesFromTupleStore(
				(Set<? extends ArrayList<String>>) p.getCharacteristicTuple());
		updateChar(p, value);
		addNewTuplesToTupleStore(
				(Set<? extends ArrayList<String>>) p.getCharacteristicTuple());
		return p.getCharacteristics();
	}

	/**
	 * Update the given {@link Annotation} with the new value. The HFC
	 * tupleStore is updated too.
	 * 
	 * @param annotation the {@link Annotation} to be updated
	 * @param newValue the new value of the effected {@link Annotation}.
	 */
	void handleAnnotationEdited(Annotation annotation, String newValue,
			boolean persistent) { // DONE
		if (annotation.getDomain() == relatedOntology) {
			if (persistent)
				relatedOntology
						.removeTupleFromTupleStore(annotation.getTuple());
			relatedOntology.editAnnotation(annotation, newValue);
			annotation.setValue(newValue);
			relatedOntology.getAnnotations().add(annotation);
		} else {
			if (persistent)
				relatedOntology
						.removeTupleFromTupleStore(annotation.getTuple());
			X_Entity effectedElement = null;
			if (annotation.getDomain().isType()) {
				effectedElement = OntologyCache
						.getType(annotation.getDomain().getShortName());
				effectedElement.editAnnotation(annotation, newValue);
			}
			if (annotation.getDomain().isProperty()) {
				effectedElement = OntologyCache
						.getProperty(annotation.getDomain().getShortName());
				effectedElement.editAnnotation(annotation, newValue);
			}
			if (annotation.getDomain().isInstance()) {
				effectedElement = OntologyCache
						.getIndividual(annotation.getDomain().getShortName());
				effectedElement.editAnnotation(annotation, newValue);
			}
		}
		if (persistent)
			relatedOntology.addNewTupleToTupleStore(annotation.getTuple());
	}

	/**
	 * Updates the model and HFC tuplestore after a successful drag and drop
	 * action.
	 * 
	 * @param node The {@link X_Entity} that has been moved
	 * @param oldParent the former direct superClass of the moved node
	 * @param parent the new direct superClass of the moved node
	 */
	void handleNodeMoved(X_Entity node, X_Entity oldParent, X_Entity parent) {
		// seems to work fine for me ;)
		this.removeTuplesFromTupleStore(oldParent.getTupleRep());
		this.removeTuplesFromTupleStore(parent.getTupleRep());
		this.removeTuplesFromTupleStore(node.getTupleRep());
		oldParent.removeSub(node);
		parent.addSub(node);
		// remove from parent (side effects)
		// ####################################################################
		if (node.isType()) {
			TreeNodeGenerator.removeFromClassModels(node);
		} else {
			TreeNodeGenerator.removeFromPropertyModels(node);
		}

		// add to parent (side effects)
		// #############################################
		if (node.isType()) {
			TreeNodeGenerator.addToClassModels(node, parent);
		} else {
			TreeNodeGenerator.addToProptertyModels(node, parent);
		}

		// restructure connected indis, properties etc.
		// #################################################################

		if (node.isType()) {
			X_Type n = (X_Type) node;
			// remove the properties entailed from the old parent - triggered
			// using boolean flag
			if (strictMode) {
				for (X_Property p : ((X_Type) oldParent)
						.getConnectedProperties()) {
					removeTuplesFromTupleStore(p.getTupleRep());
					removeDependency(p, n, 0, true, false);
					addNewTuplesToTupleStore(p.getTupleRep());
				}
			}
			// add the properties to be entailed from the new parent
			for (X_Property p : ((X_Type) parent).getConnectedProperties()) {
				if (!n.getConnectedProperties().contains(p)) {
					removeTuplesFromTupleStore(p.getTupleRep());
					addDomain(p, n, true);
					addNewTuplesToTupleStore(p.getTupleRep());
				}
			}
		} else {
			// change the bitset of the Property, if necessary
			/**
			 * 000 default 100 mixed 010 data 001 object
			 */
			AbstractX_Property p = (AbstractX_Property) node;
			int nodeType = p.getPropertyType().nextSetBit(0);
			int parentType = ((AbstractX_Property) parent).getPropertyType()
					.nextSetBit(0);
			if (nodeType != parentType) {
				changePropertyType(p, nodeType, parentType);
				for (X_Entity sp : p.getAllSubs()) {
					changePropertyType((AbstractX_Property) sp, nodeType,
							parentType);
				}
			}
		}
		this.addNewTuplesToTupleStore(oldParent.getTupleRep());
		this.addNewTuplesToTupleStore(parent.getTupleRep());
		this.addNewTuplesToTupleStore(node.getTupleRep());
	}


	private void changePropertyType(AbstractX_Property p, int nodeType,
			int parentType) {
		p.getPropertyType().flip(parentType);
		p.getPropertyType().flip(nodeType);
		// move instances of populated properties from nodeType to
		// parentType
		// update instances of explicit types
		for (X_Type t : p.getExplicitDomain()) {
			for (X_Individual i : t.getConnectedIndividualsSorted()) {
				i.updateProperties(p, nodeType, parentType);
			}
		}
		for (X_Type t : p.getImplicitDomain()) {
			for (X_Individual i : t.getConnectedIndividualsSorted()) {
				i.updateProperties(p, nodeType, parentType);

			}
		}
	}

	/**
	 * Remove the given {@link X_Type} from the disjointClasses of the currently
	 * selected {@link X_Type}.
	 * 
	 * @param value
	 *            The currently selected {@link X_Type}.
	 * @param value2
	 *            The {@link X_Type} to be removed from the disjoint types.
	 */
	void handleDisjointRemoved(X_Entity value, Object value2) { // DONE
		X_Type selectedClass = (X_Type) value;
		X_Type disjointClass = (X_Type) value2;
		this.removeTuplesFromTupleStore(selectedClass.getTupleRep());
		this.removeTuplesFromTupleStore(disjointClass.getTupleRep());
		selectedClass.removeDisjoint(disjointClass);
		disjointClass.removeDisjoint(selectedClass);
		this.addNewTuplesToTupleStore(selectedClass.getTupleRep());
		this.addNewTuplesToTupleStore(disjointClass.getTupleRep());
	}

	// ############# HFC ################

	private void removeProperty(X_Property value, boolean persistent) {
		if (persistent) {
			removeTuplesFromTupleStore(value.getTupleRep());
		}
		// update parent

		// update explicit dependencies
		Set<X_Type> clone = new HashSet<>();
		clone.addAll(value.getExplicitDomain());
		for (X_Type t : clone) {
			removeDependency(value, t, 0, persistent, true);
		}
		clone.clear();
		clone.addAll(value.getExplicitRange());
		for (X_Type t : clone) {
			removeDependency(value, t, 1, persistent, true);
		}
		clone.clear();
		clone.addAll(value.getExplicitExtra());
		for (X_Type t : clone) {
			removeDependency(value, t, 2, persistent, true);
		}
		OntologyCache.removeProperties(value);
	}

	private void removeDependency(X_Property p, X_Type t, int mode,
			boolean persistent, boolean remove) {
		if (persistent) {
			removeTuplesFromTupleStore(t.getTupleRep());
			removeTuplesFromTupleStore(p.getTupleRep());
		}
		removePopulatedProperties(t, p, mode, persistent);
		if (mode == 0) {
			// update the property
			for (X_Entity se : p.getAllSubs()) {
				X_Property sp = (X_Property) se;
				if (persistent) {
					removeTuplesFromTupleStore(sp.getTupleRep());
				}
				removePopulatedProperties(t, sp, mode, persistent);
				sp.removeExplicitDomain(t);
				sp.removeImplicitDomain(t);
				if (persistent) {
					if (remove)
						return;
					addNewTuplesToTupleStore(sp.getTupleRep());
				}
			}
			p.removeExplicitDomain(t);
			p.removeImplicitDomain(t);
			// update the connected class
			for (X_Entity se : t.getAllSubs()) {
				X_Type st = (X_Type) se;
				if (persistent) {
					removeTuplesFromTupleStore(st.getTupleRep());
				}
				removePopulatedProperties(st, p, mode, persistent);
				st.removeExplicitProperty(p);
				st.removeImplicitProperty(p);
				if (persistent) {
					addNewTuplesToTupleStore(st.getTupleRep());
				}
			}
			t.removeExplicitProperty(p);
			t.removeImplicitProperty(p);
		}
		if (mode == 1) {
			// update the property
			for (X_Entity se : p.getAllSubs()) {
				X_Property sp = (X_Property) se;
				if (persistent) {
					removeTuplesFromTupleStore(sp.getTupleRep());
				}
				removePopulatedProperties(t, sp, mode, persistent);
				sp.removeExplicitRange(t);
				sp.removeImplicitRange(t);
				if (persistent) {
					if (remove)
						return;
					addNewTuplesToTupleStore(sp.getTupleRep());
				}
			}

			p.removeExplicitRange(t);
			p.removeImplicitRange(t);
			// update the class
			for (X_Entity se : t.getAllSubs()) {
				X_Type st = (X_Type) se;
				if (persistent) {
					removeTuplesFromTupleStore(st.getTupleRep());
				}
				removePopulatedProperties(st, p, mode, persistent);
				st.removeRangeOf(p);
				st.removeImplicitRangeOf(p);
				if (persistent) {
					addNewTuplesToTupleStore(st.getTupleRep());
				}
			}

			t.removeRangeOf(p);

			t.removeImplicitRangeOf(p);
		}
		if (mode == 2) {
			// update the property
			for (X_Entity se : p.getAllSubs()) {
				X_Property sp = (X_Property) se;
				if (persistent) {
					removeTuplesFromTupleStore(sp.getTupleRep());
				}
				removePopulatedProperties(t, sp, mode, persistent);
				sp.removeExplicitExtra(t);
				sp.removeImplicitExtra(t);
				if (persistent) {
					if (remove)
						return;
					addNewTuplesToTupleStore(sp.getTupleRep());
				}
			}

			p.removeExplicitExtra(t);

			p.removeImplicitExtra(t);
			// update the class
			for (X_Entity se : t.getAllSubs()) {
				X_Type st = (X_Type) se;
				if (persistent) {
					removeTuplesFromTupleStore(st.getTupleRep());
				}
				removePopulatedProperties(st, p, mode, persistent);
				st.removeExplicitExtraOf(p);
				st.removeImplicitExtraOf(p);
				if (persistent) {
					addNewTuplesToTupleStore(st.getTupleRep());
				}
			}

			t.removeExplicitExtraOf(p);

			t.removeImplicitExtraOf(p);
		}
		if (persistent) {
			addNewTuplesToTupleStore(t.getTupleRep());
			if (remove)
				return;
			addNewTuplesToTupleStore(p.getTupleRep());
		}
	}

	private void addProperty(X_Property newProp, AbstractX_Property parent) {
		// sub add explicit domain, range and extra of parent
		for (X_Type t : parent.getExplicitDomain()) {
			newProp.addExplicitDomain(t);
			t.addImplicitProperty(newProp);
		}
		for (X_Type t : parent.getExplicitRange()) {
			newProp.addExplicitRange(t);
			t.addImplicitRangeOf(newProp);
		}
		for (X_Type t : parent.getExplicitExtra()) {
			newProp.addExplicitExtra(t);
			t.addImplicitExtraOf(newProp);
		}
		// sub add implicit domain, range and extra of parent
		for (X_Type t : parent.getImplicitDomain()) {
			newProp.addImplicitDomain(t);
			t.addImplicitProperty(newProp);
		}
		for (X_Type t : parent.getImplicitRange()) {
			newProp.addImplicitRange(t);
			t.addImplicitRangeOf(newProp);
		}
		for (X_Type t : parent.getImplicitExtra()) {
			newProp.addImplicitExtra(t);
			t.addImplicitExtraOf(newProp);
		}

	}

	private void removeTuplesFromTupleStore(
			Set<? extends ArrayList<String>> tupleRep) {
		for (ArrayList<String> t : tupleRep) {
			relatedOntology.removeTupleFromTupleStore(t);
		}
	}

	private void addNewTuplesToTupleStore(
			Set<? extends ArrayList<String>> tupleRep) {
		for (ArrayList<String> t : tupleRep) {
			relatedOntology.addNewTupleToTupleStore(t);
		}
	}

	private void addDomain(X_Property p, X_Type t, boolean persistent) {
		Set<X_Entity> implicitProps = p.getAllSubs();
		Set<X_Entity> implicitClasses = t.getAllSubs();
		// update the Class
		addProperty2Class(p, implicitProps, t, persistent);
		// update the Property
		addDomain2Property(p, implicitClasses, t, persistent);
	}

	private void addProperty2Class(X_Property p, Set<X_Entity> implicitProps,
			X_Type t, boolean persistent) {
		// (1) prepare tuplestore
		if (persistent) {
			removeTuplesFromTupleStore(t.getTupleRep());
		}
		// (2) update the class itself
		t.addExplicitProperty(p);
		// update the effected individuals
		for (X_Individual i : t.getConnectedIndividualsSorted()) {
			if (persistent) {
				removeTuplesFromTupleStore(i.getTupleRep());
			}
			i.addConnectedProperty(p);
			if (persistent) {
				addNewTuplesToTupleStore(i.getTupleRep());
			}
		}
		for (X_Entity ip : implicitProps) {
			t.addImplicitProperty((X_Property) ip);
			for (X_Individual i : t.getConnectedIndividualsSorted()) {
				i.addConnectedProperty((X_Property) ip);
			}
		}
		// (3) update the subs of t
		for (X_Entity ct : t.getSubs()) {
			addProperty2Class(p, implicitProps, (X_Type) ct, persistent);
		}
		// (4) update tuplestore
		if (persistent) {
			addNewTuplesToTupleStore(t.getTupleRep());
		}

	}

	private void addDomain2Property(X_Property p, Set<X_Entity> implicitClasses,
			X_Type t, boolean persistent) {
		// (1) prepare tuplestore
		if (persistent) {
			removeTuplesFromTupleStore(p.getTupleRep());
		}
		// (2) update the class itself
		p.addExplicitDomain(t);
		for (X_Entity ic : implicitClasses)
			p.addImplicitDomain((X_Type) ic);
		// (3) update the subs of t
		for (X_Entity ct : p.getSubs()) {
			addDomain2Property((X_Property) ct, implicitClasses, t, persistent);
		}
		// (4) update tuplestore
		if (persistent) {
			addNewTuplesToTupleStore(p.getTupleRep());
		}
	}

	private void addRange(X_Property p, X_Type t, boolean persistent) {
		Set<X_Entity> implicitProps = p.getAllSubs();
		Set<X_Entity> implicitClasses = t.getAllSubs();
		// update the Class
		addRange2Class(p, implicitProps, t, persistent);
		// update the Property
		addRange2Property(p, implicitClasses, t, persistent);
	}

	private void addRange2Class(X_Property p, Set<X_Entity> implicitProps,
			X_Type t, boolean persistent) {
		// (1) prepare tuplestore
		if (persistent) {
			removeTuplesFromTupleStore(t.getTupleRep());
		}
		// (2) update the class itself
		t.addExplicitRangeOF(p);
		// update the effected individuals
		for (X_Entity ip : implicitProps) {
			t.addImplicitRangeOf((X_Property) ip);
		}
		// (3) update the subs of t
		for (X_Entity ct : t.getSubs()) {
			addRange2Class(p, implicitProps, (X_Type) ct, persistent);
		}
		// (4) update tuplestore
		if (persistent) {
			addNewTuplesToTupleStore(t.getTupleRep());
		}

	}

	private void addRange2Property(X_Property p, Set<X_Entity> implicitClasses,
			X_Type t, boolean persistent) {
		// (1) prepare tuplestore
		if (persistent) {
			removeTuplesFromTupleStore(p.getTupleRep());
		}
		// (2) update the class itself
		p.addExplicitRange(t);
		for (X_Entity ic : implicitClasses)
			p.addImplicitRange((X_Type) ic);
		// (3) update the subs of p
		for (X_Entity ct : p.getSubs()) {
			addRange2Property((X_Property) ct, implicitClasses, t, persistent);
		}
		// (4) update tuplestore
		if (persistent) {
			addNewTuplesToTupleStore(p.getTupleRep());
		}
	}

	private void addExtra(X_Property p, X_Type t, boolean persistent) {
		Set<X_Entity> implicitProps = p.getAllSubs();
		Set<X_Entity> implicitClasses = t.getAllSubs();
		// update the Class
		addExtra2Class(p, implicitProps, t, persistent);
		// update the Property
		addExtra2Property(p, implicitClasses, t, persistent);
	}

	private void addExtra2Class(X_Property p, Set<X_Entity> implicitProps,
			X_Type t, boolean persistent) {
		// (1) prepare tuplestore
		if (persistent) {
			removeTuplesFromTupleStore(t.getTupleRep());
		}
		// (2) update the class itself
		t.addExplicitExtraOf(p);
		// update the effected individuals
		for (X_Entity ip : implicitProps) {
			t.addImplicitExtraOf((X_Property) ip);
		}
		// (3) update the subs of t
		for (X_Entity ct : t.getSubs()) {
			addExtra2Class(p, implicitProps, (X_Type) ct, persistent);
		}
		// (4) update tuplestore
		if (persistent) {
			addNewTuplesToTupleStore(t.getTupleRep());
		}

	}

	private void addExtra2Property(X_Property p, Set<X_Entity> implicitClasses,
			X_Type t, boolean persistent) {
		// (1) prepare tuplestore
		if (persistent) {
			removeTuplesFromTupleStore(p.getTupleRep());
		}
		// (2) update the class itself
		p.addExplicitExtra(t);
		for (X_Entity ic : implicitClasses)
			p.addImplicitExtra((X_Type) ic);
		// (3) update the subs of t
		for (X_Entity ct : p.getSubs()) {
			addExtra2Property((X_Property) ct, implicitClasses, t, persistent);
		}
		// (4) update tuplestore
		if (persistent) {
			addNewTuplesToTupleStore(p.getTupleRep());
		}

	}

	/**
	 * Creates a new {@link X_Individual} for the given name and {@link X_Type}.
	 * If the {@link X_Type} is instanceOf {@link CartesianX_Type} a
	 * {@link CartesianX_Individual} is created. Else a
	 * {@link AtomicX_Indivdual} is created. The resulting {@link X_Individual}
	 * is already populated with the {@link AbstractX_Property}s connected to
	 * the {@link X_Type}.
	 * 
	 * @param shortName
	 *            The name of the {@link X_Individual}.
	 * @param cl
	 *            The {@link X_Type} of the {@link X_Individual}.
	 */
	private boolean createNewIndividual(ArrayList<String> shortName, X_Type cl,
			boolean persistent) {
		X_Individual newInd;
		if (shortName.size() > 1) {
			newInd = new CartesianX_Individual(shortName);
			for (AtomicX_Indivdual si : ((CartesianX_Individual) newInd)
					.getContains()) {
				si.addPartOf(newInd);
			}
		} else {
			if (!relatedOntology.isValidIndiName(shortName.get(0))) {
				String parentPrefix = cl.getShortName().get(0).split(":")[0];
				shortName.set(0,
						parentPrefix + shortName.get(0).replace("<", ":"));
			}
			newInd = new AtomicX_Indivdual(shortName);
		}
		if (OntologyCache.isUniqueName(newInd.render())) {
			newInd.addClazz(cl);
			newInd.addConnectedProperties(cl.getConnectedProperties());
			newInd.addConnectedProperties(cl.getConnectedImProperties());
			cl.addIndividual(newInd);
			OntologyCache.addIndividual(newInd);
			if (persistent)
				addNewTuplesToTupleStore(newInd.getTupleRep());
			else
				this.relatedOntology.getTempInstances().add(newInd);
			return true;
		} else {
			return false;
		}
	}

	// TODO use rulestore to implement this!
	private void updateChar(X_Property p, int value) {
		BitSet characteristics = p.getCharacteristics();
		characteristics.flip(value);
		switch (value) {
			case 2 : {
				break;
			}
			case 3 : {
				for (X_Type r : p.getExplicitRange()) {
					if (characteristics.get(3)) {
						removeTuplesFromTupleStore(r.getTupleRep());
						r.addExplicitProperty(p);
						//ToDo check whether any instances are effected
						updateEffectedInstances(true, p, r);
						addNewTuplesToTupleStore(r.getTupleRep());
					} else {
						removeTuplesFromTupleStore(r.getTupleRep());
						r.removeExplicitProperty(p);
						//ToDo check whether any instances are effected
						updateEffectedInstances(false, p, r);
						addNewTuplesToTupleStore(r.getTupleRep());
					}
				}

				if (characteristics.get(3) && characteristics.get(4))
					characteristics.flip(4);
				if (characteristics.get(value)) {
					handleInverseOfAdd(p, p);
				} else {
					if (p.getInverse() == p) {
						handleInverseDel(p);
					}
				};
				break;
			}
			case 4 : {
				if (characteristics.get(3) && characteristics.get(4))
					characteristics.flip(3);
				break;
			}
			case 5 : {
				if (characteristics.get(5) && characteristics.get(6))
					characteristics.flip(6);
				break;
			}
			case 6 : {
				if (characteristics.get(5) && characteristics.get(6))
					characteristics.flip(5);
				break;
			}
			default :
				break;
		}
	}

	private void updateEffectedInstances(boolean add, X_Property p, X_Type t){
		int mode = p.getPropertyType().nextSetBit(0);
		for(X_Type d: p.getExplicitDomain()){
			for(X_Individual i : d.getSubIndividualsSorted()){
				Set<ArrayList<X_Individual>> instances = i.getPopulatedProperties(mode).get(p);
				if (instances == null)
					continue;
				for (ArrayList<X_Individual> ii : instances){
					for (X_Individual iii : ii){
						Set<X_Type> clone = new HashSet<X_Type>(iii.getClazz());
						clone.retainAll(t.getAllSubs());
						if (!clone.isEmpty() || iii.getClazz().contains(t)){
							List<X_Individual> copy = ii.stream()
									.map(o -> {
										if(o == iii) {
											return i;
										} else {
											return o;
										}
									}).collect(toList());
							removeTuplesFromTupleStore(iii.getTupleRep());
							if(add){
								iii.addConnectedProperty(p);
								iii.addPopulatedProperty(p,(ArrayList<X_Individual>) copy );
							} else{
								iii.removeConnectedProperty(p);
							}
							addNewTuplesToTupleStore(iii.getTupleRep());
						}
					}
				}
			}
		}
	}

	public void setAccept(boolean b) {
		this.accept = b;
	}

	/**
	 * Updates the model (removes effected entities, ) and the hfc tuple store accordingly
	 * @param value the prefix to be removed from the namespace mapping
	 */
	public void handleNamespaceRemove(String value) {

		// sort the effected entities
		HashSet<X_Entity> effectedInstances = new HashSet<>();
		HashSet<X_Entity> effectedProperties = new HashSet<>();
		HashSet<X_Entity> effectedTypes = new HashSet<>();
		for (X_Entity ee : OntologyCache.getPrefixMapping(value)) {
			if (ee.isInstance()) {
				effectedInstances.add(ee);
				continue;
			}
			if (ee.isType()) {
				effectedTypes.add(ee);
				X_Type t = (X_Type) ee;
				for (X_Individual i : t.getConnectedIndividualsSorted()) {
					effectedInstances.add(i);
				}
				continue;
			}
			if (ee.isProperty()) {
				effectedProperties.add(ee);
				continue;
			}
		}
		// This order is important to ensure that all! references are removed
		// 1) remove effected instances from model
		for (X_Entity ei : effectedInstances) {
			if (!deletedEntities.contains(ei))
				handleIndividualRemove((X_Individual) ei);
		}
		// 2) remove effected properties from model
		for (X_Entity ep : effectedProperties) {
			if (!deletedEntities.contains(ep))
				handlePropertyRemoved(ep, true, false);
		}
		// 3) remove effected types from model
		for (X_Entity et : effectedTypes) {
			if (!deletedEntities.contains(et)) {
				if (et.isAtomType()) {
					if (!((AtomicX_Type) et).getComplexClasses().isEmpty()) {
						this.accept = true;
					}
					handleClassRemoved(et, true, false);
				}
			}
		}
		// clean ontology cache
		OntologyCache.removePrefixMapping(value);
		OntologyCache.getNamespace().removeMapping(value);

	}


	public void clear() {
		this.deletedEntities.clear();

	}

}
