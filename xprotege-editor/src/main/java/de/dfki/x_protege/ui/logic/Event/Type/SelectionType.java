package de.dfki.x_protege.ui.logic.Event.Type;

import de.dfki.x_protege.ui.components.ClassHierarchyViewComponent;
import de.dfki.x_protege.ui.logic.Event.SelectionChange;

/**
 * This {@link SelectionType}s are used within the {@link SelectionChange} to
 * identify the selection. e.g., CLASSHIERARCHY indicates a selection in the
 * {@link ClassHierarchyViewComponent}.
 * 
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 13.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public enum SelectionType {
	CLASSHIERARCHY, TEMPCLASSHIERARCHY, PROPERTYHIERARCHY, TEMPPROPERTYHIERARCHY, ICLASSHIERARCHY, TEMPICLASSHIERARCHY, INDIVIDUALHIERARCHY, SETCLASSHIERARCHY, SETINDIVIDUALLIST, SETINDIVIDUALHIERARCHY, XSDONLY, OBJECTONLY, SUPERCLASSHIERARCHY, TABLE, CARTCREATIONHIERARCHY, INDIVIDUALHIERARCHYREMOVE

}
