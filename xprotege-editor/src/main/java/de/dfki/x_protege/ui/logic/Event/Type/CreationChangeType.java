package de.dfki.x_protege.ui.logic.Event.Type;

import de.dfki.x_protege.ui.components.dialogue.ClassCreator;

/**
 * The {@link CreationChangeType} is used to encode actions in the
 * {@link ClassCreator}.
 * 
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 13.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public enum CreationChangeType {

	SELECTIONCHANGED, NODECHANGED, EDIT, EDITCHANGED

}
