package de.dfki.x_protege.ui.components;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JCheckBox;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.data.X_Type;
import de.dfki.x_protege.ui.components.lists.PropertiesList;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.SelectionChange;
import de.dfki.x_protege.ui.logic.Event.Type.SelectionType;
import de.dfki.x_protege.ui.utilities.rendering.X_ProtegeRenderer;

/**
 * The {@link X_ClassesPropertiesViewComponent} is used within the class-editor
 * to maintain the {@link de.dfki.x_protege.model.data.X_Property}s connected to the currently selected
 * class. To do so it uses an instance of {@link PropertiesList}.  <br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 20.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class X_ClassesPropertiesViewComponent
		extends
			AbstractX_SelectionViewComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2421738198117796221L;

	private PropertiesList list;

	private X_Type selectedClass;

	private Action showImplicitAction = new AbstractAction(
			"Show Implicit Assignments") {

		/**
		* 
		*/
		private static final long serialVersionUID = -4293977946218798052L;

		@Override
		public void actionPerformed(ActionEvent e) {
			list.setState(showImplicitBox.isSelected());
			updateView();
		}
	};

	private JCheckBox showImplicitBox;

	@Override
	protected void handleModelChanged(final ModelChange modelChanged) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {

				switch (modelChanged.getType()) {
					case CHARCHANGED : {
						updateView();
						break;
					}
					case DOMAINADDED : {
						updateView();
						break;
					}
					case DOMAINREMOVED : {
						updateView();
						break;
					}
					case RENAME : {
						updateView();
						break;
					}
					case ACCEPT : {
						updateView();
						break;
					}
					default :
						break;
				}
			}
		};
		SwingUtilities.invokeLater(doHighlight);
	}

	@Override
	protected void handleSelectionChanges(
			final SelectionChange selectionchanges) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {
				switch (selectionchanges.getType()) {
					case CLASSHIERARCHY : {
						if (selectionchanges.getValue() != null) {
							selectedClass = (X_Type) selectionchanges.getValue()
									.getValue();
							updateView();
						}
						break;
					}
					default :
						break;
				}
			}
		};
		SwingUtilities.invokeLater(doHighlight);

	}

	@Override
	public void initialiseView() throws Exception {
		setLayout(new BorderLayout());
		this.showImplicitBox = new JCheckBox(showImplicitAction);
		list = new PropertiesList(getX_EditorKit(), "Properties");
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
			}
		});
		add(new JScrollPane(list));
		list.setCellRenderer(new X_ProtegeRenderer());
		this.list.setState(this.showImplicitBox.isSelected());
		list.setRootObject(selectedClass());
		getX_ModelManager().addSelectionChangeListener(selChangeListener);
		getX_ModelManager().addModelChangeListener(modelChangeListener);
		add(showImplicitBox, BorderLayout.SOUTH);
		updateView();
	}

	private X_Type selectedClass() {
		if (this.selectedClass == null) {
			this.selectedClass = (X_Type) getX_ModelManager().getActiveModel()
					.getSelection(SelectionType.CLASSHIERARCHY);
		}
		return this.selectedClass;
	}

	@Override
	protected X_Entity updateView() {
		this.setHeaderText(selectedClass().render());
		this.list.setRootObject(this.selectedClass);
		this.revalidate();
		return selectedClass;
	}

	@Override
	public void disposeView() {
		getX_ModelManager()
				.removeSelectionChangeListener(this.selChangeListener);
		getX_ModelManager().removeModelChangeListener(modelChangeListener);
	}

}
