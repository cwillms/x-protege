package de.dfki.x_protege.ui.components.lists;

import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.Set;

import org.protege.editor.core.ui.list.MList;
import org.protege.editor.core.ui.list.MListSectionHeader;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.model.data.AbstractX_Property;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.data.X_Individual;
import de.dfki.x_protege.ui.components.dialogue.BasicPropertySelectionPanel;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.Type.ModelChangeType;
import de.dfki.x_protege.ui.utilities.UIHelper;
import de.dfki.x_protege.ui.utilities.rendering.X_ProtegeRenderer;

/**
 * The {@link EditorList} is used to present the populated Properties connected
 * to a Individual. <br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 17.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class EditorList extends MList {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1382819503142487320L;

	/**
	 * 
	 */
	private X_EditorKit x_EditorKit;

	/**
	 * 
	 */
	private X_Individual root;

	/**
	 * 
	 */
	private static final String HEADER_TEXT = "Object Properties";

	/**
	 * 
	 */
	private static final String HEADER2_TEXT = "Datatype Properties";

	/**
	 * 
	 */
	private static final String HEADER3_TEXT = "Mixed Properties";

	/**
	 * 
	 */
	private MListSectionHeader header = new MListSectionHeader() {

		@Override
		public String getName() {
			return HEADER_TEXT;
		}

		@Override
		public boolean canAdd() {
			return true;
		}

	};

	/*
	 * 
	 */
	private MListSectionHeader header2 = new MListSectionHeader() {

		@Override
		public String getName() {
			return HEADER2_TEXT;
		}

		@Override
		public boolean canAdd() {
			return true;
		}

	};

	/**
	 * 
	 */
	private MListSectionHeader header3 = new MListSectionHeader() {

		@Override
		public String getName() {
			return HEADER3_TEXT;
		}

		@Override
		public boolean canAdd() {
			return true;
		}

	};

	/**
	 * Creates a new instance of {@link EditorList}.
	 * 
	 * @param x_EditorKit2
	 *            the connected {@link X_EditorKit}.
	 */
	public EditorList(X_EditorKit x_EditorKit2) {
		this.x_EditorKit = x_EditorKit2;
		setCellRenderer(new X_ProtegeRenderer());
	}

	@SuppressWarnings("unchecked")
	public void setRootObject(X_Individual selectedIndividual) {
		this.root = selectedIndividual;

		java.util.List<Object> data = new ArrayList<Object>();

		data.add(header3);

		if (root != null) {
			for (Entry<AbstractX_Property, Set<ArrayList<X_Individual>>> e : root
					.getPopulatedProperties(0).entrySet()) {
				for (ArrayList<X_Individual> e1 : e.getValue()) {
					data.add(new PropOFIndiListItem(this.x_EditorKit,
							e.getKey(), e1));
				}
			}
		}

		data.add(header);

		if (root != null) {
			for (Entry<AbstractX_Property, Set<ArrayList<X_Individual>>> e : root
					.getPopulatedProperties(2).entrySet()) {
				for (ArrayList<X_Individual> e1 : e.getValue()) {
					data.add(new PropOFIndiListItem(this.x_EditorKit,
							e.getKey(), e1));
				}
			}
		}

		data.add(header2);

		if (root != null) {
			for (Entry<AbstractX_Property, Set<ArrayList<X_Individual>>> e : root
					.getPopulatedProperties(1).entrySet()) {
				for (ArrayList<X_Individual> e1 : e.getValue()) {
					data.add(new PropOFIndiListItem(this.x_EditorKit,
							e.getKey(), e1));
				}
			}
		}

		setListData(data.toArray());
		revalidate();
	}

	@Override
	protected void handleAdd() {
		int returnValue;
		String name = ((MListSectionHeader) this.getSelectedValue()).getName();
		if (root != null) {
			switch (name) {
				case "Mixed Properties" :
					BasicPropertySelectionPanel mp = new BasicPropertySelectionPanel(
							x_EditorKit, root, HEADER3_TEXT, 0);
					returnValue = new UIHelper(x_EditorKit)
							.showDialog(mp.header, mp);
					if (returnValue == 0) {
						X_Entity[] value = mp.getSelection();
						if (value[0] == null || value[1] == null) {
							warn();
						} else {
							ModelChange modelChange = new ModelChange(
									ModelChangeType.INDIVIDUALPADD, value);
							x_EditorKit.getX_ModelManager()
									.handleChange(modelChange);
						}
					} else {
						ModelChange modelChange = new ModelChange(
								ModelChangeType.CANCEL, null);
						x_EditorKit.getX_ModelManager()
								.handleChange(modelChange);
					}
					break;
				case "Object Properties" :
					BasicPropertySelectionPanel op = new BasicPropertySelectionPanel(
							x_EditorKit, root, HEADER2_TEXT, 2);
					returnValue = new UIHelper(x_EditorKit)
							.showDialog(op.header, op);
					if (returnValue == 0) {
						X_Entity[] value = op.getSelection();
						if (value[0] == null || value[1] == null) {
							warn();
						} else {
							ModelChange modelChange = new ModelChange(
									ModelChangeType.INDIVIDUALPADD, value);
							x_EditorKit.getX_ModelManager()
									.handleChange(modelChange);
						}
					} else {
						ModelChange modelChange = new ModelChange(
								ModelChangeType.CANCEL, null);
						x_EditorKit.getX_ModelManager()
								.handleChange(modelChange);
					}
					break;
				case "Datatype Properties" :
					BasicPropertySelectionPanel dp = new BasicPropertySelectionPanel(
							x_EditorKit, root, HEADER2_TEXT, 1);
					returnValue = new UIHelper(x_EditorKit)
							.showDialog(dp.header, dp);
					if (returnValue == 0) {
						X_Entity[] value = dp.getSelection();
						if (value[0] == null || value[1] == null) {
							warn();
						} else {
							ModelChange modelChange = new ModelChange(
									ModelChangeType.INDIVIDUALPADD, value);
							x_EditorKit.getX_ModelManager()
									.handleChange(modelChange);
						}
					} else {
						ModelChange modelChange = new ModelChange(
								ModelChangeType.CANCEL, null);
						x_EditorKit.getX_ModelManager()
								.handleChange(modelChange);
					}
					break;
				default :
					System.err.println(
							"Something is wrong. I do not know the header "
									+ name);
					break;
			}
		}
	}

	private void warn() {
		new UIHelper(x_EditorKit).showError("Individual Editor",
				"Your property configuration is not valid ...");
	}
}