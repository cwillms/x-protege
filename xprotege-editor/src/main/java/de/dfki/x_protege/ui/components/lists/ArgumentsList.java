package de.dfki.x_protege.ui.components.lists;

import java.util.Set;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.model.data.AbstractX_Property;
import de.dfki.x_protege.model.data.X_Type;

/**
 * The {@link ArgumentsList} is used to present the Extra-Arguments (e.i.,
 * instances of {@link X_Type} represented by a {@link ClassItem}) connected to
 * a Property.<br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 17.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class ArgumentsList extends ClassList {

	private static final long serialVersionUID = -6170705274321991344L;

	/**
	 * Creates a new instance of {@link ArgumentsList}.
	 * 
	 * @param x_EditorKit
	 *            The connected {@link X_EditorKit}.
	 */
	public ArgumentsList(X_EditorKit x_EditorKit) {
		super(x_EditorKit);
		this.HEADER_TEXT = "nary:argument";
	}

	/**
	 * Creates a new instance of {@link ArgumentsList}.
	 * 
	 * @param editorKit
	 *            The connected {@link X_EditorKit}.
	 * @param b
	 *            {@link Boolean} flag indicating whether the list is part of an
	 *            temporal dialogue.
	 */
	public ArgumentsList(X_EditorKit editorKit, boolean b) {
		super(editorKit, b);
		this.HEADER_TEXT = "nary:argument";
	}

	@Override
	protected Set<X_Type> getElements(AbstractX_Property root2) {
		return root2.getExplicitExtra();
	}

	@Override
	protected Set<X_Type> getImElements(AbstractX_Property root) {
		return root.getImplicitExtra();
	}

}
