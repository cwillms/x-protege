/**
 * 
 */
package de.dfki.x_protege.ui.logic.listener;

import de.dfki.x_protege.ui.logic.Event.QueryEvent;

/**
 * The {@link QueryEventListener} is only used by the components of the
 * QueryTab. It listens to so called {@link QueryEvents}s and updates the
 * connected Component according to this events. <br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 22.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public abstract class QueryEventListener implements EventListener {

	/**
	 * Handles the given {@link QueryEvent} and updates the view afterwards.
	 * 
	 * @param modelChanged The {@link QueryEvent} to be handled. This are mainly queries, which must be forwarded to hfc.
	 */
	public abstract void queryEvent(QueryEvent query);

}
