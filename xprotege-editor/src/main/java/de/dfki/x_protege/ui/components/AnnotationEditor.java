package de.dfki.x_protege.ui.components;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.protege.editor.core.ui.util.ComponentFactory;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.model.OntologyCache;
import de.dfki.x_protege.model.data.Annotation;
import de.dfki.x_protege.model.data.AnnotationProperty;
import de.dfki.x_protege.model.data.AtomicX_Type;
import de.dfki.x_protege.ui.components.lists.AnnotationList;
import de.dfki.x_protege.ui.components.lists.AnnotationPropertyList;
import de.dfki.x_protege.ui.utilities.rendering.ComboBoxRenderer;
import de.dfki.x_protege.ui.utilities.rendering.X_ProtegeRenderer;

/**
 * The {@link AnnotationEditor} allows the creation/manipulation of
 * {@link Annotation}s. It is called from within the {@link AnnotationList}.
 * <br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 20.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class AnnotationEditor extends JPanel implements ActionListener {

	private static final long serialVersionUID = -3695602443467187604L;
	private X_EditorKit x_EditorKit;
	private AnnotationPropertyList list;
	private JTextArea textField;
	private final AtomicX_Type[] types = OntologyCache.getDataTypes();
	private AtomicX_Type selectedType;
	@SuppressWarnings("unused")
	private AtomicX_Type inputType;
	private AnnotationProperty selectedProperty;
	@SuppressWarnings("unused")
	private boolean editMode = false;
	@SuppressWarnings({"unchecked", "rawtypes"})
	private JComboBox typeList = new JComboBox(types);;

	/**
	 * Creates a new instance of {@link AnnotationEditor}.
	 * 
	 * @param x_EditorKit The {@link X_EditorKit} that is currently used to maintain this session of x-protege.
	 */
	public AnnotationEditor(X_EditorKit x_EditorKit) {
		this.x_EditorKit = x_EditorKit;
		initialise();
	}

	private void initialise() {
		setLayout(new BorderLayout());
		JSplitPane splitti = new JSplitPane();
		splitti.setDoubleBuffered(true);
		splitti.setLeftComponent(creatLeftComponent());
		splitti.setRightComponent(creatRightComponent());
		add(splitti);
	}

	private Component creatLeftComponent() {
		JPanel left = new JPanel();
		left.setLayout(new BorderLayout());
		left.add(createLowerComponent(), BorderLayout.CENTER);
		return left;
	}

	private Component creatRightComponent() {
		JPanel right = new JPanel();
		right.setLayout(new BorderLayout());
		right.add(createRightHeaderPanel(), BorderLayout.NORTH);
		this.textField = new JTextArea();
		right.add(ComponentFactory.createScrollPane(textField));
		return right;
	}

	private Component createLowerComponent() {
		JPanel center = new JPanel(new BorderLayout());
		list = new AnnotationPropertyList(x_EditorKit);
		list.setCellRenderer(new X_ProtegeRenderer());
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.getSelectionModel()
				.addListSelectionListener(new ListSelectionListener() {

					@Override
					public void valueChanged(ListSelectionEvent e) {
						if (e.getLastIndex() - 1 >= 0)
							selectedProperty = list
									.getElement(e.getLastIndex() - 1); // -1

					}
				});
		JScrollPane scroll = new JScrollPane(list);
		scroll.setHorizontalScrollBarPolicy(
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		center.add(scroll, BorderLayout.CENTER);
		return center;
	}

	@SuppressWarnings("unchecked")
	private Component createRightHeaderPanel() {
		JPanel ontologyIRIPanel = new JPanel(new GridBagLayout());
		Insets insets = new Insets(0, 4, 2, 0);
		// Type selection for annotation editor
		typeList.setSelectedIndex(0);
		typeList.setBackground(Color.WHITE);
		typeList.setRenderer(new ComboBoxRenderer());
		typeList.addActionListener(this);
		ontologyIRIPanel.add(new JLabel("Annotation Type: "));
		ontologyIRIPanel.add(typeList,
				new GridBagConstraints(1, 0, 1, 1, 100.0, 0.0,
						GridBagConstraints.BASELINE_LEADING,
						GridBagConstraints.HORIZONTAL, insets, 0, 0));
		return ontologyIRIPanel;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void actionPerformed(ActionEvent e) {
		JComboBox cb = (JComboBox) e.getSource();
		AtomicX_Type type = (AtomicX_Type) cb.getSelectedItem();
		selectedType = type;
		inputType = type;
	}

	/**
	 * 
	 * @return the selected {@link AnnotationProperty}.
	 */
	public AnnotationProperty getAnnotationProperty() {
		return this.selectedProperty;
	}

	/**
	 * 
	 * @return the value assigned to the {@link Annotation}.
	 */
	public String getAnnotationValue() {
		return this.textField.getText();
	}

	/**
	 * 
	 * @return the type of the value of the {@link Annotation}.
	 */
	public AtomicX_Type getAnnotationValueType() {
		return this.selectedType;
	}

	/**
	 * Loads the {@link AnnotationEditor} with the given {@link Annotation}.
	 * Only the assigned value can be changed.
	 * 
	 * @param annotation the {@link Annotation} to be edited.
	 */
	public void editMode(Annotation annotation) {
		this.editMode = true;
		this.textField.setText(annotation.getValue().split("^^")[0]);
		this.list.setEnabled(false);
		this.typeList.setEnabled(false);

	}
}
