package de.dfki.x_protege.ui.logic.Event;

import java.util.ArrayList;

import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.ui.logic.Event.Type.QueryEventType;

/**
 * {@link QueryEvents} represent Events related to the QueryTab. <br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 22.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class QueryEvent extends Event {

	private final QueryEventType type;

	private final ArrayList<X_Entity> value2;

	/**
	 * Creates a new instance of {@link QueryEvent}, specified by the given
	 * parameters.
	 * 
	 * @param value
	 *            the {@link Object} that actually changed or causes effects ( mainly queries)
	 * @param type
	 *            {@link QueryEventType} indicating the kind of change featured by this event.
	 * @param value2
	 *            Additional information describing/specifying the event.
	 */
	public QueryEvent(Object value, QueryEventType type, ArrayList<X_Entity> value2) {
		super(value);
		this.type = type;
		this.value2 = value2;
	}

	/**
	 *
	 * @return {@link QueryEventType} indicating the kind of change featured by this event.
     */
	public QueryEventType getType() {
		return this.type;
	}

	public ArrayList<X_Entity> getValue2() {
		return this.value2;
	}

}
