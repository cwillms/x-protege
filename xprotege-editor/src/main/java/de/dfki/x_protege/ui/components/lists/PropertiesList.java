package de.dfki.x_protege.ui.components.lists;

import java.util.ArrayList;

import org.protege.editor.core.ui.list.MList;
import org.protege.editor.core.ui.list.MListSectionHeader;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.model.data.AbstractX_Property;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.data.X_Type;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.Type.ModelChangeType;

/**
 * The {@link PropertiesList} is an essential part of all tabs of the x-protégé
 * workspace. It is used to represent {@link AbstractX_Property}s connected to
 * an {@link X_Entity}.<br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 17.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class PropertiesList extends MList {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2855980597166995224L;
	private X_EditorKit editorKit;
	private String HEADER_TEXT;
	private boolean canAdd = true;
	private boolean state = false;
	private boolean temp = false;
	private MListSectionHeader header = new MListSectionHeader() {

		@Override
		public String getName() {
			return HEADER_TEXT;
		}

		@Override
		public boolean canAdd() {
			return canAdd;
		}

	};

	private MListSectionHeader imHeader = new MListSectionHeader() {

		@Override
		public String getName() {
			return "Implicit:";
		}

		@Override
		public boolean canAdd() {
			return false;
		}
	};
	private X_Type root;

	/**
	 * Creates a new instance of {@link PropertiesList}.
	 * 
	 * @param x_EditorKit
	 *            The connected {@link X_EditorKit}.
	 */
	public PropertiesList(X_EditorKit x_EditorKit, String header) {
		this.editorKit = x_EditorKit;
		this.HEADER_TEXT = header;
	}

	/**
	 * Creates a new instance of {@link PropertiesList}.
	 * 
	 * @param x_EditorKit
	 *            The connected {@link X_EditorKit}.
	 * @param temp
	 *            {@link Boolean} flag indicating whether the list is part of an
	 *            temporal dialogue.
	 */
	public PropertiesList(X_EditorKit x_EditorKit, String header, boolean b) {
		this.editorKit = x_EditorKit;
		this.HEADER_TEXT = header;
		this.temp = b;
	}

	/**
	 * Populates the list with the {@link AbstractX_Property}s connected to the
	 * given {@link X_Type}.
	 * 
	 * @param root the {@link X_Type} used to populate the list
	 */
	@SuppressWarnings("unchecked")
	public void setRootObject(X_Type root) {

		java.util.List<Object> data = new ArrayList<>();
		this.root = root;
		data.add(header);

		if (root != null) {
			// @@TODO ordering
			for (AbstractX_Property prop : root.getConnectedProperties()) {
				data.add(new PropertiesListItem(prop, this, true));
			}
			if (state) {
				data.add(imHeader);
				for (AbstractX_Property prop : root
						.getConnectedImProperties()) {
					data.add(new PropertiesListItem(prop, this, false));
				}
			}
		}

		setListData(data.toArray());
		revalidate();
	}

	@Override
	protected void handleAdd() {
		this.editorKit.getX_Workspace().getEventUtil()
				.addProperty(this.HEADER_TEXT, temp);
	}

	public void handleDelete(AbstractX_Property prop) {
		if (temp) {
			this.editorKit.getX_ModelManager().handleChange(new ModelChange(
					ModelChangeType.TEMPDOMAINREMOVE, prop, root));
		} else {
			this.editorKit.getX_ModelManager().handleChange(
					new ModelChange(ModelChangeType.DOMAINREMOVED, prop, root));
		}
	}

	public void setCanAdd(boolean canAdd) {
		this.canAdd = canAdd;
	}

	public void setState(boolean state) {
		this.state = state;
	}
}
