package de.dfki.x_protege.ui.logic.Event.Type;

/**
 * This {@link OntoChangeTypes}s are used to identify the kind of change. e.g.,
 * NAMESPACEEDITED indicates a change of the ontologie's namespace .
 * 
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 13.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public enum OntoChangeTypes {

	IDCHANGED, NAMESPACEEDITED, ONTOSTORED, PREFIX, INFIX, VERSIONCHANGED

}
