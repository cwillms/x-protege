package de.dfki.x_protege.ui.components.tree;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;

import javax.swing.JComponent;
import javax.swing.plaf.basic.BasicTreeUI;
import javax.swing.tree.TreePath;

/**
 * An X-Protege specific implementation of the {@link BasicTreeUI}
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 07.03.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class X_ProtegeTreeUI extends BasicTreeUI {

	private final Color highlight = new Color(153, 204, 255, 155); // light blue

	/**
	 * Paints a vertical line.
	 */
	@Override
	protected void paintVerticalLine(Graphics g, JComponent c, int x, int top, int bottom) {
		drawDashedVerticalLine(g, x, top, bottom);
	}

	/**
	 * Paints a horizontal line.
	 */
	@Override
	protected void paintHorizontalLine(Graphics g, JComponent c, int y, int left, int right) {
		drawDashedHorizontalLine(g, y, left, right);
	}

	@Override
	protected void paintRow(Graphics g, Rectangle clipBounds, Insets insets, Rectangle bounds, TreePath path, int row,
			boolean isExpanded, boolean hasBeenExpanded, boolean isLeaf) {
		if (tree.isRowSelected(row)) {
			g.setColor(this.highlight);
			g.fillRect(0, row * tree.getRowHeight(), tree.getWidth(), tree.getRowHeight());
		}

		super.paintRow(g, clipBounds, insets, bounds, path, row, isExpanded, hasBeenExpanded, isLeaf);
	}
}
