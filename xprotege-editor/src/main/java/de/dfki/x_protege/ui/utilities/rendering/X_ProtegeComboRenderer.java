package de.dfki.x_protege.ui.utilities.rendering;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import de.dfki.x_protege.model.OntologyCache;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.data.X_Type;
import de.dfki.x_protege.ui.utilities.X_Icons;

/**
 * 
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 13.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class X_ProtegeComboRenderer extends JLabel implements ListCellRenderer<X_Type> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7905174952812771688L;

	@Override
	public Component getListCellRendererComponent(JList<? extends X_Type> list, X_Type value, int index,
			boolean isSelected, boolean cellHasFocus) {
		if (value != null) {
			setIcon(selectIcon(value));
			setText(value.render());
		}
		return this;
	}

	private Icon selectIcon(X_Entity e) {
		if (e.isAtomType()) {
			if (e.equals(OntologyCache.getAllType())) {
				return X_Icons.allTypes;
			} else {
				return X_Icons.owlType;
			}
		}
		if (e.isCartesianType()) {
			return X_Icons.cartType;
		}
		return X_Icons.defaultIcon;
	}

}
