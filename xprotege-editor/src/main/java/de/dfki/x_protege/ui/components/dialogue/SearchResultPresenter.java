package de.dfki.x_protege.ui.components.dialogue;

import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.JPanel;

import org.protege.editor.core.ui.util.ComponentFactory;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.data.X_Individual;
import de.dfki.x_protege.model.data.X_Property;
import de.dfki.x_protege.model.data.X_Type;
import de.dfki.x_protege.ui.components.lists.FindingListItem;
import de.dfki.x_protege.ui.components.lists.FindingsList;
import de.dfki.x_protege.ui.logic.search.EntitiyFinder;

/**
 * The {@link SearchResultPresenter} is used to present those matchings computed
 * by the {@link EntitiyFinder}. It uses an instance of {@link FindingsList} to
 * present the matching @link X_Type}s, {@link X_Property}s and
 * {@link X_Individual}s. <br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 20.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class SearchResultPresenter extends JPanel {

	private static final long serialVersionUID = 1L;
	private FindingsList resultList;

	/**
	 * Creates a new instance of {@link SearchResultPresenter}.
	 * 
	 * @param editorKit
	 *            The connected {@link X_EditorKit}.
	 * @param searchResults
	 *            An {@link ArrayList} containing the each one {@link ArrayList}
	 *            of {@link X_Entity}s for {@link X_Type}s,
	 *            {@link X_Property}s and {@link X_Individual}s
	 *            matching the searchTerm.
	 * @param searchTerm
	 *            A {@link String} representing the term of search.
	 */
	public SearchResultPresenter(X_EditorKit editorKit, ArrayList<ArrayList<X_Entity>> searchResults,
			String searchTerm) {
		this.resultList = new FindingsList(editorKit, searchTerm);
		resultList.setRootObject(searchResults);
		this.setLayout(new BorderLayout());
		this.add(ComponentFactory.createScrollPane(resultList));
	}

	/**
	 * Returns the {@link X_Entity} currently selected in the underlying
	 * {@link FindingsList}.
	 * 
	 * @return
	 */
	public X_Entity getSelectedEntity() {
		return ((FindingListItem) this.resultList.getSelectedValue()).getSource();
	}
}
