package de.dfki.x_protege.ui.components.lists;

import org.protege.editor.core.ui.list.MListItem;

import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.ui.logic.search.EntitiyFinder;

/**
 * The {@link FindingListItem} is used within the {@link FindingsList}. It
 * represents the {@link X_Entity}s computed by {@link EntitiyFinder}. The
 * {@link FindingListItem}s are read only.<br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 17.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class FindingListItem implements MListItem {

	private final X_Entity source;

	/**
	 * Creates new instance of {@link FindingListItem}.
	 * 
	 * @param s
	 *            The {@link X_Entity} to be represented.
	 */
	public FindingListItem(X_Entity s) {
		this.source = s;
	}

	@Override
	public boolean isEditable() {
		return false;
	}

	@Override
	public void handleEdit() {
		// Nothing to do here
	}

	@Override
	public boolean isDeleteable() {
		return false;
	}

	@Override
	public boolean handleDelete() {
		return false;
	}

	@Override
	public String getTooltip() {
		return null;
	}

	public X_Entity getSource() {
		return this.source;
	}

}
