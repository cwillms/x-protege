package de.dfki.x_protege.ui.components;

import java.awt.Component;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;

import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.KeyStroke;

import org.apache.log4j.Logger;
import org.protege.editor.core.ui.view.ViewComponent;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.model.X_ModelManager;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.ui.X_Workspace;

/**
 * 
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 20.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public abstract class AbstractX_ViewComponent extends ViewComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3089829405015180530L;

	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(AbstractX_ViewComponent.class);

	/**
	 *
	 * @return The {@link X_ModelManager} associated to the represented ontology.
     */
	public X_ModelManager getX_ModelManager() {
		return (X_ModelManager) getX_Workspace().getEditorKit().getModelManager();
	}

	/**
	 *
	 * @return The {@link X_EditorKit} that is currently running/used.
     */
	public X_EditorKit getX_EditorKit() {
		return (X_EditorKit) getX_Workspace().getEditorKit();
	}

	protected abstract void initializeView() throws Exception;

	@Override
	final public void initialise() throws Exception {
		initializeView();

		prepareCopyable();

		preparePasteable();

		prepareCuttable();

		// add refresh components here (perhaps) when the class trees don't do
		// their crazy stuff
	}

	private void prepareCopyable() {
		// The "global" copy action should take precedence over anything, so
		// remove any
		// copy key bindings from children
		// Remove copy from the input map of any children
		removeFromInputMap(KeyStroke.getKeyStroke(KeyEvent.VK_C, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()),
				this);
	}

	private void preparePasteable() {
		// The "global" paste action should take precedence over anything, so
		// remove any
		// paste action key bindings from children
		removeFromInputMap(KeyStroke.getKeyStroke(KeyEvent.VK_V, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()),
				this);
	}

	private void prepareCuttable() {
		// The "global" cut action should take precedence over anything, so
		// remove any
		// cut action key bindings from children
		removeFromInputMap(KeyStroke.getKeyStroke(KeyEvent.VK_X, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()),
				this);
	}

	private static void removeFromInputMap(KeyStroke ks, JComponent c) {
		// Most likely stored in the ancestor of focused component map,
		// but...
		removeKeyBinding(c.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT), ks);
		removeKeyBinding(c.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW), ks);
		removeKeyBinding(c.getInputMap(JComponent.WHEN_FOCUSED), ks);
		// Process children recursively
		for (Component child : c.getComponents()) {
			if (child instanceof JComponent) {
				removeFromInputMap(ks, (JComponent) child);
			}
		}
	}

	private static void removeKeyBinding(InputMap im, KeyStroke ks) {
		// Remove the key binding from the second "tier" input map
		// User bindings are stored in the first tier, where as UI
		// bindings (installed by the LAF) are stored in the second tier;
		if (im.getParent() != null) {
			im.getParent().remove(ks);
		}
	}

	@Override
	final public void dispose() {
		disposeView();
	}

	/**
	 * This method is called at the end of a plugin life cycle, when the plugin
	 * needs to be removed from the system. Plugins should remove any listeners
	 * that they setup and perform other cleanup, so that the plugin can be
	 * garbage collected.
	 */
	protected abstract void disposeView();

	/**
	 *
	 * @return The {@link X_Workspace} associated to the currently represented ontology.
     */
	public X_Workspace getX_Workspace() {
		return (X_Workspace) getWorkspace();
	}

	protected X_Entity getObjectToCopy() {
		return null;
	}

}
