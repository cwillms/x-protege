package de.dfki.x_protege.ui.components.lists;

import java.util.ArrayList;

import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

import org.protege.editor.core.ui.list.MList;
import org.protege.editor.core.ui.list.MListSectionHeader;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.model.data.AtomicX_Type;
import de.dfki.x_protege.model.data.CartesianX_Type;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.data.X_Type;
import de.dfki.x_protege.ui.components.dialogue.ClassCreator;
import de.dfki.x_protege.ui.utilities.selectionFrames.ClassSelectionFrame;

/**
 * The {@link SelectionList} is used within the {@link ClassCreator} to present
 * those {@link X_Type} the new {@link CartesianX_Type} will consist of. <br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 18.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class SelectionList extends MList
		implements
			TreeSelectionListener,
			ListSelectionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5211978937387926966L;
	private X_EditorKit editorKit;
	private final boolean isNaryThing;
	private CartesianX_Type root;
	private String HEADER_TEXT;
	private SelectionListItem selectedItem = null;
	private MListSectionHeader header = new MListSectionHeader() {

		@Override
		public String getName() {
			return HEADER_TEXT;
		}

		@Override
		public boolean canAdd() {
			return isNaryThing;
		}

	};

	/**
	 * Creates a new instance of {@link SelectionList}.
	 * 
	 * @param x_EditorKit
	 *            The connected {@link X_EditorKit}.
	 * @param isNaryThing
	 *            A {@link Boolean} flag indicating whether the subelements of
	 *            naryThing+ are to be presented.
	 */
	public SelectionList(X_EditorKit x_EditorKit, boolean isNaryThing) {
		this.getSelectionModel()
				.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.isNaryThing = isNaryThing;
		this.HEADER_TEXT = "elements";
		this.editorKit = x_EditorKit;
	}

	public CartesianX_Type getRoot() {
		return this.root;
	}

	private ArrayList<AtomicX_Type> getElements(X_Type root2) {
		return ((CartesianX_Type) root2).getSubElements();

	}

	/**
	 * Populates the list with the subelements of the given {@link X_Type}. It
	 * this is nary:Thing+, populate the list with two instances of owl:Thing.
	 * 
	 * @param root
	 */
	@SuppressWarnings("unchecked")
	public void setRootObject(X_Type root) {
		this.root = (CartesianX_Type) root;

		java.util.List<Object> data = new ArrayList<Object>();

		data.add(header);

		if (root != null) {
			ArrayList<AtomicX_Type> elements = getElements(root);
			for (int i = 0; i < elements.size(); i++) {
				AtomicX_Type clazz = elements.get(i);
				SelectionListItem li = new SelectionListItem(clazz, this, i);
				data.add(li);
			}
		}
		setListData(data.toArray());
		revalidate();
	}

	public boolean isNaryThing() {
		return this.isNaryThing;
	}

	/**
	 * Remove the currenlty selected {@link SelectionListItem} from the list.
	 * 
	 * @param selectionListItem
	 * @return true if successfully removed, false other wise. TODO: is this
	 *         used somewhere?
	 */
	public boolean handleItemDelete(SelectionListItem selectionListItem) {
		if (isNaryThing) {
			if (getRoot().getSubElements().size() > 2) {
				int index = selectionListItem.getIndex();

				this.root.removeSubelement(index);
				setRootObject(root);
				if (index == 0) {
					this.setSelectedIndex(0);
				} else {
					this.setSelectedIndex(index - 1);
				}
			}
		}
		return false;
	}

	@Override
	protected void handleAdd() {
		if (isNaryThing) {
			this.root.addSubelement(
					(AtomicX_Type) editorKit.getX_ModelManager().getOWLThing());
		}
		setRootObject(root);
		this.setSelectedIndex(this.root.getSubElements().size());

	}

	/**
	 * Opens an instance of {@link ClassSelectionFrame} initialized with the
	 * selected node. The selected node is replaced by the node resulting from
	 * the SelectionFrame.
	 * 
	 * @param selectionListItem
	 */
	public void handleItemEdit(SelectionListItem selectionListItem) {
		AtomicX_Type selection;
		if (isNaryThing)
			selection = this.editorKit.getX_Workspace().getEventUtil()
					.selectX_Type(null);
		else
			selection = this.editorKit.getX_Workspace().getEventUtil()
					.selectX_Type(selectionListItem.getValue());
		if (selection != null) {
			this.root.setSubelement(selectionListItem.getIndex(), selection);
			setRootObject(root);
			this.setSelectedIndex(selectionListItem.getIndex() + 1);
		}
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		if (e.getFirstIndex() != 0)
			selectedItem = (SelectionListItem) this.getSelectedValue();
		else
			this.setSelectedIndex(1);;

	}

	@Override
	public void valueChanged(TreeSelectionEvent e) {
		X_Entity selection = (X_Entity) e.getPath().getLastPathComponent();
		if (this.selectedItem != null) {
			if (this.selectedItem.isEditModeActive()
					&& selection != this.editorKit.getX_ModelManager()
							.getNaryThing()) {

				root.setSubelement(selectedItem.getIndex(),
						(AtomicX_Type) e.getPath().getLastPathComponent());
			}
		}

	}

	/**
	 * 
	 * @return The underlying value of the currently selected node.
	 */
	public X_Type getSelectedItem() {
		return ((SelectionListItem) getSelectedValue()).getValue();
	}

}
