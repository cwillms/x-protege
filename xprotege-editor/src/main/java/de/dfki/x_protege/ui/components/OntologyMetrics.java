package de.dfki.x_protege.ui.components;


import de.dfki.x_protege.ui.components.table.MetricTable;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.listener.ModelChangeListener;

import javax.swing.*;
import java.awt.BorderLayout;

/**
 * This view component uses a simple table to present some basic statistical information on the overall ontology, e.g. number of entities, number of classes and so on,
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 03.07.16<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class OntologyMetrics  extends AbstractX_ViewComponent{

    private MetricTable table;

    private ModelChangeListener modelChangeListener = new ModelChangeListener() {
        @Override
        public void modelChanged(ModelChange modelChanged) {
            handleModelChange(modelChanged);
        };
    };

    protected void handleModelChange(
            final ModelChange modelChange) {
        Runnable doHighlight = new Runnable() {
            @Override
            public void run() {

                    updateView();

            }
        };
        SwingUtilities.invokeLater(doHighlight);

    }

    @Override
    protected void initializeView() throws Exception {
        setLayout(new BorderLayout());
        System.err.println("Add table to metricPanel");
        table = new MetricTable(getX_ModelManager());
        add(new JScrollPane(table), BorderLayout.CENTER);
        updateView();
        getX_ModelManager().addModelChangeListener(modelChangeListener);
    }

    @Override
    protected void disposeView() {
    }

    private void updateView() {
        System.err.println("Update Metric Panel");
        table.setOntology();
        table.revalidate();
    }
}
