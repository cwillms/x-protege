package de.dfki.x_protege.ui.components.lists;

import java.util.Set;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.model.data.AbstractX_Property;
import de.dfki.x_protege.model.data.X_Type;

/**
 * The {@link DomainList} is used to present the Domain (e.i., instances of
 * {@link X_Type} represented by a {@link ClassItem}) connected to a Property.
 * <br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 17.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class DomainList extends ClassList {

	private static final long serialVersionUID = 2208700479063788335L;

	/**
	 * Create a new instance of {@link DomainList}.
	 * 
	 * @param x_EditorKit
	 *            The connected {@link X_EditorKit}.
	 */
	public DomainList(X_EditorKit x_EditorKit) {
		super(x_EditorKit);
		this.HEADER_TEXT = "rdfs:domain";
	}

	/**
	 * Create a new instance of {@link DomainList}.
	 * 
	 * @param editorKit
	 *            The connected {@link X_EditorKit}.
	 * @param b
	 *            {@link Boolean} flag indicating whether the list is part of an
	 *            temporal dialogue.
	 */
	public DomainList(X_EditorKit editorKit, boolean b) {
		super(editorKit, b);
		this.HEADER_TEXT = "rdfs:domain";
	}

	@Override
	protected Set<X_Type> getElements(AbstractX_Property root2) {
		return root2.getExplicitDomain();
	}

	@SuppressWarnings("unchecked")
	@Override
	protected Set<X_Type> getImElements(AbstractX_Property root) {
		return (Set<X_Type>) root.getImplicitDomain();
	}

}
