package de.dfki.x_protege.ui.logic.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import de.dfki.x_protege.ui.components.table.PrefixMapperTable;
import de.dfki.x_protege.ui.utilities.X_Icons;

/**
 * This action is used to add a new mapping to the Namespace.
 * 
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 16.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class AddPrefixMappingAction extends AbstractAction {

	private static final long serialVersionUID = -4698127352530066976L;

	private PrefixMapperTable table;

	/**
	 * Creates a new instance of {@link AddPrefixMappingAction}.
	 * 
	 * @param table
	 *            the {@link PrefixMapperTable} this action is connected to.
	 */
	public AddPrefixMappingAction(PrefixMapperTable table) {
		super("Add prefix", X_Icons.prefix_add);
		putValue(AbstractAction.SHORT_DESCRIPTION, "Add prefix mapping");
		this.table = table;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		table.createAndEditRow();
	}

}
