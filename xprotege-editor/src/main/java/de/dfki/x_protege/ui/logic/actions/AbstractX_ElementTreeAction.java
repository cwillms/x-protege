package de.dfki.x_protege.ui.logic.actions;

import javax.swing.Icon;
import javax.swing.tree.TreeSelectionModel;

import org.protege.editor.core.ui.view.DisposableAction;

import de.dfki.x_protege.ui.components.tree.X_ElementTree;

/**
 * The {@link AbstractX_ElementListAction} are Actions connected to
 * {@link X_ElementTree}s.
 * 
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 16.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public abstract class AbstractX_ElementTreeAction<T> extends DisposableAction {

	private static final long serialVersionUID = -1125379241318329551L;

	protected AbstractX_ElementTreeAction(String name, Icon icon, TreeSelectionModel treeSelectionModel) {
		super(name, icon);
	}

	@Override
	public void dispose() {
		// Nothing to do here.

	}

}
