package de.dfki.x_protege.ui.utilities;

import java.awt.BorderLayout;
import java.io.File;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import org.protege.editor.core.ui.util.JOptionPaneEx;
import org.protege.editor.core.ui.util.UIUtil;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.model.data.AtomicX_Type;
import de.dfki.x_protege.ui.components.lists.ConflictList;
import de.dfki.x_protege.ui.utilities.rendering.X_ProtegeRenderer;

/**
 * 
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 12.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class UIHelper {

	/**
	 * The used {@link X_EditorKit}.
	 */
	private X_EditorKit x_EditorKit;

	private final static Set<String> x_EXTENSIONS;

	static {
		Set<String> extensions = new HashSet<String>();
		extensions.add(".ont");
		x_EXTENSIONS = Collections.unmodifiableSet(extensions);
	}

	/**
	 * Creates a new instance of {@link UIHelper}.
	 * 
	 * @param owlEditorKit
	 *            the currently used {@link X_EditorKit}.
	 */
	public UIHelper(X_EditorKit owlEditorKit) {
		this.x_EditorKit = owlEditorKit;
	}

	private JComponent getParent() {
		return x_EditorKit.getWorkspace();
	}

	/**
	 * Creates a dialogue with the given title and message and returns the {@link String} entered by the user.
	 * @param title The title of the dialogue.
	 * @param message the message of the dialogue.
     * @return the {@link String} entered by the user.
     */
	public String getString(String title, String message) {
		String uriString = JOptionPane.showInputDialog(getParent(), message,
				title, JOptionPane.INFORMATION_MESSAGE);
		if (uriString == null) {
			return null;
		}
		return uriString;
	}

	/**
	 * Creates {@link JOptionPane} including an ERROR_MESSAGE
	 * @param title the title of the dialogue
	 * @param message the message of the dialogue
     */
	public void showError(String title, String message) {
		JOptionPane.showMessageDialog(getParent(), message, title,
				JOptionPane.ERROR_MESSAGE);
	}

    /**
     * Creates a distinct dialogue which indicates to the user that there was a invalid annotation.
     * @param message the message to be presented
     * @return the users decision ( 0 -> cancel , 1 -> ok)
     */
	public int showAnnoWarning(String message) {
		String title = "Invalid Annotation";
		JLabel info = new JLabel(message);
		return JOptionPaneEx.showConfirmDialog(getParent(), title, info,
				JOptionPane.ERROR_MESSAGE, JOptionPane.OK_CANCEL_OPTION, null);
	}

    /**
     * Creates a dialogue with the given title featuring the given component.
     * @param title the title of the dialogue.
     * @param component the {@link JComponent} this dialogue is based on.
     * @return the users decision ( 0 -> cancel , 1 -> ok)
     */
	public int showDialog(String title, JComponent component) {
		return JOptionPaneEx.showConfirmDialog(getParent(), title, component,
				JOptionPane.PLAIN_MESSAGE, JOptionPane.OK_CANCEL_OPTION, null);
	}

    /**
     * Creates a dialogue which allows the user to browse his file system and select a .ont file
     * @param title the title of the dialogue
     * @return A {@link File} representing the selected .ont file.
     */
	public File chooseOntFile(String title) {
		JFrame f = (JFrame) SwingUtilities.getAncestorOfClass(JFrame.class,
				getParent());
		if (f == null) {
			f = new JFrame();
		}
		return UIUtil.openFile(f, title, "Ontology File", x_EXTENSIONS);
	}

    /**
     * Creates a dialogue allowing the user to browse his file system and select/create a file.
     *
     * @param title the title of the dialogue
     * @return the newly creates/selected {@link File}
     */
	public File saveOntFile(String title) {
		return UIUtil
				.saveFile(
						(JFrame) SwingUtilities.getAncestorOfClass(JFrame.class,
								getParent()),
						title, "Ontology File", x_EXTENSIONS);
	}

    /**
     * Creates a dialogue which informs the user that the deletion of the given {@link AtomicX_Type} will cause side effects to {@link de.dfki.x_protege.model.data.CartesianX_Type}s.
     * @param t The {@link AtomicX_Type} that causes the problems.
     * @return the users decision ( 0 -> cancel , 1 -> ok)
     */
	public int showDeleteConflictWarning(AtomicX_Type t) {
		String infoText = "Attention!\n Removing this class will also cause the removing of the following classes.\n Are you sure?";
		String title = "Class deletion conflict";
		JPanel component = new JPanel();
		component.setLayout(new BorderLayout());
		JLabel info = new JLabel(infoText);
		component.add(info, BorderLayout.NORTH);
		// same list as the one used to present the search results
		ConflictList effectedClasses = new ConflictList(t);
		effectedClasses.setCellRenderer(new X_ProtegeRenderer());
		component.add(new JScrollPane(effectedClasses), BorderLayout.CENTER);
		return JOptionPaneEx.showConfirmDialog(getParent(), title, component,
				JOptionPane.QUESTION_MESSAGE, JOptionPane.OK_CANCEL_OPTION,
				null);
	}

}
