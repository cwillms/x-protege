package de.dfki.x_protege.ui.components.lists;

import java.util.Set;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.model.data.AbstractX_Property;
import de.dfki.x_protege.model.data.X_Type;

/**
 * The {@link RangeList} is used to present the Domain (e.i., instances of
 * {@link X_Type} represented by a {@link ClassItem}) connected to a Property.
 * <br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 17.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class RangeList extends ClassList {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3383604450383530951L;

	/**
	 * Create a new instance of {@link RangeList}.
	 * 
	 * @param x_EditorKit
	 *            The connected {@link X_EditorKit}.
	 */
	public RangeList(X_EditorKit x_EditorKit) {
		super(x_EditorKit);
		this.HEADER_TEXT = "rdfs:range";
	}

	/**
	 * Create a new instance of {@link RangeList}.
	 * 
	 * @param editorKit
	 *            The connected {@link X_EditorKit}.
	 * @param b
	 *            {@link Boolean} flag indicating whether the list is part of an
	 *            temporal dialogue.
	 */
	public RangeList(X_EditorKit editorKit, boolean b) {
		super(editorKit, b);
		this.HEADER_TEXT = "rdfs:range";
	}

	@Override
	protected Set<X_Type> getElements(AbstractX_Property root2) {
		return root2.getExplicitRange();
	}

	@Override
	protected Set<X_Type> getImElements(AbstractX_Property root) {
		return root.getImplicitRange();
	}

}
