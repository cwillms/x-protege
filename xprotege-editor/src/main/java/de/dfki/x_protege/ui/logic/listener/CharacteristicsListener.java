package de.dfki.x_protege.ui.logic.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;

import de.dfki.x_protege.model.X_ModelManager;
import de.dfki.x_protege.ui.components.X_PropertyCharacteristicsViewComponent;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.Type.ModelChangeType;

/**
 * This {@link ActionListener} listens for actions done in the
 * {@link X_PropertyCharacteristicsViewComponent}. The actions are converted
 * into {@link ModelChange}-Events and forwarded to the connected
 * {@link X_ModelManager}.
 * 
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 13.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class CharacteristicsListener implements ActionListener {

	/**
	 * The connected {@link X_ModelManager}, {@link ModelChange}-Events are
	 * forwarded to this.
	 */
	private final X_ModelManager modelManager;

	/**
	 * Creates a new instance of {@link CharacteristicsListener}.
	 * 
	 * @param x_ModelManager the X_ModelManager this Listener is associated with.
	 */
	public CharacteristicsListener(X_ModelManager x_ModelManager) {
		this.modelManager = x_ModelManager;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JCheckBox cb = (JCheckBox) e.getSource();
		@SuppressWarnings("unused")
		boolean selected = cb.getModel().isSelected();
		int index = getIndexFor(cb.getText());
		modelManager.handleChange(new ModelChange(ModelChangeType.CHARCHANGED, index));
	}

	private int getIndexFor(String text) {
		switch (text) {
		case "Functional":
			return 0;
		case "Inverse functional":
			return 1;
		case "Transitive":
			return 2;
		case "Symmetric":
			return 3;
		case "Asymmetric":
			return 4;
		case "Reflexive":
			return 5;
		case "Irreflexive":
			return 6;
		default:
			throw new IllegalArgumentException(text + " is no valid property characteristic");

		}
	}

}
