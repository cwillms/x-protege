package de.dfki.x_protege.ui.components.tree;

import java.awt.Point;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetContext;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;

import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.data.X_Property;

/**
 * 
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 17.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class TreeDropTarget implements DropTargetListener {

	private X_ElementTree targetTree;

	/**
	 * Creates a new instance of {@link TreeDropTarget}
	 * @param tree the {@link X_ElementTree} this {@link TreeDropTarget} is connected to.
     */
	public TreeDropTarget(X_ElementTree tree) {
		targetTree = tree;
		new DropTarget(targetTree, this);
	}

	/*
	 * Drop Event Handlers
	 */
	@SuppressWarnings("unused")
	private TreeNode getNodeForEvent(DropTargetDragEvent dtde) {
		Point p = dtde.getLocation();
		DropTargetContext dtc = dtde.getDropTargetContext();
		X_ElementTree tree = (X_ElementTree) dtc.getComponent();
		TreePath path = tree.getClosestPathForLocation(p.x, p.y);
		return (TreeNode) path.getLastPathComponent();
	}

	@Override
	public void dragEnter(DropTargetDragEvent dtde) {
		dtde.acceptDrag(DnDConstants.ACTION_MOVE);

	}

	@Override
	public void dragOver(DropTargetDragEvent dtde) {
		dtde.acceptDrag(DnDConstants.ACTION_MOVE);
	}

	@Override
	public void dragExit(DropTargetEvent dte) {
	}

	@Override
	public void dropActionChanged(DropTargetDragEvent dtde) {
	}

	@Override
	public void drop(DropTargetDropEvent dtde) {
		Point pt = dtde.getLocation();
		DropTargetContext dtc = dtde.getDropTargetContext();
		X_ElementTree tree = (X_ElementTree) dtc.getComponent();
		TreePath parentpath = tree.getClosestPathForLocation(pt.x, pt.y);
		X_EntityTreeNode parentNode = (X_EntityTreeNode) parentpath
				.getLastPathComponent();
		X_Entity parent = parentNode.getValue();
		try {
			Transferable tr = dtde.getTransferable();
			DataFlavor[] flavors = tr.getTransferDataFlavors();
			for (int i = 0; i < flavors.length; i++) {
				if (tr.isDataFlavorSupported(flavors[i])) {
					dtde.acceptDrop(dtde.getDropAction());
					TreePath p = (TreePath) tr.getTransferData(flavors[i]);
					X_EntityTreeNode movedTreeNode = (X_EntityTreeNode) p
							.getLastPathComponent();
					X_Entity node = movedTreeNode.getValue();
					X_Entity oldParent = movedTreeNode.getParentEntity();
					if (!parentNode.isNodeAncestor(movedTreeNode))
						if (isAllowedMove(node, parent)) {
							tree.handleNodeMoved(node, oldParent, parent);
							dtde.dropComplete(true);
							return;
						}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			dtde.rejectDrop();
		}
	}

	private boolean isAllowedMove(X_Entity node, X_Entity parent) {
		if (node != parent) {
			if (node.isCartesianType() && parent.isCartesianType()) {
				// TODO check composition
				return true;
			}
			if (node.isAtomType() && parent.isAtomType()) {
				return true;
			}
			if (node.isProperty()) {
				X_Property n = (X_Property) node;
				X_Property p = (X_Property) parent;
				int nType = n.getPropertyType().nextSetBit(0);
				int pType = p.getPropertyType().nextSetBit(0);
				// same type or target is mixedProperty -> everything is ok
				if (nType == pType || pType == 0) {
					return true;
				}
				if (nType == 0 && n.isCompatible(p)) {
					return true;
				}

			}
		}
		return false;
	}
}
