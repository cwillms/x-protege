/**
 * 
 */
package de.dfki.x_protege.ui.components.lists;

import org.protege.editor.core.ui.list.MListItem;

import de.dfki.lt.hfc.Rule;
import de.dfki.x_protege.ui.logic.X_Rule;

/**
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 24.04.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class RuleListItem implements MListItem {

	private final X_Rule rule;
	/**
	 * @param r
	 */
	public RuleListItem(X_Rule r) {
		this.rule = r;
	}

	public Rule getSource() {
		return rule.getSource();
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.protege.editor.core.ui.list.MListItem#isEditable()
	 */
	@Override
	public boolean isEditable() {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.protege.editor.core.ui.list.MListItem#handleEdit()
	 */
	@Override
	public void handleEdit() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.protege.editor.core.ui.list.MListItem#isDeleteable()
	 */
	@Override
	public boolean isDeleteable() {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.protege.editor.core.ui.list.MListItem#handleDelete()
	 */
	@Override
	public boolean handleDelete() {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.protege.editor.core.ui.list.MListItem#getTooltip()
	 */
	@Override
	public String getTooltip() {
		return rule.getTextRep();
	}

}
