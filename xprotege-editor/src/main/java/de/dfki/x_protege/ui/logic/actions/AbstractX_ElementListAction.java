package de.dfki.x_protege.ui.logic.actions;

import javax.swing.Icon;

import org.protege.editor.core.ui.list.MList;
import org.protege.editor.core.ui.view.DisposableAction;

/**
 * The {@link AbstractX_ElementListAction} are Actions connected to
 * {@link MList}s.
 * 
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 16.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public abstract class AbstractX_ElementListAction<T> extends DisposableAction {

	private static final long serialVersionUID = 8361369386764142181L;

	protected AbstractX_ElementListAction(String name, Icon icon) {
		super(name, icon);
	}

	@Override
	public void dispose() {
		// Nothing to do here for the moment

	}

}
