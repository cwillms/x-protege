package de.dfki.x_protege.ui.components;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JCheckBox;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import de.dfki.x_protege.model.data.AbstractX_Property;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.data.X_Property;
import de.dfki.x_protege.model.data.X_Type;
import de.dfki.x_protege.ui.components.lists.ClassList;
import de.dfki.x_protege.ui.components.lists.DomainList;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.SelectionChange;
import de.dfki.x_protege.ui.logic.Event.Type.SelectionType;
import de.dfki.x_protege.ui.utilities.rendering.X_ProtegeRenderer;

/**
 * The {@link X_PropertyDomainsViewComponent} is used to maintain the
 * {@link X_Type} defining the domain of the {@link X_Property} currently
 * selected in the {@link MPropertyHierarchyViewComponent}. <br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 20.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class X_PropertyDomainsViewComponent
		extends
			AbstractX_SelectionViewComponent {

	private static final long serialVersionUID = 8328614841435239210L;

	private ClassList list;

	private Action showImplicitAction = new AbstractAction(
			"Show Implicit Assignments") {

		/**
		* 
		*/
		private static final long serialVersionUID = -4293977946218798052L;

		@Override
		public void actionPerformed(ActionEvent e) {
			list.setState(showImplicitBox.isSelected());
			updateView();
		}
	};

	private JCheckBox showImplicitBox;

	@Override
	protected void handleSelectionChanges(
			final SelectionChange selectionchanges) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {

				switch (selectionchanges.getType()) {
					case PROPERTYHIERARCHY : {
						updateView();
						break;
					}

					default :
						break;
				}
			}
		};
		SwingUtilities.invokeLater(doHighlight);

	}

	@Override
	protected void handleModelChanged(final ModelChange modelChanged) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {

				switch (modelChanged.getType()) {
					case DOMAINADDED : {
						updateView();
						break;
					}
					case DOMAINREMOVED : {
						updateView();
						break;
					}
					case RENAME : {
						updateView();
						break;
					}
					case ACCEPT : {
						updateView();
						break;
					}
					default :
						break;
				}
			}
		};
		SwingUtilities.invokeLater(doHighlight);
	}

	@Override
	public void initialiseView() throws Exception {
		this.showImplicitBox = new JCheckBox(showImplicitAction);
		setLayout(new BorderLayout());
		list = new DomainList(getX_EditorKit());
		list.setCellRenderer(new X_ProtegeRenderer());
		list.setState(this.showImplicitBox.isSelected());
		list.setRootObject(getSelectedProperty());
		add(new JScrollPane(list));
		add(showImplicitBox, BorderLayout.SOUTH);
		updateView();
	}

	private AbstractX_Property getSelectedProperty() {
		return (AbstractX_Property) getX_EditorKit().getX_ModelManager()
				.getActiveModel().getSelection(SelectionType.PROPERTYHIERARCHY);
	}

	@Override
	protected X_Entity updateView() {
		AbstractX_Property sel = getSelectedProperty();
		this.setHeaderText(sel.render());
		list.setRootObject(getSelectedProperty());
		return sel;
	}

	@Override
	public void disposeView() {
		// Nothing to do here
	}

}
