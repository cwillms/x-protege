package de.dfki.x_protege.ui.components.lists;

import java.util.ArrayList;
import java.util.Set;

import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionListener;

import org.protege.editor.core.ui.list.MList;
import org.protege.editor.core.ui.list.MListSectionHeader;

import de.dfki.x_protege.model.data.AbstractX_Property;
import de.dfki.x_protege.model.data.X_Individual;
import de.dfki.x_protege.ui.components.dialogue.SelectionPanel;
import de.dfki.x_protege.ui.utilities.rendering.X_ProtegeRenderer;

/**
 * The {@link PropertySelectionList} is an essential part of the
 * {@link SelectionPanel} s, where it is used to present the property that can
 * be applied to a given {@link X_Individual} .<br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 17.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class PropertySelectionList extends MList {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7388702410901708395L;

	private AbstractX_Property[] connectedProperties;

	/**
	 * 
	 */
	private MListSectionHeader header = new MListSectionHeader() {

		@Override
		public String getName() {
			return "Property:";
		}

		@Override
		public boolean canAdd() {
			return false;
		}
	};

	/**
	 * Creates a new instance of {@link PropertySelectionList}, specified by the
	 * given {@link AbstractX_Property}s.
	 * 
	 * @param connectedProperties
	 *            The {@link AbstractX_Property}s to be represented by this
	 *            list.
	 * @param listener
	 *            An {@link ListSelectionListener} used by the
	 *            {@link SelectionPanel} to monitor which Property is selected.
	 */
	public PropertySelectionList(Set<AbstractX_Property> connectedProperties, ListSelectionListener listener) {
		this.connectedProperties = connectedProperties.toArray(new AbstractX_Property[connectedProperties.size()]);
		setCellRenderer(new X_ProtegeRenderer());
		setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		addListSelectionListener(listener);
		populate();
	}

	@SuppressWarnings("unchecked")
	private void populate() {
		java.util.List<Object> data = new ArrayList<Object>();

		data.add(header);

		if (connectedProperties != null) {
			// @@TODO ordering
			for (AbstractX_Property p : connectedProperties) {

				data.add(new PropertiesListItem(p, null, false));
			}
		}
		setListData(data.toArray());
		revalidate();
	}

}
