package de.dfki.x_protege.ui.components;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.IOException;
import java.net.URI;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.protege.editor.core.ui.error.ErrorLogPanel;
import org.protege.editor.core.ui.util.AugmentedJTextField;
import org.protege.editor.core.ui.util.LinkLabel;

import de.dfki.x_protege.model.data.Annotation;
import de.dfki.x_protege.model.data.Ontology;
import de.dfki.x_protege.ui.components.lists.AnnotationList;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.OntoChange;
import de.dfki.x_protege.ui.logic.Event.Type.ModelChangeType;
import de.dfki.x_protege.ui.logic.Event.Type.OntoChangeTypes;
import de.dfki.x_protege.ui.logic.listener.ModelChangeListener;
import de.dfki.x_protege.ui.logic.listener.OntologyChangeListener;

/**
 * The {@link X_OntologyAnnotationViewComponent} is used within the Active
 * Ontology tab to maintain the {@link Annotation}s connected to the currently
 * loaded {@link Ontology}. To do so it uses an instance of
 * {@link AnnotationList}. This component also allows the renaming of the
 * currently selected {@link Ontology} and the selection of the used I/O-format
 * (prefix vs. infix). <br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 20.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class X_OntologyAnnotationViewComponent extends AbstractX_ViewComponent {

	private static final long serialVersionUID = 4899857762612091411L;

	public static final String ONTOLOGY_IRI_FIELD_LABEL = "Ontology URI";

	public static final String ONTOLOGY_VERSION_IRI_FIELD_LABEL = "Ontology Version URI";

	public static final URI ONTOLOGY_IRI_DOCUMENTATION = URI.create(
			"http://www.w3.org/TR/2009/REC-owl2-syntax-20091027/#Ontology_IRI_and_Version_IRI");

	public static final URI VERSION_IRI_DOCUMENTATION = URI.create(
			"http://www.w3.org/TR/2009/REC-owl2-syntax-20091027/#Versioning_of_OWL_2_Ontologies");

	public static final String ONTOLOGY_IO_FORMAT_LABEL = "<html><font color='rgb(71, 107, 178)' size='3'><b> I/O Format</b></font></html";

	private AnnotationList list;

	private final AugmentedJTextField ontologyIRIField = new AugmentedJTextField(
			"e.g http://www.example.com/ontologies/myontology");

	private final AugmentedJTextField ontologyVersionIRIField = new AugmentedJTextField(
			"e.g. http://www.example.com/ontologies/myontology/1.0.0");

	private boolean updatingViewFromModel = false;

	@SuppressWarnings("unused")
	private boolean updatingModelFromView = false;

	/**
	 * The IRI of the ontology when the ontology IRI field gets the focus.
	 */
	@SuppressWarnings("unused")
	private URI initialOntologyID = null;

	@SuppressWarnings("unused")
	private boolean ontologyIRIShowing = false;

	private ModelChangeListener modelChangeListener = new ModelChangeListener() {

		@Override
		public void modelChanged(ModelChange modelChanged) {
			if (modelChanged.getType().equals(ModelChangeType.ANNOTATIONADDED))
				handleModelChanges(modelChanged);
			if (modelChanged.getType()
					.equals(ModelChangeType.ANNOTATIONREMOVED))
				handleModelChanges(modelChanged);
		}
	};

	private void handleModelChanges(ModelChange modelChanged) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {
				updateView();
			}
		};
		SwingUtilities.invokeLater(doHighlight);
	}

	private final OntologyChangeListener ontologyChangeListener = new OntologyChangeListener() {

		@Override
		public void ontologiesChanged(OntoChange ontologychanges) {
			handleOntologyChanges(ontologychanges);
		}

	};

	private void handleOntologyChanges(OntoChange changes) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {
				updateView();
			}
		};
		SwingUtilities.invokeLater(doHighlight);
	}

	@Override
	protected void initializeView() throws Exception {
		setLayout(new BorderLayout());

		setLayout(new BorderLayout());
		JPanel ontologyIRIPanel = new JPanel(new GridBagLayout());
		add(ontologyIRIPanel, BorderLayout.NORTH);
		Insets insets = new Insets(0, 4, 2, 0);
		ontologyIRIPanel.add(
				new LinkLabel(ONTOLOGY_IRI_FIELD_LABEL, new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						showOntologyIRIDocumentation();
					}
				}),
				new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
						GridBagConstraints.BASELINE_TRAILING,
						GridBagConstraints.NONE, insets, 0, 0));
		ontologyIRIPanel.add(ontologyIRIField,
				new GridBagConstraints(1, 0, 1, 1, 100.0, 0.0,
						GridBagConstraints.BASELINE_LEADING,
						GridBagConstraints.HORIZONTAL, insets, 0, 0));
		ontologyIRIField.getDocument()
				.addDocumentListener(new DocumentListener() {
					@Override
					public void insertUpdate(DocumentEvent e) {
						updateModelFromView();

					}

					@Override
					public void removeUpdate(DocumentEvent e) {
						updateModelFromView();

					}

					@Override
					public void changedUpdate(DocumentEvent e) {
						updateModelFromView();

					}
				});

		ontologyIRIField.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				// handleOntologyIRIFieldFocusLost();
			}

			@Override
			public void focusGained(FocusEvent e) {
				handleOntologyIRIFieldFocusGained();
			}
		});
		ontologyIRIShowing = ontologyIRIField.isShowing();

		ontologyIRIPanel.add(new LinkLabel(ONTOLOGY_VERSION_IRI_FIELD_LABEL,
				new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						showVersionIRIDocumentation();
					}
				}),
				new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
						GridBagConstraints.BASELINE_TRAILING,
						GridBagConstraints.NONE, insets, 0, 0));

		ontologyIRIPanel.add(ontologyVersionIRIField,
				new GridBagConstraints(1, 1, 1, 1, 100.0, 0.0,
						GridBagConstraints.BASELINE_LEADING,
						GridBagConstraints.HORIZONTAL, insets, 0, 0));

		ontologyVersionIRIField.getDocument()
				.addDocumentListener(new DocumentListener() {
					@Override
					public void insertUpdate(DocumentEvent e) {
						updateModelFromView();
					}

					@Override
					public void removeUpdate(DocumentEvent e) {
						updateModelFromView();
					}

					@Override
					public void changedUpdate(DocumentEvent e) {
						updateModelFromView();
					}
				});

		ontologyIRIPanel
				.setBorder(BorderFactory.createEmptyBorder(0, 0, 20, 0));

		ontologyIRIPanel.add(new JLabel(ONTOLOGY_IO_FORMAT_LABEL),
				new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
						GridBagConstraints.BASELINE_TRAILING,
						GridBagConstraints.NONE, insets, 0, 0));
		final JComboBox<String> ioBox = new JComboBox<String>(
				getX_ModelManager().getActiveModel().getIOFormats());
		if (getX_ModelManager().getActiveModel().getUsedIO()) {
			ioBox.setSelectedItem("Infix");
		} else {
			ioBox.setSelectedItem("Prefix");
		}
		ioBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (((String) ioBox.getSelectedItem()) == "Prefix") {
					getX_ModelManager().handleChange(
							new OntoChange(OntoChangeTypes.PREFIX, null));
				} else {
					getX_ModelManager().handleChange(
							new OntoChange(OntoChangeTypes.INFIX, null));
				}

			}
		});
		ontologyIRIPanel.add(ioBox,
				new GridBagConstraints(1, 2, 1, 1, 100.0, 0.0,
						GridBagConstraints.BASELINE_LEADING,
						GridBagConstraints.HORIZONTAL, insets, 0, 0));

		list = new AnnotationList(getX_EditorKit());
		add(new JScrollPane(list));
		list.setClassRootObject(activeOntology());
		getX_ModelManager().addOntologyChangeListener(ontologyChangeListener);
		getX_ModelManager().addModelChangeListener(modelChangeListener);
		updateView();
	}

	private void handleOntologyIRIFieldFocusGained() {
		handleOntologyIRIFieldActivated();
	}

	private void handleOntologyIRIFieldActivated() {
		initialOntologyID = getX_ModelManager().getActiveModel().getUri();
	}

	private void showVersionIRIDocumentation() {
		try {
			Desktop.getDesktop().browse(VERSION_IRI_DOCUMENTATION);
		} catch (IOException ex) {
			ErrorLogPanel.showErrorDialog(ex);
		}
	}

	private void showOntologyIRIDocumentation() {
		try {
			Desktop.getDesktop().browse(ONTOLOGY_IRI_DOCUMENTATION);
		} catch (IOException ex) {
			ErrorLogPanel.showErrorDialog(ex);
		}
	}

	/**
	 * Updates the view from the model - unless the changes were triggered by
	 * changes in the view.
	 */
	private void updateViewFromModel() {
		updatingViewFromModel = true;
		try {
			Ontology activeOntology = getX_EditorKit().getX_ModelManager()
					.getActiveModel();

			URI id = activeOntology.getUri();
			String ontologyIRIString = id.toString();
			if (id != null) {
				if (!ontologyIRIField.getText().equals(ontologyIRIString)) {
					ontologyIRIField.setText(ontologyIRIString);
				}
			}

			String versionIRI = activeOntology.getVersionUri();
			if (versionIRI != null) {
				String versionIRIString = versionIRI.toString();
				if (!ontologyVersionIRIField.getText()
						.equals(versionIRIString)) {
					ontologyVersionIRIField.setText(versionIRIString);
				}
			} else {
				ontologyVersionIRIField.setText("");
				if (id != null) {
					ontologyVersionIRIField
							.setGhostText("e.g. " + ontologyIRIString
									+ (ontologyIRIString.endsWith("/")
											? "1.0.0"
											: "/1.0.0"));
				}
			}

		} finally {
			updatingViewFromModel = false;
		}
	}

	/**
	 * Updates the model from the view - unless the changes in the view were
	 * triggered by changes in the model.
	 */
	private void updateModelFromView() {
		if (updatingViewFromModel) {
			return;
		}
		try {
			updatingModelFromView = true;
			String id = createOntologyIDFromView();
			if (id != null && !activeOntology().getOntologyID().equals(id)) {
				getX_ModelManager().handleChange(
						new OntoChange(OntoChangeTypes.IDCHANGED, id));
			}
			String version = createOntologyVersionFromView();
			if (version != null
					&& !activeOntology().getVersionUri().equals(version)) {
				getX_ModelManager().handleChange(new OntoChange(
						OntoChangeTypes.VERSIONCHANGED, version));
			}
		} finally {
			updatingModelFromView = false;
		}
	}

	private Ontology activeOntology() {
		return getX_ModelManager().getActiveModel();
	}

	private String createOntologyIDFromView() {

		ontologyIRIField.clearErrorMessage();
		ontologyIRIField.clearErrorLocation();
		String ontologyIRIString = ontologyIRIField.getText().trim();
		if (ontologyIRIString.isEmpty()) {
			return "";
		}
		// TODO add parsing towards correct URI syntax
		else {
			return ontologyIRIString;
		}

	}

	private String createOntologyVersionFromView() {
		String ontologyVersionString = ontologyVersionIRIField.getText().trim();
		String ontologyIRIString = ontologyIRIField.getText().trim() + "/";
		if (ontologyVersionString.contains(ontologyIRIString)) {
			String version = ontologyVersionString
					.substring(ontologyIRIString.length());
			return version;
		}
		return "";
	}

	private void updateView() {
		list.setClassRootObject(activeOntology());
		updateViewFromModel();
	}

	@Override
	protected void disposeView() {
		getX_ModelManager()
				.removeOntologyChangeListener(ontologyChangeListener);
		getX_ModelManager().removeModelChangeListener(modelChangeListener);

	}

}
