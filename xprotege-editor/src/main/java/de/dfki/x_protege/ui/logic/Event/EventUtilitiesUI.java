package de.dfki.x_protege.ui.logic.Event;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.swing.SwingUtilities;
import javax.swing.tree.TreePath;

import org.protege.editor.core.ui.workspace.Workspace;

import de.dfki.x_protege.model.OntologyCache;
import de.dfki.x_protege.model.X_ModelManager;
import de.dfki.x_protege.model.data.AbstractX_Property;
import de.dfki.x_protege.model.data.AtomicX_Type;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.data.X_Individual;
import de.dfki.x_protege.model.data.X_Property;
import de.dfki.x_protege.model.data.X_Type;
import de.dfki.x_protege.ui.X_Workspace;
import de.dfki.x_protege.ui.components.dialogue.ClassCreator;
import de.dfki.x_protege.ui.components.tree.TreeNodeGenerator;
import de.dfki.x_protege.ui.components.tree.X_EntityTreeNode;
import de.dfki.x_protege.ui.logic.Event.Type.ModelChangeType;
import de.dfki.x_protege.ui.logic.Event.Type.SelectionType;
import de.dfki.x_protege.ui.logic.listener.InformEventListener;
import de.dfki.x_protege.ui.logic.listener.ModelChangeListener;
import de.dfki.x_protege.ui.logic.listener.OntologyChangeListener;
import de.dfki.x_protege.ui.utilities.UIHelper;
import de.dfki.x_protege.ui.utilities.selectionFrames.ClassSelectionFrame;
import de.dfki.x_protege.ui.utilities.selectionFrames.PropertySelectionFrame;

/**
 * This class provides some methods that help handling Events.
 * 
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 13.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class EventUtilitiesUI {

	private final X_ModelManager modelManager;

	// #################### Ontology-Change-Event####################

	private final OntologyChangeListener ontologyChangeListener = new OntologyChangeListener() {

		@Override
		public void ontologiesChanged(OntoChange ontologychanges) {
			handleOntologyChanges(ontologychanges);
		}
	};

	private void handleOntologyChanges(OntoChange changes) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {
				workspace.updateURIFromModel();
			}
		};
		SwingUtilities.invokeLater(doHighlight);
	}

	// #################### Model-Change-Event####################
	private final ModelChangeListener modelChangeListener = new ModelChangeListener() {

		@Override
		public void modelChanged(ModelChange modelChanged) {
			handleModelChanges(modelChanged);
		}
	};

	private void handleModelChanges(final ModelChange modelChanged) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {
				switch (modelChanged.getType()) {
					case CARTSUBADD : {

						ClassCreator ccr = new ClassCreator(
								workspace.getX_EditorKit(),
								(X_Type) modelChanged.getValue());

						int returnValue = new UIHelper(
								workspace.getX_EditorKit()).showDialog(
										"Create a new n ary type ", ccr);
						if (returnValue == 0) {
							if (!OntologyCache.isUniqueName(workspace
									.getX_EditorKit().getX_ModelManager()
									.getActiveModel().getTypeInCreation()
									.render())) {
								new UIHelper(workspace.getX_EditorKit())
										.showError("Warning",
												"This class already exists");
								break;
							}
							modelManager.handleChange(new ModelChange(
									ModelChangeType.ACCEPT, null));
							// short-name of selected class
							ArrayList<String> snsel = ((X_Type) modelChanged
									.getValue()).getShortName();
							// short-name of the new class
							ArrayList<String> snnew = modelManager
									.getActiveModel().getTypeInCreation()
									.getShortName();

							if (snsel.equals(snnew)) {
								break;
							}

							modelManager.handleChange(new ModelChange(
									ModelChangeType.CARTSUBADDED,
									"handleModelChanges-CARTSUBADD",
									modelChanged.getValue()));
						} else {
							modelManager.handleChange(new ModelChange(
									ModelChangeType.CANCEL, null));
						}
						break;
					}
					case SHOWFINDING : {
						X_Entity e = (X_Entity) modelChanged.getValue();
						if (e.isType()) {
							workspace.setSelectedTab(workspace.getWorkspaceTab(
									"de.dfki.x_protege.X_ClassesTab"));
							modelManager
									.handleSelectionChanged(new SelectionChange(
											SelectionType.CLASSHIERARCHY,
											TreeNodeGenerator.getNode(e,
													SelectionType.CLASSHIERARCHY)));
						} else {
							if (e.isProperty()) {
								workspace.setSelectedTab(
										workspace.getWorkspaceTab(
												"de.dfki.x_protege.X_PropertiesTab"));
								modelManager.handleSelectionChanged(
										new SelectionChange(
												SelectionType.PROPERTYHIERARCHY,
												TreeNodeGenerator.getNode(e,
														SelectionType.PROPERTYHIERARCHY)));
							} else {
								if (e.isInstance()) {
									workspace.setSelectedTab(
											workspace.getWorkspaceTab(
													"de.dfki.x_protege.X_IndividualsTab"));
									modelManager.handleSelectionChanged(
											new SelectionChange(
													SelectionType.INDIVIDUALHIERARCHY,
													TreeNodeGenerator.getNode(e,
															SelectionType.ICLASSHIERARCHY)));
								}
							}
						}
						break;
					}
					case NONVALID : {
						new UIHelper(workspace.getX_EditorKit()).showError(
								"Warning", "This Entity already exists!");
						break;
					}
					default :
						break;
				}
			}
		};
		SwingUtilities.invokeLater(doHighlight);
	}

	private final InformEventListener informEventListener = new InformEventListener() {

		@Override
		public void informEvent(InfromEvent inform) {
			handleInformEvent(inform);
		}
	};

	private X_Workspace workspace;

	private void handleInformEvent(final InfromEvent changes) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {
				new UIHelper(workspace.getX_EditorKit()).showError("Warning",
						(String) changes.getValue());
			}
		};
		SwingUtilities.invokeLater(doHighlight);
	}

	/**
	 * creates a new instance of {@link EventUtilitiesUI}
	 * 
	 * @param mngr
	 *            the connected {@link X_ModelManager}
	 * @param x_Workspace
	 *            the connected {@link Workspace}
	 */
	public EventUtilitiesUI(X_ModelManager mngr, X_Workspace x_Workspace) {
		this.modelManager = mngr;
		this.workspace = x_Workspace;
		mngr.addOntologyChangeListener(ontologyChangeListener);
		mngr.addModelChangeListener(modelChangeListener);
		mngr.addInformEventListener(informEventListener);
	}

	/**
	 * register {@link ModelChangeListener} and {@link InformEventListener} to
	 * the {@link X_ModelManager} connected to this instance of
	 * {@link EventUtilitiesUI}.
	 */
	public void registerListener() {
		modelManager.addModelChangeListener(modelChangeListener);
		modelManager.addInformEventListener(informEventListener);
	}

	/**
	 * Opens a {@link javax.swing.JDialog} featuring a {@link ClassSelectionFrame} allowing the user to select an {@link X_Type}.
	 * @param root The root note used in the {@link ClassSelectionFrame}. Only subclasses of this root may be selected.
	 * @return the {@link X_Type} selected by the user.
     */
	public AtomicX_Type selectX_Type(X_Type root) {
		boolean temp = false;
		SelectionType type = SelectionType.CARTCREATIONHIERARCHY;
		AtomicX_Type result = null;
		ClassSelectionFrame sel = new ClassSelectionFrame(
				workspace.getX_EditorKit(), type, temp, root);

		int returnValue = new UIHelper(workspace.getX_EditorKit())
				.showDialog("Please select a new " + type, sel);
		if (returnValue == 0) {
			result = (AtomicX_Type) sel.selectedClass();
		} else {
			modelManager.handleChange(
					new ModelChange(ModelChangeType.CANCEL, null));
		}
		sel.dispose();
		return result;
	}

	/**
	 * Opens a {@link javax.swing.JDialog} featuring a {@link PropertySelectionFrame} allowing the user to select an {@link X_Property}, which is then assigned to the given {@link X_Type}.
	 * @param type the {@link X_Type} the {@link X_Property} will be assigned to
	 * @param temp flag indicating whether the dialogue to be created was triggered from within an other one.
	 *
	 */
	public void addProperty(String type, boolean temp) {

		ModelChangeType t = getModelChangeType(type, temp);
		PropertySelectionFrame sel = new PropertySelectionFrame(
				workspace.getX_EditorKit(), type, temp);
		int returnValue = new UIHelper(workspace.getX_EditorKit()).showDialog(
				"Please select the Property you want to assign", sel);
		if (returnValue == 0) {
			if (!temp) {
				modelManager.handleChange(
						new ModelChange(ModelChangeType.ACCEPT, null));
			}
			TreePath[] selections = sel.getSelections();
			Set<AbstractX_Property> s = new HashSet<>();
			X_Type x_type;
			if (temp) {
				x_type = (X_Type) modelManager.getActiveModel()
						.getSelection(SelectionType.TEMPCLASSHIERARCHY);
			} else {
				x_type = (X_Type) modelManager.getActiveModel()
						.getSelection(SelectionType.CLASSHIERARCHY);
			}
			for (TreePath tp : selections) {
				AbstractX_Property property = (AbstractX_Property) ((X_EntityTreeNode) tp
						.getLastPathComponent()).getValue();
				if (property.getExplicitDomain().contains(x_type)) {
					continue;
				} else {
					s.add(property);
				}
			}
			if (!s.isEmpty()) {
				for (AbstractX_Property p : s)
					modelManager.handleChange(new ModelChange(t, p, x_type));
			}
		} else {
			modelManager.handleChange(
					new ModelChange(ModelChangeType.CANCEL, null));
		}
		sel.dispose();
	}

	private ModelChangeType getModelChangeType(String type, boolean temp) {
		ModelChangeType t = null;
		if (temp) {
			switch (type) {
				case "rdfs:domain" : {
					t = ModelChangeType.TEMPDOMAINADD;
					break;
				}
				case "rdfs:range" : {
					t = ModelChangeType.TEMPRANGEADD;
					break;
				}
				case "nary:argument" : {
					t = ModelChangeType.TEMPEXTRAADD;
					break;
				}
				case "Properties" : {
					t = ModelChangeType.TEMPDOMAINADD;
					break;
				}
				case "Disjoint With" : {
					t = ModelChangeType.TEMPDISJOINTADD;
					break;
				}
				case "Type" : {
					t = ModelChangeType.TEMPINDIVIDUALTYPEADD;
				}
				default :
					break;
			}
		} else {
			switch (type) {
				case "rdfs:domain" : {
					t = ModelChangeType.DOMAINADDED;
					break;
				}
				case "rdfs:range" : {
					t = ModelChangeType.RANGEADDED;
					break;
				}
				case "nary:argument" : {
					t = ModelChangeType.EXTRAADDED;
					break;
				}
				case "Properties" : {
					t = ModelChangeType.DOMAINADDED;
					break;
				}
				case "Disjoint With" : {
					t = ModelChangeType.DISJOINTADDED;
					break;
				}
				case "Type" : {
					t = ModelChangeType.INDIVIDUALTYPEADD;
				}
				default :
					break;
			}
		}
		return t;
	}

	/**
	 * Opens a {@link javax.swing.JDialog} featuring a {@link ClassSelectionFrame} allowing the user to select an {@link X_Type}, which is then assigned to the given {@link X_Entity}.
	 * @param entity the {@link X_Entity} the {@link X_Type} will be assigned to
	 * @param type a string specifying which kind of assignment will be used. @see getModelChangeType for more information.
	 * @param temp flag indicating whether the dialogue to be created was triggered from within an other one.
	 *
	 */
	public void addX_Type(X_Entity entity, String type, boolean temp) {
		ModelChangeType t = getModelChangeType(type, temp);
		SelectionType st = getSelectionType(entity, type, temp);
		ClassSelectionFrame sel = new ClassSelectionFrame(
				workspace.getX_EditorKit(), st, temp);
		if (t.equals(ModelChangeType.DISJOINTADDED)) {
			int returnValue = new UIHelper(workspace.getX_EditorKit())
					.showDialog("Please select a new " + type, sel);
			if (returnValue == 0) {
				if (!temp) {
					modelManager.handleChange(
							new ModelChange(ModelChangeType.ACCEPT, null));
				}
				TreePath[] selections = sel.getSelections();
				X_Type mainType = (X_Type) modelManager.getActiveModel()
						.getSelection(SelectionType.CLASSHIERARCHY);
				Set<X_Type> s = new HashSet<>();
				for (TreePath tp : selections) {
					X_Type newDomain = (X_Type) ((X_EntityTreeNode) tp
							.getLastPathComponent()).getValue();
					if (mainType.getDisjoints().contains(newDomain)
							|| mainType == newDomain) {
						continue;
					} else {
						s.add(newDomain);
					}
				}
				if (!s.isEmpty()) {
					modelManager.handleChange(new ModelChange(t, mainType, s));
				}
			} else {
				modelManager.handleChange(
						new ModelChange(ModelChangeType.CANCEL, null));
			}
			sel.dispose();
		} else {
			AbstractX_Property p;
			if (temp) {
				p = (AbstractX_Property) modelManager.getActiveModel()
						.getSelection(SelectionType.TEMPPROPERTYHIERARCHY);
			} else {
				p = (AbstractX_Property) modelManager.getActiveModel()
						.getSelection(SelectionType.PROPERTYHIERARCHY);
			}
			if (p.getPropertyType().nextSetBit(0) == 0) {
				sel.setModel(TreeNodeGenerator
						.getModel(SelectionType.CLASSHIERARCHY));
			}
			int returnValue = new UIHelper(workspace.getX_EditorKit())
					.showDialog("Please select a new " + type, sel);
			if (returnValue == 0) {
				if (!temp) {
					modelManager.handleChange(
							new ModelChange(ModelChangeType.ACCEPT, null));
				}
				TreePath[] selections = sel.getSelections();
				Set<X_Type> s = new HashSet<>();
				if (type.equals("Type")) {
					for (TreePath tp : selections) {
						X_Type newType = (X_Type) ((X_EntityTreeNode) tp
								.getLastPathComponent()).getValue();
						if (((X_Individual) entity).getDisjoints()
								.contains(newType))
							new UIHelper(workspace.getX_EditorKit()).showError(
									"Disjoint Conflict",
									"It is not allowed that the types of an instance are disjoint");
						else {
							modelManager.handleChange(new ModelChange(
									ModelChangeType.INDIVIDUALTYPEADD,
									newType));
						}
					}
				} else {
					for (TreePath tp : selections) {
						X_Type newDomain = (X_Type) ((X_EntityTreeNode) tp
								.getLastPathComponent()).getValue();
						switch (type) {
							case "rdfs:domain" : {
								if (p.getExplicitDomain().contains(newDomain)) {
									continue;
								} else {
									s.add(newDomain);
								}
								break;
							}
							case "rdfs:range" : {
								if (p.getExplicitRange().contains(newDomain)) {
									continue;
								} else {
									s.add(newDomain);
								}
								break;
							}
							case "nary:argument" : {
								if (p.getExplicitExtra().contains(newDomain)) {
									continue;
								} else {
									s.add(newDomain);
								}
								break;
							}
							default :
								break;
						}
					}
					if (!s.isEmpty()) {
						modelManager.handleChange(new ModelChange(t, p, s));
					}
				}
			} else {
				modelManager.handleChange(
						new ModelChange(ModelChangeType.CANCEL, null));
			}
			sel.dispose();
		}
	}


	private SelectionType getSelectionType(X_Entity root, String type,
			boolean temp) {

		if (temp) {
			switch (type) {
				case "rdfs:domain" : {
					X_Property p = (X_Property) root;
					if (p.getPropertyType().nextSetBit(0) == 0)
						return SelectionType.TEMPCLASSHIERARCHY;
					return SelectionType.OBJECTONLY;
				}
				case "rdfs:range" : {
					X_Property p = (X_Property) root;
					if (p.getPropertyType().nextSetBit(0) == 0)
						return SelectionType.TEMPCLASSHIERARCHY;
					if (p.getPropertyType().nextSetBit(0) == 1)
						return SelectionType.XSDONLY;
					if (p.getPropertyType().nextSetBit(0) == 2)
						return SelectionType.OBJECTONLY;
				}
				case "nary:argument" : {
					return SelectionType.TEMPCLASSHIERARCHY;
				}
				case "Properties" : {
					return SelectionType.TEMPPROPERTYHIERARCHY;
					// TODO implement restrictions similar to those provided for
					// Properties
				}
				case "Disjoint With" : {
					return SelectionType.TEMPCLASSHIERARCHY;
				}
				case "Type" : {
					return SelectionType.TEMPICLASSHIERARCHY;
				}
				default :
					return null;
			}
		} else {
			switch (type) {
				case "rdfs:domain" : {
					X_Property p = (X_Property) root;
					if (p.getPropertyType().nextSetBit(0) == 0)
						return SelectionType.CLASSHIERARCHY;
					return SelectionType.OBJECTONLY;
				}
				case "rdfs:range" : {
					X_Property p = (X_Property) root;
					if (p.getPropertyType().nextSetBit(0) == 0)
						return SelectionType.CLASSHIERARCHY;
					if (p.getPropertyType().nextSetBit(0) == 1)
						return SelectionType.XSDONLY;
					if (p.getPropertyType().nextSetBit(0) == 2)
						return SelectionType.OBJECTONLY;
				}
				case "nary:argument" : {
					return SelectionType.CLASSHIERARCHY;
				}
				case "Properties" : {
					return SelectionType.PROPERTYHIERARCHY;
					// TODO implement restrictions similar to those provided for
					// Properties
				}
				case "Disjoint With" : {
					return SelectionType.CLASSHIERARCHY;
				}
				case "Type" : {
					return SelectionType.ICLASSHIERARCHY;
				}
				default :
					return null;
			}
		}
	}

}
