package de.dfki.x_protege.ui.components;


import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.data.X_Type;
import de.dfki.x_protege.ui.components.tree.TreeNodeGenerator;
import de.dfki.x_protege.ui.components.tree.X_ElementTree;
import de.dfki.x_protege.ui.components.tree.X_EntityTreeNode;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.SelectionChange;
import de.dfki.x_protege.ui.logic.Event.Type.SelectionType;
import de.dfki.x_protege.ui.logic.actions.AbstractX_ElementTreeAction;
import de.dfki.x_protege.ui.utilities.rendering.X_SuperClassProtegeRenderer;

/**
 * The {@link SuperClassHierarchyViewComponent} is part of the Class Editor and
 * used to present the superclasses of the currently selected class in a proper
 * way. <br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 20.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
@SuppressWarnings("rawtypes")
public class SuperClassHierarchyViewComponent
		extends
			AbstractX_ElementHierarchyViewComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2326654513119922467L;

	private AbstractX_ElementTreeAction<X_Type> addSuper;

	private AbstractX_ElementTreeAction<X_Type> removeSuper;

	private TreeNode[] selectionPath = null;

	@Override
	protected void handleSelectionChanges(
			final SelectionChange selectionchanges) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {
				switch (selectionchanges.getType()) {
					case CLASSHIERARCHY : {
						updateView();
						break;
					}

					default :
						break;
				}
			}
		};
		SwingUtilities.invokeLater(doHighlight);

	}

	private X_SuperClassProtegeRenderer renderer;

	@Override
	public void disposeView() {
		// TODO Auto-generated method stub

	}

	@Override
	protected X_Entity updateView() {
		X_Type sel = selectedClass();
		X_EntityTreeNode selNode = TreeNodeGenerator.getNode(sel,
				getTree().getType());
		this.setHeaderText(sel.render());
		// this is a hack ensuring that all expanded subtrees get collapsed
		X_Type root = (X_Type) getX_ModelManager().getNaryThing();
		getTree().setModel(new DefaultTreeModel(
				TreeNodeGenerator.getNode(root, getTree().getType())));
		// end of hack
		selectionPath = selNode.getPath();
		Set<TreeNode> pathAsSet = new HashSet<TreeNode>(
				Arrays.asList(selNode.getPath()));
		getTree().setSelectionPath(new TreePath(selectionPath));
		this.renderer.setPath(pathAsSet);
		getTree().setCellRenderer(renderer);
		((DefaultTreeModel) getTree().getModel()).reload(selNode);
		return selectedClass();
	}

	@Override
	protected void performExtraInitialisation() throws Exception {
		getTree().setEnabled(false);
		this.renderer = new X_SuperClassProtegeRenderer();
		DefaultTreeModel tm = TreeNodeGenerator
				.getModel(SelectionType.SUPERCLASSHIERARCHY);
		getTree().setModel(tm);
		renderer.setPath(new HashSet<TreeNode>());
		renderer.getPath().add((X_EntityTreeNode) tm.getRoot());
		getTree().setCellRenderer(renderer);
	}

	private X_Type selectedClass() {
		return (X_Type) getX_ModelManager().getActiveModel()
				.getSelection(SelectionType.CLASSHIERARCHY);
	}

	@Override
	protected X_ElementTree initTree() {
		return new X_ElementTree(SelectionType.SUPERCLASSHIERARCHY, false,
				getX_ModelManager()); // SUPERCLASSHIERARCHY
	}

	@Override
	protected void handleModelChanged(final ModelChange modelChanged) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {
				switch (modelChanged.getType()) {
					case RENAME : {
						updateView();
						break;
					}
					default :
						break;
				}
			}
		};
		SwingUtilities.invokeLater(doHighlight);
	}

}
