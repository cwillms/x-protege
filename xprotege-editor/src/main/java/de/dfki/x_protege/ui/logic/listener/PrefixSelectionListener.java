package de.dfki.x_protege.ui.logic.listener;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * 
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 13.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class PrefixSelectionListener implements ListSelectionListener {

	private ListSelectionEvent lastSelection;

	@Override
	public void valueChanged(ListSelectionEvent e) {
		this.lastSelection = e;
	}


}
