package de.dfki.x_protege.ui.logic.actions;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.swing.AbstractAction;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.protege.editor.core.ui.util.JOptionPaneEx;

import de.dfki.x_protege.model.OntologyCache;
import de.dfki.x_protege.model.data.PrefixModel;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.ui.components.table.PrefixMapperTable;
import de.dfki.x_protege.ui.utilities.X_Icons;

/**
 * This action is used to remove an existing mapping to the Namespace.
 * 
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 16.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class RemovePrefixMappingAction extends AbstractAction {

	private static final long serialVersionUID = 5507854297099664212L;

	private PrefixMapperTable table;

	private ListSelectionListener tableSelectionListener = new ListSelectionListener() {

		@Override
		public void valueChanged(ListSelectionEvent e) {
			updateEnabled();
		}
	};

	/**
	 * Creates a new instance of {@link AddPrefixMappingAction}.
	 * 
	 * @param table
	 *            the {@link PrefixMapperTable} this action is connected to.
	 */
	public RemovePrefixMappingAction(PrefixMapperTable table) {
		super("Remove prefix", X_Icons.prefix_remove);
		this.table = table;
		this.table.getSelectionModel()
				.addListSelectionListener(tableSelectionListener);
		updateEnabled();
	}

	private void updateEnabled() {
		if (table == null) {
			setEnabled(false);
			return;
		}
		int row = table.getSelectedRow();
		if (row == -1) {
			setEnabled(false);
			return;
		}
		PrefixModel model = table.getModel();
		String prefix = (String) model.getValueAt(row, 0);
		setEnabled(!model.isStandardPrefix(prefix));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int[] selIndexes = table.getSelectedRows();
		List<String> prefixesToRemove = new ArrayList<String>();
		for (int i = 0; i < selIndexes.length; i++) {
			prefixesToRemove.add(
					table.getModel().getValueAt(selIndexes[i], 0).toString());
		}
		for (String s : prefixesToRemove) {
			Set<X_Entity> mapping = OntologyCache.getPrefixMapping(s);
			if (!mapping.isEmpty()) {
				StringBuilder strb = new StringBuilder("<html>");
				strb.append(
						"This action will remove the following entities:<br> ");
				for (X_Entity en : mapping) {
					strb.append(en.render(true));
					strb.append("<br>");
				}
				strb.append("</html>");
				JLabel info = new JLabel(strb.toString());
				int returnValue = JOptionPaneEx.showConfirmDialog(
						table.getRootPane(), "Deletion Conflict", info,
						JOptionPane.ERROR_MESSAGE, JOptionPane.OK_CANCEL_OPTION,
						null);
				if (returnValue != 2) {
					// Update model
					table.removeFromModel(s);

				}
			}
		}
	}
}
