package de.dfki.x_protege.ui.components.tree;

import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;
import java.awt.dnd.DragSourceListener;

import javax.swing.tree.TreePath;

import de.dfki.x_protege.model.data.AtomicX_Type;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.utils.TransferableTreeNode;

/**
 * Main part of the drag and drop framework used by x-protégé. <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 17.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class TreeDragSource implements DragGestureListener, DragSourceListener {

	private DragSource source;

	private TransferableTreeNode transferable;

	private X_EntityTreeNode oldNode;

	private X_ElementTree sourceTree;

	/**
	 * Creates a new instance of {@link TreeDragSource}
	 * @param tree The {@link X_ElementTree} this instance of {@link TreeDragSource} is connected to.
	 * @param actions Integer value specifying the action connected to this instance of dragSource
     */
	public TreeDragSource(X_ElementTree tree, int actions) {
		sourceTree = tree;
		source = new DragSource();
		source.createDefaultDragGestureRecognizer(sourceTree, actions, this);
	}

	/*
	 * Drag Gesture Handler
	 */
	@Override
	public void dragGestureRecognized(DragGestureEvent dge) {
		TreePath path = sourceTree.getSelectionPath();
		if ((path == null) || (path.getPathCount() <= 1)) {
			// We can't move the root node or an empty selection
			return;
		}
		oldNode = (X_EntityTreeNode) path.getLastPathComponent();
		// We can't move XSD data types
		X_Entity nodeValue = oldNode.getValue();
		if (nodeValue.isAtomType()) {
			if (((AtomicX_Type) nodeValue).isXSDType())
				return;
		}
		transferable = oldNode.getTransferable();
		// If you support dropping the node anywhere, you should probably
		// start with a valid move cursor:
		source.startDrag(dge, DragSource.DefaultMoveDrop, transferable, this);
	}

	/*
	 * Drag Event Handlers
	 */
	@Override
	public void dragEnter(DragSourceDragEvent dsde) {
	}

	@Override
	public void dragExit(DragSourceEvent dse) {
	}

	@Override
	public void dragOver(DragSourceDragEvent dsde) {
	}

	@Override
	public void dropActionChanged(DragSourceDragEvent dsde) {
	}

	@Override
	public void dragDropEnd(DragSourceDropEvent dsde) {
		/* TODO for reasons this was moved into the event handler
		 * to support move only...
		 * if (dsde.getDropSuccess()) {
		 * ((DefaultTreeModel)sourceTree.getModel()).removeNodeFromParent(
		 * oldNode); }
		 */
	}
}
