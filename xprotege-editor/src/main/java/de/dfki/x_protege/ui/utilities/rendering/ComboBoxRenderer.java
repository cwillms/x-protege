package de.dfki.x_protege.ui.utilities.rendering;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.ui.utilities.X_Icons;

/**
 * This {@link ComboBoxRenderer} is used to render instances of {@link javax.swing.JComboBox} used by the SelectionFrames.
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 13.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class ComboBoxRenderer extends JLabel implements ListCellRenderer {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4223723544491903303L;

	@Override
	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
			boolean cellHasFocus) {
		Icon icon = new ImageIcon();
		X_Entity e = (X_Entity) value;
		icon = X_Icons.selectIcon(e);
		setIcon(icon);

		setText(e.render());
		return this;
	}

}
