package de.dfki.x_protege.ui.components;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import de.dfki.x_protege.model.OntologyCache;
import de.dfki.x_protege.model.data.AtomicX_Indivdual;
import de.dfki.x_protege.model.data.CartesianX_Type;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.data.X_Individual;
import de.dfki.x_protege.model.data.X_Type;
import de.dfki.x_protege.ui.components.dialogue.ComplexCreatorPanel;
import de.dfki.x_protege.ui.components.lists.IndividualsList;
import de.dfki.x_protege.ui.components.lists.IndividualsListItem;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.SelectionChange;
import de.dfki.x_protege.ui.logic.Event.Type.ModelChangeType;
import de.dfki.x_protege.ui.logic.Event.Type.SelectionType;
import de.dfki.x_protege.ui.logic.actions.AbstractX_ElementListAction;
import de.dfki.x_protege.ui.utilities.UIHelper;
import de.dfki.x_protege.ui.utilities.X_Icons;
import de.dfki.x_protege.ui.utilities.rendering.X_ProtegeRenderer;

/**
 * The {@link X_IndividualEditorViewComponent} is used to browse and maintain
 * the {@link X_Individual}s connected to the {@link X_Type} currently selected
 * in the {@link IndividualsHierarchyViewComponent}. <br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 20.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class X_IndividualListViewComponent
		extends
			AbstractX_IndividualViewComponent {

	private static final long serialVersionUID = 1L;

	private IndividualsList list;

	private AbstractX_ElementListAction<X_Individual> addInd;

	private AbstractX_ElementListAction<X_Individual> remInd;

	private JCheckBox showImplicitBox;

	private Action showImplicitAction = new AbstractAction(
			"Show Implicit Assignments") {

		/**
		* 
		*/
		private static final long serialVersionUID = -4293977946218798052L;

		@Override
		public void actionPerformed(ActionEvent e) {
			list.setState(showImplicitBox.isSelected());
			updateView();
		}
	};

	@Override
	protected void handleSelectionChanges(
			final SelectionChange selectionchanges) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {

				switch (selectionchanges.getType()) {
					case ICLASSHIERARCHY : {
						updateView();
						break;
					}
					case SETINDIVIDUALLIST : {
						IndividualsListItem match = list
								.findMatch((X_Individual) selectionchanges
										.getValue().getValue());
						if (match != null)
							list.setSelectedValue(match, true);
						break;
					}
					default :
						break;
				}
			}
		};
		SwingUtilities.invokeLater(doHighlight);
	}

	@Override
	protected void handleModelChanged(final ModelChange modelChange) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {

				switch (modelChange.getType()) {
					case INDIVIDUALADD : {
						updateView();
						break;
					}
					case INDIVIDUALREMOVE : {
						updateView();
						break;
					}
					case RENAME : {
						updateView(getSelectedIndividual());
						break;
					}
					default :
						break;
				}
			}
		};
		SwingUtilities.invokeLater(doHighlight);
	}

	private void updateView(X_Entity selectedIndividual) {
		this.updateHeader(selectedIndividual);
		this.list.revalidate();
		this.list.repaint();
	}

	@Override
	public X_Individual updateView() {
		this.list.setRootObject(this.selectedClass());
		boolean hasInstances = !this.selectedClass()
				.getConnectedIndividualsSorted().isEmpty();
		boolean hasImplicitInstances = !this.selectedClass()
				.getSubIndividualsSorted().isEmpty();
		if (hasInstances
				|| (hasImplicitInstances && this.showImplicitBox.isSelected()))
			this.list.selectFirstItem();
		IndividualsListItem item = (IndividualsListItem) this.list
				.getSelectedValue();
		if (item != null) {
			this.remInd.setEnabled(true);
			this.updateHeader(
					((IndividualsListItem) this.list.getSelectedValue())
							.getValue());
		} else {
			this.remInd.setEnabled(false);
			this.updateHeader(null);
		}
		this.revalidate();
		return getSelectedIndividual();
	}

	@Override
	public void initialiseIndividualsView() throws Exception {
		this.showImplicitBox = new JCheckBox(showImplicitAction);
		setLayout(new BorderLayout());
		performExtraInitialisation();

		list = new IndividualsList(getX_EditorKit());
		add(new JScrollPane(list));
		list.setRootObject(selectedClass());
		list.setCellRenderer(new X_ProtegeRenderer());
		add(showImplicitBox, BorderLayout.SOUTH);
		updateView();
	}

	private void performExtraInitialisation() {

		this.addInd = new AbstractX_ElementListAction<X_Individual>(
				"Create Individual", X_Icons.createIndividual) {

			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent event) {
				ArrayList<String> shortName = new ArrayList<String>();
				if (selectedClass().isCartesianType()) {
					ComplexCreatorPanel selectionPanel = new ComplexCreatorPanel(
							getX_EditorKit(),
							(CartesianX_Type) selectedClass());
					int returnValue = new UIHelper(getX_EditorKit())
							.showDialog("Individual", selectionPanel);
					if (returnValue == 0) {
						X_Individual[] newIndis = selectionPanel.getNewIndis();
						// if there were new individuals creates within the
						// specification of the complex one acknowledge them to
						// the model
						// this must be done before the complex individual is
						// acknowledged
						if (newIndis.length != 0) {
							for (int i = 0; i < newIndis.length; i++) {
								if (newIndis[i] != null) {
									AtomicX_Indivdual si = (AtomicX_Indivdual) newIndis[i];
									if (!si.isXSD() && !OntologyCache
											.containsIndividual(
													si.getShortName())) {
										getX_ModelManager()
												.handleChange(new ModelChange(
														ModelChangeType.INDIVIDUALADD,
														newIndis[i]
																.getShortName(),
														i));
									}
								}
							}
						}
						shortName = selectionPanel.getResult();
					}
				} else {
					String returnValue = new UIHelper(getX_EditorKit())
							.getString("Individual",
									"Please enter a name for the new Individual");
					if (returnValue != null && !returnValue.equals("")) {
						if (returnValue.contains(":")) {
							String[] splitted = returnValue.split(":");
							String namespace = splitted[0];
							if (!OntologyCache.getNamespace()
									.contains(namespace)) {
								new UIHelper(getX_EditorKit()).showError(
										"Warning",
										"No valid namespace: " + namespace);
								shortName = new ArrayList<>();
							} else {
								shortName.add("<" + returnValue + ">");
							}
						} else {
							shortName.add("<" + returnValue + ">");
						}
					}
				}
				if (!shortName.isEmpty())

					getX_ModelManager().handleChange(new ModelChange(
							ModelChangeType.INDIVIDUALADD, shortName));

			}

		};

		addAction(addInd, "A", "A");

		this.remInd = new AbstractX_ElementListAction<X_Individual>(
				"Create Individual", X_Icons.deleteIndividual) {

			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent event) {
				int decision = JOptionPane.showConfirmDialog(null,
						"Deleting a Individual may also effect other... ",
						"Individual Remove", JOptionPane.YES_NO_OPTION);
				if (decision == JOptionPane.YES_OPTION) {
					getX_ModelManager().handleChange(
							new ModelChange(ModelChangeType.INDIVIDUALREMOVE,
									getSelectedIndividual()));
				}
			}

		};

		addAction(remInd, "A", "B");

	}

	private X_Type selectedClass() {
		X_Type t = (X_Type) getX_ModelManager().getActiveModel()
				.getSelection(SelectionType.ICLASSHIERARCHY);
		if (t != null) {
			return OntologyCache.getType(t.getShortName());
		}
		return t;
	}

}
