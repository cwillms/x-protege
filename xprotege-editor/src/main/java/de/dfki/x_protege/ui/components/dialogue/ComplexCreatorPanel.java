package de.dfki.x_protege.ui.components.dialogue;

import java.awt.BorderLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.model.data.CartesianX_Individual;
import de.dfki.x_protege.model.data.CartesianX_Type;
import de.dfki.x_protege.model.data.X_Individual;
import de.dfki.x_protege.model.data.X_Type;

/**
 * The {@link ComplexCreatorPanel} is used to create new instances of
 * {@link CartesianX_Individual}s. To do so it uses so called
 * {@link SelectionCell}s representing the subelements of the
 * {@link CartesianX_Individual}.<br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 20.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class ComplexCreatorPanel extends JPanel {

	private static final long serialVersionUID = 744886313453650589L;

	private final CartesianX_Type parentType;
	private JLabel textField;
	private final X_EditorKit editorKit;
	private ArrayList<SelectionCell> selectionCells;

	private ItemListener listener = new ItemListener() {

		@Override
		public void itemStateChanged(ItemEvent e) {
			if (e.getStateChange() == ItemEvent.SELECTED) {
				updateTextField();
			}
		}
	};

	private DocumentListener docListener = new DocumentListener() {

		@Override
		public void removeUpdate(DocumentEvent e) {
			updateTextField();

		}

		@Override
		public void insertUpdate(DocumentEvent e) {
			updateTextField();
		}

		@Override
		public void changedUpdate(DocumentEvent e) {
			updateTextField();
		}
	};

	private ArrayList<X_Individual> selections;

	/**
	 * Create an new instance of {@link ComplexCreatorPanel}.
	 * 
	 * @param editorKit
	 *            The connected {@link X_EditorKit}.
	 * @param type
	 *            The Type of the {@link CartesianX_Individual} represented by
	 *            its {@link X_Type}.
	 */
	public ComplexCreatorPanel(X_EditorKit editorKit, CartesianX_Type type) {
		this.editorKit = editorKit;
		this.parentType = type;
		selectionCells = new ArrayList<>();
		initView();
	}

	private void updateTextField() {
		this.selections = getSelections();
		StringBuilder strb = new StringBuilder("<html>");
		for (int i = 0; i < selections.size(); i++) {
			X_Individual indi = selections.get(i);
			if (i != 0) {
				strb.append(" x ");
			}
			if (indi != null) {
				strb.append(indi.render(true));
			} else {
				strb.append("<font color=rgb(96,96,96)>");
				strb.append(this.parentType.getSubElements().get(i).render());
				strb.append("</font>");
			}
		}
		strb.append("</html>");
		this.textField.setText(strb.toString());
	}

	private void initView() {
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.add(createTextField());
		this.add(new JSeparator());
		this.add(Box.createVerticalStrut(7));
		this.add(createSelectionPanel());
		this.add(Box.createVerticalStrut(7));
		this.add(new JSeparator());
	}

	private JPanel createTextField() {
		JPanel textPanel = new JPanel();
		textPanel.setLayout(new BorderLayout());
		JLabel label = new JLabel("Individual: ");
		textPanel.add(label, BorderLayout.WEST);
		label.setFocusable(false);
		textField = new JLabel(this.parentType.render());
		textPanel.add(textField, BorderLayout.CENTER);
		textField.setFocusable(false);
		return textPanel;
	}

	private JPanel createSelectionPanel() {
		JPanel selectionPanel = new JPanel();
		selectionPanel
				.setLayout(new BoxLayout(selectionPanel, BoxLayout.X_AXIS));
		populateSelectionPanel(selectionPanel);
		return selectionPanel;
	}

	private void populateSelectionPanel(JPanel selectionPanel) {
		for (X_Type t : parentType.getSubElements()) {
			SelectionCell c = new SelectionCell(editorKit, t, this.listener,
					this.docListener);
			this.selectionCells.add(c);
			selectionPanel.add(c);
			selectionPanel.add(Box.createHorizontalStrut(7));
		}
		updateTextField();
	}

	/**
	 * Returns the created {@link CartesianX_Individual} represented as an Array
	 * of {@link String}s. Each entry represents one subelement of the
	 * {@link CartesianX_Individual}. This strings will later be converted to
	 * instances of {@link X_Individual}.
	 * 
	 * @return
	 */
	public ArrayList<String> getResult() {
		ArrayList<String> shortName = new ArrayList<>();
		for (X_Individual i : selections)
			shortName.addAll(i.getShortName());
		return shortName;
	}

	private ArrayList<X_Individual> getSelections() {
		ArrayList<X_Individual> selections = new ArrayList<>();
		for (SelectionCell c : selectionCells) {
			selections.add(c.getSelection());
		}
		return selections;
	}

	/**
	 * Returns the {@link X_Individual}s that were created to populate the
	 * {@link CartesianX_Individual} connected to this
	 * {@link ComplexCreatorPanel}.
	 * 
	 * @return
	 */
	public X_Individual[] getNewIndis() {
		X_Individual[] newIndis = new X_Individual[parentType.getSubElements()
				.size()];
		for (int i = 0; i < newIndis.length; i++) {
			newIndis[i] = selectionCells.get(i).getNewIndis();
		}
		return newIndis;
	}

}
