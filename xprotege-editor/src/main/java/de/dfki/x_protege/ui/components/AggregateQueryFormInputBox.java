package de.dfki.x_protege.ui.components;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.text.BadLocationException;

import de.dfki.x_protege.ui.utilities.X_Icons;

/**
 * This class is a {@link QueryFormInputBox} specialized towards the AGGREGATE statements of a SparSQL query.
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 26.03.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class AggregateQueryFormInputBox extends QueryFormInputBox {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1283214476760318401L;

	private final String[] aggregates = {"Count", "CountDistinct", "DTMax",
			"DTMin", "Identity", "LGetLatest", "LGetLatest2", "LMax", "LMean",
			"LMin", "LSum"};

	private final ActionListener speclistener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			String suggestion = ((JMenuItem) e.getSource()).getText();
			try {
				inputField.getDocument().insertString(
						inputField.getCaretPosition(), suggestion, null);
			} catch (BadLocationException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}
	};

	/**
     * Creates a new instance of {@link AggregateQueryFormInputBox}
	 * @param form the form ({@link QueryFormViewComponent}) this input field is part of
	 * @param header the header of the input field
	 */
	public AggregateQueryFormInputBox(QueryFormViewComponent form,
			String header) {
		super(form, header);
		// TODO Auto-generated constructor stub
	}

    /**
     * This String is shown in the input field if there is  no user generated input. it is used as example
     */
	protected final String GHOSTTEXT = "?number = CountDistinct ?p & ?subject = Identity ?p";


	@Override
	protected JComponent initHeader() {
		JPanel headerPanel = new JPanel();
		JLabel headerLabel = new JLabel(header);
		headerLabel.setBackground(background);
		headerLabel.setForeground(Color.WHITE);
		headerLabel.setOpaque(true);
		headerPanel.add(headerLabel, BorderLayout.WEST);
		JButton helpButton = new JButton(new AbstractAction() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.err.println("helpaction triggered");
				JPopupMenu popup = new JPopupMenu();
				JMenuItem menuItem;
				if (aggregates.length <= 5)
					for (String s : aggregates) {
						menuItem = new JMenuItem(s);
						menuItem.addActionListener(speclistener);
						popup.add(menuItem);
					}
				else {
					int rows = 5;
					int cols = (int) Math.floor(aggregates.length / rows);
					if ((aggregates.length % 2) != 0)
						cols++;
					popup.setLayout(new GridLayout(rows, cols));
					int count = 0;
					for (int r = 0; r < rows; r++) {
						for (int c = 0; c < cols; c++) {
							if (count < aggregates.length)
								popup.add(new JMenuItem(aggregates[count++]));
							else {
								JMenuItem empty = new JMenuItem(" ");
								empty.setEnabled(false);
								popup.add(empty);
							}
						}
					}
				}

				Rectangle rectangle;
				try {
					rectangle = inputField
							.modelToView(inputField.getCaretPosition());
					popup.show(inputField, rectangle.x, rectangle.y);
				} catch (BadLocationException ex) {
				}
			}
		});
		helpButton.setIcon(X_Icons.help);
		helpButton.setBackground(background);
		headerPanel.add(helpButton, BorderLayout.EAST);
		headerPanel.setBackground(background);
		return headerPanel;
	}

	@Override
	public String getValue() {
		if (!this.inputField.getText().isEmpty()) {
			return this.header + " " + this.inputField.getText();
		}
		return "";
	}

	@Override
	protected String getGhostText() {
		return this.GHOSTTEXT;
	}

}
