/**
 * 
 */
package de.dfki.x_protege.ui.logic.Event.Type;

/**
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 22.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public enum QueryEventType {
	SUGGESTION, QUERY, NOTVALID
}
