package de.dfki.x_protege.ui.logic.Event;

/**
 * The {@link Event}s are used to update the model according to GUI actions and
 * vice versa.
 * 
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 13.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class Event {

	protected final Object value;

	/**
	 * Creates a new instance of {@link Event}
	 * @param value An {@link Object} describing the content of this specific {@link Event}( {@link de.dfki.x_protege.model.data.X_Entity}s, in general).
     */
	public Event(Object value) {
		this.value = value;
	}

	public Object getValue() {
		return value;
	}

	public Object getOriginalValue() {
		return value;
	}

	@Override
	public String toString() {
		StringBuilder strb = new StringBuilder();
		strb.append("-");
		if (value == null || value.toString() == "") {
			strb.append("NULL");
		} else {
			strb.append("value = " + value.toString());
		}
		return strb.toString();
	}
}
