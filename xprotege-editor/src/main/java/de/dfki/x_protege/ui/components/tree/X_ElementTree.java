package de.dfki.x_protege.ui.components.tree;

import java.awt.dnd.DnDConstants;
import java.io.NotActiveException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import de.dfki.x_protege.model.X_ModelManager;
import de.dfki.x_protege.model.data.Ontology;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.SelectionChange;
import de.dfki.x_protege.ui.logic.Event.Type.ModelChangeType;
import de.dfki.x_protege.ui.logic.Event.Type.SelectionType;
import de.dfki.x_protege.ui.utilities.rendering.X_ProtegeRenderer;

/**
 * The {@link X_ElementTree} is used by x-protégé to visualiue class and
 * property hierarchies. <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 17.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class X_ElementTree extends JTree {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4075936450361014587L;

	private final SelectionType type;

	TreeDragSource ds;

	TreeDropTarget dt;

	private X_ProtegeTreeUI basicTreeUI = new X_ProtegeTreeUI();

	private TreeSelectionListener listener = new TreeSelectionListener() {

		@Override
		public void valueChanged(TreeSelectionEvent e) {
			modelManagerImpl.handleSelectionChanged(
					new SelectionChange(type, (X_EntityTreeNode) getLastSelectedPathComponent()));// (X_EntityTreeNode)
																									// e.getPath().getLastPathComponent()));
		}

	};

	private boolean dndSupported;

	private X_ModelManager modelManagerImpl;

	/**
	 * Creates a new instance of {@link X_ElementTree}.
	 * 
	 * @param type
	 *            The {@link SelectionType} of this tree.
	 * @param dndSupported
	 *            {@link Boolean} flag indicating whether drag and drop is
	 *            supported.
	 * @param modelManagerImpl
	 *            The {@link X_ModelManager} connected to the currently loaded
	 *            {@link Ontology}.
	 */
	public X_ElementTree(SelectionType type, boolean dndSupported, X_ModelManager modelManagerImpl) {
		initialiseTreeListener();
		this.type = type;
		setUI(this.basicTreeUI);
		this.dndSupported = dndSupported;
		this.modelManagerImpl = modelManagerImpl;
		if (this.dndSupported) {
			ds = new TreeDragSource(this, DnDConstants.ACTION_MOVE);
			dt = new TreeDropTarget(this);
		}
		DefaultTreeModel model = TreeNodeGenerator.getModel(type);
		setModel(model);
		setCellRenderer(new X_ProtegeRenderer());
		setSelectedX_Element(((X_EntityTreeNode) model.getRoot()));
	}

	/**
	 * Selects the given node in the tree.
	 * 
	 * @param element
	 *            the node to be selected.
	 */
	public void setSelectedX_Element(X_EntityTreeNode element) {
		TreeNode[] path = ((DefaultTreeModel) this.treeModel).getPathToRoot(element);
		// System.err.println("-------------------------------------------------");
		// System.err.println("For tree: " + type + " set Selection to " +
		// element.getValue().getShortName());
		// System.err.println("-------------------------------------------------");
		this.selectionModel.setSelectionPath(new TreePath(path));
	}

	/**
	 * Selects the given nodes in the tree.
	 * 
	 * @param elements
	 *            the nodes to be selected.
	 */
	public void setSelectedX_Elements(Set<X_EntityTreeNode> elements) {
		TreePath[] paths = new TreePath[elements.size()];
		int counter = 0;
		for (X_EntityTreeNode e : elements) {
			TreeNode[] path = ((DefaultTreeModel) this.treeModel).getPathToRoot(e);
			paths[counter++] = new TreePath(path);
		}
		this.selectionModel.setSelectionPaths(paths);
	}

	public void dispose() {
		this.removeTreeSelectionListener(listener);
	}


	private void initialiseTreeListener() {
		this.addTreeSelectionListener(listener);
	}

    /**
     *
     * @return the {@link X_Entity} represented by the currently selected {@link X_EntityTreeNode}. In case more then one element is selected, the first one is returned.
     */
	public X_Entity getSelectedX_Element() {
		if (this.getSelectionPath() != null)
			return ((X_EntityTreeNode) this.getSelectionPath().getLastPathComponent()).getValue();
		return null;
	}

    /**
     *
     * @return the {@link X_Entity}s represented by the currently selected {@link X_EntityTreeNode}s.
     */
	public Collection<? extends X_Entity> getSelectedX_Elements() {
		Set<X_Entity> selectedX_Elements = new HashSet<>();
		for (TreePath p : this.getSelectionPaths()) {
			selectedX_Elements.add((X_Entity) p.getLastPathComponent());
		}
		return selectedX_Elements;
	}

	/**
	 * Try to move a node. If drag and drop is supported for this tree inform
	 * the model using a {@link ModelChange}-Event. Else throw an exception.
	 * 
	 * @param node
	 *            The moved node.
	 * @param parent
	 *            The new parent of the node.
	 * @throws NotActiveException
	 */
	public void handleNodeMoved(X_Entity node, X_Entity oldParent, X_Entity parent) throws NotActiveException {
		if (!this.dndSupported)
			throw new NotActiveException();
		else {
			X_Entity[] parents = new X_Entity[] { oldParent, parent };
			this.modelManagerImpl.handleChange(new ModelChange(ModelChangeType.NODEMOVED, node, parents));
		}
	}

    /**
     *
     * @return The selection type connected to this instance of {@link X_ElementTree}
     */
	public SelectionType getType() {
		return this.type;
	}

	/**
	 * @return
	 */
	public X_ModelManager getX_ModelManager() {
		return this.modelManagerImpl;
	}

}
