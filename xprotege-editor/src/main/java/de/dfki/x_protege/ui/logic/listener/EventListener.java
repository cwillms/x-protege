package de.dfki.x_protege.ui.logic.listener;

/**
 * An {@link EventListener} is an essential part of the management system used
 * to maintain the model using the UI and vice Versa.
 * 
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 13.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public interface EventListener {

}
