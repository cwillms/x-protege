package de.dfki.x_protege.ui.utilities.rendering;

import java.awt.Color;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.ListCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.tree.TreeCellRenderer;

import org.protege.editor.core.ui.list.MListItem;

import de.dfki.lt.hfc.Rule;
import de.dfki.x_protege.model.data.AbstractX_Property;
import de.dfki.x_protege.model.data.AnnotationProperty;
import de.dfki.x_protege.model.data.AtomicX_Indivdual;
import de.dfki.x_protege.model.data.CartesianX_Individual;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.data.X_Individual;
import de.dfki.x_protege.model.data.X_Property;
import de.dfki.x_protege.model.data.X_Type;
import de.dfki.x_protege.ui.components.lists.AnnotationPropertiesListItem;
import de.dfki.x_protege.ui.components.lists.ClassItem;
import de.dfki.x_protege.ui.components.lists.DisjointListItem;
import de.dfki.x_protege.ui.components.lists.FindingListItem;
import de.dfki.x_protege.ui.components.lists.IndividualsListItem;
import de.dfki.x_protege.ui.components.lists.PropOFIndiListItem;
import de.dfki.x_protege.ui.components.lists.PropertiesListItem;
import de.dfki.x_protege.ui.components.lists.RuleListItem;
import de.dfki.x_protege.ui.components.lists.SelectionListItem;
import de.dfki.x_protege.ui.components.lists.TypeListItem;
import de.dfki.x_protege.ui.components.tree.X_EntityTreeNode;
import de.dfki.x_protege.ui.utilities.X_Icons;

/**
 * The default renderer used by x-Protege to render {@link X_Entity}s within
 * trees and lists.
 * 
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 13.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class X_ProtegeRenderer extends JLabel
		implements
			ListCellRenderer<MListItem>,
			TreeCellRenderer,
			TableCellRenderer {

	private static final long serialVersionUID = 5896453832614035646L;

	private final Color highlight = new Color(153, 204, 255, 155); // light blue

	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value,
			boolean selected, boolean expanded, boolean leaf, int row,
			boolean hasFocus) {
		if (selected) {
			setForeground(Color.BLACK);
		} else {
			setForeground(Color.BLACK);
		}
		X_Entity e = ((X_EntityTreeNode) value).getValue();
		setIcon(selectIcon(e));
		setText(e.render(false));
		return this;
	}

	@Override
	public Component getListCellRendererComponent(
			JList<? extends MListItem> list, MListItem value, int index,
			boolean isSelected, boolean cellHasFocus) {
		Icon icon = new ImageIcon();
		this.setBackground(Color.WHITE);
		if (value instanceof DisjointListItem) {
			X_Type t = ((DisjointListItem) value).getDisjoints();
			icon = X_Icons.selectIcon(t);
			setIcon(icon);
			setText(t.render());
			return this;
		} else {
			if (value instanceof PropertiesListItem) {

				AbstractX_Property t = ((PropertiesListItem) value)
						.getPropertie();
				icon = X_Icons.selectIcon(t);
				setIcon(icon);
				setText(t.render());
				return this;
			} else {
				if (value instanceof ClassItem
						|| value instanceof TypeListItem) {
					X_Type t;
					if (value instanceof ClassItem)
						t = ((ClassItem) value).getValue();
					else
						t = ((TypeListItem) value).getValue();
					icon = X_Icons.selectIcon(t);
					setIcon(icon);
					setText(t.render());
					return this;
				} else {
					if (value instanceof SelectionListItem) {

						X_Type t = ((SelectionListItem) value).getValue();
						icon = X_Icons.selectIcon(t);
						setIcon(icon);
						setText(t.render());
						return this;
					} else {
						if (value instanceof AnnotationPropertiesListItem) {

							AnnotationProperty t = ((AnnotationPropertiesListItem) value)
									.getValue();
							icon = X_Icons.selectIcon(t);
							setIcon(icon);
							setText(t.render());
							return this;
						} else {
							if (value instanceof IndividualsListItem) {
								X_Individual t = ((IndividualsListItem) value)
										.getIndividual();
								if (t instanceof CartesianX_Individual) {
									CartesianX_Individual ci = (CartesianX_Individual) t;
									StringBuilder strb = new StringBuilder(
											"<html>");
									List<AtomicX_Indivdual> contains = ci
											.getContains();
									for (int c = 0; c < contains.size(); c++) {
										if (c != 0) {
											strb.append(" x ");
										}
										strb.append(
												contains.get(c).render(true));
									}
									strb.append("</html>");
									setText(strb.toString());
									setIcon(null);
									revalidate();
								} else {
									if (t != null) {
										icon = X_Icons.selectIcon(t);
										setIcon(icon);
										setText(t.render());
									}
								}
								return this;
							} else {
								if (value instanceof PropOFIndiListItem) {
									AbstractX_Property p = ((PropOFIndiListItem) value)
											.getProperty();
									ArrayList<X_Individual> values = ((PropOFIndiListItem) value)
											.getValues();
									icon = X_Icons.selectIcon(p);
									setText(render(p, values));
									return this;

								} else {
									if (value instanceof FindingListItem) {
										X_Entity t = ((FindingListItem) value)
												.getSource();
										icon = X_Icons.selectIcon(t);
										setIcon(icon);
										setText(t.render());
										return this;
									} else {
										if (value instanceof RuleListItem) {
											Rule r = ((RuleListItem) value)
													.getSource();
											setIcon(X_Icons.RuleIcon);
											setText(r.getName());
											return this;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		throw new IllegalArgumentException(
				"[X-ProtegeRenderer]not able to compute icon for: " + value);

	}

	private String render(AbstractX_Property p,
			ArrayList<X_Individual> values) {
		StringBuilder strb = new StringBuilder("<html>");
		strb.append(p.render(true)); // replace icon by &lhblk; or #02584 in the
										// right color
		strb.append("<p>&nbsp;&nbsp;&nbsp;&nbsp;");
		// html range
		for (X_Individual i : values) {
			if (values.indexOf(i) < values.size() - 1) {
				if (i != null) {
					strb.append(i.render(true));
					strb.append("</p><p>&nbsp;&nbsp;&nbsp;&nbsp;");
				}
			} else {
				strb.append(i.render(true));
			}
		}
		// html extra
		strb.append("</p></html>");
		return strb.toString().trim();
	}

	// #################### PRIVATE METHODS ################

	private Icon selectIcon(X_Entity e) {
		if (e.isAtomType()) {
			if (e.getSubs().size() == 0) {
				return X_Icons.owlType;
			} else {
				return X_Icons.owlTypePop;
			}
		}
		if (e.isCartesianType()) {
			if (e.getSubs().size() == 0) {
				return X_Icons.cartType;
			} else {
				return X_Icons.cartTypePop;
			}
		}
		if (e instanceof AnnotationProperty) {
			return X_Icons.annoProp;
		}
		if (e.isProperty()) {
			X_Property pr = (X_Property) e;
			switch (pr.getPropertyType().nextSetBit(0)) {
				case 0 :
					return X_Icons.mixedProperty;
				case 1 :
					return X_Icons.dataProperty;
				case 2 :
					return X_Icons.objectProperty;
				default :
					return X_Icons.defaultProperty;
			}
		}
		if (e.isAtomInstance()) {
			AtomicX_Indivdual si = (AtomicX_Indivdual) e;
			if (si.isXSD()) {
				return X_Icons.xsdType;
			}
			return X_Icons.simpleIdividual;
		}
		if (e.isCartesianInstance()) {
			return X_Icons.cartIndividual;
		}
		return X_Icons.defaultIcon;
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		this.setOpaque(true);
		if (isSelected) {
			setForeground(Color.BLACK);
			setBackground(highlight);
		} else {
			setBackground(Color.WHITE);
			setForeground(Color.BLACK);
		}
		X_Entity e = (X_Entity) value;
		if (e != null) {
			setIcon(selectIcon(e));
			setText(e.render(false));
			return this;
		}
		return null;
	}

}
