package de.dfki.x_protege.ui.components.table;

import javax.swing.event.ChangeEvent;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import de.dfki.x_protege.model.OntologyCache;
import de.dfki.x_protege.model.X_ModelManager;
import de.dfki.x_protege.model.data.PrefixModel;
import de.dfki.x_protege.model.data.PrefixModel.Column;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.Type.ModelChangeType;
import de.dfki.x_protege.ui.logic.listener.PrefixSelectionListener;

/**
 * The {@link PrefixMapperTable} is used to visualize the namespace mappings.
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 17.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class PrefixMapperTable extends BasicX_Table {

	private static final long serialVersionUID = -530327139681742341L;
	private PrefixModel model;
	private final X_ModelManager modelManager;

	private TableModelListener editListener = new TableModelListener() {

		/**
		 * Update the model whenever a mapping was changed.
		 */
		@Override
		public void tableChanged(TableModelEvent e) {
			if (model.commitPrefixes()) {
				modelManager.setDirty(modelManager.getActiveModel());
				modelManager.handleChange(
						new ModelChange(ModelChangeType.PREFIXCHANGED, null));
			}
		}
	};

	/**
	 * Creates a new instance of {@link PrefixMapperTable}.
	 * 
	 * @param modelManager
	 *            The connected {@link X_ModelManager}.
	 */
	public PrefixMapperTable(X_ModelManager modelManager) {
		super(OntologyCache.getNamespace());
		this.model = OntologyCache.getNamespace();
		this.modelManager = modelManager;
		this.model.setConnectedTable(this);
		setShowGrid(true);
		setRowHeight(getRowHeight() + 3);
		getColumnModel().getColumn(0).setPreferredWidth(150);
		getColumnModel().getColumn(1).setPreferredWidth(600);
		getColumnModel().getColumn(0)
				.setCellEditor(new PrefixTableCellEditor());
		getColumnModel().getColumn(1)
				.setCellEditor(new PrefixTableCellEditor());
		this.getSelectionModel()
				.addListSelectionListener(new PrefixSelectionListener());
		this.getModel().addTableModelListener(editListener);
	}

	@Override
	protected boolean isHeaderVisible() {
		return true;
	}

	@Override
	public PrefixModel getModel() {
		return (PrefixModel) super.getModel();
	}

	/**
	 * Create a new row and edit focus the first cell.
	 */
	public void createAndEditRow() {
		int index;
		for (int i = 0; true; i++) {
			String candidatePrefix = "p" + i;
			if (model.getIndexOfPrefix(candidatePrefix) < 0) {
				index = model.addMapping(candidatePrefix, "");
				break;
			}
		}
		setRowSelectionInterval(index, index);
		editCellAt(index, Column.PREFIX.ordinal());
	}

	@Override
	public void editingStopped(ChangeEvent arg0) {
		int editingColumn = getEditingColumn();
		String cellValue = (String) getCellEditor().getCellEditorValue();

		super.editingStopped(arg0);
		if (editingColumn == Column.PREFIX_NAME.ordinal()) {
			int newRow = getModel().getIndexOfPrefix(cellValue);

			if (newRow >= 0) {
				setRowSelectionInterval(newRow, newRow);
				editCellAt(newRow, Column.PREFIX.ordinal());
				requestFocus();
			}
		}
	}

	/**
	 * Fires a {@link ModelChange} event indicating that one of the prefixes used within the namespace of the loaded ontology was changed.
	 * @param currentPrefixName the prefix before it was changed.
	 * @param newPrefixName the prefix after it was changed.
     */
	public void prefixChanged(String currentPrefixName, String newPrefixName) {
		modelManager.handleChange(new ModelChange(ModelChangeType.PREFIXCHANGED,
				currentPrefixName, newPrefixName));
	}

	/**
	 * This method is called after a new ontology was created or loaded to update the table accordingly.
	 */
	public void setOntology() {
		this.model = OntologyCache.getNamespace();
	}

	/**
	 * Fires a {@link ModelChange} event indicating that one of the prefixes used within the namespace of the loaded ontology was removed.
	 * @param prefix the prefix to be removed
	 */
	public void removeFromModel(String prefix) {
		modelManager.handleChange(
				new ModelChange(ModelChangeType.NAMESPACEREMOVE, prefix));

	}

}
