package de.dfki.x_protege.ui.components.table;

import java.awt.Color;

import javax.swing.JTable;
import javax.swing.table.TableModel;

/**
 * The {@link BasicX_Table} is a specified {@link JTable} used by x-protégé.
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 17.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class BasicX_Table extends JTable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8087347278152196559L;

	private static int count = 0;

	/**
	 * Creates a new {@link BasicX_Table} featuring the given model.
	 * @param model A {@link TableModel} containing the data to be shown in this instance of {@link BasicX_Table}
     */
	public BasicX_Table(TableModel model) {
		super(model);
		count++;
		setRowHeight(getFontMetrics(getFont()).getHeight() + 3);
		setRowMargin(1);
		if (!isHeaderVisible()) {
			setTableHeader(null);
		}
		setShowGrid(false);
		setShowHorizontalLines(false);
		setShowVerticalLines(false);
		setGridColor(Color.LIGHT_GRAY);
		getColumnModel().setColumnSelectionAllowed(false);
	}

	/**
	 * By default, the table header isn't visible. This method can be overridden
	 * to return true.
	 */
	protected boolean isHeaderVisible() {
		return false;
	}

	@Override
	public String toString() {
		return "table" + count;
	}
}
