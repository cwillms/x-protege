package de.dfki.x_protege.ui.components.lists;

import java.util.ArrayList;
import java.util.Set;

import org.protege.editor.core.ui.list.MList;

import de.dfki.x_protege.model.data.AtomicX_Type;
import de.dfki.x_protege.model.data.CartesianX_Type;
import de.dfki.x_protege.model.data.X_Type;

/**
 * A conflict list is used to inform the user whenever the deletion of an atomic
 * {@link X_Type} would cause changes in cartesian {@link X_Type}s. <br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 17.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class ConflictList extends MList {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5085091736684062281L;

	/**
	 * Create a new instance of {@link ConflictList}.
	 * 
	 * @param t
	 *            The {@link AtomicX_Type} which causes the conflict.
	 */
	public ConflictList(AtomicX_Type t) {
		setRootObject(t);
	}

	@SuppressWarnings("unchecked")
	public void setRootObject(AtomicX_Type t) {

		java.util.List<Object> data = new ArrayList<Object>();

		if (t != null) {
			Set<CartesianX_Type> elements = t.getComplexClasses();
			for (X_Type c : elements) {
				data.add(new FindingListItem(c));
			}
		}

		setListData(data.toArray());
		revalidate();
	}

}
