package de.dfki.x_protege.ui.components;

import java.awt.Component;
import java.util.HashSet;
import java.util.Set;

import org.protege.editor.core.ui.RefreshableComponent;
import org.protege.editor.core.ui.view.ViewAction;

import de.dfki.x_protege.model.data.Ontology;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.SelectionChange;
import de.dfki.x_protege.ui.logic.listener.ModelChangeListener;
import de.dfki.x_protege.ui.logic.listener.SelectionChangeListener;

/**
 * * The {@link AbstractX_SelectionViewComponent} is the basic {@link Component}
 * used by nearly all major GUI components of x-protégé. It provides them with
 * the opportunity to listen to {@link ModelChange} and {@link SelectionChange}
 * -Events. <br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 20.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public abstract class AbstractX_SelectionViewComponent
		extends
			AbstractX_ViewComponent
		implements RefreshableComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3436509499024697735L;

	private Set<ViewAction> registeredActions;

	private boolean initialUpdatePerformed;

	private X_Entity lastDisplayedObject;

	private boolean needsRefresh;

	protected ModelChangeListener modelChangeListener = new ModelChangeListener() {

		@Override
		public void modelChanged(ModelChange modelChanged) {
			handleModelChanged(modelChanged);

		}
	};

	protected SelectionChangeListener selChangeListener = new SelectionChangeListener() {

		@Override
		public void selectionChanged(SelectionChange selectionchanges) {

			handleSelectionChanges(selectionchanges);
		}
	};

	/**
	 * Update the view according to the given {@link ModelChange}.
	 * If the Component is not affine to the {@link de.dfki.x_protege.ui.logic.Event.Type.ModelChangeType} of the event nothing will happen.
	 *
	 * @param modelChanged The {@link ModelChange} event to be handled.
	 */
	protected abstract void handleModelChanged(ModelChange modelChanged);

	/**
	 * Update the view according to the given {@link SelectionChange}.
	 * If the Component is not affine to the {@link de.dfki.x_protege.ui.logic.Event.Type.SelectionType} of the event nothing will happen.
	 *
	 * @param selectionchanges The {@link SelectionChange} event to be handled.
	 */
	protected abstract void handleSelectionChanges(
			SelectionChange selectionchanges);

	/**
	 * The initialize method is called at the start of a plugin instance life
	 * cycle. This method is called to give the plugin a chance to initialize
	 * itself. All plugin initialization should be done in this method rather
	 * than the plugin constructor, since the initialization might need to occur
	 * at a point after plugin instance creation, and a each plugin must have a
	 * zero argument constructor.
	 */
	@Override
	final public void initializeView() throws Exception {
		this.registeredActions = new HashSet<>();
		getX_ModelManager().addSelectionChangeListener(selChangeListener);
		getX_ModelManager().addModelChangeListener(modelChangeListener);
		initialiseView();
		updateViewContentAndHeader();
	}

	@Override
	public void refreshComponent() {
		updateHeader(lastDisplayedObject);
	}

	public abstract void initialiseView() throws Exception;

	@Override
	public abstract void disposeView();

	private void disableRegisteredActions() {
		for (ViewAction action : registeredActions) {
			action.setEnabled(false);
		}
	}

	private void updateViewContentAndHeader() {
		if (!isShowing()) {
			setNeedsRefresh(true);
			return;
		}
		setNeedsRefresh(false);
		if (isPinned() && initialUpdatePerformed) {
			return;
		}
		initialUpdatePerformed = true;
		if (isSynchronizing()) {
			lastDisplayedObject = updateView();
			updateHeader(lastDisplayedObject);
		}
	}

	/**
	 * 	 Set the label in the header to reflect the {@link X_Entity} that the view
	 * is displaying
	 * @param object the {@link X_Entity} that is currently represented by this component.
     */
	protected void updateHeader(X_Entity object) {
		if (object != null) {
			getView().setHeaderText(object.render());
		} else {
			// Not displaying an entity, so disable all actions
			disableRegisteredActions();
			getView().setHeaderText("");
		}
	}

	/**
	 * Request that the view is updated to display the current selection.
	 * 
	 * @return The OWLEntity that the view is displaying. This list is typically
	 *         used to generate the view header text to give the user an
	 *         indication of what the view is displaying.
	 */
	protected abstract X_Entity updateView();

	/**
	 * @return the needsRefresh
	 */
	public boolean isNeedsRefresh() {
		return needsRefresh;
	}

	/**
	 *
	 * @return The ontology specific namespace.
     */
	protected String getOntoNameSpace() {
		Ontology onto = getX_ModelManager().getActiveModel();
		return onto.getLocalNameSpace();
	}

	/**
	 * @param needsRefresh
	 *            the needsRefresh to set
	 */
	public void setNeedsRefresh(boolean needsRefresh) {
		this.needsRefresh = needsRefresh;
	}

}
