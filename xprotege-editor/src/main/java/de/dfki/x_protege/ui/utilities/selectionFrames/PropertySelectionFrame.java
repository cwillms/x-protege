package de.dfki.x_protege.ui.utilities.selectionFrames;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.protege.editor.core.ui.util.AugmentedJTextField;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.model.OntologyCache;
import de.dfki.x_protege.model.data.AbstractX_Property;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.data.X_Property;
import de.dfki.x_protege.ui.components.lists.AnnotationList;
import de.dfki.x_protege.ui.components.lists.ArgumentsList;
import de.dfki.x_protege.ui.components.lists.DomainList;
import de.dfki.x_protege.ui.components.lists.RangeList;
import de.dfki.x_protege.ui.components.tree.TreeNodeGenerator;
import de.dfki.x_protege.ui.components.tree.X_ElementTree;
import de.dfki.x_protege.ui.components.tree.X_EntityTreeNode;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.SelectionChange;
import de.dfki.x_protege.ui.logic.Event.Type.ModelChangeType;
import de.dfki.x_protege.ui.logic.Event.Type.SelectionType;
import de.dfki.x_protege.ui.logic.listener.ModelChangeListener;
import de.dfki.x_protege.ui.logic.listener.SelectionChangeListener;
import de.dfki.x_protege.ui.utilities.UIHelper;
import de.dfki.x_protege.ui.utilities.X_Icons;
import de.dfki.x_protege.ui.utilities.rendering.X_ProtegeAnnotationRenderer;
import de.dfki.x_protege.ui.utilities.rendering.X_ProtegeRenderer;

/**
 * Used when ever a {@link X_Property} needs to be selected.
 * 
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 13.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class PropertySelectionFrame extends AbstractSelectionFrame {

	private static final long serialVersionUID = 3796246646303451264L;

	private final X_EditorKit editorKit;
	private X_ElementTree tree;
	private DefaultTreeModel model;
	private JButton newButton;
	private JButton deleteButton;
	private AugmentedJTextField uriField = new AugmentedJTextField(
			"e.g http://www.example.com/ontologies/myontology");
	private DomainList domainList;
	private RangeList rangeList;
	private ArgumentsList extraList;
	private boolean temp = false;
	private static final String ONTOLOGY_IRI_FIELD_LABEL = "Property URI";

	private Action newAction = new AbstractAction("Add Property",
			X_Icons.property_addSub) {

		/**
		 * 
		 */
		private static final long serialVersionUID = -5518867991991256197L;

		@Override
		public void actionPerformed(ActionEvent e) {
			X_Entity parent = getSelectedProperty();
			editorKit.getX_ModelManager().handleChange(new ModelChange(
					ModelChangeType.TEMPPROPERTYSUBADD, parent));

		}
	};
	private Action deleteAction = new AbstractAction("Delete Property",
			X_Icons.property_delete) {

		/**
		 * 
		 */
		private static final long serialVersionUID = -528454539566842363L;

		@Override
		public void actionPerformed(ActionEvent e) {
			X_Entity selected = getSelectedProperty();
			editorKit.getX_ModelManager().handleChange(new ModelChange(
					ModelChangeType.TEMPPROPERTYDELETE, selected));

		}
	};

	private ModelChangeListener modelChangeListener = new ModelChangeListener() {

		@Override
		public void modelChanged(ModelChange modelChanged) {
			if (modelChanged.getType() == ModelChangeType.TEMPPROPERTYSUBADD
					|| modelChanged
							.getType() == ModelChangeType.TEMPPROPERTYDELETE
					|| modelChanged.getType() == ModelChangeType.TEMPRENAME
					|| modelChanged
							.getType() == ModelChangeType.TEMPANNOTATIONADDED
					|| modelChanged
							.getType() == ModelChangeType.TEMPANNOTATIONREMOVED
					|| modelChanged
							.getType() == ModelChangeType.TEMPANNOTATIONEDITED
					|| modelChanged.getType() == ModelChangeType.TEMPDOMAINADD
					|| modelChanged
							.getType() == ModelChangeType.TEMPDOMAINREMOVE
					|| modelChanged.getType() == ModelChangeType.TEMPEXTRAADD
					|| modelChanged
							.getType() == ModelChangeType.TEMPEXTRAREMOVED
					|| modelChanged.getType() == ModelChangeType.TEMPRANGEADD
					|| modelChanged
							.getType() == ModelChangeType.TEMPRANGEREMOVED) {
				handleModelChange(modelChanged);
			}
		}
	};

	private SelectionChangeListener selChangeListener = new SelectionChangeListener() {

		@Override
		public void selectionChanged(SelectionChange ontoChanged) {
			if (ontoChanged.getType() == SelectionType.TEMPPROPERTYHIERARCHY) {
				handleSelectionChanged();
			}
		}
	};

	private AnnotationList annoList;

	@Override
	protected void handleModelChange(final ModelChange modelChanged) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {
				if (modelChanged.getType() == ModelChangeType.TEMPPROPERTYSUBADD
						|| modelChanged
								.getType() == ModelChangeType.TEMPPROPERTYDELETE) {
					tree.setSelectedX_Element(TreeNodeGenerator.getNode(
							(X_Entity) modelChanged.getValue(),
							tree.getType()));
				}
				updateView();
			}
		};
		SwingUtilities.invokeLater(doHighlight);
	}

	/**
	 * create a new instance of {@link PropertySelectionFrame}
	 * 
	 * @param x_EditorKit
	 *            the connected {@link X_EditorKit}
	 * @param type
	 *            {@link String} representing the type of Property to be
	 *            selected
	 * @param temp
	 *            {@link Boolean} flag indicating whether the dialogue is part
	 *            of an other dialogue
	 */
	public PropertySelectionFrame(X_EditorKit x_EditorKit, String type,
			boolean temp) {
		this.editorKit = x_EditorKit;
		this.setLayout(new BorderLayout());
		this.setDoubleBuffered(true);
		this.temp = temp;
		this.model = TreeNodeGenerator
				.getModel(SelectionType.TEMPPROPERTYHIERARCHY);
		this.buildExtendedView();
		if (type.equals("inverse"))
			this.tree.getSelectionModel()
					.setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		editorKit.getX_ModelManager()
				.addModelChangeListener(modelChangeListener);
		editorKit.getX_ModelManager()
				.addSelectionChangeListener(selChangeListener);
		setSize(new Dimension(540, 10));
		setPreferredSize(new Dimension(540, this.getPreferredSize().height));
		updateView();
	}

	@Override
	protected void updateView() {
		boolean flag = canDeleteProperty();
		tree.repaint();
		tree.revalidate();
		this.deleteButton.setEnabled(flag);
		if (getSelectedProperty() != null) {
			this.uriField.setText(getSelectedProperty().render());
			this.uriField.setFocusable(flag);
			this.annoList.setCanAdd(flag);
			this.annoList.setClassRootObject(getSelectedProperty());
			this.domainList.revalidate();
			this.domainList.setCanAdd(flag);
			this.domainList.setRootObject(getSelectedProperty());
			this.rangeList.revalidate();
			this.rangeList.setCanAdd(flag);
			this.rangeList.setRootObject(getSelectedProperty());
			this.extraList.revalidate();
			this.extraList.setCanAdd(flag);
			this.extraList.setRootObject(getSelectedProperty());
		}
		this.revalidate();
	}

	private boolean canDeleteProperty() {
		if (this.temp) {
			return false;
		}
		if (editorKit.getX_ModelManager().getActiveModel().getTempProperty()
				.contains(getSelectedProperty())) {
			return true;
		}
		return false;
	}

	// ################ PRIVATE METHODS ##################################

	private void buildExtendedView() {
		JSplitPane splitPanel = new JSplitPane();
		JPanel left = new JPanel();
		left.setLayout(new BorderLayout());
		JPanel buttons = createButtons();
		JPanel treePanel = createTreePanel();
		left.add(buttons, BorderLayout.NORTH);
		left.add(treePanel, BorderLayout.CENTER);
		splitPanel.setLeftComponent(left);
		JPanel rightComponent = new JPanel();
		JPanel anno = createAnno();
		JPanel relation = createRelation();
		rightComponent.setLayout(new BorderLayout());
		rightComponent.add(anno, BorderLayout.NORTH);
		rightComponent.add(relation, BorderLayout.CENTER);
		splitPanel.setRightComponent(rightComponent);
		this.add(splitPanel, BorderLayout.CENTER);
	}

	// ######### create Anno start ###############################

	private JPanel createAnno() {
		JPanel annoPanel = new JPanel();
		annoPanel.setLayout(new BorderLayout());
		JPanel uriPanel = createUriPanel();
		JPanel annoList = createAnnoList();
		annoPanel.add(uriPanel, BorderLayout.NORTH);
		annoPanel.add(annoList, BorderLayout.CENTER);
		return annoPanel;
	}

	private JPanel createUriPanel() {
		JPanel uri = new JPanel(new GridBagLayout());
		Insets insets = new Insets(0, 4, 2, 0);
		uri.add(new JLabel(ONTOLOGY_IRI_FIELD_LABEL));
		uri.add(uriField,
				new GridBagConstraints(1, 0, 1, 1, 100.0, 0.0,
						GridBagConstraints.BASELINE_LEADING,
						GridBagConstraints.HORIZONTAL, insets, 0, 0));
		uriField.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				warn(false);
			}
		});

		uriField.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				warn(true);
			}

			@Override
			public void focusGained(FocusEvent e) {
				// Nothing to do here
			}
		});
		return uri;
	}

	private JPanel createAnnoList() {
		JPanel annoList = new JPanel();
		annoList.setLayout(new BorderLayout());
		this.annoList = new AnnotationList(editorKit, true);
		this.annoList.setCellRenderer(new X_ProtegeAnnotationRenderer());
		annoList.add(new JScrollPane(this.annoList), BorderLayout.CENTER);
		X_Entity root = getSelectedProperty();
		if (root != null) {
			this.annoList.setClassRootObject(root);
		}
		return annoList;
	}

	// ######### create Anno end ###############################

	// ######### create Relation begin ###############################

	private JPanel createRelation() {
		JPanel relationPanel = new JPanel();
		relationPanel.setLayout(new BoxLayout(relationPanel, BoxLayout.X_AXIS));
		relationPanel.add(createDomainPanel());
		relationPanel.add(createRangePanel());
		relationPanel.add(createExtraPanel());
		return relationPanel;
	}

	private JPanel createDomainPanel() {
		JPanel domainPanel = new JPanel();
		domainPanel.setLayout(new BorderLayout());
		this.domainList = new DomainList(editorKit, true);
		domainList.setCellRenderer(new X_ProtegeRenderer());
		domainList.setRootObject(getSelectedProperty());
		domainPanel.add(new JScrollPane(domainList), BorderLayout.CENTER);
		return domainPanel;
	}

	private JPanel createRangePanel() {
		JPanel rangePanel = new JPanel();
		rangePanel.setLayout(new BorderLayout());
		this.rangeList = new RangeList(editorKit, true);
		rangeList.setCellRenderer(new X_ProtegeRenderer());
		rangeList.setRootObject(getSelectedProperty());
		rangePanel.add(new JScrollPane(rangeList), BorderLayout.CENTER);
		return rangePanel;
	}

	private JPanel createExtraPanel() {
		JPanel extraPanel = new JPanel();
		extraPanel.setLayout(new BorderLayout());
		this.extraList = new ArgumentsList(editorKit, true);
		extraList.setCellRenderer(new X_ProtegeRenderer());
		extraList.setRootObject(getSelectedProperty());
		extraPanel.add(new JScrollPane(extraList), BorderLayout.CENTER);
		return extraPanel;
	}

	// ######### create Relation end ###############################

	private JPanel createButtons() {
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout());
		newButton = new JButton(newAction);
		deleteButton = new JButton(deleteAction);
		buttonPanel.add(newButton);
		buttonPanel.add(deleteButton);
		return buttonPanel;
	}

	/**
	 * This method simply creates a instance of a {@link JTree} and adds this to
	 * the given {@link JPanel}
	 * 
	 * @param frame
	 *            The {@link JPanel} to add the {@link JTree}
	 */
	private JPanel createTreePanel() {
		JPanel treePanel = new JPanel();
		treePanel.setLayout(new BorderLayout());
		this.tree = new X_ElementTree(SelectionType.TEMPPROPERTYHIERARCHY,
				false, editorKit.getX_ModelManager());
		this.tree.setCellRenderer(new X_ProtegeRenderer());
		this.tree.setModel(model);
		tree.setSelectedX_Element((X_EntityTreeNode) model.getRoot());
		treePanel.add(new JScrollPane(tree), BorderLayout.CENTER);
		return treePanel;
	}

	/**
	 *
	 * @return the currently selected class (instance of {@link X_Property}) in the
	 *         <code>tree</code>.
	 */
	public TreePath[] getSelections() {
		return this.tree.getSelectionModel().getSelectionPaths();
	}

	private AbstractX_Property getSelectedProperty() {
		TreePath[] path = getSelections();
		if (path.length > 0) {
			AbstractX_Property newDomain = (AbstractX_Property) ((X_EntityTreeNode) path[0]
					.getLastPathComponent()).getValue();
			return newDomain;
		}
		return null;
	}

	protected void warn(boolean focus) {
		String name = uriField.getText();
		if (name != null && editorKit.getX_ModelManager().getActiveModel()
				.isValidName(name) && !focus) {
			if (name.contains(":"))
				editorKit.getX_ModelManager().handleChange(
						new ModelChange(ModelChangeType.TEMPRENAME,
								getSelectedProperty(), name));
			else {
				String localOntoName = OntologyCache.relatedOntology
						.getLocalNameSpace() + ":" + name;
				editorKit.getX_ModelManager().handleChange(
						new ModelChange(ModelChangeType.TEMPRENAME,
								getSelectedProperty(), localOntoName));
			}
		} else {
			if (focus && getSelectedProperty() != null) {
				uriField.setText(getSelectedProperty().render());
			} else {
				new UIHelper(editorKit).showError("Rename",
						"You selected a non valid name ...");
				uriField.setText(getSelectedProperty().render());
			}
		}
	}

	/**
	 * 
	 */
	public void dispose() {
		this.editorKit.getX_ModelManager()
				.removeSelectionChangeListener(selChangeListener);
		this.editorKit.getX_ModelManager()
				.removeModelChangeListener(modelChangeListener);
	}
}
