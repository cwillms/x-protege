
package de.dfki.x_protege.ui.components.table;

import de.dfki.x_protege.model.X_ModelManager;
import de.dfki.x_protege.model.utils.Metric;

import javax.swing.*;

/**
 * The {@link MetricTable} is a specified {@link JTable} used by x-protégé to present some statistical information connected to the currently loaded ontology.
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 03.07.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class MetricTable extends JTable{

    private X_ModelManager modelManager;
    private Metric model;

    /**
     * Creates a new instance of {@link MetricTable}
     * @param x_modelManager the {@link X_ModelManager} maintaining the loaded ontologies
     */
    public MetricTable(X_ModelManager x_modelManager) {
        super(x_modelManager.getActiveModel().getMetric());
        this.modelManager = x_modelManager;
    }


    /**
     * This method is called after a new ontology was created or loaded to update the table accordingly.
     */
    public void setOntology() {
        this.model = modelManager.getActiveModel().getMetric();
        System.err.println(model.getValueAt(0,1));
    }
}
