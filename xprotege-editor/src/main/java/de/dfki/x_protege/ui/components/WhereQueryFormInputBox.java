package de.dfki.x_protege.ui.components;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * This class is a {@link QueryFormInputBox} specialized towards the WHERE statements of a SparSQL query.
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 26.03.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class WhereQueryFormInputBox extends QueryFormInputBox {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7418786669661573311L;
	protected final String GHOSTTEXT = "?s ?p ?o";

    /**
     * Creates a new instance of {@link WhereQueryFormInputBox}
     * @param form the form ({@link QueryFormViewComponent}) this input field belongs to.
     * @param header the header of the input field
     */
	public WhereQueryFormInputBox(QueryFormViewComponent form, String header) {
		super(form, header);
		// TODO Auto-generated constructor stub
	}


	@Override
	protected JComponent initHeader() {
		JPanel headerPanel = new JPanel();
		JLabel headerLabel = new JLabel(header);
		headerLabel.setBackground(background);
		headerLabel.setForeground(Color.WHITE);
		headerLabel.setOpaque(true);
		headerPanel.add(headerLabel, BorderLayout.WEST);
		headerPanel.setBackground(background);
		return headerPanel;
	}


	@Override
	public String getValue() {
		if (!this.inputField.getText().isEmpty()) {
			return this.header + " " + this.inputField.getText();
		}
		return "";
	}

	@Override
	protected String getGhostText() {
		return this.GHOSTTEXT;
	}

}
