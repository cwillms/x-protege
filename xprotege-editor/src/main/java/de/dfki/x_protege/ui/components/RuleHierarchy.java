package de.dfki.x_protege.ui.components;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.JScrollPane;

import de.dfki.x_protege.model.data.Ontology;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.ui.components.lists.RuleList;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.SelectionChange;
import de.dfki.x_protege.ui.utilities.rendering.X_ProtegeRenderer;

/**
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 24.04.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class RuleHierarchy extends AbstractX_SelectionViewComponent {

	RuleList hierarchy;
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * main.de.dfki.x_protege.ui.components.AbstractX_SelectionViewComponent#
	 * handleModelChanged(main.de.dfki.x_protege.ui.logic.Event.ModelChange)
	 */
	@Override
	protected void handleModelChanged(ModelChange modelChanged) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * main.de.dfki.x_protege.ui.components.AbstractX_SelectionViewComponent#
	 * handleSelectionChanges(main.de.dfki.x_protege.ui.logic.Event.
	 * SelectionChange)
	 */
	@Override
	protected void handleSelectionChanges(SelectionChange selectionchanges) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * main.de.dfki.x_protege.ui.components.AbstractX_SelectionViewComponent#
	 * initialiseView()
	 */
	@Override
	public void initialiseView() throws Exception {
		this.setLayout(new BorderLayout());
		JLabel header = new JLabel("Loaded rules");
		this.add(header, BorderLayout.NORTH);
		this.hierarchy = new RuleList(this.getX_EditorKit());
		hierarchy.setCellRenderer(new X_ProtegeRenderer());
		this.add(new JScrollPane(hierarchy));
		Ontology ont = getX_ModelManager().getActiveModel();
		hierarchy.setRootObject(ont);
		// updateView();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * main.de.dfki.x_protege.ui.components.AbstractX_SelectionViewComponent#
	 * disposeView()
	 */
	@Override
	public void disposeView() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * main.de.dfki.x_protege.ui.components.AbstractX_SelectionViewComponent#
	 * updateView()
	 */
	@Override
	protected X_Entity updateView() {
		Ontology ont = getX_ModelManager().getActiveModel();
		this.hierarchy.setRootObject(ont);
		return null;
	}

}
