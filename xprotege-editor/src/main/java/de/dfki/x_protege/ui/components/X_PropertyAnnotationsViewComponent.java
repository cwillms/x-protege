package de.dfki.x_protege.ui.components;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import org.protege.editor.core.ui.util.AugmentedJTextField;

import de.dfki.x_protege.model.data.AbstractX_Property;
import de.dfki.x_protege.model.data.Annotation;
import de.dfki.x_protege.ui.components.lists.AnnotationList;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.SelectionChange;
import de.dfki.x_protege.ui.logic.Event.Type.ModelChangeType;
import de.dfki.x_protege.ui.logic.Event.Type.SelectionType;
import de.dfki.x_protege.ui.utilities.UIHelper;
import de.dfki.x_protege.ui.utilities.rendering.X_ProtegeAnnotationRenderer;

/**
 * The {@link X_PropertyAnnotationViewComponent} is used within the class-editor
 * to maintain the {@link Annotation}s connected to the currently selected
 * property. To do so it uses an instance of {@link AnnotationList}. This
 * component also allows the renaming of the currently selected property. <br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 20.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class X_PropertyAnnotationsViewComponent
		extends
			AbstractX_PropertyViewComponent {

	private static final long serialVersionUID = 7821046818687645046L;

	private AnnotationList list;

	private final AugmentedJTextField propertyUriField = new AugmentedJTextField(
			"e.g http://www.example.com/ontologies/myontology");

	public static final String ONTOLOGY_IRI_FIELD_LABEL = "Property URI";

	@Override
	protected void handleSelectionChanges(
			final SelectionChange selectionchanges) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {

				switch (selectionchanges.getType()) {
					case PROPERTYHIERARCHY : {
						updateView();
						boolean defaultProperty = getX_ModelManager()
								.getActiveModel()
								.isStandard(selectedProperty());
						int suffixbegin, suffixEnd;
						if (!defaultProperty) {
							propertyUriField.setFocusable(true);
							propertyUriField.requestFocusInWindow();
							suffixbegin = propertyUriField.getText()
									.indexOf(":") + 1;
							suffixEnd = propertyUriField.getText().length();
							propertyUriField.select(suffixbegin, suffixEnd);
						} else {
							propertyUriField.setFocusable(false);
						}
						break;
					}

					default :
						break;
				}
			}
		};
		SwingUtilities.invokeLater(doHighlight);

	}

	@Override
	protected AbstractX_Property updateView(AbstractX_Property property) {
		AbstractX_Property sel = selectedProperty();
		this.setHeaderText(sel.render());
		propertyUriField.setText(sel.render());

		list.setClassRootObject(sel);
		return sel;
	}

	private AbstractX_Property selectedProperty() {
		return (AbstractX_Property) getX_ModelManager().getActiveModel()
				.getSelection(SelectionType.PROPERTYHIERARCHY);
	}

	@Override
	public void initialiseView() throws Exception {
		setLayout(new BorderLayout());
		propertyUriField.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				warn(false);
			}
		});

		propertyUriField.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				warn(true);
			}

			@Override
			public void focusGained(FocusEvent e) {
				// Nothing to do here
			}
		});
		JPanel ontologyIRIPanel = new JPanel(new GridBagLayout());
		add(ontologyIRIPanel, BorderLayout.NORTH);
		Insets insets = new Insets(0, 4, 2, 0);
		ontologyIRIPanel.add(new JLabel(ONTOLOGY_IRI_FIELD_LABEL));
		ontologyIRIPanel.add(propertyUriField,
				new GridBagConstraints(1, 0, 1, 1, 100.0, 0.0,
						GridBagConstraints.BASELINE_LEADING,
						GridBagConstraints.HORIZONTAL, insets, 0, 0));
		list = new AnnotationList(getX_EditorKit());
		list.setCellRenderer(new X_ProtegeAnnotationRenderer());
		add(new JScrollPane(list));
		list.setClassRootObject(selectedProperty());
		updateView();
	}

	/**
	 * Inform the user if there are problems when renaming the selected class.
	 * 
	 * @param focus this boolean flag indicates whether the warning was triggered because the URI-Field lost focus while there is an unsubmitted change to the uri.
	 *            //TODO Refactor warn in all annotation related view components
	 */
	protected void warn(boolean focus) {
		String name = propertyUriField.getText();
		if (name != null
				&& getX_ModelManager().getActiveModel().isValidName(name)
				&& !focus) {
			if (name.contains(":"))
				getX_ModelManager().handleChange(new ModelChange(
						ModelChangeType.RENAME, selectedProperty(), name));
			else {
				String localOntoName = getOntoNameSpace() + ":" + name;
				getX_ModelManager()
						.handleChange(new ModelChange(ModelChangeType.RENAME,
								selectedProperty(), localOntoName));
			}
		} else {
			if (focus) {
				propertyUriField.setText(selectedProperty().render());
			} else {
				new UIHelper(getX_EditorKit()).showError("Rename",
						"You selected a non valid name ...");
				propertyUriField.setText(selectedProperty().render());
			}
		}
	}

	@Override
	protected void handleModelChanged(final ModelChange modelChanged) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {
				if (modelChanged.getType().equals(ModelChangeType.RENAME)
						|| modelChanged.getType()
								.equals(ModelChangeType.ANNOTATIONADDED)
						|| modelChanged.getType()
								.equals(ModelChangeType.ANNOTATIONEDITED)
						|| modelChanged.getType()
								.equals(ModelChangeType.ANNOTATIONREMOVED))
					updateView();
			}
		};
		SwingUtilities.invokeLater(doHighlight);
	}

}
