package de.dfki.x_protege.ui.components.dialogue;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.protege.editor.core.ui.util.AugmentedJTextField;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.model.OntologyCache;
import de.dfki.x_protege.model.data.AbstractX_Property;
import de.dfki.x_protege.model.data.AtomicX_Indivdual;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.data.X_Individual;
import de.dfki.x_protege.model.data.X_Type;
import de.dfki.x_protege.ui.components.lists.IndividualSelectionList;
import de.dfki.x_protege.ui.components.lists.IndividualsListItem;
import de.dfki.x_protege.ui.components.lists.PropertiesListItem;
import de.dfki.x_protege.ui.components.lists.PropertySelectionList;
import de.dfki.x_protege.ui.utilities.X_Icons;

/**
 * The {@link BasicPropertySelectionPanel} is a {@link SelectionPanel}
 * specialized on Mixed-Properties(e.i., those Properties that are no
 * DataTypeProperties or ObjectProperties). <br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 20.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class BasicPropertySelectionPanel extends SelectionPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4263092303011437853L;
	private PropertySelectionList list;
	private IndividualSelectionList list2;

	private HashSet<AbstractX_Property> props;
	private final static String PANEL1 = "default";
	private final static String PANEL2 = "xsd";
	private JPanel extraPanel;

	private final static String PANEL3 = "default";
	private final static String PANEL4 = "xsd";
	private JPanel rangePanel;

	/**
	 * Creates a new instance of {@link BasicPropertySelectionPanel}.
	 * 
	 * @param i
	 *            The {@link X_Individual} the ObjectProperties are connected
	 *            to.
	 * @param header
	 *            {@link String} used as header for the Dialogue.
	 * @param mode
	 *            The mode indicates whether the selection panel is used to
	 *            represent MixedProperties (0) or DatatypeProperties (1)
	 */
	public BasicPropertySelectionPanel(X_EditorKit editorKit, X_Individual i,
			String header, int mode) {
		super(editorKit, i, header, mode);
	}

	@Override
	protected void performExtraInitialisation() {

		// -----------------------------------------------------------------
		xsdType2 = new JLabel("XSDType:");
		xsdType2.setIcon(X_Icons.xsdType);
		xsdType2.setFocusable(false);
		xsdType2.setOpaque(true);
		xsdType2.setBackground(Color.WHITE);
		textField2 = new AugmentedJTextField("write your stuff here");
		// ----------------------------------------------------------------------
		// -----------------------------------------------------------------
		xsdType = new JLabel("XSDType:");
		xsdType.setIcon(X_Icons.xsdType);
		xsdType.setFocusable(false);
		xsdType.setOpaque(true);
		xsdType.setBackground(Color.WHITE);

		textField = new AugmentedJTextField("write your stuff here");
		// ----------------------------------------------------------------------
		this.props = individual.getConnectedProperties().get(mode);
		initListener();
		textField2.getDocument().addDocumentListener(docListenerExtra);
		textField.getDocument().addDocumentListener(docRangeListener);
		this.list = new PropertySelectionList(props, prpListener);
		this.list2 = new IndividualSelectionList(editorKit, rangeListener,
				"Range:");
		this.list3 = new IndividualSelectionList(editorKit, extraListener,
				"Extra:");
		if (list.getSelectedValue() != null) {
			this.list2.setRootObject(
					((PropertiesListItem) list.getSelectedValue())
							.getPropertie(),
					null);
			this.list3.setRootObject(
					((PropertiesListItem) list.getSelectedValue())
							.getPropertie(),
					null);
		} else {
			this.list2.setRootObject(null, null);
			this.list3.setRootObject(null, null);
		}
		this.centerPane.add(createLeftComponent());
		this.centerPane.add(createRangeComponent());
		this.centerPane.add(createExtraComponent());
		initListener();
	}

	@Override
	protected JPanel createLeftComponent() {
		JPanel left = new JPanel(new BorderLayout());
		JScrollPane scroll = new JScrollPane(list);
		scroll.setHorizontalScrollBarPolicy(
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		left.add(scroll, BorderLayout.CENTER);
		return left;
	}

	@Override
	protected JPanel createRangeComponent() {
		this.rangePanel = new JPanel();
		rangePanel.setLayout(new CardLayout());
		// default -> Select Individual from list;
		JPanel nonXsd = new JPanel();
		nonXsd.setLayout(new BorderLayout());
		initRangeBox();
		nonXsd.add(this.rangeType, BorderLayout.NORTH);
		JScrollPane scroll2 = new JScrollPane(list2);
		scroll2.setHorizontalScrollBarPolicy(
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		nonXsd.add(scroll2, BorderLayout.CENTER);
		rangePanel.add(nonXsd, PANEL3);
		// xsd -> create instance of given xsd Type
		JPanel leftSide = new JPanel();
		leftSide.setBorder(new EtchedBorder());
		leftSide.setLayout(new BorderLayout());
		JPanel north = new JPanel();
		north.add(xsdType, BorderLayout.WEST);
		north.setOpaque(true);
		north.setBackground(Color.WHITE);
		leftSide.add(north, BorderLayout.NORTH);
		leftSide.add(textField, BorderLayout.CENTER);
		rangePanel.add(leftSide, PANEL4);
		return rangePanel;
	}

	@Override
	protected JPanel createExtraComponent() {
		return createExtraPanel();
	}

	private JPanel createExtraPanel() {
		this.extraPanel = new JPanel();
		extraPanel.setLayout(new CardLayout());
		// default -> Select Individual from list;
		JPanel nonXsd = new JPanel();
		nonXsd.setLayout(new BorderLayout());
		initExtraBox();
		nonXsd.add(this.extraType, BorderLayout.NORTH);
		JScrollPane scroll2 = new JScrollPane(list3);
		scroll2.setHorizontalScrollBarPolicy(
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		nonXsd.add(scroll2, BorderLayout.CENTER);
		extraPanel.add(nonXsd, PANEL1);
		// xsd -> create instance of given xsd Type
		JPanel leftSide = new JPanel();
		leftSide.setBorder(new EtchedBorder());
		leftSide.setLayout(new BorderLayout());
		JPanel north = new JPanel();
		north.add(xsdType2, BorderLayout.WEST);
		north.setOpaque(true);
		north.setBackground(Color.WHITE);
		leftSide.add(north, BorderLayout.NORTH);
		leftSide.add(textField2, BorderLayout.CENTER);
		extraPanel.add(leftSide, PANEL2);
		return extraPanel;
	}

	@Override
	public X_Entity[] getSelection() {
		if (extra != null) {
			if (selectedExtraType.isXSDType()
					&& !selectedExtraType.isCartesianType())
				if (!extra.equals("")) {
					ArrayList<String> name = new ArrayList<>();
					name.add("\"" + textField2.getText() + "\"^^"
							+ xsdType2.getText());
					AtomicX_Indivdual si = new AtomicX_Indivdual(name);
					si.setXSD();
					this.selExtra = si;
				}
		} else {
			this.selExtra = null;
		}
		if (range != null) {
			if (selectedRangeType.isXSDType()
					&& !selectedRangeType.isCartesianType())
				if (!range.equals("")) {
					ArrayList<String> name = new ArrayList<>();
					name.add("\"" + textField.getText() + "\"^^"
							+ xsdType.getText());
					AtomicX_Indivdual si = new AtomicX_Indivdual(name);
					si.setXSD();
					this.selRange = si;
				}
		} else {
			this.selRange = null;
		}
		X_Entity[] selections = new X_Entity[]{selProp, selRange, selExtra};
		return selections;
	}

	@Override
	protected void initSpecificListener() {
		this.prpListener = new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (list.getSelectedValue() instanceof PropertiesListItem) {
					PropertiesListItem item = (PropertiesListItem) list
							.getSelectedValue();
					property = item.getPropertie().getShortName().get(0);
					selProp = item.getPropertie();
					updateTupleField();
					// update the extraSelectionPanel
					Set<X_Type> extra = new HashSet<>();
					extra.addAll(item.getPropertie().getExplicitExtra());
					extra.addAll(item.getPropertie().getImplicitExtra());
					CardLayout layout = (CardLayout) extraPanel.getLayout();
					if (extra.size() == 1) {
						X_Type t = (X_Type) extra.toArray()[0];
						if (t.isXSDType() && t.isAtomType()) {
							xsdType2.setText(t.getShortName().get(0));
							selectedExtraType = t;
							layout.show(extraPanel, PANEL2);
						} else {
							extraType.setModel(item.getPropertie()
									.getExtraComboBoxModel());
							selectedExtraType = (X_Type) extraType
									.getSelectedItem();
							list3.setRootObject(item.getPropertie(),
									OntologyCache.getAllType());
							layout.show(extraPanel, PANEL1);
						}
					} else {
						extraType.setModel(
								item.getPropertie().getExtraComboBoxModel());
						selectedExtraType = (X_Type) extraType
								.getSelectedItem();
						list3.setRootObject(item.getPropertie(),
								OntologyCache.getAllType());
						layout.show(extraPanel, PANEL1);
					}
					Set<X_Type> range = new HashSet<>();
					range.addAll(item.getPropertie().getExplicitRange());
					range.addAll(item.getPropertie().getImplicitRange());
					CardLayout layout2 = (CardLayout) rangePanel.getLayout();
					if (range.size() >= 1) {
						X_Type t = (X_Type) range.toArray()[0];
						if (t.isXSDType() && t.isAtomType()) {
							xsdType.setText(t.getShortName().get(0));
							selectedRangeType = t;
							layout2.show(rangePanel, PANEL4);
						} else {
							rangeType.setModel(item.getPropertie()
									.getRangeComboBoxModel());
							selectedRangeType = (X_Type) rangeType
									.getSelectedItem();
							list2.setRootObject(item.getPropertie(),
									OntologyCache.getAllType());
							layout2.show(rangePanel, PANEL3);
						}
					}
				}
			}
		};
		this.rangeListener = new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {

				if (list2.getSelectedValue() instanceof IndividualsListItem) {
					IndividualsListItem item = (IndividualsListItem) list2
							.getSelectedValue();
					selRange = item.getIndividual();
					range = selRange.getPrettyPrint(true);
					updateTupleField();
				}
			}
		};

	}

	@Override
	protected void updateRangeList(X_Type t) {
		if (OntologyCache.getAllType().equals(t)) {
			// show all individuals of all instances
			list2.setRootObject(this.selProp, null);
		} else {
			// show only the individuals of the selected class
			list2.setRootObject(this.selProp, t);
		}
	}

	@Override
	protected void updateExtraList(X_Type t) {
		if (OntologyCache.getAllType().equals(t)) {
			// show all individuals of all instances
			list3.setRootObject(this.selProp, null);
		} else {
			// show only the individuals of the selected class
			list3.setRootObject(this.selProp, t);
		}
	}

}
