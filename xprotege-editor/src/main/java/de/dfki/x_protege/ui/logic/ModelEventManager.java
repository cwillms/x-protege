package de.dfki.x_protege.ui.logic;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import de.dfki.x_protege.model.OntologyCache;
import de.dfki.x_protege.model.data.AbstractX_Property;
import de.dfki.x_protege.model.data.Annotation;
import de.dfki.x_protege.model.data.Ontology;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.data.X_Individual;
import de.dfki.x_protege.model.data.X_Property;
import de.dfki.x_protege.model.data.X_Type;
import de.dfki.x_protege.ui.components.tree.TreeNodeGenerator;
import de.dfki.x_protege.ui.components.tree.X_EntityTreeNode;
import de.dfki.x_protege.ui.logic.Event.Event;
import de.dfki.x_protege.ui.logic.Event.InfromEvent;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.Type.ModelChangeType;
import de.dfki.x_protege.ui.logic.Event.Type.SelectionType;
import de.dfki.x_protege.ui.logic.listener.EventListener;
import de.dfki.x_protege.ui.logic.listener.InformEventListener;
import de.dfki.x_protege.ui.logic.listener.ModelChangeListener;

/**
 * The {@link ModelEventManager} is used to maintain the model (e.i.,
 * {@link Ontology} and {@link OntologyCache}) of x-protégé according to
 * {@link ModelChange}-Events. It is also used to maintain the GUI elements
 * after a change in the backend. <br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 16.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class ModelEventManager extends EventManager {

	private InformEventListener informEventListener;

	private EventUtilsModel eventUtilsModel;

	/**
	 * Creates a new instance of {@link ModelEventManager}
	 * 
	 * @param ontology
	 *            The connected instance of {@link Ontology}.
	 */
	public ModelEventManager(Ontology ontology) {
		super(ontology);
		this.eventUtilsModel = new EventUtilsModel(ontology, this);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void handle(Event modelChange) {
		this.eventUtilsModel.clear();
		ModelChange change = null;
		relatedOntology.setDirty(true);
		ModelChange c = (ModelChange) modelChange;
		switch (c.getType()) {
			case PREFIXCHANGED : {
				Set<X_Entity> effected = new HashSet<>();
				if (OntologyCache
						.getPrefixMapping((String) c.getValue()) != null)
					effected.addAll(OntologyCache
							.getPrefixMapping((String) c.getValue()));
				for (X_Entity e : effected) {
					if (!e.isCartesianType() && !e.isCartesianInstance()) {
						String name = e.getShortName().get(0).replaceFirst(
								(String) c.getValue(), (String) c.getValue2());
						this.eventUtilsModel.handleRename(e, name, true);
					}
				}
				change = c;
				break;
			}
			case RENAME : {
				this.eventUtilsModel.handleRename((X_Entity) c.getValue(),
						"<" + c.getValue2() + ">", true);
				change = c;
				break;
			}
			case TEMPRENAME : {
				this.eventUtilsModel.handleRename((X_Entity) c.getValue(),
						"<" + c.getValue2() + ">", false);
				change = c;
				break;
			}
			case CLASSREMOVED : {
				if (c.getValue2() != null) {
					this.eventUtilsModel.setAccept(true);
				}
				X_EntityTreeNode node = TreeNodeGenerator.getNode(
						(X_Entity) c.getValue(), SelectionType.CLASSHIERARCHY);
				X_Entity parent = node.getParentEntity();

				if (this.eventUtilsModel.handleClassRemoved(
						(X_Entity) c.getValue(), true, false)) {
					change = new ModelChange(ModelChangeType.CLASSREMOVED,
							parent, c.getValue());
				} else {
					change = new ModelChange(ModelChangeType.WARNING,
							modelChange.getValue());
				}
				break;
			}
			case CLASSSUBADDED : {
				X_Entity newChild = this.eventUtilsModel
						.handleSubClassAdded((X_Entity) c.getValue(), true);
				change = new ModelChange(ModelChangeType.CLASSSUBADDED,
						newChild);
				break;
			}
			case CARTSUBADD : {
				change = c;
				break;
			}
			case CARTSUBADDED : {
				X_Entity newChild = this.eventUtilsModel
						.handleCartSubClassAdded(null, true);
				change = new ModelChange(ModelChangeType.CARTSUBADDED,
						newChild);
				break;
			}
			case CLASSSIBADDED : {
				X_Entity newSibling = this.eventUtilsModel
						.handleSibClassAdded((X_Entity) c.getValue(), true);
				change = new ModelChange(ModelChangeType.CLASSSIBADDED,
						newSibling);
				break;
			}
			case PROPERTYREMOVED : {
				X_EntityTreeNode node = TreeNodeGenerator.getNode(
						(X_Entity) c.getValue(),
						SelectionType.PROPERTYHIERARCHY);
				X_Entity parent = node.getParentEntity();
				this.eventUtilsModel.handlePropertyRemoved(
						(X_Entity) c.getValue(), true, false);
				change = new ModelChange(ModelChangeType.PROPERTYREMOVED,
						parent);
				break;
			}
			case PROPERTYSUBADDED : {
				X_Entity newChild = this.eventUtilsModel
						.handleSubPropertyAdded((X_Entity) c.getValue(), true);
				change = new ModelChange(ModelChangeType.PROPERTYSUBADDED,
						newChild);
				break;
			}
			case PROPERTYSIBADDED : {
				X_Entity newSibling = this.eventUtilsModel
						.handleSibPropertyAdded((X_Entity) c.getValue(), true);
				change = new ModelChange(ModelChangeType.PROPERTYSIBADDED,
						newSibling);
				break;
			}
			case INDIVIDUALADD : {
				boolean valid = true;
				if (c.getValue2() != null) {
					if (c.getValue2() instanceof X_Type) {
						X_Type type = (X_Type) c.getValue2();
						valid = this.eventUtilsModel.handleIndividualAdd(
								(ArrayList<String>) c.getValue(), type);
					} else {
						valid = this.eventUtilsModel.handleIndividualAdd(
								(ArrayList<String>) c.getValue(),
								(Integer) c.getValue2());
					}
				} else {
					valid = this.eventUtilsModel.handleIndividualAdd(
							(ArrayList<String>) c.getValue());
				}
				if (valid)
					change = c;
				else
					change = new ModelChange(ModelChangeType.NONVALID,
							c.getValue());
				break;
			}
			case INDIVIDUALREMOVE : {
				this.eventUtilsModel
						.handleIndividualRemove((X_Individual) c.getValue());
				change = c;
				break;
			}
			case DOMAINADDED : {
				if (c.getValue2() instanceof Set) {
					Set<X_Type> v = (Set<X_Type>) c.getValue2();
					for (X_Type t : v) {
						this.eventUtilsModel.handleDomainAdded(
								(X_Entity) c.getValue(), t, true);
					}
				} else {
					this.eventUtilsModel.handleDomainAdded(
							(X_Entity) c.getValue(), (X_Type) c.getValue2(),
							true);
				}
				change = c;
				break;
			}
			case DOMAINREMOVED : {
				// TODO add warning if Removing would affect instances !
				this.eventUtilsModel.handleDomainRemoved(
						(X_Entity) c.getValue(), (X_Entity) c.getValue2(),
						true);
				change = c;
				break;
			}
			case RANGEADDED : {
				this.eventUtilsModel.handleRangeAdded((X_Entity) c.getValue(),
						c.getValue2(), true);
				change = c;
				break;
			}
			case RANGEREMOVED : {
				// TODO add warning if Removing would affect instances !
				this.eventUtilsModel.handleRangeRemoved((X_Entity) c.getValue(),
						(X_Entity) c.getValue2(), true);
				change = c;
				break;
			}
			case EXTRAADDED : {
				this.eventUtilsModel.handleExtraAdded((X_Entity) c.getValue(),
						c.getValue2(), true);
				change = c;
				break;
			}
			case EXTRAREMOVED : {
				// TODO add warning if Removing would affect instances !
				this.eventUtilsModel.handleExtraRemoved((X_Entity) c.getValue(),
						(X_Entity) c.getValue2(), true);
				change = c;
				break;
			}
			case DISJOINTADDED : {
				this.eventUtilsModel.handleDisjointAdded(
						(X_Entity) c.getValue(), (Set<X_Type>) c.getValue2());
				change = c;
				break;
			}
			case DISJOINTREMOVED : {
				this.eventUtilsModel.handleDisjointRemoved(
						(X_Entity) c.getValue(), c.getValue2());
				change = c;
				break;
			}
			case ANNOTATIONADDED : {
				this.eventUtilsModel
						.handleAnnotationAdded((Annotation) c.getValue(), true);
				change = c;
				break;
			}
			case TEMPANNOTATIONADDED : {
				this.eventUtilsModel.handleAnnotationAdded(
						(Annotation) c.getValue(), false);
				change = c;
				break;
			}
			case TEMPANNOTATIONREMOVED : {
				this.eventUtilsModel.handleAnnotationRemoved(
						(Annotation) c.getValue(), false);
				change = c;
				break;
			}
			case TEMPANNOTATIONEDITED : {
				this.eventUtilsModel.handleAnnotationEdited(
						(Annotation) c.getValue(), (String) c.getValue2(),
						false);
				change = c;
				break;
			}
			case ANNOTATIONREMOVED : {
				this.eventUtilsModel.handleAnnotationRemoved(
						(Annotation) c.getValue(), true);
				change = c;
				break;
			}
			case ANNOTATIONEDITED : {
				this.eventUtilsModel.handleAnnotationEdited(
						(Annotation) c.getValue(), (String) c.getValue2(),
						true);
				change = c;
				break;
			}
			case CHARCHANGED : {
				change = new ModelChange(ModelChangeType.CHARCHANGED,
						this.eventUtilsModel
								.handleCharChanged((int) c.getValue()));
				break;
			}
			case SHOWFINDING : {
				change = c;
				break;
			}
			case INVERSEADD : {
				this.eventUtilsModel.handleInverseOfAdd(
						(AbstractX_Property) c.getValue(),
						(AbstractX_Property) c.getValue2());
				change = c;
				break;
			}
			case INVERSEDEL : {
				this.eventUtilsModel
						.handleInverseDel((AbstractX_Property) c.getValue());
				change = c;
				break;
			}
			case INDIVIDUALPADD : {
				this.eventUtilsModel
						.handleIndividualPropAdd((X_Entity[]) c.getValue());
				change = c;
				break;
			}
			case INDIVIDUALPROPREMOVE : {
				this.eventUtilsModel.handleIndividualPropRemove(
						(AbstractX_Property) c.getValue(),
						(ArrayList<X_Individual>) c.getValue2());
				change = c;
				break;
			}
			case INDIVIDUALTYPEADD : {
				this.eventUtilsModel
						.handleIndividualTypeAdd((X_Type) c.getValue());
				change = c;
				break;
			}
			case INDIVIDUALTYPEREMOVE : {
				this.eventUtilsModel
						.handleIndividualTypeRemove((X_Type) c.getValue());
				change = c;
				break;
			}
			case TEMPCLASSSUBADD : {
				X_Entity child = this.eventUtilsModel
						.handleSubClassAdded((X_Type) c.getValue(), false);
				change = new ModelChange(ModelChangeType.TEMPCLASSSUBADD,
						child);
				break;
			}
			case TEMPCLASSSIBADD : {
				X_Entity sibling = this.eventUtilsModel
						.handleSibClassAdded((X_Type) c.getValue(), false);
				change = new ModelChange(ModelChangeType.TEMPCLASSSIBADD,
						sibling);
				break;
			}
			case TEMPCLASSDELETE : {
				X_Entity parent = TreeNodeGenerator
						.getNode((X_Entity) c.getValue(),
								SelectionType.CLASSHIERARCHY)
						.getParentEntity();
				change = new ModelChange(ModelChangeType.TEMPCLASSDELETE,
						parent);
				this.eventUtilsModel.handleClassRemoved((X_Type) c.getValue(),
						false, false);
				break;
			}
			case CANCEL : {
				this.eventUtilsModel.handleCancel();
				change = new ModelChange(ModelChangeType.CANCEL, null);
				break;
			}
			case TEMPDOMAINADD : {
				if (c.getValue2() instanceof Set) {
					Set<X_Type> v = (Set<X_Type>) c.getValue2();
					for (X_Type t : v) {
						this.eventUtilsModel.handleDomainAdded(
								(X_Entity) c.getValue(), t, true);
					}
				} else {
					this.eventUtilsModel.handleDomainAdded(
							(X_Entity) c.getValue(), (X_Type) c.getValue2(),
							true);
				}
				change = c;
				break;
			}
			case TEMPDOMAINREMOVE : {
				this.eventUtilsModel.handleDomainRemoved(
						(X_Entity) c.getValue(), (X_Entity) c.getValue2(),
						false);
				change = c;
				break;
			}
			case TEMPPROPERTYSUBADD : {
				X_Entity child = this.eventUtilsModel
						.handleSubPropertyAdded((X_Entity) c.getValue(), false);
				change = new ModelChange(ModelChangeType.TEMPPROPERTYSUBADD,
						child);
				break;
			}
			case TEMPPROPERTYDELETE : {
				X_Entity parent = TreeNodeGenerator
						.getNode((X_Entity) c.getValue(),
								SelectionType.PROPERTYHIERARCHY)
						.getParentEntity();
				change = new ModelChange(ModelChangeType.TEMPPROPERTYDELETE,
						parent);
				this.eventUtilsModel.handlePropertyRemoved(
						(X_Entity) c.getValue(), false, false);
				break;
			}
			case ACCEPT : {
				this.eventUtilsModel.handleAccept();
				change = c;
				break;
			}
			case TEMPRANGEADD : {
				this.eventUtilsModel.handleRangeAdded((X_Entity) c.getValue(),
						c.getValue2(), false);
				change = c;
				break;
			}
			case TEMPRANGEREMOVED : {
				this.eventUtilsModel.handleRangeRemoved((X_Entity) c.getValue(),
						(X_Entity) c.getValue2(), false);
				change = c;
				break;
			}
			case TEMPEXTRAADD : {
				this.eventUtilsModel.handleExtraAdded((X_Entity) c.getValue(),
						c.getValue2(), false);
				change = c;
				break;
			}
			case TEMPEXTRAREMOVED : {
				this.eventUtilsModel.handleExtraRemoved((X_Entity) c.getValue(),
						(X_Entity) c.getValue2(), false);
				change = c;
				break;
			}
			case TEMPCARTSUBADDED : {
				this.eventUtilsModel.handleCartSubClassAdded(
						(X_Entity) c.getValue(), false);
				change = c;
				break;
			}
			case EXTERNINDIVIDUALADD : {
				if (this.eventUtilsModel.handleExternIndividualAdd(
						(ArrayList<String>) c.getValue(),
						(X_Type) c.getValue2())) {
					change = c;
				} else {
					change = new ModelChange(ModelChangeType.NONVALID,
							c.getValue());
				}
				break;
			}
			case NODEMOVED : {
				X_Entity[] parents = (X_Entity[]) c.getValue2();
				this.eventUtilsModel.handleNodeMoved((X_Entity) c.getValue(),
						parents[0], parents[1]);
				change = c;
				break;
			}
			case NAMESPACEREMOVE : {
				this.eventUtilsModel
						.handleNamespaceRemove((String) c.getValue());

				change = c;

				break;
			}
			default :
				break;
		}
		for (EventListener mcl : this.listener) {
			((ModelChangeListener) mcl).modelChanged(change);
		}
	}

	// ###################### HELPER ####################################

	public void setInformEventListener(
			InformEventListener informEventListener2) {
		this.informEventListener = informEventListener2;

	}

	/**
	 * This method informs the user in case he wants to add a invalid
	 * {@link X_Type} to the domain, range or extra arguments of a
	 * {@link X_Property}
	 */
	void inform(AbstractX_Property p, X_Type t, int type) {
		int propertyArity;
		String v = "";
		switch (type) {
			case 0 :
				v = "domain";
				propertyArity = p.getDomainArity();
				break;
			case 1 :
				v = "range";
				propertyArity = p.getRangeArity();
				break;
			case 2 :
				v = "extra";
				propertyArity = p.getExtraArity();
				break;
			default :
				propertyArity = 1;
				break;
		}
		String message = "You can not add " + t.render() + " (arity: "
				+ t.getArity() + " ) to the " + v + " (arity: " + propertyArity
				+ ") of " + p.render() + ".";
		this.informEventListener
				.informEvent(new InfromEvent("ModelEventManager", message));
	}

	/**
	 * This method informs the user in case he wants to add a invalid
	 * {@link X_Type} to the domain, range or extra arguments of a
	 * {@link X_Property}
	 */
	void inform2(AbstractX_Property p, X_Type t, int type) {
		String message = "";
		switch (type) {
			case 0 :
				message = "You are not allowed to add any atomic xsd-Types to the domain of "
						+ p.render();
				break;
			case 1 :
				message = "You are not allowed to have atomic xsd-Types and URIs as the range of "
						+ p.render();
				break;
			case 2 :
				message = "You are not allowed to have atomic xsd-Types and URIs as ExtraArguments of "
						+ p.render();
				break;
			default :
				break;
		}
		this.informEventListener
				.informEvent(new InfromEvent("ModelEventManager", message));
	}

}
