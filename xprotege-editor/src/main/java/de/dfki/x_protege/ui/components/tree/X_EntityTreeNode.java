package de.dfki.x_protege.ui.components.tree;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.utils.TransferableTreeNode;

/**
 * The {@link X_EntityTreeNode} is used to represent a {@link X_Entity} within the various trees used in the Frontend of X-protege.
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 05.03.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class X_EntityTreeNode extends DefaultMutableTreeNode {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8768324706723282538L;

	private final X_Entity value;

	/**
	 * Create a new instance of {@link X_EntityTreeNode}.
	 * 
	 * @param the
	 *            {@link X_Entity} to be represented.
	 */
	public X_EntityTreeNode(X_Entity value) {
		this.value = value;
	}

    /**
     *
     * @return The {@link X_Entity} represented by this instance of {@link X_EntityTreeNode}
     */
	public X_Entity getValue() {
		return value;
	}

	public TransferableTreeNode getTransferable() {
		return new TransferableTreeNode(new TreePath(this.getPath()));
	}

    /**
     *
     * @return the {@link X_Entity} associated with the parent {@link X_EntityTreeNode} of this instance.
     */
	public X_Entity getParentEntity() {
		if (this.getParent() == null)
			return null;
		return ((X_EntityTreeNode) this.getParent()).getValue();
	}

}
