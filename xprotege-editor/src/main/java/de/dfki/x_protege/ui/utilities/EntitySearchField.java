package de.dfki.x_protege.ui.utilities;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.protege.editor.core.ui.util.AugmentedJTextField;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.ui.X_Workspace;
import de.dfki.x_protege.ui.logic.search.EntitiyFinder;

/**
 * This extended {@link AugmentedJTextField} is used as input-field for queries.
 * In general the {@link EntitySearchField} is a part of the {@link X_Workspace}
 * directly located beneath the MenuBar.
 * 
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 13.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class EntitySearchField extends AugmentedJTextField {

	private static final long serialVersionUID = -5383341925424297227L;

	private ResearchCreator creator;

	/**
	 * 
	 */
	private EntitiyFinder entfinder;

	/**
	 * Creates a new instance of the {@link EntitySearchField}.
	 * 
	 * @param editorKit
	 *            The {@link X_EditorKit}
	 */
	public EntitySearchField(ResearchCreator creator) {
		super(20, "Search for entity");
		this.creator = creator;
		putClientProperty("JTextField.variant", "search");
		this.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				warn(false);
			}
		});

		this.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				warn(true);
			}

			@Override
			public void focusGained(FocusEvent e) {
				// Nothing to do here
			}
		});
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
				}
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_UP) {
					// decrementListSelection();
				}
				if (e.getKeyCode() == KeyEvent.VK_DOWN) {
					// incrementListSelection();
				}
			}
		});
		getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void changedUpdate(DocumentEvent e) {
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				// performFind();
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				// performFind();
			}
		});
	}

	private void warn(boolean focus) {
		if (entfinder != null) {
			entfinder.cancel(true);
		}
		String searchTerm = this.getText();
		if (searchTerm != null && !searchTerm.equals("") && !focus) {
			this.entfinder = creator.getEntitySearcher(searchTerm);
			entfinder.execute();
		} else {
			if (focus) {
				this.setText("");
				this.setGhostText("Search for entity");
			}
		}
	}

}
