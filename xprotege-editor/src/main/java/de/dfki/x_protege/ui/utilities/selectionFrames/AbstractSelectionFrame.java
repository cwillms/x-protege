package de.dfki.x_protege.ui.utilities.selectionFrames;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import de.dfki.x_protege.model.OntologyCache;
import de.dfki.x_protege.model.data.Ontology;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.listener.SelectionChangeListener;

/**
 * The SelectionFrames are used when ever there is the need to select a entity
 * of the ontology.
 * 
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 13.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public abstract class AbstractSelectionFrame extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * this method is called by the {@link SelectionChangeListener}.
	 */
	protected void handleSelectionChanged() {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {
				updateView();
			}
		};
		SwingUtilities.invokeLater(doHighlight);
	}

	/**
	 * Update the Selection Frame after the model or the selection changes.
	 */
	protected abstract void updateView();

	/**
	 * Called when ever there is a change in the model ( {@link Ontology} or
	 * {@link OntologyCache}). Mostly results in a update of the {@link JPanel}
	 * 
	 * @param modelChanged
	 *            {@link ModelChange} indicating the change of the model.
	 */
	protected abstract void handleModelChange(final ModelChange modelChanged);

}
