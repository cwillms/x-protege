package de.dfki.x_protege.ui.logic.listener;

import de.dfki.x_protege.ui.logic.Event.ModelChange;

/**
 * The {@link ModelChangeListener} is used by almost all components of
 * X-Protege. It listens to so called {@link ModelChange}s and updates the
 * connected Component according to this events. <br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 13.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public abstract class ModelChangeListener implements EventListener {

	/**
	 * Handles the given {@link ModelChange} and updates the view afterwards.
	 * 
	 * @param modelChanged The {@link ModelChange} event to be handled. May be everything from the creation of a new type, over renaming an entity to the change of the entire subclass hierarchy.
	 */
	public abstract void modelChanged(ModelChange modelChanged);

}
