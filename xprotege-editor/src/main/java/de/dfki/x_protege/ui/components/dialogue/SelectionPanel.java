package de.dfki.x_protege.ui.components.dialogue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Set;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.bind.Marshaller.Listener;

import org.protege.editor.core.ui.util.AugmentedJTextField;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.model.data.AbstractX_Property;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.data.X_Individual;
import de.dfki.x_protege.model.data.X_Property;
import de.dfki.x_protege.model.data.X_Type;
import de.dfki.x_protege.ui.components.X_IndividualEditorViewComponent;
import de.dfki.x_protege.ui.components.lists.IndividualSelectionList;
import de.dfki.x_protege.ui.components.lists.IndividualsListItem;
import de.dfki.x_protege.ui.utilities.rendering.X_ProtegeComboRenderer;

/**
 * {@link SelectionPanel}s are used by x-protege in 3 incarnations, namely
 * {@link BasicPropertySelectionPanel}, {@link DataTypePropertySelectionPanel}
 * and {@link ObjectPropertySelectionPanel}. They are used within the
 * {@link X_IndividualEditorViewComponent} to allow the population of
 * {@link X_Property}s connected to an {@link X_Individual}. <br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 20.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public abstract class SelectionPanel extends JPanel {

	private static final long serialVersionUID = 8737051787400136333L;

	protected final Set<X_Type> connectedTypes;

	protected final X_Individual individual;

	public final String header;

	private final JLabel tupleField = new JLabel();

	protected final X_EditorKit editorKit;

	protected final JPanel centerPane = new JPanel();

	protected final String domain;

	protected String property;

	protected String range;

	protected String extra;

	protected JComboBox<X_Type> rangeType;

	protected AugmentedJTextField textField;

	protected AugmentedJTextField textField2;

	protected JLabel xsdType2;

	protected JLabel xsdType;

	protected IndividualSelectionList list3;

	protected AbstractX_Property selProp;

	protected X_Individual selRange;

	protected X_Individual selExtra;

	protected JComboBox<X_Type> extraType;

	protected ListSelectionListener prpListener;

	protected ListSelectionListener rangeListener;

	protected ListSelectionListener extraListener;

	protected DocumentListener docListenerExtra;

	protected DocumentListener docRangeListener;

	protected X_Type selectedExtraType;

	protected X_Type selectedRangeType;

	/**
	 * The mode indicates whether the selection panel is used to represent
	 * MixedProperties (0) or DatatypeProperties (1)
	 */
	protected final int mode;

	/**
	 * Creates a new instance of {@link SelectionPanel}.
	 * 
	 * @param i
	 *            The {@link X_Individual} the DataTypeProperties are connected
	 *            to.
	 * @param header
	 *            {@link String} used as header for the Dialogue.
	 */
	public SelectionPanel(X_EditorKit editorKit, X_Individual i, String header,
			int mode) {
		this.editorKit = editorKit;
		this.individual = i;
		this.domain = prettyPrint(i.getShortName());
		this.connectedTypes = i.getClazz();
		this.header = header;
		this.tupleField.setFocusable(false);
		this.tupleField.setOpaque(true);
		this.tupleField.setBackground(Color.WHITE);
		this.tupleField.setBorder(new LineBorder(Color.BLACK));
		this.mode = mode;
		initDialogue();
		setSize(new Dimension(540, 10));
		setPreferredSize(new Dimension(540, this.getPreferredSize().height));
	}

	private String prettyPrint(ArrayList<String> shortName) {
		StringBuilder strb = new StringBuilder();
		for (String s : shortName) {
			strb.append(s);
			strb.append(" ");
		}
		return strb.toString().trim();
	}

	private void initDialogue() {
		this.setLayout(new BorderLayout());
		initTupleView();
		initCenterPane();
		performExtraInitialisation();
	}

	private void initCenterPane() {
		centerPane.setLayout(new BoxLayout(centerPane, BoxLayout.X_AXIS));
		this.add(centerPane, BorderLayout.CENTER);
	}

	/**
	 * Updates the {@link JTextField} used to present the Tuple representation
	 * of the Property assignment.
	 */
	protected void updateTupleField() {
		this.tupleField.setText(render());

	}

	/**
	 * initialize a {@link JPanel} showing the tuple representation of the
	 * specified {@link X_Property}
	 */
	private void initTupleView() {
		JPanel tuplePanel = new JPanel();
		tuplePanel.setLayout(new BorderLayout());
		tuplePanel.add(new JLabel("Tuple-Representation: "), BorderLayout.WEST);
		tuplePanel.add(tupleField, BorderLayout.CENTER);
		tupleField.setText(render());
		tuplePanel.add(Box.createVerticalStrut(7), BorderLayout.SOUTH);
		this.add(tuplePanel, BorderLayout.NORTH);
	}

	private String render() {
		StringBuilder strb = new StringBuilder("<html>");
		strb.append(domain.replace("<", "&lt;").replace(">", "&gt;"));
		strb.append(" ");
		if (property == null)
			strb.append("<font color=#green>property</font>");
		else
			strb.append(property.replace("<", "&lt;").replace(">", "&gt;"));
		strb.append(" ");
		if (range == null)
			strb.append("<font color=#green>range</font>");
		else
			strb.append(range.replace("<", "&lt;").replace(">", "&gt;"));
		strb.append(" ");
		if (extra == null)
			strb.append("<font color=#green>extra</font>");
		else
			strb.append(extra.replace("<", "&lt;").replace(">", "&gt;"));
		strb.append("</html>");
		return strb.toString();
	}

	protected void initRangeBox() {
		this.rangeType = new JComboBox<X_Type>();
		this.rangeType.setRenderer(new X_ProtegeComboRenderer());
		this.setOpaque(true);
		this.rangeType.setBackground(Color.WHITE);
		this.rangeType.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					X_Type item = (X_Type) e.getItem();
					selectedRangeType = item;
					updateRangeList(item);
				}
			}
		});
	}

	protected void initExtraBox() {
		this.extraType = new JComboBox<X_Type>();
		this.extraType.setRenderer(new X_ProtegeComboRenderer());
		this.setOpaque(true);
		this.extraType.setBackground(Color.WHITE);
		this.extraType.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					X_Type item = (X_Type) e.getItem();
					selectedExtraType = item;
					updateExtraList(item);

				}
			}
		});
	}

	/**
	 * perform further initializations specific for
	 * {@link DataTypePropertySelectionPanel} and
	 * {@link ObjectPropertySelectionPanel} (e.g. populate the left and right
	 * hand side of the {@link JSplitPane})
	 */
	protected abstract void performExtraInitialisation();

	/**
	 * Creates the properties component of the centerPane used in this dialogue
	 * 
	 * @return {@link JPanel} the properties component
	 */
	protected abstract JPanel createLeftComponent();

	/**
	 * Creates the range component of the centerPane used in this dialogue
	 * 
	 * @return {@link JPanel} the right component
	 */
	protected abstract JPanel createRangeComponent();

	/**
	 * Creates the extra component of the centerPane used in this dialogue
	 * 
	 * @return {@link JPanel} the right component
	 */
	protected abstract JPanel createExtraComponent();

	/**
	 * First element is the selected {@link X_Property} and the second and third
	 * element are {@link X_Individual}s selected as Range and extra argument
	 * respectively.
	 * 
	 * @return {@link X_Entity} Array of length 3.
	 */
	protected abstract X_Entity[] getSelection();

	/**
	 * Initialize {@link Listener}s specific for
	 * {@link DataTypePropertySelectionPanel},
	 * {@link ObjectPropertySelectionPanel} or
	 * {@link BasicPropertySelectionPanel}.
	 */
	protected abstract void initSpecificListener();

	/**
	 * Initializes the {@link Listener}s that are used by all incarnations of
	 * {@link SelectionPanel}.
	 */
	protected void initListener() {
		this.docListenerExtra = new DocumentListener() {

			@Override
			public void removeUpdate(DocumentEvent e) {
				String newText = textField2.getText() + "^^"
						+ xsdType2.getText();
				extra = newText;
				updateTupleField();
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				String newText = textField2.getText() + "^^"
						+ xsdType2.getText();
				extra = newText;
				updateTupleField();
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				String newText = textField2.getText() + "^^"
						+ xsdType2.getText();
				extra = newText;
				updateTupleField();
			}
		};

		this.docRangeListener = new DocumentListener() {

			@Override
			public void removeUpdate(DocumentEvent e) {

				String newText = textField.getText() + "^^" + xsdType.getText();
				range = newText;
				updateTupleField();
			}

			@Override
			public void insertUpdate(DocumentEvent e) {

				String newText = textField.getText() + "^^" + xsdType.getText();
				range = newText;
				updateTupleField();
			}

			@Override
			public void changedUpdate(DocumentEvent e) {

				String newText = textField.getText() + "^^" + xsdType.getText();
				range = newText;
				updateTupleField();
			}
		};

		this.extraListener = new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (list3.getSelectedValue() instanceof IndividualsListItem) {
					IndividualsListItem item = (IndividualsListItem) list3
							.getSelectedValue();
					extra = item.getIndividual().render();
					selExtra = item.getIndividual();
					updateTupleField();
				}
			}
		};
		initSpecificListener();
	}

	/**
	 * Restricts the Elements presented in the Range of the
	 * {@link SelectionPanel} to the given {@link X_Type}.
	 * 
	 * @param t
	 */
	protected abstract void updateRangeList(X_Type t);

	/**
	 * Restricts the Elements presented in the Extra of the
	 * {@link SelectionPanel} to the given {@link X_Type}.
	 * 
	 * @param t
	 */
	protected abstract void updateExtraList(X_Type t);
}
