package de.dfki.x_protege.ui.utilities.selectionFrames;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import org.protege.editor.core.ui.util.AugmentedJTextField;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.model.OntologyCache;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.data.X_Type;
import de.dfki.x_protege.ui.components.dialogue.ClassCreator;
import de.dfki.x_protege.ui.components.lists.AnnotationList;
import de.dfki.x_protege.ui.components.lists.PropertiesList;
import de.dfki.x_protege.ui.components.tree.TreeNodeGenerator;
import de.dfki.x_protege.ui.components.tree.X_ElementTree;
import de.dfki.x_protege.ui.components.tree.X_EntityTreeNode;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.SelectionChange;
import de.dfki.x_protege.ui.logic.Event.Type.ModelChangeType;
import de.dfki.x_protege.ui.logic.Event.Type.SelectionType;
import de.dfki.x_protege.ui.logic.listener.ModelChangeListener;
import de.dfki.x_protege.ui.logic.listener.SelectionChangeListener;
import de.dfki.x_protege.ui.utilities.UIHelper;
import de.dfki.x_protege.ui.utilities.X_Icons;
import de.dfki.x_protege.ui.utilities.rendering.X_ProtegeAnnotationRenderer;
import de.dfki.x_protege.ui.utilities.rendering.X_ProtegeRenderer;

/**
 * Used when ever a {@link X_Type} needs to be selected.
 * 
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 13.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class ClassSelectionFrame extends AbstractSelectionFrame {

	private static final long serialVersionUID = 3796246646303451264L;

	/**
	 * The used {@link X_EditorKit}.
	 */
	private final X_EditorKit editorKit;

	/**
	 * {@link Boolean} flag indicating whether this instance of
	 * {@link ClassSelectionFrame} was called from within an other dialogue
	 * (e.g. {@link PropertySelectionFrame}).
	 */
	private final boolean temp;

	/**
	 * 
	 */
	private final SelectionType type;

	/**
	 * The {@link X_ElementTree} showing the hierarchy of the {@link X_Type}s of
	 * the Ontology.
	 */
	private X_ElementTree tree;

	/**
	 * The {@link DefaultTreeModel} used to populate the {@link X_ElementTree}
	 * presented in this instance of {@link ClassSelectionFrame}.
	 */
	private DefaultTreeModel model;

	private JButton newButton;

	private JButton deleteButton;

	private AugmentedJTextField uriField = new AugmentedJTextField(
			"e.g http://www.example.com/ontologies/myontology");

	private static final String ONTOLOGY_IRI_FIELD_LABEL = "Class URI";

	private Action newAction = new AbstractAction("", X_Icons.class_add_sub) {

		/**
		 * 
		 */
		private static final long serialVersionUID = -3222561448804954850L;

		@Override
		public void actionPerformed(ActionEvent e) {
			X_Entity parent = selectedClass();
			if (parent.isAtomType())
				editorKit.getX_ModelManager().handleChange(new ModelChange(
						ModelChangeType.TEMPCLASSSUBADD, parent));
			else {
				ClassCreator creator = new ClassCreator(editorKit,
						(X_Type) parent);
				int returnValue = new UIHelper(editorKit)
						.showDialog("Create a new n ary type ", creator);
				if (returnValue == 0) {
					// short-name of selected class
					ArrayList<String> snsel = parent.getShortName();
					// short-name of the new class
					ArrayList<String> snnew = editorKit.getX_ModelManager()
							.getActiveModel().getTypeInCreation()
							.getShortName();

					if (!snsel.equals(snnew))
						editorKit.getX_ModelManager()
								.handleChange(new ModelChange(
										ModelChangeType.TEMPCARTSUBADDED,
										parent));
				}
			}
		}

	};
	private Action deleteAction = new AbstractAction("", X_Icons.class_delete) {

		/**
		 * 
		 */
		private static final long serialVersionUID = 3678942585786434075L;

		@Override
		public void actionPerformed(ActionEvent e) {
			X_Entity selected = selectedClass();
			editorKit.getX_ModelManager().handleChange(
					new ModelChange(ModelChangeType.TEMPCLASSDELETE, selected));
		}

	};


	private Action sibAction = new AbstractAction("", X_Icons.class_add_sib) {

		/**
		 * 
		 */
		private static final long serialVersionUID = -6982067131029124968L;

		@Override
		public void actionPerformed(ActionEvent e) {
			X_Entity parent = selectedClass();
			editorKit.getX_ModelManager().handleChange(
					new ModelChange(ModelChangeType.TEMPCLASSSIBADD, parent));

		}

	};

	private PropertiesList propList;

	private ModelChangeListener modelChangeListener = new ModelChangeListener() {

		@Override
		public void modelChanged(ModelChange modelChanged) {
			if (modelChanged.getType() == ModelChangeType.TEMPCLASSSUBADD
					|| modelChanged.getType() == ModelChangeType.TEMPCLASSSIBADD
					|| modelChanged.getType() == ModelChangeType.TEMPCLASSDELETE
					|| modelChanged.getType() == ModelChangeType.TEMPRENAME
					|| modelChanged
							.getType() == ModelChangeType.TEMPANNOTATIONADDED
					|| modelChanged
							.getType() == ModelChangeType.TEMPANNOTATIONREMOVED
					|| modelChanged
							.getType() == ModelChangeType.TEMPANNOTATIONEDITED
					|| modelChanged.getType() == ModelChangeType.TEMPDOMAINADD
					|| modelChanged
							.getType() == ModelChangeType.TEMPDOMAINREMOVE) {
				handleModelChange(modelChanged);
			}
		}
	};

	private SelectionChangeListener selChangeListener = new SelectionChangeListener() {

		@Override
		public void selectionChanged(SelectionChange ontoChanged) {
			if (ontoChanged.getType() == SelectionType.TEMPCLASSHIERARCHY
					|| ontoChanged
							.getType() == SelectionType.CARTCREATIONHIERARCHY) {
				handleSelectionChanged();
			}
		}
	};

	private AnnotationList annolist;

	/**
	 * Create a new instance of {@link ClassSelectionFrame}
	 * @param x_EditorKit the {@link X_EditorKit}, used to ensure proper event handling
	 * @param type the {@link SelectionType}, used to ensure proper Selection handling
	 * @param temp flag indicating whether this selection frame is part of an dialogue called from within an other dialogue.
	 */
	public ClassSelectionFrame(X_EditorKit x_EditorKit, SelectionType type,
			boolean temp) {
		super();
		this.temp = temp;
		this.type = type;
		this.editorKit = x_EditorKit;
		this.setLayout(new BorderLayout());
		this.setDoubleBuffered(true);
		this.model = TreeNodeGenerator.getModel(type);
		this.buildExtendedView();
		setSize(new Dimension(480, 10));
		setPreferredSize(new Dimension(480, this.getPreferredSize().height));
	}

	/**
	 * Create a new instance of {@link ClassSelectionFrame}
	 * @param x_EditorKit the {@link X_EditorKit}, used to ensure proper event handling
	 * @param type the {@link SelectionType}, used to ensure proper Selection handling
	 * @param temp flag indicating whether this selection frame is part of an dialogue called from within an other dialogue.
     * @param root the root class of this selection frame. only subcasses of this one may be selected in the selectionframe.
     */
	public ClassSelectionFrame(X_EditorKit x_EditorKit, SelectionType type,
			boolean temp, X_Type root) {
		super();
		this.temp = temp;
		this.type = type;
		this.editorKit = x_EditorKit;
		this.setLayout(new BorderLayout());
		this.setDoubleBuffered(true);
		if (root == null) {
			this.model = TreeNodeGenerator.getModel(type);
			this.buildExtendedView();
			this.tree.setRootVisible(false);
			tree.setShowsRootHandles(true);
		} else {
			this.model = new DefaultTreeModel(
					TreeNodeGenerator.getNode(root, type));
			this.buildExtendedView();
		}
		setSize(new Dimension(480, 10));
		setPreferredSize(new Dimension(480, this.getPreferredSize().height));
	}

	@Override
	protected void handleModelChange(final ModelChange modelChanged) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {
				if (modelChanged.getType() == ModelChangeType.TEMPCLASSSUBADD
						|| modelChanged
								.getType() == ModelChangeType.TEMPCLASSSIBADD
						|| modelChanged
								.getType() == ModelChangeType.TEMPCLASSDELETE) {
					tree.setSelectedX_Element(TreeNodeGenerator
							.getNode((X_Entity) modelChanged.getValue(), type));
				}
				updateView();
			}
		};
		SwingUtilities.invokeLater(doHighlight);
	}

	@Override
	protected void updateView() {
		tree.repaint();
		if (selectedClass() == null) {
			tree.setSelectedX_Element(TreeNodeGenerator.getNode(
					editorKit.getX_ModelManager().getNaryThing(), type));
		} else {
			boolean flag = canDeleteClass();
			this.deleteButton.setEnabled(flag);
			this.uriField.setText(selectedClass().render());
			this.uriField.setFocusable(flag);
			this.propList.setCanAdd(flag);
			this.propList.setRootObject((X_Type) selectedClass());
			this.annolist.setCanAdd(flag);
			this.annolist.setClassRootObject(selectedClass());
		}
		this.newButton.setEnabled(canCreateNewSub());
		tree.revalidate();
		this.propList.revalidate();
		this.revalidate();
	}

	private boolean canDeleteClass() {
		if (this.temp) {
			return false;
		}
		if (editorKit.getX_ModelManager().getActiveModel().getTempClasses()
				.contains(selectedClass())) {
			return true;
		}
		return false;
	}

    /**
     *
     * @return A {@link TreePath} representing the path to the selected nodes.
     */
	public TreePath[] getSelections() {
		return this.tree.getSelectionModel().getSelectionPaths();
	}

	// ################ PRIVATE METHODS ###########################

	private void buildExtendedView() {
		JSplitPane splitPane = new JSplitPane();
		JPanel left = new JPanel();
		left.setLayout(new BorderLayout());
		JPanel buttons = createButtons();
		JPanel treePanel = createTreePanel();
		left.add(buttons, BorderLayout.NORTH);
		left.add(treePanel, BorderLayout.CENTER);
		splitPane.setLeftComponent(left);
		JPanel rightComponent = new JPanel();
		JPanel anno = createAnno();
		JPanel prop = createProp();
		rightComponent.setLayout(new BorderLayout());
		rightComponent.add(anno, BorderLayout.NORTH);
		rightComponent.add(prop, BorderLayout.CENTER);
		splitPane.setRightComponent(rightComponent);
		this.add(splitPane, BorderLayout.CENTER);
		editorKit.getX_ModelManager()
				.addSelectionChangeListener(selChangeListener);
		editorKit.getX_ModelManager()
				.addModelChangeListener(modelChangeListener);
		tree.setSelectedX_Element(TreeNodeGenerator
				.getNode(editorKit.getX_ModelManager().getNaryThing(), type));
		updateView();
	}

	// ######### create Anno start ###############################

	private JPanel createAnno() {
		JPanel annoPanel = new JPanel();
		annoPanel.setLayout(new BorderLayout());
		JPanel uriPanel = createUriPanel();
		JPanel annoList = createAnnoList();
		annoPanel.add(uriPanel, BorderLayout.NORTH);
		annoPanel.add(annoList, BorderLayout.CENTER);
		return annoPanel;
	}

	private JPanel createUriPanel() {
		JPanel uri = new JPanel(new GridBagLayout());
		Insets insets = new Insets(0, 4, 2, 0);
		uri.add(new JLabel(ONTOLOGY_IRI_FIELD_LABEL));
		uri.add(uriField,
				new GridBagConstraints(1, 0, 1, 1, 100.0, 0.0,
						GridBagConstraints.BASELINE_LEADING,
						GridBagConstraints.HORIZONTAL, insets, 0, 0));
		uriField.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				warn(false);
			}
		});

		uriField.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				warn(true);
			}

			@Override
			public void focusGained(FocusEvent e) {
				// Nothing to do here
			}
		});
		return uri;
	}

	private JPanel createAnnoList() {
		JPanel annoList = new JPanel();
		annoList.setLayout(new BorderLayout());
		annolist = new AnnotationList(editorKit, true);
		annolist.setCellRenderer(new X_ProtegeAnnotationRenderer());
		annoList.add(new JScrollPane(annolist), BorderLayout.CENTER);
		X_Entity root = selectedClass();
		if (root != null) {
			annolist.setClassRootObject(root);
		}
		return annoList;
	}
	// ######### create Anno end ###############################

	// ######### create prop begin ###############################

	private JPanel createProp() {
		JPanel propPanel = new JPanel();
		propPanel.setLayout(new BorderLayout());
		propList = new PropertiesList(editorKit, "Properties", !temp);
		propList.setCellRenderer(new X_ProtegeRenderer());
		// X_Type root = (X_Type) selectedClass();
		// if (root != null) {
		propList.setRootObject((X_Type) selectedClass());
		// }
		propPanel.add(new JScrollPane(propList), BorderLayout.CENTER);
		return propPanel;
	}

	// ######### create prop end ###############################

	/**
	 * 
	 * @return the currently selected class (instance of {@link X_Type}) in the
	 *         <code>tree</code>.
	 */
	public X_Entity selectedClass() {
		TreePath[] path = getSelections();
		if (path.length > 0) {
			X_Type newDomain = OntologyCache
					.getType(((X_EntityTreeNode) path[0].getLastPathComponent())
							.getValue().getShortName());
			return newDomain;
		}
		return null;
	}

	/**
	 * This method simply creates a instance of a {@link JTree} and adds this to
	 * the given {@link JPanel}
	 * 
	 * @param frame
	 *            The {@link JPanel} to add the {@link JTree}
	 */
	private JPanel createTreePanel() {
		JPanel treePanel = new JPanel();
		treePanel.setLayout(new BorderLayout());
		this.tree = new X_ElementTree(SelectionType.TEMPCLASSHIERARCHY, false,
				editorKit.getX_ModelManager());
		tree.setCellRenderer(new X_ProtegeRenderer());
		this.tree.setModel(model);
		tree.setSelectedX_Element((X_EntityTreeNode) model.getRoot());
		treePanel.add(new JScrollPane(tree), BorderLayout.CENTER);
		return treePanel;
	}

	/**
	 * This methods simply creates a three buttons and adds those buttons to
	 * this instance of {@link ClassSelectionFrame}
	 */
	private JPanel createButtons() {
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
		newButton = new JButton(newAction);
		newButton.setEnabled(true);
		// sibButton = new JButton(sibAction);
		deleteButton = new JButton(deleteAction);
		buttonPanel.add(newButton);
		// buttonPanel.add(sibButton);
		buttonPanel.add(deleteButton);
		return buttonPanel;
	}

	/**
	 * Disambiguates whether a the button allowing to create a new subclass of
	 * the current selected {@link X_Type} should be enabled. The decision is
	 * based on whether this instance of {@link ClassSelectionFrame} is called
	 * from an other dialogue (e.g. {@link PropertySelectionFrame}) and the
	 * currently selected {@link X_Type}.
	 * 
	 * @return {@link Boolean} flag indicating whether the
	 *         <code>newButton</code> should be enabled.
	 */
	private boolean canCreateNewSub() {
		if (this.temp) {
			return false;
		}
		if (selectedClass() != null) {
			return !((X_Type) selectedClass()).isXSDType();
		}
		return false;
	}

	/**
	 * 
	 */
	public void dispose() {
		this.editorKit.getX_ModelManager()
				.removeSelectionChangeListener(selChangeListener);
		this.editorKit.getX_ModelManager()
				.removeModelChangeListener(modelChangeListener);
	}

	/**
	 * TODO rename and restructure this method.
	 * 
	 * @param focus
	 */
	private void warn(boolean focus) {
		String name = uriField.getText();
		if (name != null && editorKit.getX_ModelManager().getActiveModel()
				.isValidName(name) && !focus) {
			if (name.contains(":"))
				editorKit.getX_ModelManager().handleChange(new ModelChange(
						ModelChangeType.TEMPRENAME, selectedClass(), name));
			else {
				String localOntoName = OntologyCache.relatedOntology
						.getLocalNameSpace() + ":" + name;
				editorKit.getX_ModelManager().handleChange(
						new ModelChange(ModelChangeType.TEMPRENAME,
								selectedClass(), localOntoName));
			}
		} else {
			if (focus) {
				uriField.setText(selectedClass().render());
			} else {
				new UIHelper(editorKit).showError("Rename",
						"You selected a non valid name ...");
				uriField.setText(selectedClass().render());
			}
		}
	}

	public void setModel(DefaultTreeModel cartOnlyModel) {
		this.tree.setModel(cartOnlyModel);
	}

}
