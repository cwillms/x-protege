package de.dfki.x_protege.ui.utilities.rendering;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 19.03.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class TableHeaderRenderer extends JLabel implements TableCellRenderer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8002556439653481133L;


	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		this.setOpaque(true);
		this.setBackground(Color.lightGray);
		this.setText((String) value);
		this.setHorizontalAlignment(JLabel.CENTER);
		return this;
	}
}
