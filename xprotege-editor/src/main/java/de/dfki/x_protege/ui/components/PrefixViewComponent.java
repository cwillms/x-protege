package de.dfki.x_protege.ui.components;

import java.awt.BorderLayout;

import javax.swing.JScrollPane;
import javax.swing.JToolBar;

import de.dfki.x_protege.model.data.Ontology;
import de.dfki.x_protege.ui.components.table.PrefixMapperTable;
import de.dfki.x_protege.ui.logic.actions.AddPrefixMappingAction;
import de.dfki.x_protege.ui.logic.actions.RemovePrefixMappingAction;

/**
 * The {@link PrefixViewComponent} is used to present and manipulate the
 * namespace mapping of the currently loaded ontology. <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 20.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class PrefixViewComponent extends AbstractX_ViewComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5912387830953615782L;
	private PrefixMapperTable table;

	private JToolBar createButtons() {
		JToolBar panel = new JToolBar();
		panel.add(new AddPrefixMappingAction(table));
		panel.add(new RemovePrefixMappingAction(table));
		return panel;
	}

	private void updateView(Ontology activeOntology) throws Exception {
		table.setOntology();
	}

	@Override
	protected void initializeView() throws Exception {
		setLayout(new BorderLayout());
		table = new PrefixMapperTable(getX_ModelManager());
		add(createButtons(), BorderLayout.NORTH);
		add(new JScrollPane(table), BorderLayout.CENTER);
		updateView(getX_ModelManager().getActiveModel());
	}

	@Override
	protected void disposeView() {
		// TODO Auto-generated method stub

	}

}
