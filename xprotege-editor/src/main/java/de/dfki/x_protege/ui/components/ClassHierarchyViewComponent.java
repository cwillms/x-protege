package de.dfki.x_protege.ui.components;

import java.awt.event.ActionEvent;

import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultTreeModel;

import de.dfki.x_protege.model.data.AtomicX_Type;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.data.X_Type;
import de.dfki.x_protege.ui.components.tree.TreeNodeGenerator;
import de.dfki.x_protege.ui.components.tree.X_ElementTree;
import de.dfki.x_protege.ui.components.tree.X_EntityTreeNode;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.SelectionChange;
import de.dfki.x_protege.ui.logic.Event.Type.ModelChangeType;
import de.dfki.x_protege.ui.logic.Event.Type.SelectionType;
import de.dfki.x_protege.ui.logic.actions.AbstractX_ElementTreeAction;
import de.dfki.x_protege.ui.utilities.UIHelper;
import de.dfki.x_protege.ui.utilities.X_Icons;

/**
 * The {@link ClassHierarchyViewComponent} contains an instance of
 * {@link X_ElementTree} to present the subclass hierarchy for the
 * {@link X_Type}s of connected to the loaded ontology. It offers options to
 * manipulate this hierarchy.<br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 20.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class ClassHierarchyViewComponent
		extends
			AbstractX_ElementHierarchyViewComponent<X_Type> {

	private static final long serialVersionUID = 998812852765660322L;

	private AbstractX_ElementTreeAction<X_Type> delete;

	private AbstractX_ElementTreeAction<X_Type> addSub;

	private AbstractX_ElementTreeAction<X_Type> addSib;

	@Override
	protected void handleSelectionChanges(
			final SelectionChange selectionchanges) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {
				switch (selectionchanges.getType()) {
					case CLASSHIERARCHY : {
						updateView();
						break;
					}
					case SETCLASSHIERARCHY : {
						getTree().setSelectedX_Element(TreeNodeGenerator
								.getNode((X_Entity) selectionchanges.getValue(),
										SelectionType.CLASSHIERARCHY));
						break;
					}
					default :
						break;
				}
			}
		};
		SwingUtilities.invokeLater(doHighlight);

	}

	@Override
	protected void handleModelChanged(final ModelChange modelChanged) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {
				switch (modelChanged.getType()) {
					case CLASSREMOVED : {
						handleRemove((X_Entity) modelChanged.getValue());
						break;
					}
					case CLASSSUBADDED : {
						handleAddSub((X_Entity) modelChanged.getValue());
						break;
					}
					case CLASSSIBADDED : {
						handleAddSib((X_Entity) modelChanged.getValue());
						break;
					}
					case CARTSUBADDED : {
						handleAddSub((X_Entity) modelChanged.getValue());
						break;
					}
					case WARNING : {
						int result = warn(
								(AtomicX_Type) modelChanged.getValue());
						if (result == 0) {
							getX_EditorKit().getX_ModelManager()
									.handleChange(new ModelChange(
											ModelChangeType.CLASSREMOVED,
											modelChanged.getValue(), true));
						}
						break;
					}
					case RENAME : {
						X_Entity e = (X_Entity) modelChanged.getValue();
						X_EntityTreeNode node = TreeNodeGenerator.getNode(e,
								getTree().getType());
						if (e.isType()) {
							setSelectedEntity(e);
							((DefaultTreeModel) getTree().getModel())
									.nodeChanged(node);

							if (e.isAtomType())
								for (X_Type t : ((AtomicX_Type) e)
										.getComplexClasses())
									((DefaultTreeModel) getTree().getModel())
											.nodeChanged(TreeNodeGenerator
													.getNode(t, getTree()
															.getType()));
							updateView();
						}
						break;
					}
					case SHOWFINDING : {
						X_Entity e = (X_Entity) modelChanged.getValue();
						if (e.isType()) {
							setSelectedEntity(e);
							updateView();
						}
						break;
					}
					default :
						break;
				}
			}

		};
		SwingUtilities.invokeLater(doHighlight);

	}

	/**
	 * Update the view after the given {@link X_Type} was added to the class
	 * Hierarchy.
	 * 
	 * @param value the {@link X_Type} which was added to the ontology.
	 */
	protected void handleAddSib(X_Entity value) {
		getTree().revalidate();
		getTree().repaint();
		this.setSelectedEntity(value);
	}

	@Override
	public void performExtraInitialisation() throws Exception {
		addSub = new AbstractX_ElementTreeAction<X_Type>("Add subclass",
				X_Icons.class_add_sub, getTree().getSelectionModel()) {
			private static final long serialVersionUID = -4067967212391062364L;

			@Override
			public void actionPerformed(ActionEvent event) {
				createNewChild();
			}

		};
		addAction(addSub, "A", "A");

		addSib = new AbstractX_ElementTreeAction<X_Type>("Add sibling class",
				X_Icons.class_add_sib, getTree().getSelectionModel()) {

			private static final long serialVersionUID = 9163133195546665441L;

			@Override
			public void actionPerformed(ActionEvent event) {
				createNewSibling();
			}

		};

		addAction(addSib, "A", "B");

		delete = new AbstractX_ElementTreeAction<X_Type>(
				"Delete selected class", X_Icons.class_delete,
				getTree().getSelectionModel()) {
			private static final long serialVersionUID = -4067967212391062364L;

			@Override
			public void actionPerformed(ActionEvent event) {
				removeClass();

			}

		};
		addAction(delete, "B", "A");

		updateView();
	}

	/**
	 * Fire a {@link ModelChange}-Event of {@link ModelChangeType}.CLASSSIBADDED
	 * and the currently selected class.
	 */
	protected void createNewSibling() {
		X_Entity sel = selectedClass();
		getX_ModelManager().handleChange(
				new ModelChange(ModelChangeType.CLASSSIBADDED, sel));
	}

	/**
	 * Fire a {@link ModelChange}-Event of {@link ModelChangeType}.CLASSSUBADDED
	 * or {@link ModelChangeType}.CARTSUBADD and the currently selected class.
	 */
	protected void createNewChild() {
		X_Entity sel = selectedClass();
		if (sel.isCartesianType()) {
			getX_ModelManager().handleChange(
					new ModelChange(ModelChangeType.CARTSUBADD, sel));
		} else {
			getX_ModelManager().handleChange(
					new ModelChange(ModelChangeType.CLASSSUBADDED, sel));
		}
	}

	/**
	 * Fire a {@link ModelChange}-Event of {@link ModelChangeType}.CLASSREMOVED
	 * and the currently selected class.
	 */
	protected void removeClass() {
		X_Entity sel = selectedClass();
		getX_ModelManager().handleChange(
				new ModelChange(ModelChangeType.CLASSREMOVED, sel));
	}

	/**
	 * 
	 * @return true: the currently selected class can be deleted, otherwise
	 *         false (this is the case for default types e.g. owl:Thing)
	 */
	protected boolean canDeleteClass() {
		return !getX_ModelManager().getActiveModel()
				.isStandard(selectedClass());
	}

	/**
	 * 
	 * @return true: it is possible to create a new sibling/subclass of the
	 *         currently selected class false : otherwise
	 */
	protected boolean canCreateNewSibling() {
		if (TreeNodeGenerator.getNode(selectedClass(), getTree().getType())
				.getParent() == null) {
			return false;
		} else {
			if (((X_Type) TreeNodeGenerator
					.getNode(selectedClass(), getTree().getType())
					.getParentEntity()).isCartesianType())
				return false;
			if (selectedClass().isXSDType()) {
				return false;
			}
			return true;
		}
	}

	/**
	 * Updates the Component after a {@link X_Type} was removed from the ontology. The given {@link X_Entity} is the newly selected one.
	 * @param newSelection The {@link X_Entity} to be selected.
     */
	protected void handleRemove(X_Entity newSelection) {
		setSelectedEntity(newSelection);
		updateView();
	}

	/**
	 * Update the view after the given {@link X_Type} was added to the class
	 * Hierarchy.
	 *
	 * @param x_Element the {@link X_Type} which was added to the ontology.
	 */
	protected void handleAddSub(X_Entity x_Element) {
		setSelectedEntity(x_Element);
		updateView();
	}

	/**
	 * 
	 * @return true: it is possible to create a new sibling/subclass of the
	 *         currently selected class false : otherwise
	 */
	protected boolean canCreateNewSub() {
		return !(selectedClass().isXSDType() && !selectedClass().isCartesianType() ) ;
	}

	@Override
	protected X_Entity updateView() {
		this.setHeaderText(selectedClass().render());
		this.addSib.setEnabled(canCreateNewSibling());
		this.delete.setEnabled(canDeleteClass());
		this.addSub.setEnabled(canCreateNewSub());
		getTree().revalidate();
		getTree().repaint();
		return selectedClass();
	}

	private X_Type selectedClass() {
		return (X_Type) getX_ModelManager().getActiveModel()
				.getSelection(SelectionType.CLASSHIERARCHY);
	}

	private int warn(AtomicX_Type t) {
		return new UIHelper(getX_EditorKit()).showDeleteConflictWarning(t);
	}

	@Override
	protected X_ElementTree initTree() {
		return new X_ElementTree(SelectionType.CLASSHIERARCHY, true,
				getX_ModelManager());
	}

}
