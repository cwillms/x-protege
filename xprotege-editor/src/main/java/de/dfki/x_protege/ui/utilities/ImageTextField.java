package de.dfki.x_protege.ui.utilities;

import java.awt.*;
import java.awt.image.BufferedImage;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

/**
 * The {@link ImageTextField} is an extended {@link JTextField}, used in the
 * Ontology-Header. A given icon is located in front of the text itself.
 * 
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 13.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class ImageTextField extends JPanel {

	private static final long serialVersionUID = -1758686738158436805L;

	private JLabel icon ;
	private JTextField textField;

	/**
	 * Creates a new instance of the {@link ImageTextField}.
	 * 
	 * @param icon
	 *            a {@link BufferedImage} preceding the text.
	 * @param text
	 *            the {@link String} to be presented by the
	 *            {@link ImageTextField}
	 */
	public ImageTextField(Icon icon, String text) {
		this.setLayout(new FlowLayout(0));
		this.textField = new JTextField(text);
		this.textField.setBorder(new EmptyBorder(0,0,0,0));
		this.textField.setBackground(Color.WHITE);
		this.icon = new JLabel(icon);
		this.icon.setBackground(Color.WHITE);
		this.icon.setOpaque(true);
		this.icon.setBorder(new EmptyBorder(0,0,0,0));
		this.add(this.icon);
		this.add(textField);
		this.textField.setEditable(false);
		this.setBackground(Color.WHITE);
		this.setBorder(new EtchedBorder());
	}

	/**
	 * Set the text of the {@link ImageTextField} to the given value.
	 * @param text the text to be set.
     */
	public void setText(String text){
		this.textField.setText(text);
	}

}
