package de.dfki.x_protege.ui.logic.search;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import javax.swing.SwingWorker;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.model.OntologyCache;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.ui.components.dialogue.SearchResultPresenter;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.Type.ModelChangeType;
import de.dfki.x_protege.ui.utilities.UIHelper;

/**
 * A {@link SwingWorker} used to compute the matching {@link X_Entity}s for a
 * search query represented by a {@link String}.
 * 
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 13.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class EntitiyFinder extends SwingWorker<ArrayList<ArrayList<X_Entity>>, ArrayList<X_Entity>> {

	/**
	 * A {@link String} representing the searchterm.
	 */
	private String searchTerm;

	/**
	 * The connected {@link X_EditorKit}.
	 */
	private X_EditorKit editorKit;

	/**
	 * Creates an instance of {@link EntitiyFinder}.
	 * 
	 * @param searchTerm
	 *            the search term represented by a {@link String}.
	 * @param editorKit
	 *            the connected {@link X_EditorKit}.
	 */
	public EntitiyFinder(String searchTerm, X_EditorKit editorKit) {
		this.searchTerm = searchTerm;
		this.editorKit = editorKit;
	}

	@Override
	protected ArrayList<ArrayList<X_Entity>> doInBackground() throws Exception {
		return OntologyCache.search(searchTerm);
	}

	@Override
	protected void done() {
		try {
			showResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * When the {@link SwingWorker} is done, present the results using a
	 * {@link SearchResultPresenter} or inform the user if there are no matches.
	 * 
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	private void showResult() throws InterruptedException, ExecutionException {

		if (get().get(0).isEmpty() && get().get(1).isEmpty() && get().get(2).isEmpty()) {
			new UIHelper(editorKit).showError("Search Finished", "Sorry, wasn't able to find any match ...");
		} else {
			SearchResultPresenter srp = new SearchResultPresenter(editorKit, get(), searchTerm);
			int returnValue = new UIHelper(editorKit).showDialog("Search Finished", srp);
			if (returnValue == 0) {
				editorKit.getX_ModelManager()
						.handleChange(new ModelChange(ModelChangeType.SHOWFINDING, srp.getSelectedEntity(), false));
			}
		}

	}
}
