package de.dfki.x_protege.ui.components;

import java.awt.BorderLayout;

import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import de.dfki.x_protege.model.data.X_Type;
import de.dfki.x_protege.ui.components.lists.DisjointList;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.SelectionChange;
import de.dfki.x_protege.ui.logic.Event.Type.SelectionType;

/**
 * The {@link X_DisjointClassesViewComponent} is used within the class-editor to
 * maintain the classes who are disjoint to the currently connected one. <br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 20.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class X_DisjointClassesViewComponent
		extends
			AbstractX_SelectionViewComponent {

	private static final long serialVersionUID = 7837393343321704665L;

	private DisjointList list;

	@Override
	protected void handleSelectionChanges(
			final SelectionChange selectionchanges) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {
				//
				switch (selectionchanges.getType()) {
					case CLASSHIERARCHY : {
						updateView();
						break;
					}
					default :
						break;
				}
			}
		};
		SwingUtilities.invokeLater(doHighlight);

	}

	@Override
	protected void handleModelChanged(final ModelChange modelChanged) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {
				//
				switch (modelChanged.getType()) {
					case DISJOINTADDED : {
						updateView();
						break;
					}
					case DISJOINTREMOVED : {
						updateView();
						break;
					}
					case RENAME : {
						updateView();
						break;
					}
					default :
						break;
				}
			}
		};
		SwingUtilities.invokeLater(doHighlight);
	}

	@Override
	public void disposeView() {
		getX_ModelManager().removeModelChangeListener(modelChangeListener);
		getX_ModelManager().removeSelectionChangeListener(selChangeListener);

	}

	@Override
	public void initialiseView() throws Exception {
		setLayout(new BorderLayout());
		list = new DisjointList(getX_EditorKit());
		add(new JScrollPane(list));
		list.setRootObject(selectedClass());
		getX_EditorKit().getX_ModelManager()
				.addSelectionChangeListener(this.selChangeListener);
		getX_EditorKit().getX_ModelManager()
				.addModelChangeListener(this.modelChangeListener);
		updateView();

	}

	private X_Type selectedClass() {
		return (X_Type) getX_ModelManager().getActiveModel()
				.getSelection(SelectionType.CLASSHIERARCHY);
	}

	@Override
	protected X_Type updateView() {
		X_Type sel = selectedClass();
		this.setHeaderText(sel.render());
		list.setRootObject(sel);
		return sel;
	}
}
