package de.dfki.x_protege.ui.logic;

import java.util.HashSet;
import java.util.Set;

import de.dfki.lt.hfc.TupleStore;
import de.dfki.x_protege.model.OntologyCache;
import de.dfki.x_protege.model.data.Ontology;
import de.dfki.x_protege.ui.logic.Event.Event;
import de.dfki.x_protege.ui.logic.listener.EventListener;

/**
 * An {@link EventManager} is used to maintain the backend of x-protégé
 * according to different kinds of events. It is also used to maintain the GUI
 * elements after a change in the backend, by sending events to {@link EventListener}s. <br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 16.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public abstract class EventManager {

	protected Ontology relatedOntology;
	protected Set<EventListener> listener;

	/**
	 * Creates a new instance of {@link EventManager}
	 * 
	 * @param ontology
	 *            The connected {@link Ontology}.
	 */
	public EventManager(Ontology ontology) {
		relatedOntology = ontology;
		this.listener = new HashSet<>();
	}

	/**
	 * This method updates the model ( {@link OntologyCache}, {@link TupleStore}
	 * ) according to the given {@link Event} Event.
	 * 
	 * @param e
	 *            The {@link Event} to be handled.
	 */
	public abstract void handle(Event e);

	/**
	 * Adds the given {@link EventListener} to the set of maintained listeners.
	 * @param l the {@link EventListener} to be added.
     */
	public void addListener(EventListener l) {
		this.listener.add(l);
	}

	/**
	 * Removes the given {@link EventListener} from the set of maintained listeners.
	 * @param l the {@link EventListener} to be removed.
     */
	public void removeListener(EventListener l) {
		this.listener.remove(l);
	}

}
