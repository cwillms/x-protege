package de.dfki.x_protege.ui.components;

import java.awt.BorderLayout;

import javax.swing.*;

import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.data.X_Individual;
import de.dfki.x_protege.model.data.X_Property;
import de.dfki.x_protege.model.data.X_Type;
import de.dfki.x_protege.ui.components.lists.EditorList;
import de.dfki.x_protege.ui.components.lists.TypeList;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.SelectionChange;

/**
 * The {@link X_IndividualEditorViewComponent} is used to maintain the
 * {@link X_Type}s and {@link X_Property} connected to the currently selected
 * {@link X_Individual}.
 * 
 * @see {@link EditorList}, {@link TypeList}
 * 
 *      <br>
 *      <br>
 *      Author: Christian Willms<br>
 *      German Research Center for Artificial Intelligence (DFKI)<br>
 *      Date: 20.02.2016<br>
 *      <br>
 *      christian.willms@dfki.de<br>
 *      <br>
 */
public class X_IndividualEditorViewComponent
		extends
			AbstractX_IndividualViewComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7781323347132954192L;
	private EditorList mPropertyList;
	private TypeList classList;
	private TypeList containsList;
	private JPanel right;

	@Override
	protected void handleModelChanged(final ModelChange modelChanged) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {
				switch (modelChanged.getType()) {
					case INDIVIDUALPADD : {
						updateView(true);
						break;
					}
					case INDIVIDUALPROPREMOVE : {
						updateView(true);
						break;
					}
					case INDIVIDUALTYPEADD : {
						updateView(true);
						break;
					}
					case INDIVIDUALTYPEREMOVE : {
						updateView(true);
						break;
					}
					case PROPERTYREMOVED : {
						if (getSelectedIndividual() != null) {
							updateView(true);
						}
						break;
					}
					case RENAME : {
						if (getSelectedIndividual() != null) {
							updateView(true);
						}
						break;
					}
					default :
						break;
				}
			}
		};
		SwingUtilities.invokeLater(doHighlight);
	}

	@Override
	protected void handleSelectionChanges(
			final SelectionChange selectionchanges) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {
				switch (selectionchanges.getType()) {
					case ICLASSHIERARCHY : {
						updateView();
						break;
					}
					case INDIVIDUALHIERARCHY : {
						updateView(true);
						break;
					}
					default :
						break;
				}
			}
		};
		SwingUtilities.invokeLater(doHighlight);

	}

	public X_Individual updateView(boolean b) {
		if (b) {
			if (getSelectedIndividual().isCartesianInstance()) {
				this.containsList.setMode(0);
			} else {
				this.containsList.setMode(2);
			}

			this.classList.setRootObject(getSelectedIndividual());
			this.containsList.setRootObject(getSelectedIndividual());
			this.mPropertyList.setRootObject(getSelectedIndividual());
			updateHeader(getSelectedIndividual());
		} else {
			this.containsList.setMode(0);
			this.classList.setRootObject(null);
			this.mPropertyList.setRootObject(null);
			this.containsList.setRootObject(null);
			setHeaderText("");
		}
		return getSelectedIndividual();
	}

	@Override
	public void initialiseIndividualsView() throws Exception {
		setLayout(new BorderLayout());
		setDoubleBuffered(true);
		JSplitPane splitter = new JSplitPane();
		JSplitPane left = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		classList = new TypeList(getX_EditorKit(), 1);
		containsList = new TypeList(getX_EditorKit(), 2);
		left.setLeftComponent(new JScrollPane(classList));
		left.setRightComponent(new JScrollPane(containsList));
		left.setDividerLocation(.7d);
		left.setResizeWeight(.7d);
		mPropertyList = new EditorList(getX_EditorKit());
		splitter.setLeftComponent(left);
		this.right = new JPanel();
		right.setLayout(new BorderLayout());
		right.add(new JScrollPane(mPropertyList), BorderLayout.CENTER);
		splitter.setRightComponent(right);
		add(splitter);
		splitter.setDividerLocation(.3d);
		splitter.setResizeWeight(.3d);
		classList.setRootObject(getSelectedIndividual());
		add(new JLabel(" "), BorderLayout.SOUTH);
		updateView();

	}

	@Override
	protected X_Entity updateView() {
		return updateView(false);
	}

}
