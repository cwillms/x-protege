package de.dfki.x_protege.ui.components.lists;

import de.dfki.x_protege.model.data.X_Type;
import org.protege.editor.core.ui.list.MListItem;

/**
 * The {@link ClassItem} represents an instance of {@link X_Type}. It is used by
 * {@link ClassList}s to visualize those {@link X_Type}s.<br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 17.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class ClassItem implements MListItem {

	private X_Type value;
	private ClassList classList;
	private boolean interactionAllowed;

	/**
	 * Create a new instance of {@link ClassItem}.
	 * 
	 * @param clazz
	 *            The {@link X_Type} to be represented.
	 * @param classList
	 *            The connected {@link ClassList}.
	 */
	ClassItem(X_Type clazz, ClassList classList, boolean interactionAllowed) {
		this.value = clazz;
		this.classList = classList;
		this.interactionAllowed = interactionAllowed;
	}

	@Override
	public boolean isEditable() {
		return false;
	}

	@Override
	public void handleEdit() {
		// Nothing to do here
	}

	@Override
	public boolean isDeleteable() {
		return interactionAllowed;
	}

	@Override
	public boolean handleDelete() {
		this.classList.handleDelete(value);
		return true;
	}

	@Override
	public String getTooltip() {
		return "";
	}

	public X_Type getValue() {
		return value;
	}

	public void setValue(X_Type value) {
		this.value = value;
	}

}
