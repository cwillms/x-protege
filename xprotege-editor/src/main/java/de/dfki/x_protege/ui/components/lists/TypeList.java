package de.dfki.x_protege.ui.components.lists;

import java.util.ArrayList;

import org.protege.editor.core.ui.list.MList;
import org.protege.editor.core.ui.list.MListSectionHeader;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.model.data.AtomicX_Indivdual;
import de.dfki.x_protege.model.data.CartesianX_Individual;
import de.dfki.x_protege.model.data.X_Individual;
import de.dfki.x_protege.model.data.X_Type;
import de.dfki.x_protege.ui.components.X_IndividualEditorViewComponent;
import de.dfki.x_protege.ui.utilities.rendering.X_ProtegeRenderer;

/**
 * {@link TypeList}s are used within the {@link X_IndividualEditorViewComponent}
 * to show the {@link X_Type} the given {@link X_Individual} belongs to. <br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 18.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class TypeList extends MList {

	private static final String HEADER1_TEXT = "Types:";
	private static final MListSectionHeader header1 = new MListSectionHeader() {

		@Override
		public String getName() {
			return HEADER1_TEXT;
		}

		@Override
		public boolean canAdd() {
			return true;
		}
	};

	private static final String HEADER2_TEXT = "Part of:";
	private static final MListSectionHeader header2 = new MListSectionHeader() {

		@Override
		public String getName() {
			return HEADER2_TEXT;
		}

		@Override
		public boolean canAdd() {
			return false;
		}
	};

	private static final String HEADER0_TEXT = "Contains:";
	private static final MListSectionHeader header0 = new MListSectionHeader() {

		@Override
		public String getName() {
			return HEADER0_TEXT;
		}

		@Override
		public boolean canAdd() {
			return false;
		}
	};

	/**
	 * 
	 */
	private static final long serialVersionUID = 8909269468161210937L;
	private X_Individual root;
	private X_EditorKit x_EditorKit;
	private int mode;

	/**
	 * Create a new instance of {@link TypeList}.
	 * 
	 * @param x_EditorKit
	 *            The connected {@link X_EditorKit}
	 * @param i
	 *            The "mode" this list is used.<br>
	 *            0 = contains, i.e. the presented instances are part of the
	 *            currently selected individual. <br>
	 *            1 = Type: The currently selected individual belongs to the
	 *            selected types. <br>
	 *            2 = Part of: The currently selected individual is part of the
	 *            presented individuals. TODO refactor this shit... use several
	 *            lists for this purpose, at least exclude Types and use a
	 *            standard ClassList.
	 */
	public TypeList(X_EditorKit x_EditorKit, int i) {
		this.x_EditorKit = x_EditorKit;
		this.mode = i;
		setCellRenderer(new X_ProtegeRenderer());
	}

	/**
	 * Populate the list according to the given {@link X_Individual} and the
	 * mode of the list. 0 = contains, i.e. the presented instances are part of
	 * the currently selected individual. <br>
	 * 1 = Type: The currently selected individual belongs to the selected
	 * types. <br>
	 * 2 = Part of: The currently selected individual is part of the presented
	 * individuals.
	 * 
	 * @param selectedIndividual
	 */
	@SuppressWarnings("unchecked")
	public void setRootObject(X_Individual selectedIndividual) {
		this.root = selectedIndividual;

		java.util.List<Object> data = new ArrayList<Object>();

		if (mode == 1) {
			data.add(header1);
			if (root != null) {
				for (X_Type indi : root.getClazz()) {
					data.add(new TypeListItem(indi, x_EditorKit,
							selectedIndividual));
				}
			}
		} else if (mode == 2) {
			data.add(header2);
			if (root != null) {
				AtomicX_Indivdual r = (AtomicX_Indivdual) root;
				for (CartesianX_Individual indi : r.getPartOf()) {
					data.add(new IndividualsListItem(indi));
				}
			}
		} else {
			data.add(header0);
			if (root != null) {
				CartesianX_Individual r = (CartesianX_Individual) root;
				for (AtomicX_Indivdual i : r.getContains()) {
					data.add(new IndividualsListItem(i));
				}
			}
		}
		setListData(data.toArray());
		revalidate();
	}

	@Override
	protected void handleAdd() {
		if (root != null) {
			this.x_EditorKit.getX_Workspace().getEventUtil().addX_Type(root,
					"Type", false);
		}
	}

	public void setMode(int i) {
		this.mode = i;

	}
}
