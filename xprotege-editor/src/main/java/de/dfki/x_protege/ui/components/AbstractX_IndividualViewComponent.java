package de.dfki.x_protege.ui.components;

import java.awt.Component;

import de.dfki.x_protege.model.data.X_Individual;
import de.dfki.x_protege.ui.logic.Event.Type.SelectionType;

/**
 * * The {@link AbractIndividualViewComponent} is the basic {@link Component}
 * used within the IndividualTab.<br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 20.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public abstract class AbstractX_IndividualViewComponent
		extends
			AbstractX_SelectionViewComponent {

	private static final long serialVersionUID = -319185595652292767L;

	@Override
	final public void initialiseView() throws Exception {
		initialiseIndividualsView();
	}

	/**
	 * 
	 * @return the currently selected {@link X_Individual}.
	 */
	public X_Individual getSelectedIndividual() {
		return (X_Individual) getX_ModelManager().getActiveModel()
				.getSelection(SelectionType.INDIVIDUALHIERARCHY);
	}

	/**
	 * Initialize the component with further, specific elements such as buttons or lists
	 * @throws Exception
     */
	public abstract void initialiseIndividualsView() throws Exception;

	@Override
	public void disposeView() {
		// TODO Auto-generated method stub

	}

}
