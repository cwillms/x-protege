package de.dfki.x_protege.ui.utilities.rendering;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import de.dfki.x_protege.model.data.AbstractX_Property;
import de.dfki.x_protege.model.data.AnnotationProperty;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.data.X_Individual;
import de.dfki.x_protege.model.data.X_Property;
import de.dfki.x_protege.model.data.X_Type;
import de.dfki.x_protege.ui.components.dialogue.SearchResultPresenter;
import de.dfki.x_protege.ui.components.lists.FindingListItem;
import de.dfki.x_protege.ui.utilities.X_Icons;

/**
 * This renderer is used by the {@link SearchResultPresenter} to visualize the output .
 * 
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 13.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class X_ProtegeSearchRenderer extends JLabel implements ListCellRenderer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1725740396111789467L;
	private Icon icon;
	private String searchTerm;

	public X_ProtegeSearchRenderer(String searchTerm) {
		this.searchTerm = searchTerm;
	}

	@Override
	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
			boolean cellHasFocus) {
		StringBuilder strb = new StringBuilder("<html>");
		if (((FindingListItem) value).getSource().isType()) {
			X_Type t = (X_Type) ((FindingListItem) value).getSource();
			icon = selectIcon(t);
			setIcon(icon);
			strb.append(t.render().replaceAll(searchTerm, "<font color='red'>" + searchTerm + "</font>"));
		} else {
			if (((FindingListItem) value).getSource() instanceof AnnotationProperty) {
				AnnotationProperty t = (AnnotationProperty) ((FindingListItem) value).getSource();
				icon = selectIcon(t);
				setIcon(icon);
				strb.append(t.render().replaceAll(searchTerm, "<font color='red'>" + searchTerm + "</font>"));
			} else {
				if (((FindingListItem) value).getSource().isProperty()) {

					AbstractX_Property t = (AbstractX_Property) ((FindingListItem) value).getSource();
					icon = selectIcon(t);
					setIcon(icon);
					strb.append(t.render().replaceAll(searchTerm, "<font color='red'>" + searchTerm + "</font>"));

				} else {
					X_Individual t = (X_Individual) ((FindingListItem) value).getSource();
					icon = selectIcon(t);
					setIcon(icon);
					strb.append(t.render().replaceAll(searchTerm, "<font color='red'>" + searchTerm + "</font>"));
				}
			}
		}
		strb.append("</html>");
		this.setText(strb.toString());
		return this;
	}

	private Icon selectIcon(X_Entity e) {
		if (e.isAtomType()) {

			return X_Icons.owlType;

		}
		if (e.isCartesianType()) {

			return X_Icons.cartType;

		}
		if (e instanceof AnnotationProperty) {
			return X_Icons.annoProp;
		}
		if (e.isProperty()) {
			X_Property pr = (X_Property) e;
			switch (pr.getPropertyType().nextSetBit(0)) {
			case 0:
				return X_Icons.mixedProperty;
			case 1:
				return X_Icons.dataProperty;
			case 2:
				return X_Icons.objectProperty;
			default:
				break;
			}
		}
		if (e.isAtomInstance()) {
			return X_Icons.simpleIdividual;
		}
		if (e.isCartesianInstance()) {
			return X_Icons.cartIndividual;
		}
		return X_Icons.defaultIcon;
	}
}
