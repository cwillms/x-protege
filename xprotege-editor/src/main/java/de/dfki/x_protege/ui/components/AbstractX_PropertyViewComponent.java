package de.dfki.x_protege.ui.components;

import java.awt.Component;

import de.dfki.x_protege.model.data.AbstractX_Property;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.ui.logic.Event.Type.SelectionType;

/**
 * /** * The {@link AbractPropertyViewComponent} is the basic {@link Component}
 * used within the PropertyTab.<br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 20.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
abstract class AbstractX_PropertyViewComponent
		extends
			AbstractX_SelectionViewComponent {

	private static final long serialVersionUID = -4956524723040648892L;

	@Override
	public void disposeView() {
	}

	@Override
	final protected X_Entity updateView() {
		AbstractX_Property selProp = null;
		selProp = updateView(
				(AbstractX_Property) getX_ModelManager().getActiveModel()
						.getSelection(SelectionType.PROPERTYHIERARCHY));
		return selProp;
	}

	/**
	 * Updates the view according to the given {@link AbstractX_Property}.
	 * 
	 * @param property the property that has been changed/selected
	 * @return the currently selected {@link de.dfki.x_protege.model.data.X_Property}
	 */
	protected abstract AbstractX_Property updateView(
			AbstractX_Property property);
}
