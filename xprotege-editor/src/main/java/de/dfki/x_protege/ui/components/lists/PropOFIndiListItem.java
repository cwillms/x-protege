package de.dfki.x_protege.ui.components.lists;

import java.util.ArrayList;

import org.protege.editor.core.ui.list.MListItem;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.model.data.AbstractX_Property;
import de.dfki.x_protege.model.data.X_Individual;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.Type.ModelChangeType;

/**
 * The {@link PropOFIndiListItem}s are used to populate {@link EditorList}s.
 * <br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 17.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class PropOFIndiListItem implements MListItem {

	private AbstractX_Property prop;
	private ArrayList<X_Individual> value;
	private X_EditorKit x_editorKit;

	public PropOFIndiListItem(X_EditorKit editorKit, AbstractX_Property prop,
			ArrayList<X_Individual> e1) {
		this.prop = prop;
		this.value = e1;
		this.x_editorKit = editorKit;
	}

	@Override
	public boolean isEditable() {
		return false;
	}

	@Override
	public void handleEdit() {
		// nothing to do here.

	}

	@Override
	public boolean isDeleteable() {
		return true;
	}

	@Override
	public boolean handleDelete() {
		this.x_editorKit.getX_ModelManager().handleChange(new ModelChange(
				ModelChangeType.INDIVIDUALPROPREMOVE, this.prop, this.value));
		return false;
	}

	@Override
	public String getTooltip() {
		return null;
	}

	public AbstractX_Property getProperty() {
		return this.prop;
	}

	public ArrayList<X_Individual> getValues() {
		return this.value;
	}

}
