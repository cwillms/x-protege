package de.dfki.x_protege.ui.components.lists;

import org.protege.editor.core.ui.list.MListItem;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.model.data.X_Individual;
import de.dfki.x_protege.model.data.X_Type;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.Type.ModelChangeType;

/**
 * Used by the {@link TypeList} to represent either {@link X_Individual}s or
 * {@link X_Type}s connected to the rootNode of this lists. <br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 18.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class TypeListItem implements MListItem {

	private X_Type value;
	private X_EditorKit x_EditorKit;
	private X_Individual selectedIndividual;

	public TypeListItem(X_Type indi, X_EditorKit x_EditorKit, X_Individual selectedIndividual) {
		this.value = indi;
		this.x_EditorKit = x_EditorKit;
		this.selectedIndividual = selectedIndividual;
	}

	@Override
	public boolean isEditable() {
		return false;
	}

	@Override
	public void handleEdit() {
		// Nothing to do.

	}

	@Override
	public boolean isDeleteable() {
		return true;
	}

	@Override
	public boolean handleDelete() {
		if (selectedIndividual.getClazz().size() > 1) {
			ModelChange modelChange = new ModelChange(ModelChangeType.INDIVIDUALTYPEREMOVE, value);
			this.x_EditorKit.getX_ModelManager().handleChange(modelChange);
		}
		return false;
	}

	@Override
	public String getTooltip() {
		return null;
	}

	public X_Type getValue() {
		return this.value;
	}

}
