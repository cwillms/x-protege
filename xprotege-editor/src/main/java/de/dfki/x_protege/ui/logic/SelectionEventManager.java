package de.dfki.x_protege.ui.logic;

import de.dfki.x_protege.model.data.Ontology;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.ui.components.tree.X_EntityTreeNode;
import de.dfki.x_protege.ui.logic.Event.Event;
import de.dfki.x_protege.ui.logic.Event.SelectionChange;
import de.dfki.x_protege.ui.logic.listener.EventListener;
import de.dfki.x_protege.ui.logic.listener.SelectionChangeListener;

/**
 * The {@link SelectionEventManager} is mainly used to update the affected GUI
 * elements after a selection in one element was changed. <br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 16.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class SelectionEventManager extends EventManager {

	/**
	 * Creates a new instance of {@link SelectionEventManager}.
	 * 
	 * @param ontology
	 *            The connected {@link Ontology}.
	 */
	public SelectionEventManager(Ontology ontology) {
		super(ontology);
	}

	@Override
	public void handle(Event selectionChange) {
		SelectionChange sc = (SelectionChange) selectionChange;
		if (sc.getOriginalValue() instanceof X_EntityTreeNode)
			if (sc.getValue().getValue() == relatedOntology
					.getSelection(sc.getType())) {
				return;
			}
		this.handleSelectionChanged((SelectionChange) selectionChange);
		for (EventListener scl : this.listener) {
			((SelectionChangeListener) scl)
					.selectionChanged((SelectionChange) selectionChange);
		}
	}

	private void handleSelectionChanged(SelectionChange selectionChange) {
		X_Entity value;
		if (selectionChange.getOriginalValue() instanceof X_EntityTreeNode) {
			value = selectionChange.getValue().getValue();
		} else {
			value = selectionChange.getIndi();
		}
		relatedOntology.updateSelections(selectionChange.getType(), value);
	}

}
