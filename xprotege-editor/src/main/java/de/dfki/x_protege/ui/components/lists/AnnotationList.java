package de.dfki.x_protege.ui.components.lists;

import java.util.ArrayList;

import org.protege.editor.core.ui.list.MList;
import org.protege.editor.core.ui.list.MListSectionHeader;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.model.data.Annotation;
import de.dfki.x_protege.model.data.AnnotationProperty;
import de.dfki.x_protege.model.data.AtomicX_Type;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.ui.components.AnnotationEditor;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.Type.ModelChangeType;
import de.dfki.x_protege.ui.utilities.UIHelper;
import de.dfki.x_protege.ui.utilities.rendering.X_ProtegeAnnotationRenderer;

/**
 * The {@link AnnotationList} is a central part of each tab in the workspace of
 * x-protégé. It allows the creation, manipulation and deletion of Annotations
 * connected to {@link X_Entity}s. <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 17.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class AnnotationList extends MList {

	/**
	* 
	*/
	private static final long serialVersionUID = 4328133544462211667L;
	private static final String HEADER_TEXT = "Annotations";
	private X_Entity root;
	private boolean canAdd = true;
	private boolean temp = false;
	private AnnotationProperty ap;
	private String av;
	private AtomicX_Type avt;
	private MListSectionHeader header = new MListSectionHeader() {

		@Override
		public String getName() {
			return HEADER_TEXT;
		}

		@Override
		public boolean canAdd() {
			return canAdd;
		}
	};

	private X_EditorKit editorKit;

	/**
	 * Creates a new instance of {@link AnnotationList}.
	 * 
	 * @param x_EditorKit
	 *            the connected {@link X_EditorKit}
	 * @param temp
	 *            A {@link Boolean} flag indicating whether the
	 *            {@link AnnotationList} is part of an Dialogue.
	 */
	public AnnotationList(X_EditorKit x_EditorKit, boolean temp) {
		this.editorKit = x_EditorKit;
		setCellRenderer(new X_ProtegeAnnotationRenderer());
		this.temp = temp;
	}

	/**
	 * Creates a new instance of {@link AnnotationList}.
	 * 
	 * @param x_EditorKit
	 *            the connected {@link X_EditorKit}
	 */
	public AnnotationList(X_EditorKit x_EditorKit) {
		this.editorKit = x_EditorKit;
		setCellRenderer(new X_ProtegeAnnotationRenderer());
	}

	@SuppressWarnings("unchecked")
	public void setClassRootObject(X_Entity entity) {
		this.root = entity;

		java.util.List<Object> data = new ArrayList<Object>();

		data.add(header);

		if (entity != null) {
			for (Annotation annot : entity.getAnnotations()) {
				data.add(new AnnotationsListItem(annot, this));
			}
		}

		setListData(data.toArray());
		revalidate();
	}

	@Override
	protected void handleAdd() {
		if (root != null) {
			AnnotationEditor ae = new AnnotationEditor(editorKit);
			ap = null;
			av = null;
			avt = null;
			if (createAnnotation(ae)) {
				Annotation anno = new Annotation(root, ap, av + "^^" + avt.getShortName().get(0));
				if (temp) {
					editorKit.getX_ModelManager()
							.handleChange(new ModelChange(ModelChangeType.TEMPANNOTATIONADDED, anno));
				} else {
					editorKit.getX_ModelManager().handleChange(new ModelChange(ModelChangeType.ANNOTATIONADDED, anno));
				}
			}
		}
	}

	/**
	 * Remove the given Annotation from the List. This results in an update of
	 * the model. TEMPANNOTATIONREMOVED if the temp is true<br>
	 * ANNOTATIONREMOVED otherwise
	 * 
	 * @param annotation
	 *            The annotation to be removed.
	 */
	void deleteAnnotation(Annotation annotation) {
		if (temp) {
			this.editorKit.getX_ModelManager()
					.handleChange(new ModelChange(ModelChangeType.TEMPANNOTATIONREMOVED, annotation));
		} else {
			this.editorKit.getX_ModelManager()
					.handleChange(new ModelChange(ModelChangeType.ANNOTATIONREMOVED, annotation));
		}
	}

	/**
	 * Edit the given Annotation from the List. This opens an instance of
	 * {@link AnnotationEditor}. Results in an update of the model.
	 * TEMPANNOTATIONEDITED if the temp is true<br>
	 * ANNOTATIONEDITED otherwise
	 * 
	 * @param annotation
	 *            The annotation to be removed.
	 */
	void editAnnotation(Annotation annotation) {
		AnnotationEditor ae = new AnnotationEditor(editorKit);
		ae.editMode(annotation);
		int returnValue = new UIHelper(editorKit).showDialog("Annotation Edit ", ae);
		if (returnValue == 0) {
			String av = ae.getAnnotationValue();
			if (temp) {
				editorKit.getX_ModelManager()
						.handleChange(new ModelChange(ModelChangeType.TEMPANNOTATIONEDITED, annotation, av));
			} else {
				editorKit.getX_ModelManager()
						.handleChange(new ModelChange(ModelChangeType.ANNOTATIONEDITED, annotation, av));
			}
		}
	}

	public void setCanAdd(boolean flag) {
		this.canAdd = flag;
	}

	private int warn(AnnotationEditor ae) {
		String message = "<html>Your annotation configuration is not valid ...<br>";
		StringBuilder strb = new StringBuilder(message);
		strb.append("<br>");
		if (ae.getAnnotationProperty() == null)
			strb.append("  - There is no Annotationproperty selected.<br>");
		if (ae.getAnnotationValueType() == null)
			strb.append("  - There is no type (e.g., <xsd:string>) for the annotation selected.<br>");
		strb.append("<br>");
		strb.append("You can return to the AnnotationDialoge by pressing OK.</html>");
		return new UIHelper(editorKit).showAnnoWarning(strb.toString());
	}

	private boolean createAnnotation(AnnotationEditor ae) {
		int returnValue = new UIHelper(editorKit).showDialog("Create a new annotation ", ae);
		if (returnValue == 0) {
			ap = ae.getAnnotationProperty();
			av = ae.getAnnotationValue();
			avt = ae.getAnnotationValueType();
			if (ap == null || av == null || avt == null) {
				if (warn(ae) == 0) {
					return createAnnotation(ae);
				}
			} else {
				return true;
			}
		}
		return false;
	}
}
