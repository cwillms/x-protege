package de.dfki.x_protege.ui.components;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import de.dfki.x_protege.model.data.X_BindingTableModel;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.QueryEvent;
import de.dfki.x_protege.ui.logic.Event.SelectionChange;
import de.dfki.x_protege.ui.logic.Event.Type.ModelChangeType;
import de.dfki.x_protege.ui.logic.Event.Type.QueryEventType;
import de.dfki.x_protege.ui.utilities.rendering.TableHeaderRenderer;
import de.dfki.x_protege.ui.utilities.rendering.X_ProtegeRenderer;

/**
 * This component  presents the result of a SPARSQL query using a table based on the {@link X_BindingTableModel}.
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 19.03.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class X_BindingTableViewComponent extends AbstractX_QueryViewComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2087057465842380069L;


	private final JTable table = new JTable() {

		/**
		 * 
		 */
		private static final long serialVersionUID = 8725125384635145451L;

		// Implement table cell tool tips.
		@Override
		public String getToolTipText(MouseEvent e) {
			String tip = null;
			Point p = e.getPoint();
			int rowIndex = rowAtPoint(p);
			int colIndex = columnAtPoint(p);
			try {
				tip = ((X_Entity) getValueAt(rowIndex, colIndex))
						.getDescription();
			} catch (RuntimeException e1) {
				// catch null pointer exception if mouse is over an empty line
			}

			return tip;
		}
	};

	private final JLabel queryField = new JLabel();

	private ListSelectionListener tableSelectionListener = new ListSelectionListener() {

		@Override
		public void valueChanged(ListSelectionEvent e) {
			int row = table.getSelectedRow();
			int column = table.getSelectedColumn();
			getX_ModelManager().handleSelectionChanged(new SelectionChange(
					((X_Entity) table.getModel().getValueAt(row, column))));
		}
	};


	@Override
	protected void handleQueryEvent(final QueryEvent queryEvent) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {
				if (queryEvent.getType() == QueryEventType.QUERY) {
					updateView((X_BindingTableModel) queryEvent.getValue());
				}
			}

		};
		SwingUtilities.invokeLater(doHighlight);

	}

	private void updateView(X_BindingTableModel value) {
		this.queryField.setText(value.getQuery());
		// this.queryField.setText(render(value.getQuery()));
		this.table.setModel(value);
		this.table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.table.setRowSelectionAllowed(true);
		this.table.setColumnSelectionAllowed(true);
		String[] header = value.getHeader();
		for (int i = 0; i < value.getColumnCount(); i++)
			this.table.getColumnModel().getColumn(i).setHeaderValue(header[i]);
	}


	@Override
	protected void handleModelChanged(ModelChange modelChanged) {
		// TODO Auto-generated method stub

	}


	@Override
	protected void handleSelectionChanges(SelectionChange selectionchanges) {
		// TODO Auto-generated method stub

	}


	@Override
	public void initialiseView() throws Exception {
		this.setLayout(new BorderLayout());
		this.table.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent me) {
				JTable table = (JTable) me.getSource();
				Point p = me.getPoint();
				int row = table.rowAtPoint(p);
				int col = table.columnAtPoint(p);
				if (me.getClickCount() == 2 && row != -1 && col != -1) {
					getX_ModelManager().handleChange(
							new ModelChange(ModelChangeType.SHOWFINDING,
									table.getValueAt(row, col), false));
				}
			}
		});
		// Arrange components --------------------------------------------------
		// add Header containing the Query
		this.add(createHeader(), BorderLayout.NORTH);
		JPanel tableContainer = new JPanel(new BorderLayout());
		tableContainer.setBackground(Color.WHITE);
		tableContainer.add(new JSeparator(), BorderLayout.NORTH);
		// add inpuptboxes
		this.table.setBackground(Color.WHITE);
		this.table.setOpaque(true);
		this.table.setDefaultRenderer(X_Entity.class, new X_ProtegeRenderer());
		this.table.getSelectionModel()
				.addListSelectionListener(this.tableSelectionListener);
		table.getTableHeader().setDefaultRenderer(new TableHeaderRenderer());
		JScrollPane scrollPane = new JScrollPane(this.table);
		tableContainer.add(scrollPane);
		scrollPane.setOpaque(true);
		scrollPane.setBackground(Color.WHITE);
		this.add(tableContainer, BorderLayout.CENTER);
		// Init Listener -------------------------------------------------------
		// we do not need the Selection change listener in this component, so we
		// have to remove it from the modelmanager
		getX_ModelManager().removeSelectionChangeListener(selChangeListener);
		getX_ModelManager().addQueryEvenetListener(qListener);

	}

	private JPanel createHeader() {
		JPanel header = new JPanel(new BorderLayout());
		JLabel label = new JLabel("Output for Query: ");
		header.add(label, BorderLayout.WEST);
		this.queryField.setHorizontalAlignment(JLabel.CENTER);
		header.add(this.queryField, BorderLayout.CENTER);
		header.add(new JLabel(" "), BorderLayout.SOUTH);
		return header;
	}


	@Override
	public void disposeView() {
		// TODO Auto-generated method stub

	}


	@Override
	protected X_Entity updateView() {
		// TODO Auto-generated method stub
		return null;
	}

}
