package de.dfki.x_protege.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.text.JTextComponent;

import de.dfki.x_protege.ui.logic.listener.InformEventListener;
import de.dfki.x_protege.ui.logic.listener.ModelChangeListener;
import org.apache.log4j.Logger;
import org.protege.editor.core.ProtegeApplication;
import org.protege.editor.core.ProtegeManager;
import org.protege.editor.core.editorkit.EditorKit;
import org.protege.editor.core.prefs.Preferences;
import org.protege.editor.core.prefs.PreferencesManager;
import org.protege.editor.core.ui.RefreshableComponent;
import org.protege.editor.core.ui.error.ErrorLog;
import org.protege.editor.core.ui.error.ErrorNotificationLabel;
import org.protege.editor.core.ui.error.SendErrorReportHandler;
import org.protege.editor.core.ui.progress.BackgroundTaskLabel;
import org.protege.editor.core.ui.workspace.TabbedWorkspace;
import org.protege.editor.core.ui.workspace.WorkspaceTab;
import org.protege.editor.core.ui.workspace.WorkspaceTabPlugin;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.model.X_ModelManager;
import de.dfki.x_protege.model.data.Ontology;
import de.dfki.x_protege.ui.logic.Event.EventUtilitiesUI;
import de.dfki.x_protege.ui.utilities.EntitySearchField;
import de.dfki.x_protege.ui.utilities.ImageTextField;
import de.dfki.x_protege.ui.utilities.ResearchCreator;
import de.dfki.x_protege.ui.utilities.X_Icons;

/**
 * A TabbedWorkspace represents a Workspace that contains tabs. The tabs are
 * loaded automatically depending on the Workspace.<br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 12.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class X_Workspace extends TabbedWorkspace
		implements
			SendErrorReportHandler {

	private static final Logger logger = Logger.getLogger(X_Workspace.class);
	public static final String TABS_PREFERENCES_SET = "tabs";

	public static final String VISIBLE_TABS_PREFERENCE_KEY = "visible_tabs";
	private static final long serialVersionUID = 1629224487011756857L;
	private static final int FINDER_BORDER = 1;
	private static final int FINDER_MIN_WIDTH = 250;

	private JPanel statusArea;

	/**
	 * This {@link JTextField} shows the URI of the currently loaded
	 * {@link Ontology}.
	 */
	private ImageTextField ontologie;

	/**
	 * indicates whether Background tasks (e.g., updates) are running.
	 */
	private BackgroundTaskLabel backgroundTaskLabel;


	private X_ModelManager mngr;

	private EventUtilitiesUI eventUtilities;

	private ResearchCreator entityfinder;

	@Override
	public void initialise() {
		super.initialise();
		if (getPreferences().getStringList(VISIBLE_TABS_PREFERENCE_KEY,
				new ArrayList<>()).isEmpty()) {
			try {
				for (WorkspaceTabPlugin plugin : this.getOrderedPlugins()) {
					WorkspaceTab tab;
					tab = plugin.newInstance();
					this.addTab(tab);
					this.save();
				}
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		backgroundTaskLabel = new BackgroundTaskLabel(
				ProtegeApplication.getBackgroundTaskManager());

		this.mngr = getX_ModelManager();
		eventUtilities = new EventUtilitiesUI(this.mngr, this);
		this.entityfinder = new ResearchCreator(getX_EditorKit());
		try {
			createActiveOntologyPanel();
		} catch (IOException e) {
			e.printStackTrace();
		}
	};

	/**
	 * Update the String in {@link JTextField} containing the uri of the
	 * ontology according to the model.
	 */
	public void updateURIFromModel() {
		this.ontologie.setText(mngr.getActiveModel().getUri().toString());

	}

	@Override
	public WorkspaceTab createWorkspaceTab(final String name) {
		logger.info("init workspaceTab");
		final X_WorkspaceViewsTab tab = new X_WorkspaceViewsTab();
		tab.setup(new WorkspaceTabPlugin() {
			@Override
			public TabbedWorkspace getWorkspace() {
				return X_Workspace.this;
			}

			@Override
			public String getLabel() {
				return name;
			}

			@Override
			public Icon getIcon() {
				return null;
			}

			@Override
			public String getIndex() {
				return "Z";
			}

			@Override
			public URL getDefaultViewConfigFile() {
				try {
					return new File(getId() + "-config.xml").toURI().toURL();
				} catch (MalformedURLException uriex) {
					System.err.println("Something went wrong! " + uriex.getMessage());
				}
				return null;
			}

			@Override
			public String getId() {
				return "WorkspaceTab" + System.nanoTime();
			}

			@Override
			public String getDocumentation() {
				return null;
			}

			@Override
			public WorkspaceTab newInstance() throws ClassNotFoundException,
					IllegalAccessException, InstantiationException {
				return tab;
			}
		});
		logger.info("return tag: " + tab);
		return tab;

	}

	@Override
	public void setup(EditorKit editorKit) {
		super.setup(editorKit);
	}

	@Override
	public JComponent getStatusArea() {
		if (statusArea == null) {
			statusArea = new JPanel();
			statusArea.setLayout(new BoxLayout(statusArea, BoxLayout.X_AXIS));
			statusArea.add(Box.createHorizontalGlue());
			statusArea.add(new JLabel());
			statusArea.add(Box.createHorizontalGlue());
			statusArea.add(new JLabel());
		}
		return statusArea;
	}

	/**
	 * Refreshs all components connected to the {@link X_Workspace}.
	 */
	public void refreshComponents() {
		refreshComponents(this);
	}

	private void createActiveOntologyPanel() throws IOException {

		JPanel topBarPanel = new JPanel(new GridBagLayout());
		topBarPanel.setBorder(BorderFactory.createEmptyBorder(5, 10, 3, 10));

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.insets = new Insets(0, 4, 0, 4);
		gbc.gridy = 0;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.fill = GridBagConstraints.NONE;
		gbc.anchor = GridBagConstraints.CENTER;

		ImageIcon icon = (ImageIcon) X_Icons.protegeIcon;
		topBarPanel.add(new JLabel(icon), gbc);

		final X_ModelManager mngr = getX_ModelManager();

		// Install the active ontology combo box
		Icon ontoImageIcon =X_Icons.ontologyIcon;
		ontologie = new ImageTextField(ontoImageIcon, mngr.getActiveModel().getUri().toString());
		ontologie.setToolTipText("Active ontology");
		ontologie.setFocusable(false);


		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.weightx = 100; // Grow along the x axis
		gbc.weighty = 0;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.anchor = GridBagConstraints.CENTER;
		topBarPanel.add(ontologie, gbc);

		// Global find field
		JPanel finderHolder = new JPanel();
		finderHolder
				.setLayout(new BoxLayout(finderHolder, BoxLayout.LINE_AXIS));
		final EntitySearchField entityFinderField = new EntitySearchField(
				this.entityfinder);
		final int height = entityFinderField.getPreferredSize().height;
		finderHolder.setMinimumSize(new Dimension(FINDER_MIN_WIDTH,
				height + ((FINDER_BORDER + 1) * 2)));
		finderHolder.add(entityFinderField);

		gbc.gridx = 2;
		gbc.gridy = 0;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.fill = GridBagConstraints.NONE;
		gbc.anchor = GridBagConstraints.CENTER;
		topBarPanel.add(finderHolder, gbc);

		gbc.gridx = 3;
		gbc.gridy = 0;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.fill = GridBagConstraints.NONE;
		gbc.anchor = GridBagConstraints.CENTER;
		topBarPanel.add(backgroundTaskLabel);

		add(topBarPanel, BorderLayout.NORTH);

		// Find focus accelerator
		KeyStroke findKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_F,
				Toolkit.getDefaultToolkit().getMenuShortcutKeyMask());
		getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(findKeyStroke,
				"FOCUS_FIND");
		getActionMap().put("FOCUS_FIND", new AbstractAction() {
			/**
			 *
			 */
			private static final long serialVersionUID = -2205711779338124168L;

			@Override
			public void actionPerformed(ActionEvent e) {
				entityFinderField.requestFocus();
			}
		});
		updateTitleBar();
	}

	private void updateTitleBar() {
		Frame f = ProtegeManager.getInstance().getFrame(this);
		if (f != null) {
			f.setTitle(getTitle());
		}
		ontologie.repaint();
	}

	private void refreshComponents(Component component) {
		if (component instanceof Container) {
			Container cont = (Container) component;
			for (Component childComp : cont.getComponents()) {
				refreshComponents(childComp);
			}
		}
		if (isComponentFontSizeSensitive(component)) {
			Font f = component.getFont();
			if (f != null) {
				component.setFont(f.deriveFont(f.getStyle(), 12));
			}
		}
		if (component instanceof RefreshableComponent) {
			((RefreshableComponent) component).refreshComponent();
		}
	}

	private boolean isComponentFontSizeSensitive(Component component) {
		return component instanceof JTextComponent
				|| component instanceof JLabel || component instanceof JTree
				|| component instanceof JList || component instanceof JTable;
	}

    /**
     *
     * @return The {@link X_EditorKit} this {@link X_Workspace} belongs to.
     */
	public X_EditorKit getX_EditorKit() {
		return (X_EditorKit) getEditorKit();
	}

    /**
     *
     * @return The {@link X_ModelManager} this {@link X_Workspace} belongs to.
     */
	public X_ModelManager getX_ModelManager() {
		return getX_EditorKit().getX_ModelManager();
	}

	@Override
	public boolean sendErrorReport(ErrorLog errorLog) {
		return true;
	}

    /**
     * register {@link ModelChangeListener} and {@link InformEventListener} to
     * the {@link X_ModelManager} connected to this instance of
     * {@link EventUtilitiesUI}.
     */
	public void registerListener() {
		this.eventUtilities.registerListener();
	}

    /**
     *
     * @return the instance of {@link EventUtilitiesUI} used by the {@link X_Workspace}
     */
	public EventUtilitiesUI getEventUtil() {
		return this.eventUtilities;
	}

	protected static Preferences getPreferences() {
		return PreferencesManager.getInstance()
				.getApplicationPreferences(TABS_PREFERENCES_SET);
	}
}
