package de.dfki.x_protege.ui.components.dialogue;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.protege.editor.core.ui.util.AugmentedJTextField;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.model.data.AtomicX_Type;
import de.dfki.x_protege.model.data.CartesianX_Type;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.data.X_Type;
import de.dfki.x_protege.model.utils.TypeElementProvider;
import de.dfki.x_protege.ui.components.lists.AnnotationList;
import de.dfki.x_protege.ui.components.lists.PropertiesList;
import de.dfki.x_protege.ui.components.lists.SelectionList;
import de.dfki.x_protege.ui.utilities.rendering.X_ProtegeAnnotationRenderer;
import de.dfki.x_protege.ui.utilities.rendering.X_ProtegeRenderer;

/**
 * The intention of a {@link ClassCreator} is the creation of
 * {@link CartesianX_Type}s. It uses a {@link SelectionList} to present the
 * subelements of the cartesian types and an {@link AnnotationList} as well as a
 * {@link PropertiesList} to present the information of the currently selected
 * subelement. <br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 20.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class ClassCreator extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7360271346917706797L;
	private final X_EditorKit x_EditorKit;
	private final X_Type parent;
	private final CartesianX_Type newType;
	private final AugmentedJTextField classURIField = new AugmentedJTextField(
			"e.g http://www.example.com/ontologies/myontology");
	private SelectionList list;
	private static final String ONTOLOGY_IRI_FIELD_LABEL = "Class URI: ";

	private AugmentedJTextField uriField = new AugmentedJTextField(
			"e.g http://www.example.com/ontologies/myontology");
	private AnnotationList annolist;
	private PropertiesList propList;

	/**
	 * Creates a new Instance of {@link ClassCreator}.
	 * 
	 * @param editorKit
	 *            The connected {@link X_EditorKit}.
	 * @param parent
	 *            The parent of the {@link CartesianX_Type} to be created.
	 */
	public ClassCreator(X_EditorKit editorKit, X_Type parent) {
		this.x_EditorKit = editorKit;
		this.parent = parent;
		this.newType = initNewType();
		newType.setShortName(computeName());
		classURIField.setText(newType.render());
		this.x_EditorKit.getX_ModelManager().getActiveModel()
				.setTypeInCreation(newType);
		initialise();
		setSize(new Dimension(480, 10));
		setPreferredSize(new Dimension(480, this.getPreferredSize().height));
	}

	private X_Type selectedClass() {
		X_Type selectedClass = this.list.getSelectedItem();
		if (selectedClass != null) {
			return selectedClass;
		}
		return null;
	}

	// ############################# PRIVATE METHODS

	private ArrayList<String> computeName() {
		ArrayList<AtomicX_Type> subElements = newType.getSubElements();
		ArrayList<String> result = new ArrayList<String>(subElements.size());
		for (int i = 0; i < subElements.size(); i++) {
			result.add(i, subElements.get(i).getShortName().get(0));
		}
		return result;
	}

	private CartesianX_Type initNewType() {
		TypeElementProvider tp = new TypeElementProvider(false);

		CartesianX_Type child;
		ArrayList<String> name = new ArrayList<>();
		if (isNaryThing()) {
			name.addAll(x_EditorKit.getX_ModelManager().getOWLThing()
					.getShortName());
			name.addAll(x_EditorKit.getX_ModelManager().getOWLThing()
					.getShortName());
			child = (CartesianX_Type) tp.createNewEntity(name, this.parent);
			// child.addSubelement((AtomicX_Type)
			// x_EditorKit.getX_ModelManager().getOWLThing());
			// child.addSubelement((AtomicX_Type)
			// x_EditorKit.getX_ModelManager().getOWLThing());
		} else {
			for (X_Entity e : ((CartesianX_Type) this.parent)
					.getSubElements()) {
				name.addAll(e.getShortName());
			}
			child = (CartesianX_Type) tp.createNewEntity(name, this.parent);
			// child.addSubelements(((CartesianX_Type)
			// this.parent).getSubElements());
		}
		return child;
	}

	private void initialise() {
		setLayout(new BorderLayout());
		add(createUpperComponent(), BorderLayout.NORTH);
		JSplitPane splitti = new JSplitPane();
		splitti.setDoubleBuffered(true);
		splitti.setLeftComponent(createLeftComponent());
		splitti.setRightComponent(createRightComponent());
		splitti.setDividerLocation(150);
		add(splitti);
		this.list.setSelectedIndex(1);
	}

	private boolean isNaryThing() {
		boolean isNaryThing = this.parent.getShortName().get(0)
				.equals("<nary:Thing+>");
		return isNaryThing;
	}

	private JPanel createUpperComponent() {
		JPanel ontologyIRIPanel = new JPanel(new GridBagLayout());
		Insets insets = new Insets(0, 4, 2, 0);
		ontologyIRIPanel.add(new JLabel(ONTOLOGY_IRI_FIELD_LABEL));
		ontologyIRIPanel.add(classURIField,
				new GridBagConstraints(1, 0, 1, 1, 100.0, 0.0,
						GridBagConstraints.BASELINE_LEADING,
						GridBagConstraints.HORIZONTAL, insets, 0, 0));

		return ontologyIRIPanel;
	}

	private JScrollPane createLeftComponent() {
		list = new SelectionList(x_EditorKit, isNaryThing());
		list.setCellRenderer(new X_ProtegeRenderer());
		list.setRootObject(newType);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.getSelectionModel()
				.addListSelectionListener(new ListSelectionListener() {

					@Override
					public void valueChanged(ListSelectionEvent e) {
						if (list.getSelectedIndex() > 0) {
							updateView();
						}
					}
				});
		JScrollPane scroll = new JScrollPane(list);
		return scroll;
	}

	private void updateView() {
		X_Type selectedClass = selectedClass();
		// update overall URI Field
		ArrayList<String> name = computeName();
		newType.setShortName(name);
		classURIField.setText(newType.render());
		x_EditorKit.getX_ModelManager().getActiveModel()
				.setTypeInCreation(newType);
		// update specific URI Field
		this.uriField.setText(selectedClass.render());
		// update annoList
		this.annolist.setClassRootObject(selectedClass);
		// update propertyList
		this.propList.setRootObject(selectedClass);
	}

	private JPanel createRightComponent() {
		JPanel rightPanel = new JPanel();
		JPanel anno = createAnno();
		JPanel prop = createProp();
		rightPanel.setLayout(new BorderLayout());
		rightPanel.add(anno, BorderLayout.NORTH);
		rightPanel.add(prop, BorderLayout.CENTER);
		return rightPanel;
	}

	private JPanel createAnno() {
		JPanel annoPanel = new JPanel();
		annoPanel.setLayout(new BorderLayout());
		JPanel uriPanel = createUriPanel();
		JPanel annoList = createAnnoList();
		annoPanel.add(uriPanel, BorderLayout.NORTH);
		annoPanel.add(annoList, BorderLayout.CENTER);
		return annoPanel;
	}

	private JPanel createUriPanel() {
		JPanel uri = new JPanel(new GridBagLayout());
		Insets insets = new Insets(0, 4, 2, 0);
		uri.add(new JLabel(ONTOLOGY_IRI_FIELD_LABEL));
		uri.add(uriField,
				new GridBagConstraints(1, 0, 1, 1, 100.0, 0.0,
						GridBagConstraints.BASELINE_LEADING,
						GridBagConstraints.HORIZONTAL, insets, 0, 0));
		uriField.setFocusable(false);
		return uri;
	}

	private JPanel createAnnoList() {
		JPanel annoList = new JPanel();
		annoList.setLayout(new BorderLayout());
		annolist = new AnnotationList(x_EditorKit, true);
		annolist.setCellRenderer(new X_ProtegeAnnotationRenderer());
		annoList.add(new JScrollPane(annolist), BorderLayout.CENTER);
		this.annolist.setCanAdd(false);
		return annoList;
	}

	private JPanel createProp() {
		JPanel propPanel = new JPanel();
		propPanel.setLayout(new BorderLayout());
		propList = new PropertiesList(x_EditorKit, "Properties", true);
		propList.setCanAdd(false);
		propList.setCellRenderer(new X_ProtegeRenderer());
		propPanel.add(new JScrollPane(propList), BorderLayout.CENTER);
		return propPanel;
	}

}
