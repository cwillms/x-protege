package de.dfki.x_protege.ui.utilities.selectionFrames;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import org.protege.editor.core.ui.util.ComponentFactory;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.model.data.X_Individual;
import de.dfki.x_protege.ui.components.tree.TreeNodeGenerator;
import de.dfki.x_protege.ui.components.tree.X_ElementTree;
import de.dfki.x_protege.ui.logic.Event.Type.SelectionType;
import de.dfki.x_protege.ui.utilities.rendering.X_ProtegeRenderer;

/**
 * Used when ever a {@link X_Individual} needs to be selected.
 * 
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 13.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class IndividualSelectionFrame extends JPanel {

	private final X_EditorKit editorKit;
	private X_ElementTree tree;
	private TreeModel model;
	private static final long serialVersionUID = 3796246646303451264L;

	public IndividualSelectionFrame(X_EditorKit x_EditorKit) {
		this.editorKit = x_EditorKit;
		this.setLayout(new BorderLayout());
		this.model = TreeNodeGenerator.getModel(SelectionType.ICLASSHIERARCHY);
		;
		this.addTree();
	}

	private void addTree() {
		JPanel treePanel = new JPanel();
		treePanel.setLayout(new BorderLayout());
		this.tree = new X_ElementTree(SelectionType.TEMPICLASSHIERARCHY, false, editorKit.getX_ModelManager());
		this.tree.setCellRenderer(new X_ProtegeRenderer());
		this.tree.setModel(model);
		treePanel.add(ComponentFactory.createScrollPane(tree));
		this.add(treePanel, BorderLayout.CENTER);
	}

	/**
	 * @return {@link TreePath[]} indicating the selected nodes in the
	 *         {@link X_ElementTree}.
	 */
	public TreePath[] getSelections() {
		return this.tree.getSelectionModel().getSelectionPaths();
	}

}
