package de.dfki.x_protege.ui.components.tree;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.tree.DefaultTreeModel;

import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.data.X_Type;
import de.dfki.x_protege.ui.logic.Event.Type.SelectionType;

/**
 * The {@link TreeNodeGenerator} is used to create and maintain the {@link X_EntityTreeNode}s and the models  connected to the {@link X_Entity} and the different trees used to depict the different hierarchical connections between those entities.
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 05.03.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class TreeNodeGenerator {

	private static HashMap<SelectionType, DefaultTreeModel> models = new HashMap<>();

	private static HashMap<X_Entity, X_EntityTreeNode> classTreeNodes = new HashMap<>();

	private static HashMap<X_Entity, X_EntityTreeNode> tempClassTreeNodes = new HashMap<>();

	private static HashMap<X_Entity, X_EntityTreeNode> propertyTreeNodes = new HashMap<>();

	private static HashMap<X_Entity, X_EntityTreeNode> tempPropertyTreeNodes = new HashMap<>();

	private static HashMap<X_Entity, X_EntityTreeNode> iClassTreeNodes = new HashMap<>();

	private static HashMap<X_Entity, X_EntityTreeNode> tempIClassTreeNodes = new HashMap<>();

	private static HashMap<X_Entity, X_EntityTreeNode> XSDTreeNodes = new HashMap<>();

	private static HashMap<X_Entity, X_EntityTreeNode> objTreeNodes = new HashMap<>();

	private static HashMap<X_Entity, X_EntityTreeNode> supTreeNodes = new HashMap<>();

	private static HashMap<X_Entity, X_EntityTreeNode> cartCreationNodes = new HashMap<>();

	private static LinkedList<X_EntityTreeNode> nodesToBeRemoved = new LinkedList<>();

    /**
     * Provides a {@link X_EntityTreeNode} for the given entity to be used in the {@link X_ElementTree} connected to the given {@link SelectionType}.
     * @param entity The {@link X_Entity} to be represented by the treeNode
     * @param type the {@link SelectionType} of the {@link X_ElementTree} this treeNode is used within
     * @return an entirely new {@link X_EntityTreeNode} if there doesn't exist a node connected to the given entity and the selection type, an existing node otherwise.
     */
	public static X_EntityTreeNode getNode(X_Entity entity,
			SelectionType type) {
		switch (type) {
			case CLASSHIERARCHY :
				if (classTreeNodes.containsKey(entity)) {
					return classTreeNodes.get(entity);
				} else {
					X_EntityTreeNode node = new X_EntityTreeNode(entity);
					computeSubtree(node, type);
					classTreeNodes.put(entity, node);
					return node;
				}
			case TEMPCLASSHIERARCHY :
				if (tempClassTreeNodes.containsKey(entity)) {
					return tempClassTreeNodes.get(entity);
				} else {
					X_EntityTreeNode node = new X_EntityTreeNode(entity);
					computeSubtree(node, type);
					tempClassTreeNodes.put(entity, node);
					return node;
				}
			case PROPERTYHIERARCHY :
				if (propertyTreeNodes.containsKey(entity)) {
					return propertyTreeNodes.get(entity);
				} else {
					X_EntityTreeNode node = new X_EntityTreeNode(entity);
					computeSubtree(node, type);
					propertyTreeNodes.put(entity, node);
					return node;
				}
			case TEMPPROPERTYHIERARCHY :
				if (tempPropertyTreeNodes.containsKey(entity)) {
					return tempPropertyTreeNodes.get(entity);
				} else {
					X_EntityTreeNode node = new X_EntityTreeNode(entity);
					computeSubtree(node, type);
					tempPropertyTreeNodes.put(entity, node);
					return node;
				}
			case ICLASSHIERARCHY :
				if (iClassTreeNodes.containsKey(entity)) {
					return iClassTreeNodes.get(entity);
				} else {
					X_EntityTreeNode node = new X_EntityTreeNode(entity);
					computeSubtree(node, type);
					iClassTreeNodes.put(entity, node);
					return node;
				}
			case TEMPICLASSHIERARCHY :
				if (tempIClassTreeNodes.containsKey(entity)) {
					return tempIClassTreeNodes.get(entity);
				} else {
					X_EntityTreeNode node = new X_EntityTreeNode(entity);
					computeSubtree(node, type);
					tempIClassTreeNodes.put(entity, node);
					return node;
				}
			case XSDONLY :
				if (XSDTreeNodes.containsKey(entity)) {
					return XSDTreeNodes.get(entity);
				} else {
					X_EntityTreeNode node = new X_EntityTreeNode(entity);
					computeSubtree(node, type);
					XSDTreeNodes.put(entity, node);
					return node;
				}
			case OBJECTONLY :
				if (objTreeNodes.containsKey(entity)) {
					return objTreeNodes.get(entity);
				} else {
					X_EntityTreeNode node = new X_EntityTreeNode(entity);
					computeSubtree(node, type);
					objTreeNodes.put(entity, node);
					return node;
				}
			case SUPERCLASSHIERARCHY :
				if (supTreeNodes.containsKey(entity)) {
					return supTreeNodes.get(entity);
				} else {
					X_EntityTreeNode node = new X_EntityTreeNode(entity);
					computeSubtree(node, type);
					supTreeNodes.put(entity, node);
					return node;
				}
			case CARTCREATIONHIERARCHY :
				if (cartCreationNodes.containsKey(entity)) {
					return cartCreationNodes.get(entity);
				} else {
					X_EntityTreeNode node = new X_EntityTreeNode(entity);
					computeSubtree(node, type);
					cartCreationNodes.put(entity, node);
					return node;
				}
			default :
				throw new IllegalArgumentException(type + " is not known");
		}

	}

	private static X_EntityTreeNode computeSubtree(X_EntityTreeNode root,
			SelectionType type) {
		X_Entity entity = root.getValue();
		for (X_Entity sub : entity.getSubs()) {
			boolean toBeConsidered = true;
			if (type == SelectionType.XSDONLY)
				if (!(sub.isType() && ((X_Type) sub).isXSDType()))
					toBeConsidered = false;

			if (type == SelectionType.TEMPICLASSHIERARCHY
					|| type == SelectionType.ICLASSHIERARCHY) {
				if (sub.isAtomType() && ((X_Type) sub).isXSDType()) {
					// in the Class hierarchy used in the individual Tab we want
					// to ignore XSD datatypes.
					toBeConsidered = false;
				}
			}
			if (type == SelectionType.CARTCREATIONHIERARCHY) {
				if (sub.isCartesianType())
					toBeConsidered = false;
			}
			if (type == SelectionType.OBJECTONLY) {
				if (((X_Type) sub).isXSDType()) {
					toBeConsidered = false;
				}
			}
			if (sub.getShortName().get(0).contains("nary:Nothing+")
					|| sub.getShortName().get(0).contains("owl:Nothing")) {
				toBeConsidered = false;
			}

			if (toBeConsidered) {
				if (models.get(type) != null)
					models.get(type).insertNodeInto(getNode(sub, type), root,
							root.getChildCount());
			}
		}
		return root;
	}

    /**
     * Provides a set of {@link X_EntityTreeNode} for the given entities to be used in the {@link X_ElementTree} connected to the given {@link SelectionType}.
     * @param entities The {@link X_Entity}s to be represented by the treeNodes
     * @param type the {@link SelectionType} of the {@link X_ElementTree} this treeNode is used within
     * @return an entirely new {@link X_EntityTreeNode} if there doesn't exist a node connected to the given entity and the selection type, an existing node otherwise.
     */
	public static Set<X_EntityTreeNode> getNodes(Set<X_Entity> entities,
			SelectionType type) {
		HashSet<X_EntityTreeNode> nodes = new HashSet<>();
		for (X_Entity e : entities) {
			nodes.add(getNode(e, type));
		}
		return nodes;
	}


    /**
     * Adds a {@link X_EntityTreeNode} to each model representing class/type hierarchies. The position of the new treenode is defined by the given entities.
     * @param child the {@link X_Entity} to be represented in the models
     * @param parent this {@link X_Entity} specifies the position of the node to be added. The newly created treeNodes will be direct children of the treeNodes representing this one
     */
	public static void addToClassModels(X_Entity child, X_Entity parent) {
		addNode(child, parent, classTreeNodes, SelectionType.CLASSHIERARCHY);
		addNode(child, parent, tempClassTreeNodes,
				SelectionType.TEMPCLASSHIERARCHY);
		if (!child.isCartesianType())
			addNode(child, parent, cartCreationNodes,
					SelectionType.CARTCREATIONHIERARCHY);
		addNode(child, parent, iClassTreeNodes, SelectionType.ICLASSHIERARCHY);
		addNode(child, parent, tempIClassTreeNodes,
				SelectionType.TEMPICLASSHIERARCHY);
		addNode(child, parent, supTreeNodes, SelectionType.SUPERCLASSHIERARCHY);
		// check for xsd
		if (((X_Type) child).isXSDType()) {
			addNode(child, parent, XSDTreeNodes, SelectionType.XSDONLY);
		} else {
			addNode(child, parent, objTreeNodes, SelectionType.OBJECTONLY);
		}
	}

    /**
     * Removes the representation of the given {@link X_Entity} from all class/type related hierarchies.
     * @param entity The {@link X_Entity} to be removed from the models.
     */
	public static void removeFromClassModels(X_Entity entity) {
		X_Type xtype = (X_Type) entity;
		removeNode(entity, SelectionType.CLASSHIERARCHY, classTreeNodes);
		removeNode(entity, SelectionType.TEMPCLASSHIERARCHY,
				tempClassTreeNodes);
		if (!entity.isCartesianType())
			removeNode(entity, SelectionType.CARTCREATIONHIERARCHY,
					cartCreationNodes);
		if (!(xtype.isAtomType() && xtype.isXSDType())) {
			removeNode(entity, SelectionType.ICLASSHIERARCHY, iClassTreeNodes);
			removeNode(entity, SelectionType.TEMPICLASSHIERARCHY,
					tempIClassTreeNodes);
		}
		if (xtype.isXSDType()) {
			removeNode(entity, SelectionType.XSDONLY, XSDTreeNodes);
		}
		if (!xtype.isXSDType()) {
			removeNode(entity, SelectionType.OBJECTONLY, objTreeNodes);
		}
		removeNode(entity, SelectionType.SUPERCLASSHIERARCHY, supTreeNodes);
	}

    /**
     * Adds a {@link X_EntityTreeNode} to each model representing property hierarchies. The position of the new treenode is defined by the given entities.
     * @param child the {@link X_Entity} to be represented in the models
     * @param parent this {@link X_Entity} specifies the position of the node to be added. The newly created treeNodes will be direct children of the treeNodes representing this one
     */
	public static void addToProptertyModels(X_Entity child, X_Entity parent) {
		addNode(child, parent, propertyTreeNodes,
				SelectionType.PROPERTYHIERARCHY);
		addNode(child, parent, tempPropertyTreeNodes,
				SelectionType.TEMPPROPERTYHIERARCHY);
	}

    /**
     * Removes the representation of the given {@link X_Entity} from all property related hierarchies.
     * @param entity The {@link X_Entity} to be removed from the models.
     */
	public static void removeFromPropertyModels(X_Entity entity) {
		removeNode(entity, SelectionType.PROPERTYHIERARCHY, propertyTreeNodes);
		removeNode(entity, SelectionType.TEMPPROPERTYHIERARCHY,
				tempPropertyTreeNodes);
	}

	private static void addNode(X_Entity child, X_Entity parent,
			HashMap<X_Entity, X_EntityTreeNode> map, SelectionType type) {
		X_EntityTreeNode parentNode = map.get(parent);
		X_EntityTreeNode childNode = getNode(child, type);
		models.get(type).insertNodeInto(childNode, parentNode,
				parentNode.getChildCount());
	}

	private static void removeNode(X_Entity child, SelectionType type,
			HashMap<X_Entity, X_EntityTreeNode> map) {
		DefaultTreeModel model = models.get(type);
		X_EntityTreeNode childNode = map.get(child);
		// remove all subnodes too
		collectNodes(childNode);
		for (X_EntityTreeNode x_EntityTreeNode : nodesToBeRemoved) {
			model.removeNodeFromParent(x_EntityTreeNode);
			map.remove(x_EntityTreeNode);
		}
		nodesToBeRemoved.clear();
		// This check is necessary, because in background mode it is possible
		// that the parent was already deleted.

		model.removeNodeFromParent(childNode);

		map.remove(child);

	}

	private static void collectNodes(X_EntityTreeNode node) {
		if (!node.isLeaf()) {
			int childCount = node.getChildCount();
			for (int i = 0; i < childCount; i++) {
				if (!nodesToBeRemoved.contains(node.getChildAt(i))) {
					nodesToBeRemoved
							.addFirst((X_EntityTreeNode) node.getChildAt(i));
					collectNodes((X_EntityTreeNode) node.getChildAt(i));
				}
			}
		}
	}

    /**
     *
     * @param type The {@link SelectionType} used to maintain the selections in the tree populated by the model
     * @return  the model connected to the given {@link SelectionType}
     */
	public static DefaultTreeModel getModel(SelectionType type) {
		return models.get(type);
	}

    /**
     * Creates an entire {@link javax.swing.tree.TreeModel} based on the given {@link X_Entity} (root) and connected to the given {@link SelectionType}
     * @param t the type connected to this model
     * @param e the root node of this model, all other nodes are recursively computed based on the subclass hierarchy (represented in the subclassOf field of the {@link X_Entity}
     */
	public static void computeModel(SelectionType t, X_Entity e) {
		DefaultTreeModel model = new DefaultTreeModel(new X_EntityTreeNode(e));
		models.put(t, model);
		setNode(e, ((X_EntityTreeNode) model.getRoot()), t);
		computeSubtree((X_EntityTreeNode) model.getRoot(), t);
	}

	private static void setNode(X_Entity e, X_EntityTreeNode node,
			SelectionType t) {
		// System.out.println("Set Node: " + e + " for " + t);
		if (t == SelectionType.CLASSHIERARCHY) {
			classTreeNodes.put(e, node);
		}
		if (t == SelectionType.CARTCREATIONHIERARCHY) {
			cartCreationNodes.put(e, node);
		}
		if (t == SelectionType.TEMPCLASSHIERARCHY) {
			tempClassTreeNodes.put(e, node);
		}
		if (t == SelectionType.ICLASSHIERARCHY) {
			iClassTreeNodes.put(e, node);
		}
		if (t == SelectionType.TEMPICLASSHIERARCHY) {
			tempIClassTreeNodes.put(e, node);
		}
		if (t == SelectionType.PROPERTYHIERARCHY) {
			propertyTreeNodes.put(e, node);
		}
		if (t == SelectionType.TEMPPROPERTYHIERARCHY) {
			tempPropertyTreeNodes.put(e, node);
		}
		if (t == SelectionType.SUPERCLASSHIERARCHY) {
			supTreeNodes.put(e, node);
		}
		if (t == SelectionType.XSDONLY) {
			XSDTreeNodes.put(e, node);
		}
		if (t == SelectionType.OBJECTONLY) {
			objTreeNodes.put(e, node);
		}
	}

	/**
	 * @deprecated for testing/debugging only
	 */
	public static void printCache() {
		System.out.println("#####################################");
		System.out.println("Class: " + classTreeNodes);
		for (Entry<X_Entity, X_EntityTreeNode> e : classTreeNodes.entrySet()) {
			System.out.println("	" + e.getKey().getShortName() + " value := "
					+ e.getValue());
		}
		System.out.println("TClass: " + tempClassTreeNodes);
		System.out.println("CC: " + cartCreationNodes);
		System.out.println("iClass: " + iClassTreeNodes);
		System.out.println("TiClass: " + tempClassTreeNodes);
		System.out.println("PR: " + propertyTreeNodes);
		System.out.println("TPR: " + tempPropertyTreeNodes);
		System.out.println("#####################################");
	}

}
