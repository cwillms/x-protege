package de.dfki.x_protege.ui.logic.listener;

import de.dfki.x_protege.ui.logic.Event.SelectionChange;

/**
 * The {@link SelectionChangeListener} is used by almost all components of
 * X-Protege. It listens to so called {@link SelectionChange}s and updates the
 * connected Component according to this events.
 * 
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 13.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public abstract class SelectionChangeListener implements EventListener {

	/**
	 * Handles the given {@link SelectionChange} and updates the view
	 * afterwards.
	 * 
	 * @param modelChanged
	 */
	public abstract void selectionChanged(SelectionChange ontoChanged);

}
