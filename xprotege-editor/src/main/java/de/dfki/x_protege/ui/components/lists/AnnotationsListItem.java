package de.dfki.x_protege.ui.components.lists;

import org.protege.editor.core.ui.list.MListItem;

import de.dfki.x_protege.model.data.Annotation;

/**
 * The AnnotationListItem represents a populated instance of
 * {@link Annotation}s. It is used by the {@link AnnotationList} to visualize
 * these Properties. <br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 17.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class AnnotationsListItem implements MListItem {

	private Annotation annot;
	private AnnotationList parentList;

	/**
	 * Creates a new instance of {@link AnnotationsListItem}.
	 * 
	 * @param annot
	 *            The {@link Annotation} to be presented.
	 * @param parent
	 *            The connected {@link AnnotationList}
	 */
	AnnotationsListItem(Annotation annot, AnnotationList parent) {
		this.annot = annot;
		this.parentList = parent;
	}

	/**
	 *
	 * @return The {@link Annotation} featured by this {@link AnnotationsListItem}
     */
	public Annotation getAnnotation() {
		return annot;
	}

	@Override
	public boolean isEditable() {
		return true;
	}

	@Override
	public void handleEdit() {
		parentList.editAnnotation(this.getAnnotation());
	}

	@Override
	public boolean isDeleteable() {
		return true;
	}

	@Override
	public boolean handleDelete() {
		parentList.deleteAnnotation(this.getAnnotation());
		return true;
	}

	@Override
	public String getTooltip() {
		return "";
	}
}
