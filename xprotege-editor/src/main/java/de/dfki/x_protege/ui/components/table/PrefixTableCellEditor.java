package de.dfki.x_protege.ui.components.table;

import java.awt.Component;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.util.EventObject;

import javax.swing.AbstractCellEditor;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;

/**
 * The {@link PrefixTableCellEditor} allows us to edit the cells of the
 * {@link PrefixMapperTable}. <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 17.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class PrefixTableCellEditor extends AbstractCellEditor implements TableCellEditor {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7035098905029531093L;
	private JTextField textField;

	/**
	 * Creates a new instace of {@link PrefixTableCellEditor}.
	 */
	public PrefixTableCellEditor() {
		textField = new JTextField();
		textField.setFont(new JLabel().getFont());
	}

	@Override
	public Object getCellEditorValue() {
		return textField.getText().trim();
	}

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		Font f = table.getCellRenderer(row, column).getTableCellRendererComponent(table, "", false, false, row, column)
				.getFont();
		textField.setFont(new Font(f.getName(), f.getStyle(), f.getSize()));
		textField.setText(value.toString());
		return textField;
	}

	@Override
	public boolean isCellEditable(EventObject e) {
		if (e instanceof MouseEvent) {
			return ((MouseEvent) e).getClickCount() == 2;
		}
		return false;
	}
}
