package de.dfki.x_protege.ui.components.lists;

import org.protege.editor.core.ui.list.MListItem;

import de.dfki.x_protege.model.data.X_Type;

/**
 * * The {@link DisjointListItem} represents an instance of {@link X_Type}. It
 * is used by {@link DisjointList}s to visualize those {@link X_Type}s.<br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 17.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class DisjointListItem implements MListItem {

	private X_Type disjoints;
	private DisjointList disjointList;

	/**
	 * Creates a new instance of {@link DisjointListItem}.
	 * 
	 * @param disjoints
	 *            The {@link X_Type} to be represented.
	 * @param list
	 *            The connected {@link DisjointList}.
	 */
	DisjointListItem(X_Type disjoints, DisjointList list) {
		this.setDisjoints(disjoints);
		this.disjointList = list;
	}

	@Override
	public boolean isEditable() {
		return true;
	}

	@Override
	public void handleEdit() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isDeleteable() {
		return true;
	}

	@Override
	public boolean handleDelete() {
		this.disjointList.handleDelete(disjoints);
		return true;
	}

	@Override
	public String getTooltip() {
		return "";
	}

	public X_Type getDisjoints() {
		return disjoints;
	}

	public void setDisjoints(X_Type disjoints) {
		this.disjoints = disjoints;
	}

}
