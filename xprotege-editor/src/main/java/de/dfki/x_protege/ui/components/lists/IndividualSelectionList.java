package de.dfki.x_protege.ui.components.lists;

import java.util.ArrayList;

import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionListener;

import org.protege.editor.core.ui.list.MList;
import org.protege.editor.core.ui.list.MListSectionHeader;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.model.OntologyCache;
import de.dfki.x_protege.model.data.AbstractX_Property;
import de.dfki.x_protege.model.data.AtomicX_Indivdual;
import de.dfki.x_protege.model.data.CartesianX_Type;
import de.dfki.x_protege.model.data.X_Individual;
import de.dfki.x_protege.model.data.X_Type;
import de.dfki.x_protege.ui.components.dialogue.ComplexCreatorPanel;
import de.dfki.x_protege.ui.components.dialogue.SelectionPanel;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.Type.ModelChangeType;
import de.dfki.x_protege.ui.utilities.UIHelper;
import de.dfki.x_protege.ui.utilities.rendering.X_ProtegeRenderer;

/**
 * The {@link IndividualSelectionList} is an essential part of the
 * {@link SelectionPanel} s, where it is used to present the individuals that
 * can be applied as range or extra arguments to an property.<br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 17.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class IndividualSelectionList extends MList {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8794450772871535033L;

	private final String HEADER;

	private AbstractX_Property root;

	private X_EditorKit editorKit;

	private X_Type type;

	/**
	 * 
	 */
	private MListSectionHeader header = new MListSectionHeader() {

		@Override
		public String getName() {
			return HEADER;
		}

		@Override
		public boolean canAdd() {
			boolean canAdd = false;
			if (root != null) {
				switch (HEADER) {
					case "Range:" :
						if ((root.getExplicitRange().size()
								+ root.getImplicitRange().size()) == 1) {
							canAdd = true;
						} else {
							if (type == null) {
								canAdd = false;
							} else {
								if (type.equals(OntologyCache.getAllType())) {
									canAdd = false;
								} else {
									canAdd = true;
								}
							}
						}
						break;
					case "Extra:" :
						if ((root.getExplicitExtra().size()
								+ root.getImplicitExtra().size()) == 1) {
							canAdd = true;
						} else {
							if (type == null) {
								canAdd = false;
							} else {
								if (type.equals(OntologyCache.getAllType())) {
									canAdd = false;
								} else {
									canAdd = true;
								}
							}
						}
						break;
					default :
						System.err.println("Unknow Header: " + HEADER);
						canAdd = false;
						break;
				}
			}
			return canAdd;

		}
	};

	private ArrayList<Object> data;

	/**
	 * Creates a new instance of {@link IndividualSelectionList}.
	 * 
	 * @param editorKit
	 *            The connected {@link X_EditorKit}.
	 * @param listener
	 *            The {@link ListSelectionListener} used by the
	 *            {@link SelectionPanel} to monitor the selected
	 *            {@link X_Individual}.
	 * @param header
	 *            A {@link String} representing the header of the list.
	 */
	public IndividualSelectionList(X_EditorKit editorKit,
			ListSelectionListener listener, String header) {
		this.HEADER = header;
		this.editorKit = editorKit;
		setCellRenderer(new X_ProtegeRenderer());
		setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		addListSelectionListener(listener);
	}

	/**
	 * populates the list with those individuals that are instances of the given
	 * {@link X_Type} and that are in range/extra position of the given
	 * {@link AbstractX_Property}.
	 * 
	 * @param root
	 * @param type
	 */
	@SuppressWarnings("unchecked")
	public void setRootObject(AbstractX_Property root, X_Type type) {
		this.root = root;
		this.type = type;
		this.data = new ArrayList<Object>();
		this.data.add(header);
		if (root != null) {
			// @@TODO ordering
			if (type == null || type == OntologyCache.getAllType()) {
				if (HEADER.equals("Range:")) {
					for (X_Type t : root.getExplicitRange()) {
						for (X_Individual i : t
								.getConnectedIndividualsSorted()) {
							this.data.add(new IndividualsListItem(i));
						}
					}
					for (X_Type t : root.getImplicitRange()) {
						for (X_Individual i : t
								.getConnectedIndividualsSorted()) {
							this.data.add(new IndividualsListItem(i));
						}
					}
				} else {
					for (X_Type t : root.getExplicitExtra()) {
						for (X_Individual i : t
								.getConnectedIndividualsSorted()) {
							this.data.add(new IndividualsListItem(i));
						}
					}
					for (X_Type t : root.getImplicitExtra()) {
						for (X_Individual i : t
								.getConnectedIndividualsSorted()) {
							this.data.add(new IndividualsListItem(i));
						}
					}
				}
			} else {
				for (X_Individual i : type.getConnectedIndividualsSorted()) {
					this.data.add(new IndividualsListItem(i));
				}
			}

		}
		setListData(this.data.toArray());
		revalidate();
	}

	@Override
	protected void handleAdd() {
		ArrayList<String> shortName = new ArrayList<String>();
		switch (this.HEADER) {
			case "Range:" :
				X_Type[] ranges = root.getExplicitRange()
						.toArray(new X_Type[root.getExplicitRange().size()]);
				if (this.type == OntologyCache.getAllType())
					this.type = ranges[0];
				if (this.type.isCartesianType()) {
					ComplexCreatorPanel selectionPanel = new ComplexCreatorPanel(
							editorKit, (CartesianX_Type) type);
					int returnValue = new UIHelper(editorKit)
							.showDialog("Individual", selectionPanel);
					if (returnValue == 0) {
						X_Individual[] newIndis = selectionPanel.getNewIndis();
						// if there were new individuals creates within the
						// specification of the complex one acknowledge them
						// to
						// the model
						// this must be done before the complex individual
						// is
						// acknowledged
						if (newIndis.length != 0) {
							for (int i = 0; i < newIndis.length; i++) {
								if (newIndis[i] != null) {
									AtomicX_Indivdual si = (AtomicX_Indivdual) newIndis[i];
									if (!si.isXSD()) {
										X_Type type = ((CartesianX_Type) ranges[0])
												.getSubElements().get(i);
										editorKit.getX_ModelManager()
												.handleChange(new ModelChange(
														ModelChangeType.EXTERNINDIVIDUALADD,
														si.getShortName(),
														type));
									}
								}
							}
						}
						shortName = selectionPanel.getResult();
					} else {
						editorKit.getX_ModelManager().handleChange(
								new ModelChange(ModelChangeType.CANCEL, null));
					}
				} else {
					String returnValue = new UIHelper(editorKit).getString(
							"Individual",
							"Please enter a name for the new Individual");
					if (returnValue != null && !returnValue.equals("")) {
						shortName.add("<" + returnValue + ">");
					}
				}
				if (!shortName.isEmpty())
					editorKit.getX_ModelManager()
							.handleChange(new ModelChange(
									ModelChangeType.EXTERNINDIVIDUALADD,
									shortName, this.type));
				this.setRootObject(root, this.type);
				break;
			case "Extra:" :
				X_Type[] extras = root.getExplicitExtra()
						.toArray(new X_Type[root.getExplicitExtra().size()]);
				if (this.type == OntologyCache.getAllType())
					this.type = extras[0];
				if (this.type.isCartesianType()) {
					ComplexCreatorPanel selectionPanel = new ComplexCreatorPanel(
							editorKit, (CartesianX_Type) type);
					int returnValue = new UIHelper(editorKit)
							.showDialog("Individual", selectionPanel);
					if (returnValue == 0) {
						X_Individual[] newIndis = selectionPanel.getNewIndis();
						// if there were new individuals creates within the
						// specification of the complex one acknowledge them
						// to
						// the model
						// this must be done before the complex individual
						// is
						// acknowledged
						if (newIndis.length != 0) {
							for (int i = 0; i < newIndis.length; i++) {
								if (newIndis[i] != null) {
									AtomicX_Indivdual si = (AtomicX_Indivdual) newIndis[i];
									if (!si.isXSD()) {
										X_Type type = ((CartesianX_Type) extras[0])
												.getSubElements().get(i);
										editorKit.getX_ModelManager()
												.handleChange(new ModelChange(
														ModelChangeType.INDIVIDUALADD,
														si.getShortName(),
														type));
									}
								}
							}
						}
						shortName = selectionPanel.getResult();
					} else {
						editorKit.getX_ModelManager().handleChange(
								new ModelChange(ModelChangeType.CANCEL, null));
					}
				} else {
					String returnValue = new UIHelper(editorKit).getString(
							"Individual",
							"Please enter a name for the new Individual");
					if (returnValue != null && !returnValue.equals("")) {
						shortName.add("<" + returnValue + ">");
					}
				}
				if (!shortName.isEmpty())
					editorKit.getX_ModelManager()
							.handleChange(new ModelChange(
									ModelChangeType.EXTERNINDIVIDUALADD,
									shortName, this.type));
				this.setRootObject(root, this.type);

				break;
			default :
				System.err.println("Unknow Header: " + HEADER);
				break;
		}
		selectNewElement(shortName);
	}

	private void selectNewElement(ArrayList<String> shortName) {
		IndividualsListItem item;
		for (int i = 0; i < this.data.size(); i++) {
			if (data.get(i) != header) {
				item = (IndividualsListItem) this.data.get(i);
				if (item.getValue().getShortName().equals(shortName)) {
					setSelectedIndex(i);
				}
			}
		}
	}
}
