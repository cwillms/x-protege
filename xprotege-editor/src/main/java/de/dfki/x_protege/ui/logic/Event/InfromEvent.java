package de.dfki.x_protege.ui.logic.Event;

/**
 * 
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 13.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class InfromEvent extends Event {

	public InfromEvent(String caller, Object value) {
		super(value);
	}

}
