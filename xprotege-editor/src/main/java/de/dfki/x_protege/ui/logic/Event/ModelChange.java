package de.dfki.x_protege.ui.logic.Event;

import de.dfki.x_protege.ui.logic.Event.Type.ModelChangeType;

/**
 * {@link ModelChange}s indicate changes in the model.
 * 
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 13.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class ModelChange extends Event {

	private final ModelChangeType type;
	private final Object value2;

	/**
	 * creates a new instance of {@link ModelChange}.
	 * 
	 * @param t
	 *            the {@link ModelChangeType}
	 * @param value
	 *            the new Value
	 */
	public ModelChange(ModelChangeType t, Object value) {
		super(value);
		this.type = t;
		this.value2 = null;

	}

	/**
	 * creates a new instance of {@link ModelChange}
	 * 
	 * @param t
	 *            the {@link ModelChangeType}
	 * @param value
	 *            the entity to be changed
	 * @param value2
	 *            the new value
	 */
	public ModelChange(ModelChangeType t, Object value, Object value2) {
		super(value);
		this.type = t;
		this.value2 = value2;

	}

	/**
	 *
	 * @return The {@link ModelChangeType} specifying the "kind" of change
     */
	public ModelChangeType getType() {
		return type;
	}

	/**
	 *
	 * @return the additional value connected to this {@link ModelChange}, may be null if not used.
     */
	public Object getValue2() {
		return value2;
	}

}
