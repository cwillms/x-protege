package de.dfki.x_protege.ui.components;

import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.data.X_Individual;
import de.dfki.x_protege.model.data.X_Type;
import de.dfki.x_protege.ui.components.tree.TreeNodeGenerator;
import de.dfki.x_protege.ui.components.tree.X_ElementTree;
import de.dfki.x_protege.ui.components.tree.X_EntityTreeNode;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.SelectionChange;
import de.dfki.x_protege.ui.logic.Event.Type.SelectionType;

/**
 * The {@link IndividualHierarchyViewComponent} contains an instance of
 * {@link X_ElementTree} to present the subclass hierarchy for the
 * {@link X_Type}s of connected to the loaded ontology. The presented hierarchy
 * is identical to the one presented by the {@link ClassHierarchyViewComponent},
 * but there are no options to manipulate it.<br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 20.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class IndividualsHierarchyViewComponent
		extends
			AbstractX_ElementHierarchyViewComponent<X_Individual> {

	private static final long serialVersionUID = -282564608284455436L;

	@Override
	protected void handleSelectionChanges(
			final SelectionChange selectionchanges) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {
				switch (selectionchanges.getType()) {
					case ICLASSHIERARCHY : {
						updateView();
						break;
					}
					default :
						break;
				}
			}
		};
		SwingUtilities.invokeLater(doHighlight);
	}

	@Override
	protected void handleModelChanged(final ModelChange modelChanged) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {
				switch (modelChanged.getType()) {
					case CLASSREMOVED : {
						handleClassRemoved((X_Entity) modelChanged.getValue2(),
								(X_Entity) modelChanged.getValue());
						break;
					}
					case CLASSSUBADDED : {
						updateView();
						break;
					}
					case CLASSSIBADDED : {
						updateView();
						break;
					}
					case CARTSUBADDED : {
						updateView();
						break;
					}
					case RENAME : {
						X_Entity e = (X_Entity) modelChanged.getValue();
						if (e.isType()) {
							X_EntityTreeNode node = TreeNodeGenerator.getNode(e,
									getTree().getType());
							TreePath path = new TreePath(TreeNodeGenerator
									.getNode((X_Entity) modelChanged.getValue(),
											getTree().getType()));
							getTree().getSelectionModel()
									.setSelectionPath(path);
							((DefaultTreeModel) getTree().getModel())
									.nodeChanged(node);
							updateView();
						}
						break;
					}
					default :
						break;
				}
			}

		};
		SwingUtilities.invokeLater(doHighlight);

	}

	/**
	 * Updates this component after a {@link X_Type} was removed from the ontology. Selects the given node.
 	 * @param value2 TODO remove this one.
	 * @param parent The node to be selected.
     */
	protected void handleClassRemoved(X_Entity value2, X_Entity parent) {
		if (this.getSelectedEntity() == null) {
			this.getTree().setSelectedX_Element(TreeNodeGenerator
					.getNode(parent, SelectionType.ICLASSHIERARCHY));
		} else {
			updateView();
		}

	}

	private X_Type selectedClass() {
		return (X_Type) getX_ModelManager().getActiveModel()
				.getSelection(SelectionType.ICLASSHIERARCHY);
	}

	@Override
	protected X_ElementTree initTree() {
		return new X_ElementTree(SelectionType.ICLASSHIERARCHY, false,
				getX_ModelManager());
	}

	@Override
	protected X_Entity updateView() {
		X_Type selC = selectedClass();
		if (selC != null) {
			this.setHeaderText(selectedClass().render());
		}
		getTree().revalidate();
		getTree().repaint();
		this.revalidate();
		return selC;
	}


	@Override
	protected void performExtraInitialisation() throws Exception {
		// nothing to do here

	}

}
