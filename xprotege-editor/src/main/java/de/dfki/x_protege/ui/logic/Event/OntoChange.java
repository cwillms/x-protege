package de.dfki.x_protege.ui.logic.Event;

import de.dfki.x_protege.ui.logic.Event.Type.OntoChangeTypes;

/**
 * {@link OntoChange}s indicate changes in the ontology.
 * 
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 13.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class OntoChange extends Event {

	private final OntoChangeTypes type;

	/**
	 * Creates a new instance of {@link OntoChange}
	 * @param t {@link OntoChangeTypes} indicating the kind of change featured by this event.
	 * @param value the {@link Object} that actually changed ( the used io-format, the URI of the ontology or its version ...)
     */
	public OntoChange(OntoChangeTypes t, Object value) {
		super(value);
		this.type = t;
	}

	/**
	 *
	 * @return {@link OntoChangeTypes} indicating the kind of change featured by this event.
     */
	public OntoChangeTypes getType() {
		return type;
	}

}
