package de.dfki.x_protege.ui.components.lists;

import org.protege.editor.core.ui.list.MListItem;

import de.dfki.x_protege.model.data.AbstractX_Property;

/**
 * The {@link PropertiesListItem} represents an instance of
 * {@link AbstractX_Property}. It is used by {@link PropertiesList}s to
 * visualize those {@link AbstractX_Property}s.<br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 17.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class PropertiesListItem implements MListItem {

	private AbstractX_Property prop;
	private PropertiesList propList;
	private boolean interactionAllowed;

	/**
	 * Creates a new instance of {@link PropertiesListItem} for the given
	 * {@link AbstractX_Property}.
	 * 
	 * @param prop
	 *            The {@link AbstractX_Property} to be represented.
	 * @param propList
	 *            The connected {@link PropertiesList}.
	 * @param interactionAllowed
	 *            A {@link Boolean} flag indicating whether it is allowed to
	 *            edit the list item
	 *
	 */
	public PropertiesListItem(AbstractX_Property prop, PropertiesList propList,
			boolean interactionAllowed) {
		this.prop = prop;
		this.propList = propList;
		this.interactionAllowed = interactionAllowed;
	}

	public AbstractX_Property getPropertie() {
		return this.prop;
	}

	@Override
	public boolean isEditable() {
		return false;
	}

	@Override
	public void handleEdit() {
		// Nothing to do here

	}

	@Override
	public boolean isDeleteable() {
		return interactionAllowed;
	}

	@Override
	public boolean handleDelete() {
		this.propList.handleDelete(prop);
		return false;
	}

	@Override
	public String getTooltip() {
		return null;
	}

}
