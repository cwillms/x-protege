package de.dfki.x_protege.ui.components;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import org.protege.editor.core.ui.util.AugmentedJTextField;

import de.dfki.x_protege.model.data.Annotation;
import de.dfki.x_protege.model.data.Ontology;
import de.dfki.x_protege.model.data.X_Type;
import de.dfki.x_protege.ui.components.lists.AnnotationList;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.SelectionChange;
import de.dfki.x_protege.ui.logic.Event.Type.ModelChangeType;
import de.dfki.x_protege.ui.logic.Event.Type.SelectionType;
import de.dfki.x_protege.ui.logic.listener.ModelChangeListener;
import de.dfki.x_protege.ui.logic.listener.SelectionChangeListener;
import de.dfki.x_protege.ui.utilities.UIHelper;
import de.dfki.x_protege.ui.utilities.rendering.X_ProtegeAnnotationRenderer;

/**
 * The {@link X_ClassesAnnotationViewComponent} is used within the class-editor
 * to maintain the {@link Annotation}s connected to the currently selected
 * class. To do so it uses an instance of {@link AnnotationList}. This component
 * also allows the renaming of the currently selected class. <br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 20.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class X_ClassesAnnotationViewComponent extends AbstractX_ViewComponent {

	private static final long serialVersionUID = 7821046818687645046L;

	private AnnotationList list;

	private final AugmentedJTextField classURIField = new AugmentedJTextField(
			"e.g http://www.example.com/ontologies/myontology");

	@SuppressWarnings("unused")
	private boolean classURIShowing = false;

	public static final String ONTOLOGY_IRI_FIELD_LABEL = "Class URI";

	private SelectionChangeListener selChangeListener = new SelectionChangeListener() {

		@Override
		public void selectionChanged(SelectionChange selectionchanges) {
			handleSelectionChanges(selectionchanges);
		}
	};

	private ModelChangeListener modelChangeListener = new ModelChangeListener() {

		@Override
		public void modelChanged(ModelChange modelChanged) {
			if (modelChanged.getType().equals(ModelChangeType.RENAME))
				handleModelChanges(modelChanged);
			if (modelChanged.getType().equals(ModelChangeType.ANNOTATIONADDED))
				handleModelChanges(modelChanged);
			if (modelChanged.getType().equals(ModelChangeType.ANNOTATIONEDITED))
				handleModelChanges(modelChanged);
			if (modelChanged.getType()
					.equals(ModelChangeType.ANNOTATIONREMOVED))
				handleModelChanges(modelChanged);
		}
	};

	private void handleModelChanges(ModelChange modelChanged) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {
				updateView();
			}
		};
		SwingUtilities.invokeLater(doHighlight);
	}

	/**
	 * Update the view according to the given {@link SelectionChange}-Event.
	 * 
	 * @param selectionchanges the {@link SelectionChange} event to be handled.
	 */
	protected void handleSelectionChanges(
			final SelectionChange selectionchanges) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {

				switch (selectionchanges.getType()) {
					case CLASSHIERARCHY : {
						updateView();
						boolean defaultClass = getX_ModelManager()
								.getActiveModel().isStandard(selectedClass());
						boolean cartesian = selectedClass().getArity() != 1;
						int suffixbegin, suffixEnd;
						if (!(defaultClass || cartesian)) {
							classURIField.setFocusable(true);
							classURIField.requestFocusInWindow();
							suffixbegin = classURIField.getText().indexOf(":")
									+ 1;
							suffixEnd = classURIField.getText().length();
							classURIField.select(suffixbegin, suffixEnd);
						} else {
							classURIField.setFocusable(false);
						}
						break;
					}

					default :
						break;
				}
			}
		};
		SwingUtilities.invokeLater(doHighlight);

	}

	/**
	 * This method is called to request that the view is updated with the
	 * specified class.
	 * 
	 * @param selectedClass
	 *            The class that the view should be updated with. Note that this
	 *            may be <code>null</code>, which indicates that the view should
	 *            be cleared
	 * @return The actual class that the view is displaying after it has been
	 *         updated (may be <code>null</code>)
	 */
	protected X_Type updateView() {
		X_Type sel = selectedClass();
		this.setHeaderText(selectedClass().render());
		this.classURIField.setText(sel.render());
		list.setClassRootObject(sel);
		return sel;
	}

	@Override
	protected void initializeView() throws Exception {
		setLayout(new BorderLayout());

		classURIField.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				warn(false);
			}
		});

		classURIField.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				warn(true);
			}

			@Override
			public void focusGained(FocusEvent e) {
				// Nothing to do here
			}
		});

		JPanel ontologyIRIPanel = new JPanel(new GridBagLayout());
		add(ontologyIRIPanel, BorderLayout.NORTH);
		Insets insets = new Insets(0, 4, 2, 0);
		ontologyIRIPanel.add(new JLabel(ONTOLOGY_IRI_FIELD_LABEL));
		ontologyIRIPanel.add(classURIField,
				new GridBagConstraints(1, 0, 1, 1, 100.0, 0.0,
						GridBagConstraints.BASELINE_LEADING,
						GridBagConstraints.HORIZONTAL, insets, 0, 0));

		// ontologyIRIPanel.add(new JButton(rename));
		classURIShowing = classURIField.isShowing();
		this.classURIField.setText(selectedClass().render());
		list = new AnnotationList(getX_EditorKit());
		list.setCellRenderer(new X_ProtegeAnnotationRenderer());
		add(new JScrollPane(list));
		list.setClassRootObject(selectedClass());

		getX_ModelManager().addSelectionChangeListener(this.selChangeListener);
		getX_ModelManager().addModelChangeListener(modelChangeListener);
		updateView();

	}

	/**
	 * Inform the user if there are problems when renaming the selected class.
	 * 
	 * @param focus this boolean flag indicates whether the warning was triggered because the URI-Field lost focus while there is an unsubmitted change to the uri.
	 */
	protected void warn(boolean focus) {
		String name = classURIField.getText();
		if (name != null
				&& getX_ModelManager().getActiveModel().isValidName(name)
				&& !focus) {
			if (name.contains(":"))
				getX_ModelManager().handleChange(new ModelChange(
						ModelChangeType.RENAME, selectedClass(), name));
			else {
				String localOntoName = getOntoNameSpace() + ":" + name;
				getX_ModelManager()
						.handleChange(new ModelChange(ModelChangeType.RENAME,
								selectedClass(), localOntoName));
			}
		} else {
			if (focus) {
				classURIField.setText(selectedClass().render());
			} else {
				new UIHelper(getX_EditorKit()).showError("Rename",
						"You selected a non valid name ...");
				classURIField.setText(selectedClass().render());
			}

		}

	}

	private String getOntoNameSpace() {
		Ontology onto = getX_ModelManager().getActiveModel();
		return onto.getLocalNameSpace();
	}

	private X_Type selectedClass() {
		return (X_Type) getX_ModelManager().getActiveModel()
				.getSelection(SelectionType.CLASSHIERARCHY);
	}

	@Override
	protected void disposeView() {
		getX_ModelManager().removeModelChangeListener(modelChangeListener);
		getX_ModelManager().removeSelectionChangeListener(selChangeListener);

	}

}
