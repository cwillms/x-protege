package de.dfki.x_protege.ui.components.lists;

import java.util.ArrayList;
import java.util.Set;

import org.protege.editor.core.ui.list.MList;
import org.protege.editor.core.ui.list.MListSectionHeader;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.model.data.AbstractX_Property;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.data.X_Type;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.Type.ModelChangeType;

/**
 * The {@link ClassList} is an essential part of all tabs of the x-protégé
 * workspace. It is used to represent {@link X_Type}s connected to an
 * {@link X_Entity}. <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 17.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public abstract class ClassList extends MList {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5211978937387926966L;
	private X_EditorKit editorKit;
	protected String HEADER_TEXT;
	private boolean canAdd = true;
	private boolean temp = false;
	private boolean state = false;
	private MListSectionHeader header = new MListSectionHeader() {

		@Override
		public String getName() {
			return HEADER_TEXT;
		}

		@Override
		public boolean canAdd() {
			return canAdd;
		}
	};

	private MListSectionHeader imHeader = new MListSectionHeader() {

		@Override
		public String getName() {
			return "Implicit:";
		}

		@Override
		public boolean canAdd() {
			return false;
		}
	};
	private AbstractX_Property root;

	/**
	 * Creates a new instance of {@link ClassList}.
	 * 
	 * @param x_EditorKit
	 *            The connected {@link X_EditorKit}.
	 */
	ClassList(X_EditorKit x_EditorKit) {
		this.editorKit = x_EditorKit;
	}

	/**
	 * Creates a new instance of {@link ClassList}.
	 * 
	 * @param x_EditorKit
	 *            The connected {@link X_EditorKit}.
	 * @param temp
	 *            {@link Boolean} flag indicating whether the list is part of an
	 *            temporal dialogue.
	 */
	ClassList(X_EditorKit x_EditorKit, boolean temp) {
		this.editorKit = x_EditorKit;
		this.temp = temp;
	}

	@SuppressWarnings("unchecked")
	/**
	 * populates the list according to the given {@link AbstractX_Property}
	 */
	public void setRootObject(AbstractX_Property root) {

		java.util.List<Object> data = new ArrayList<>();
		this.root = root;
		data.add(header);

		if (root != null) {
			Set<X_Type> elements = getElements(root);
			// @@TODO ordering
			for (X_Type clazz : elements) {
				data.add(new ClassItem(clazz, this, true));
			}
			if (state) {
				data.add(imHeader);
				for (X_Type imClazz : getImElements(root)) {
					data.add(new ClassItem(imClazz, this, false));
				}
			}
		}

		setListData(data.toArray());
		revalidate();
	}

	protected abstract Set<X_Type> getElements(AbstractX_Property root2);

	protected abstract Set<X_Type> getImElements(AbstractX_Property root);

	@Override
	protected void handleAdd() {
		this.editorKit.getX_Workspace().getEventUtil().addX_Type(root,
				this.HEADER_TEXT, temp);
	}

	/**
	 * Remove the given node from this list.
	 * 
	 * @param value
	 *            The node to be removed.
	 */
	void handleDelete(X_Type value) {
		ModelChangeType t = getModelChangeType(HEADER_TEXT, temp);

		this.editorKit.getX_ModelManager()
				.handleChange(new ModelChange(t, root, value));

	}

	private ModelChangeType getModelChangeType(String type, boolean temp) {
		ModelChangeType t = null;
		if (temp) {
			switch (type) {
				case "rdfs:domain" : {
					t = ModelChangeType.TEMPDOMAINREMOVE;
					break;
				}
				case "rdfs:range" : {
					t = ModelChangeType.TEMPRANGEREMOVED;
					break;
				}
				case "nary:argument" : {
					t = ModelChangeType.TEMPEXTRAREMOVED;
					break;
				}
				default :
					break;
			}
		} else {
			switch (type) {
				case "rdfs:domain" : {
					t = ModelChangeType.DOMAINREMOVED;
					break;
				}
				case "rdfs:range" : {
					t = ModelChangeType.RANGEREMOVED;
					break;
				}
				case "nary:argument" : {
					t = ModelChangeType.EXTRAREMOVED;
					break;
				}
				default :
					break;
			}
		}
		return t;
	}

	public void setCanAdd(boolean flag) {
		this.canAdd = flag;
	}

	public void setState(boolean state) {
		this.state = state;
	}
}
