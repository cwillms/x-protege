package de.dfki.x_protege.ui.components;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import de.dfki.x_protege.model.OntologyCache;
import de.dfki.x_protege.model.data.Annotation;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.ui.utilities.X_Icons;

/**
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 21.03.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class DescriptionPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4874127231705810818L;

	private final JLabel uri = new JLabel();

	protected X_Entity representedEntity;

	private JPanel annoCenterPanel;

	protected JPanel mainPanel;

	/**
	 * Creates a new instance of {@link DescriptionPanel}.
	 */
	public DescriptionPanel() {
		layoutComponents();
	}

	public void setEntity(X_Entity e) {
		this.representedEntity = e;
		updateView();
	}

	private void layoutComponents() {
		this.setDoubleBuffered(true);
		this.mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		// add URIPANEL
		JPanel uriPanel = new JPanel(new FlowLayout());
		uriPanel.add(new JLabel("URI: "));
		uri.setBackground(Color.WHITE);
		uriPanel.add(uri);
		uriPanel.add(new JButton("EDIT"));
		mainPanel.add(uriPanel);
		// add AnnotationPanel
		JPanel annotationsPanel = new JPanel();
		JLabel anno = new JLabel("Annotations:");
		anno.setIcon(X_Icons.annoProp);
		annoCenterPanel = new JPanel();
		annoCenterPanel
				.setLayout(new BoxLayout(annoCenterPanel, BoxLayout.Y_AXIS));
		annotationsPanel.add(anno, BorderLayout.NORTH);
		annotationsPanel.add(annoCenterPanel, BorderLayout.CENTER);
		mainPanel.add(annotationsPanel);
		addAdditionalComponents();
	}

	protected void addAdditionalComponents() {
		// nothing to do here;
	}

	private void updateView() {
		this.uri.setText(OntologyCache.getNamespace()
				.getLongForShort(representedEntity.render()));
		this.annoCenterPanel.removeAll();
		for (Annotation a : representedEntity.getAnnotations()) {
			this.annoCenterPanel.add(new JLabel(a.render()));
		}
		extendedUpdateView();
		this.revalidate();
	}

	protected void extendedUpdateView() {
		// nothing to do here
	}
}
