/**
 * 
 */
package de.dfki.x_protege.ui.components.lists;

import java.util.ArrayList;

import org.protege.editor.core.ui.list.MList;
import org.protege.editor.core.ui.list.MListSectionHeader;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.model.data.AbstractX_Property;
import de.dfki.x_protege.model.data.Ontology;
import de.dfki.x_protege.model.data.X_Type;
import de.dfki.x_protege.ui.logic.X_Rule;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.Type.ModelChangeType;

/**
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 24.04.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class RuleList extends MList {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2855980597166995224L;
	private X_EditorKit editorKit;
	private String HEADER_TEXT;
	private boolean canAdd = false;
	private boolean state = false;
	private boolean temp = false;
	private MListSectionHeader header = new MListSectionHeader() {

		@Override
		public String getName() {
			return HEADER_TEXT;
		}

		@Override
		public boolean canAdd() {
			return canAdd;
		}

	};

	private X_Type root;

	/**
	 * Creates a new instance of {@link PropertiesList}.
	 * 
	 * @param x_EditorKit
	 *            The connected {@link X_EditorKit}.
	 */
	public RuleList(X_EditorKit x_EditorKit) {
		this.editorKit = x_EditorKit;
		this.HEADER_TEXT = "For debugging :-)";
	}

	/**
	 * Populates the list with the {@link AbstractX_Property}s connected to the
	 * given {@link X_Type}.
	 * 
	 * @param root
	 */
	@SuppressWarnings("unchecked")
	public void setRootObject(Ontology ont) {

		java.util.List<Object> data = new ArrayList<Object>();
		data.add(header);

		if (ont != null) {
			// @@TODO ordering
			for (X_Rule r : ont.getRules()) {
				data.add(new RuleListItem(r));
			}

		}

		setListData(data.toArray());
		revalidate();
	}

	@Override
	protected void handleAdd() {
		System.err.println("Not yet implemented. Sorry!");
		// this.editorKit.getX_Workspace().getEventUtil()
		// .addProperty(this.HEADER_TEXT, temp);
	}

	public void handleDelete(AbstractX_Property prop) {
		if (temp) {
			this.editorKit.getX_ModelManager().handleChange(new ModelChange(
					ModelChangeType.TEMPDOMAINREMOVE, prop, root));
		} else {
			this.editorKit.getX_ModelManager().handleChange(
					new ModelChange(ModelChangeType.DOMAINREMOVED, prop, root));
		}
	}

}
