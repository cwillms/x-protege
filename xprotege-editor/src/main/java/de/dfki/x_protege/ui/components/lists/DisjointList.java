package de.dfki.x_protege.ui.components.lists;

import java.util.ArrayList;

import org.protege.editor.core.ui.list.MList;
import org.protege.editor.core.ui.list.MListSectionHeader;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.model.data.X_Type;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.Type.ModelChangeType;
import de.dfki.x_protege.ui.utilities.rendering.X_ProtegeRenderer;

/**
 * The {@link DisjointList} is a part of the classes tab. It is used to
 * visualize the {@link X_Type}s disjoint to a given {@link X_Type}. <br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 17.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class DisjointList extends MList {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6409777508408202820L;
	private final String HEADER_TEXT = "Disjoint With";
	private MListSectionHeader header = new MListSectionHeader() {

		@Override
		public String getName() {
			return HEADER_TEXT;
		}

		@Override
		public boolean canAdd() {
			return true;
		}
	};

	private X_EditorKit editorKit;
	private X_Type root;

	/**
	 * Create a new instance of {@link DisjointList}.
	 * 
	 * @param x_EditorKit
	 *            The connected {@link X_EditorKit}.
	 */
	public DisjointList(X_EditorKit x_EditorKit) {
		this.editorKit = x_EditorKit;
		setCellRenderer(new X_ProtegeRenderer());
	}

	/**
	 * Populates the list with the {@link X_Type}s disjoint from the given
	 * {@link X_Type}.
	 * 
	 * @param t
	 *            The {@link X_Type} to which the nodes in the list are
	 *            disjoint.
	 */
	@SuppressWarnings("unchecked")
	public void setRootObject(X_Type t) {

		java.util.List<Object> data = new ArrayList<Object>();
		this.root = t;
		data.add(header);

		if (t != null) {
			// @@TODO ordering
			for (X_Type disjoints : t.getDisjoints()) {
				data.add(new DisjointListItem(disjoints, this));
			}
		}

		setListData(data.toArray());
		revalidate();
	}

	@Override
	protected void handleAdd() {
		this.editorKit.getX_Workspace().getEventUtil().addX_Type(root,
				this.HEADER_TEXT, false);
	}

	/**
	 * Remove the given {@link X_Type}.
	 * 
	 * @param disjoints
	 *            The {@link X_Type} to be removed.
	 */
	void handleDelete(X_Type disjoints) {
		this.editorKit.getX_ModelManager().handleChange(new ModelChange(
				ModelChangeType.DISJOINTREMOVED, root, disjoints));
	}

}
