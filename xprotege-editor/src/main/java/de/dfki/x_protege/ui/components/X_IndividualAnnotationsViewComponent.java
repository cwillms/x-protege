package de.dfki.x_protege.ui.components;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import org.protege.editor.core.ui.util.AugmentedJTextField;

import de.dfki.x_protege.model.OntologyCache;
import de.dfki.x_protege.model.data.Annotation;
import de.dfki.x_protege.model.data.X_Individual;
import de.dfki.x_protege.model.data.X_Type;
import de.dfki.x_protege.ui.components.lists.AnnotationList;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.SelectionChange;
import de.dfki.x_protege.ui.logic.Event.Type.ModelChangeType;
import de.dfki.x_protege.ui.logic.Event.Type.SelectionType;
import de.dfki.x_protege.ui.utilities.UIHelper;
import de.dfki.x_protege.ui.utilities.rendering.X_ProtegeAnnotationRenderer;

/**
 * The {@link X_IndividualAnnotationViewComponent} is used within the
 * class-editor to maintain the {@link Annotation}s connected to the currently
 * selected {@link X_Individual}. To do so it uses an instance of
 * {@link AnnotationList}. This component also allows the renaming of the
 * currently selected {@link X_Individual}. <br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 20.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class X_IndividualAnnotationsViewComponent
		extends
			AbstractX_IndividualViewComponent {

	private static final long serialVersionUID = -3036939007144710932L;

	private AnnotationList list;

	private final AugmentedJTextField individualUriField = new AugmentedJTextField(
			"e.g http://www.example.com/ontologies/myontology");

	public static final String ONTOLOGY_IRI_FIELD_LABEL = "Instance URI";

	@Override
	protected void handleModelChanged(final ModelChange modelChanged) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {
				if (modelChanged.getType().equals(ModelChangeType.RENAME)
						|| modelChanged.getType()
								.equals(ModelChangeType.ANNOTATIONADDED)
						|| modelChanged.getType()
								.equals(ModelChangeType.ANNOTATIONEDITED)
						|| modelChanged.getType()
								.equals(ModelChangeType.ANNOTATIONREMOVED))
					updateView();
			}
		};
		SwingUtilities.invokeLater(doHighlight);
	}

	@Override
	protected void handleSelectionChanges(
			final SelectionChange selectionchanges) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {
				switch (selectionchanges.getType()) {
					case INDIVIDUALHIERARCHY : {
						updateView();
						break;
					}
					case ICLASSHIERARCHY : {
						updateView();
						break;
					}
					default :
						break;
				}
			}
		};
		SwingUtilities.invokeLater(doHighlight);

	}

	@Override
	public void initialiseIndividualsView() throws Exception {
		setLayout(new BorderLayout());
		this.individualUriField.setBackground(Color.WHITE);
		list = new AnnotationList(getX_EditorKit());
		list.setCellRenderer(new X_ProtegeAnnotationRenderer());
		individualUriField.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				warn(false);
			}
		});

		individualUriField.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				warn(true);
			}

			@Override
			public void focusGained(FocusEvent e) {
				// Nothing to do here
			}
		});
		JPanel ontologyIRIPanel = new JPanel(new GridBagLayout());
		add(ontologyIRIPanel, BorderLayout.NORTH);
		Insets insets = new Insets(0, 4, 2, 0);
		ontologyIRIPanel.add(new JLabel(ONTOLOGY_IRI_FIELD_LABEL));
		ontologyIRIPanel.add(individualUriField,
				new GridBagConstraints(1, 0, 1, 1, 100.0, 0.0,
						GridBagConstraints.BASELINE_LEADING,
						GridBagConstraints.HORIZONTAL, insets, 0, 0));
		JScrollPane sp = new JScrollPane(list);
		sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		add(sp, BorderLayout.CENTER);
		updateView();
	}

	@Override
	public X_Individual updateView() {
		// TODO Fix this ugly hack below ... there should only be 2 cases. (1.
		X_Type selT = selectedClass();
		X_Individual selI = getSelectedIndividual();
		if (selI != null) {
			if (selT.getConnectedIndividualsSorted().contains(selI)) {
				this.individualUriField.setText(getSelectedIndividual() == null
						? null
						: getSelectedIndividual().render());
				list.setClassRootObject(getSelectedIndividual() == null
						? null
						: getSelectedIndividual());
				setHeaderText(getSelectedIndividual() == null
						? null
						: getSelectedIndividual().render());
				if (selI.isAtomInstance()) {
					this.individualUriField.setEditable(true);
				} else {
					this.individualUriField.setEditable(false);

				}
				return getSelectedIndividual();

			} else {
				this.individualUriField.setText("");
				setHeaderText("");
				list.setClassRootObject(null);
				return null;
			}
		} else {
			this.individualUriField.setText("");
			list.setClassRootObject(null);
			return null;
		}

	}

	/**
	 * Inform the user if there are problems when renaming the selected class.
	 * 
	 * @param focus this boolean flag indicates whether the warning was triggered because the URI-Field lost focus while there is an unsubmitted change to the uri.
	 */
	protected void warn(boolean focus) {
		String name = individualUriField.getText();
		if (name != null
				&& getX_ModelManager().getActiveModel().isValidName(name)
				&& !focus) {

			getX_ModelManager().handleChange(new ModelChange(
					ModelChangeType.RENAME, getSelectedIndividual(), name));

		} else {
			if (focus) {
				individualUriField.setText(getSelectedIndividual().render());
			} else {
				new UIHelper(getX_EditorKit()).showError("Rename",
						"You selected a non valid name ...");
				individualUriField.setText(getSelectedIndividual().render());
			}

		}

	}

	private X_Type selectedClass() {
		X_Type t = (X_Type) getX_ModelManager().getActiveModel()
				.getSelection(SelectionType.ICLASSHIERARCHY);
		if (t != null) {
			return OntologyCache.getType(t.getShortName());
		}
		return t;
	}

}
