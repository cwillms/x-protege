package de.dfki.x_protege.ui.components.lists;

import org.protege.editor.core.ui.list.MListItem;

import de.dfki.x_protege.model.data.X_Individual;
import de.dfki.x_protege.model.data.X_Type;

/**
 * The {@link IndividualsListItem} represents the individuals which are
 * instances of a given {@link X_Type}. It is used by the
 * {@link IndividualsList} to visualize these Individuals. <br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 17.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class IndividualsListItem implements MListItem {

	private final X_Individual indi;

	public IndividualsListItem(X_Individual indi) {
		this.indi = indi;
	}

	public X_Individual getIndividual() {
		return this.indi;
	}

	@Override
	public boolean isEditable() {
		return false;
	}

	@Override
	public void handleEdit() {
		// Nothing to do here.
	}

	@Override
	public boolean isDeleteable() {
		return false;
	}

	@Override
	public boolean handleDelete() {
		// Nothing to do here.
		return false;
	}

	@Override
	public String getTooltip() {
		return null;
	}

	public X_Individual getValue() {
		return this.indi;
	}

}
