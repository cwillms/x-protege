package de.dfki.x_protege.ui.components;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashSet;
import java.util.Set;

import javax.swing.SwingUtilities;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.event.TreeSelectionListener;

import org.protege.editor.core.ui.util.ComponentFactory;

import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.ui.components.tree.TreeNodeGenerator;
import de.dfki.x_protege.ui.components.tree.X_ElementTree;

/**
 * The {@link AbstractX_ElementHierarchyViewComponent} is the basic
 * {@link Component} used by {@link Component} representing hierarchies, e.g.,
 * the subclass hierarchy ( {@link ClassHierarchyViewComponent} ).<br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 20.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public abstract class AbstractX_ElementHierarchyViewComponent<E extends X_Entity>
		extends AbstractX_SelectionViewComponent {

	private static final long serialVersionUID = -5980351290853739210L;

	private X_ElementTree tree;

	private TreeSelectionListener listener;

	@Override
	final public void initialiseView() throws Exception {
		setLayout(new BorderLayout(7, 7));

		tree = initTree();
		add(ComponentFactory.createScrollPane(tree));
		performExtraInitialisation();
		tree.getModel().addTreeModelListener(new TreeModelListener() {
			@Override
			public void treeNodesChanged(TreeModelEvent e) {
			}

			@Override
			public void treeNodesInserted(TreeModelEvent e) {
				ensureSelection();
			}

			@Override
			public void treeNodesRemoved(TreeModelEvent e) {
				ensureSelection();
			}

			@Override
			public void treeStructureChanged(TreeModelEvent e) {
				ensureSelection();
			}
		});
		tree.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				transmitSelection();
			}
		});
		updateView();
	}

	/**
	 * Initializes the underlying {@link X_ElementTree}.
	 * 
	 * @return The {@link X_ElementTree} featured/maintained by this Component.
	 */
	protected abstract X_ElementTree initTree();

	/**
	 * Initialize further, more specific Elements of the component, e.g. buttons
	 * @throws Exception
     */
	protected abstract void performExtraInitialisation() throws Exception;

	/**
	 * Select the given {@link X_Entity} in the underlying tree.
	 * 
	 * @param entity The {@link X_Entity} to be selected in the underlying {@link X_ElementTree}.
	 */
	public void setSelectedEntity(X_Entity entity) {
		tree.setSelectedX_Element(TreeNodeGenerator.getNode(entity, tree.getType()));
		tree.repaint();
	}

	/**
	 * Select the given {@link X_Entity}s in the underlying
	 * {@link X_ElementTree}.
	 * 
	 * @param entities The {@link X_Entity}s to be selected in the underlying {@link X_ElementTree}.
	 */
	public void setSelectedEntities(Set<X_Entity> entities) {
		tree.setSelectedX_Elements(TreeNodeGenerator.getNodes(entities, tree.getType()));
	}

	/**
	 * Returns {@link X_Entity} currently selected in the underlying
	 * {@link X_ElementTree}.
	 * 
	 * @return The {@link X_Entity} that is currently selected in the underlying tree.
	 */
	public X_Entity getSelectedEntity() {
		return tree.getSelectedX_Element();
	}

	/**
	 *
	 * @return {@link X_Entity}s currently selected in the underlying
	 * {@link X_ElementTree}.
	 */
	public Set<X_Entity> getSelectedEntities() {
		return new HashSet<X_Entity>(tree.getSelectedX_Elements());
	}

	private void ensureSelection() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				final X_Entity entity = getSelectedEntity();
				if (entity != null) {
					X_Entity treeSel = tree.getSelectedX_Element();
					if (treeSel == null || !treeSel.equals(entity)) {
						tree.setSelectedX_Element(TreeNodeGenerator.getNode(entity, tree.getType()));
					}
				}
			}
		});
	}

	@Override
	public boolean requestFocusInWindow() {
		return tree.requestFocusInWindow();
	}

	/**
	 *
	 * @return The underlying {@link X_ElementTree} showing the hierarchy of a distict subset of entities ( properties, classes or instances)
     */
	protected X_ElementTree getTree() {
		return tree;
	}

	private void transmitSelection() {

		X_Entity selEntity = getSelectedEntity();
		updateHeader(selEntity);
	}

	@Override
	public void disposeView() {
		// Dispose of the tree selection listener
		if (tree != null) {
			tree.removeTreeSelectionListener(listener);
			tree.dispose();
		}
	}

	@Override
	protected X_Entity getObjectToCopy() {
		return tree.getSelectedX_Element();
	}

}
