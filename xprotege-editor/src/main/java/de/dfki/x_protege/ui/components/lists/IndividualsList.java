package de.dfki.x_protege.ui.components.lists;

import java.util.ArrayList;
import java.util.Comparator;

import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.protege.editor.core.ui.list.MList;
import org.protege.editor.core.ui.list.MListSectionHeader;

import de.dfki.x_protege.X_EditorKit;
import de.dfki.x_protege.model.data.X_Individual;
import de.dfki.x_protege.model.data.X_Type;
import de.dfki.x_protege.ui.logic.Event.SelectionChange;
import de.dfki.x_protege.ui.logic.Event.Type.SelectionType;

/**
 * The {@link IndividualsList} is a central part of the Individual Browser
 * located in the Instances tab of x-protégé. It is used to present the
 * {@link X_Individual}s (represented by {@link IndividualsListItem}) connected
 * to a given {@link X_Type}. <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 17.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class IndividualsList extends MList {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2937986996464797249L;
	private X_EditorKit editorKit;
	private boolean state = false;

	public final Comparator<X_Individual> nameComparator = new Comparator<X_Individual>() {

		@Override
		public int compare(X_Individual o1, X_Individual o2) {
			return o1.toString().compareTo(o2.toString());
		}

	};

	private MListSectionHeader ImplicitHeader = new MListSectionHeader() {

		@Override
		public String getName() {
			return "Implicit Instances";
		}

		@Override
		public boolean canAdd() {
			return false;
		}
	};

	private final ListSelectionListener selectionListener = new ListSelectionListener() {

		@Override
		public void valueChanged(ListSelectionEvent e) {
			if (getSelectedValue() != null) {
				editorKit.getX_ModelManager()
						.handleSelectionChanged(new SelectionChange(
								SelectionType.INDIVIDUALHIERARCHY,
								((IndividualsListItem) getSelectedValue())
										.getIndividual()));
			}

		}
	};

	/**
	 * Create a new instance of {@link IndividualsList}.
	 * 
	 * @param x_EditorKit
	 *            The connected {@link X_EditorKit}.
	 */
	public IndividualsList(X_EditorKit x_EditorKit) {
		this.editorKit = x_EditorKit;
		this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.addListSelectionListener(selectionListener);
	}

	/**
	 * populates the list with the {@link X_Individual}s connected to the given
	 * {@link X_Type}.
	 * 
	 * @param root the {@link X_Type} used to populate the list
	 */
	@SuppressWarnings("unchecked")
	public void setRootObject(X_Type root) {
		java.util.List<Object> data = new ArrayList<>();

		if (root != null) {
			for (X_Individual indi : root.getConnectedIndividualsSorted()) {
				data.add(new IndividualsListItem(indi));
			}
			if (state) {
				data.add(ImplicitHeader);
				for (X_Individual indi : root.getSubIndividualsSorted()) {
					data.add(new IndividualsListItem(indi));
				}
			}
		}

		setListData(data.toArray());
		revalidate();
	}

	/**
	 *
	 * @param indi the {@link X_Individual}
	 * @return The {@link IndividualsListItem} representing the given {@link X_Individual} if such a item is in the list, null otherwise
     */
	public IndividualsListItem findMatch(X_Individual indi) {
		for (int i = 0; i < this.getModel().getSize(); i++) {
			if (((IndividualsListItem) this.getModel().getElementAt(i))
					.getIndividual() == indi)
				return (IndividualsListItem) this.getModel().getElementAt(i);
		}
		return null;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	/**
	 * 
	 */
	public void selectFirstItem() {
		if (getModel().getElementAt(0) instanceof MListSectionHeader)
			setSelectedIndex(1);
		else
			setSelectedIndex(0);

	}
}
