package de.dfki.x_protege.ui.logic;

import java.util.ArrayList;

import de.dfki.lt.hfc.Rule;

/**
 * TODO remove this class, we will not use rules in the near future
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 24.04.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class X_Rule {

	private final Rule hfcRule;

	private ArrayList<String[]> lhs = new ArrayList<>();

	private ArrayList<String[]> rhs = new ArrayList<>();

	private String operator;

	private String textualRep;

	private boolean changed = false;

	/**
	 * 
	 */
	public X_Rule(Rule hfcRule, String textualRep) {
		this.hfcRule = hfcRule;
		parseText(textualRep);
	}

	public Rule getSource() {
		return this.hfcRule;
	}

	private void parseText(String textRep) {

		String[] rules = textRep.split("\\r?\\n");
		boolean lhs = true;
		for (String r : rules) {
			if (r.contains("->") || r.contains("-->") || r.contains("=>")
					|| r.contains("==>")) {
				lhs = false;
				operator = r;
			} else {
				if (lhs)
					this.lhs.add(r.split("\\s"));
				else
					this.rhs.add(r.split("\\s"));
			}
		}
	}

	/**
	 * @return
	 */
	public String getTextRep() {
		if (textualRep == null || changed == true) {
			StringBuilder strb = new StringBuilder();
			strb.append("<html>");
			for (String[] r : lhs) {
				for (String t : r) {
					strb.append(t.replace("<", "&lt;").replace(">", "&gt;"));
					strb.append(" ");
				}
				strb.append("<br>");
			}
			strb.append(operator);
			strb.append("<br>");
			for (String[] r : rhs) {
				for (String t : r) {
					strb.append(t.replace("<", "&lt;").replace(">", "&gt;"));
					strb.append(" ");
				}
				strb.append("<br>");
			}
			textualRep = strb.toString();
			strb.append("</html>");
		}
		return textualRep;
	}
}
