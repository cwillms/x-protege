package de.dfki.x_protege.ui.utilities.rendering;

import java.awt.Component;
import java.util.Set;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeNode;

import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.ui.components.SuperClassHierarchyViewComponent;
import de.dfki.x_protege.ui.components.tree.X_EntityTreeNode;
import de.dfki.x_protege.ui.utilities.X_Icons;

/**
 * This class is used of the {@link JTree} located in the
 * {@link SuperClassHierarchyViewComponent}.
 * 
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 13.02.2016<br>
 * <br>
 * <p/>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class X_SuperClassProtegeRenderer extends JLabel implements TreeCellRenderer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1831835753984263594L;

	private Set<TreeNode> path;

	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded,
			boolean leaf, int row, boolean hasFocus) {
		Icon icon = new ImageIcon();
		if (value instanceof TreeNode) {
			X_Entity e = ((X_EntityTreeNode) value).getValue();
			boolean inPath = !path.contains(value);
			if (inPath) {
				icon = selectIcon(e, false);
			} else {
				icon = selectIcon(e, true);
			}

			setIcon(icon);
			String text = e.render();
			setText(text);

			return this;
		} else {
			throw new IllegalArgumentException("[X-ProtegeRenderer]not able to compute icon for: " + value);
		}
	}

	private Icon selectIcon(X_Entity e, boolean b) {

		if (b) {
			if (e.isAtomType()) {
				return X_Icons.owlType;
			}
			if (e.isCartesianType()) {
				return X_Icons.cartType;
			}
		} else {
			if (e.isAtomType()) {
				return X_Icons.disabledowlType;
			}
			if (e.isCartesianType()) {
				return X_Icons.disabledcartType;
			}
		}
		return X_Icons.defaultIcon;
	}

	/**
	 *
	 * @return the path to be highlighted.
     */
	public Set<TreeNode> getPath() {
		return this.path;
	}

	/**
	 * Set the path to be highlighted to the given one.
	 * @param path A {@link Set} of {@link TreeNode}s representing the nodes to be highlighted.
     */
	public void setPath(Set<TreeNode> path) {
		this.path = path;
	}
}
