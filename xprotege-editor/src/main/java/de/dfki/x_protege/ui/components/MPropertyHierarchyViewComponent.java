package de.dfki.x_protege.ui.components;

import java.awt.event.ActionEvent;

import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultTreeModel;

import de.dfki.x_protege.model.data.AbstractX_Property;
import de.dfki.x_protege.model.data.X_Entity;
import de.dfki.x_protege.model.data.X_Property;
import de.dfki.x_protege.model.data.X_Type;
import de.dfki.x_protege.ui.components.tree.TreeNodeGenerator;
import de.dfki.x_protege.ui.components.tree.X_ElementTree;
import de.dfki.x_protege.ui.components.tree.X_EntityTreeNode;
import de.dfki.x_protege.ui.logic.Event.ModelChange;
import de.dfki.x_protege.ui.logic.Event.SelectionChange;
import de.dfki.x_protege.ui.logic.Event.Type.ModelChangeType;
import de.dfki.x_protege.ui.logic.Event.Type.SelectionType;
import de.dfki.x_protege.ui.logic.actions.AbstractX_ElementTreeAction;
import de.dfki.x_protege.ui.utilities.X_Icons;

/**
 * The {@link MPropertyHierarchyViewComponent} contains an instance of
 * {@link X_ElementTree} to present the subproperty hierarchy for the
 * {@link X_Property}s of connected to the loaded ontology. It offers options to
 * manipulate this hierarchy.<br>
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 20.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class MPropertyHierarchyViewComponent
		extends
			AbstractX_ElementHierarchyViewComponent<AbstractX_Property> {

	private static final long serialVersionUID = 6526618061615813132L;

	private AbstractX_ElementTreeAction<X_Type> addSub;

	private AbstractX_ElementTreeAction<X_Type> addSib;

	private AbstractX_ElementTreeAction<X_Type> delete;

	@Override
	protected void handleSelectionChanges(
			final SelectionChange selectionchanges) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {

				switch (selectionchanges.getType()) {
					case PROPERTYHIERARCHY : {
						updateView();
						break;
					}
					default :
						break;
				}
			}
		};
		SwingUtilities.invokeLater(doHighlight);

	}

	@Override
	protected void handleModelChanged(final ModelChange modelChanged) {
		Runnable doHighlight = new Runnable() {
			@Override
			public void run() {
				switch (modelChanged.getType()) {
					case PROPERTYREMOVED : {
						handleRemove((X_Entity) modelChanged.getValue());
						break;
					}
					case PROPERTYSUBADDED : {
						handleAddSub((X_Entity) modelChanged.getValue());
						break;
					}
					case PROPERTYSIBADDED : {
						handleAddSib((X_Entity) modelChanged.getValue());
						break;
					}
					case RENAME : {
						X_Entity e = (X_Entity) modelChanged.getValue();
						if (e.isProperty()) {
							X_EntityTreeNode node = TreeNodeGenerator.getNode(e,
									getTree().getType());
							setSelectedEntity(e);
							((DefaultTreeModel) getTree().getModel())
									.nodeChanged(node);
							updateView();
						}
						break;
					}
					case SHOWFINDING : {
						X_Entity e = (X_Entity) modelChanged.getValue();
						if (e.isProperty()) {
							setSelectedEntity(e);
							updateView();
						}
						break;
					}
					case TEMPRENAME : {
						X_Entity e = (X_Entity) modelChanged.getValue();
						if (e.isProperty()) {
							X_EntityTreeNode node = TreeNodeGenerator.getNode(e,
									getTree().getType());
							((DefaultTreeModel) getTree().getModel())
									.nodeChanged(node);
							updateView();
						}
						break;
					}
					default :
						break;
				}

			}

		};
		SwingUtilities.invokeLater(doHighlight);

	}

	/**
	 * Update the view after the given {@link X_Entity} was added to the
	 * property Hierarchy.
	 * 
	 * @param value the {@link X_Entity} which was added to the ontology.
	 */
	protected void handleAddSib(X_Entity value) {
		getTree().revalidate();
		getTree().repaint();
		setSelectedEntity(value);
	}

	/**
	 * Update the view after the given {@link X_Entity} was added to the
	 * property Hierarchy.
	 * 
	 * @param value the {@link X_Entity} which was added to the ontology.
	 */
	protected void handleAddSub(X_Entity value) {
		getTree().revalidate();
		getTree().repaint();
		setSelectedEntity(value);
	}

	/**
	 * Update the view after a property was removed from the
	 * property Hierarchy and select the given {@link X_Entity}.
	 * 
	 * @param value the {@link X_Entity} to be selected.
	 */
	protected void handleRemove(X_Entity value) {
		setSelectedEntity(value);
		updateView();
	}

	@Override
	protected void performExtraInitialisation() throws Exception {
		this.addSub = new AbstractX_ElementTreeAction<X_Type>(
				"Add sub property", X_Icons.property_addSub,
				getTree().getSelectionModel()) {
			private static final long serialVersionUID = -4067967212391062364L;

			@Override
			public void actionPerformed(ActionEvent event) {
				addSubProperty();
			}

		};
		addAction(addSub, "A", "A");

		this.addSib = new AbstractX_ElementTreeAction<X_Type>(
				"Add sibling sibling", X_Icons.property_addSib,
				getTree().getSelectionModel()) {

			private static final long serialVersionUID = 9163133195546665441L;

			@Override
			public void actionPerformed(ActionEvent event) {
				addSiblingProperty();
			}

		};
		addAction(this.addSib, "A", "B");

		this.delete = new AbstractX_ElementTreeAction<X_Type>("Delete property",
				X_Icons.property_delete, getTree().getSelectionModel()) {

			private static final long serialVersionUID = 9163133195546665441L;

			@Override
			public void actionPerformed(ActionEvent event) {
				removeProperty();
			}

		};

		addAction(this.delete, "B", "A");
		updateView();
	}

	/**
	 * Fire a {@link ModelChange}-Event of {@link ModelChangeType}
	 * .PROPERTYREMOVED and the currently selected property.
	 */
	protected void removeProperty() {
		X_Entity sel = selectedProperty();
		getX_ModelManager().handleChange(
				new ModelChange(ModelChangeType.PROPERTYREMOVED, sel));
	}

	/**
	 * Fire a {@link ModelChange}-Event of {@link ModelChangeType}
	 * .PROPERTYSIBADDED and the currently selected property.
	 */
	protected void addSiblingProperty() {
		X_Entity sel = selectedProperty();
		getX_ModelManager().handleChange(
				new ModelChange(ModelChangeType.PROPERTYSIBADDED, sel));
	}

	/**
	 * Fire a {@link ModelChange}-Event of {@link ModelChangeType}
	 * .PROPERTYSUBADDED and the currently selected property.
	 */
	protected void addSubProperty() {
		X_Entity sel = selectedProperty();
		getX_ModelManager().handleChange(
				new ModelChange(ModelChangeType.PROPERTYSUBADDED, sel));
	}

	private X_Entity selectedProperty() {
		return getX_ModelManager().getActiveModel()
				.getSelection(SelectionType.PROPERTYHIERARCHY);
	}

	@Override
	protected X_Entity updateView() {
		X_Entity sel = selectedProperty();
		this.setHeaderText(sel.render());
		this.addSib.setEnabled(canCreateNewSibling(sel));
		this.delete.setEnabled(canDeleteClass(sel));
		getTree().revalidate();
		this.revalidate();
		return sel;
	}

	private boolean canDeleteClass(X_Entity selected) {
		return !getX_ModelManager().getActiveModel().isStandard(selected);
	}

	private boolean canCreateNewSibling(X_Entity selected) {
		if (TreeNodeGenerator.getNode(selected, getTree().getType())
				.getParent() == null) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	protected X_ElementTree initTree() {
		return new X_ElementTree(SelectionType.PROPERTYHIERARCHY, true,
				getX_ModelManager());
	}
}
