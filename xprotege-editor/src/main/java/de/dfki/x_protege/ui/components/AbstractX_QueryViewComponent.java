/**
 * 
 */
package de.dfki.x_protege.ui.components;

import de.dfki.x_protege.ui.logic.Event.QueryEvent;
import de.dfki.x_protege.ui.logic.listener.QueryEventListener;

/**
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 22.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public abstract class AbstractX_QueryViewComponent extends AbstractX_SelectionViewComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = -905620362308296749L;

	/**
	 * used to compute the completion suggestions and forward them to the
	 * {@link QueryInputForm}.
	 */
	protected QueryEventListener qListener = new QueryEventListener() {

		@Override
		public void queryEvent(QueryEvent query) {
			handleQueryEvent(query);

		}
	};

	/**
	 * updates the QueryInputForm according to the given {@link QueryEvent}.
	 * 
	 * @param queryEvent the {@link QueryEvent} to be handled.
	 */
	protected abstract void handleQueryEvent(final QueryEvent queryEvent);

}
