package de.dfki.x_protege;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * This class is quite minimal for the moment. It only implements the osgi.BundleActivator interface <br>
 * @see BundleActivator
 * <br>
 * Author: Christian Willms<br>
 * German Research Center for Artificial Intelligence (DFKI)<br>
 * Date: 17.02.2016<br>
 * <br>
 * christian.willms@dfki.de<br>
 * <br>
 */
public class X_Protege implements BundleActivator {

  public static final String ID = "de.dfki.x_protege";

  private static BundleContext context;

  public void start(BundleContext context) throws Exception {
    X_Protege.context = context;
  }

  public void stop(BundleContext context) throws Exception {
    X_Protege.context = null;
  }

  public static BundleContext getBundleContext() {
    return context;
  }

}
