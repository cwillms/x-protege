#
# the below axiomatic triples can be found in ter Horst's
# papers and are a straightforward extension of work done
# by Pat Hayes for RDF Schema;
# I have omitted the RDF-specific triples, since they do
# not have an effect on the soundness of the entailed triples;
# I have also added a few extensions to the set of axioms;
#
# NOTE: six axiomatic triples have been removed for the
#       equivalence class reduction, viz.,
#         <owl:sameAs> <rdf:type> <owl:TransitiveProperty> .
#         <owl:sameAs> <rdf:type> <owl:SymmetricProperty> .
#         <owl:equivalentClass> <rdf:type> <owl:TransitiveProperty> .
#         <owl:equivalentClass> <rdf:type> <owl:SymmetricProperty> .
#         <owl:equivalentProperty> <rdf:type> <owl:TransitiveProperty> .
#         <owl:equivalentProperty> <rdf:type> <owl:SymmetricProperty> .
#
# NOTE: three axiomatic triples have been added instead:
#         <owl:equivalentClass> <rdf:type> <rdf:Property> .
#         <owl:equivalentProperty> <rdf:type> <rdf:Property> .
#         <owl:sameAs> <rdf:type> <rdf:Property> .
#
# @author Hans-Ulrich Krieger
# @version Mon Jun 20 17:40:21 CEST 2011
#

<rdf:type> <rdf:type> <rdf:Property> .
<rdf:subject> <rdf:type> <rdf:Property> .
<rdf:predicate> <rdf:type> <rdf:Property> .
<rdf:object> <rdf:type> <rdf:Property> .
<rdf:first> <rdf:type> <rdf:Property> .
<rdf:rest> <rdf:type> <rdf:Property> .
<rdf:nil> <rdf:type> <rdf:List> .

<owl:equivalentClass> <rdf:type> <rdf:Property> .
<owl:equivalentClass> <rdfs:subPropertyOf> <rdfs:subClassOf> .

<owl:equivalentProperty> <rdf:type> <rdf:Property> .
<owl:equivalentProperty> <rdfs:subPropertyOf> <rdfs:subPropertyOf> .

<owl:sameAs> <rdf:type> <rdf:Property> .

<rdfs:subClassOf> <rdf:type> <owl:TransitiveProperty> .
<rdfs:subPropertyOf> <rdf:type> <owl:TransitiveProperty> .

<owl:disjointWith> <rdf:type> <owl:SymmetricProperty> .

<owl:inverseOf> <rdf:type> <owl:SymmetricProperty> .

<owl:FunctionalProperty> <rdfs:subClassOf> <rdf:Property> .
<owl:TransitiveProperty> <rdfs:subClassOf> <rdf:Property> .
<owl:SymmetricProperty> <rdfs:subClassOf> <rdf:Property> .
<owl:InverseFunctionalProperty> <rdfs:subClassOf> <rdf:Property> .
<owl:AnnotationProperty> <rdfs:subClassOf> <rdf:Property> .

<owl:differentFrom> <rdf:type> <owl:SymmetricProperty> .

# I have added the four below triples
<owl:Thing> <rdf:type> <owl:Class> .
<owl:Nothing> <rdf:type> <owl:Class> .
<owl:Nothing> <rdfs:subClassOf> <owl:Thing> .
<owl:Thing> <owl:disjointWith> <owl:Nothing> .

# I have added the below triples
<nary:Thing+> <rdf:type> <nary:Class>.
<nary:Nothing+> <rdf:type> <nary:Class>.
<nary:Thing+> <owl:disjointWith> <nary:Nothing+>.
<nary:Nothing+> <rdfs:subClassOf> <nary:Thing+>.
<owl:Thing> <rdfs:subClassOf> <nary:Thing+>.
<xsd:AnyType> <rdfs:subClassOf> <nary:Thing+>.
<xsd:AnyType> <rdf:type> <rdfs:Datatype>.

<nary:MixedProperty> <rdfs:subClassOf> <rdf:Property> 
<owl:DatatypeProperty> <rdfs:subClassOf> <nary:MixedProperty> .
<owl:ObjectProperty> <rdfs:subClassOf> <nary:MixedProperty>.
# XSD Datatypes
<xsd:int> <rdf:type> <rdfs:Datatype> .
<xsd:int> <rdfs:subClassOf> <xsd:AnyType>.
<xsd:long> <rdf:type> <rdfs:Datatype> .
<xsd:long> <rdfs:subClassOf> <xsd:AnyType>.
<xsd:float> <rdf:type> <rdfs:Datatype> .
<xsd:float> <rdfs:subClassOf> <xsd:AnyType>.
<xsd:double> <rdf:type> <rdfs:Datatype> .
<xsd:double> <rdfs:subClassOf> <xsd:AnyType>.
<xsd:string> <rdf:type> <rdfs:Datatype> .
<xsd:string> <rdfs:subClassOf> <xsd:AnyType>.
<xsd:boolean> <rdf:type> <rdfs:Datatype> .
<xsd:boolean> <rdfs:subClassOf> <xsd:AnyType>.
<xsd:date> <rdf:type> <rdfs:Datatype> .
<xsd:data> <rdfs:subClassOf> <xsd:AnyType>.
<xsd:dateTime> <rdf:type> <rdfs:Datatype> .
<xsd:dateTime> <rdfs:subClassOf> <xsd:AnyType>.
<xsd:gYear> <rdf:type> <rdfs:Datatype> .
<xsd:gYear> <rdfs:subClassOf> <xsd:AnyType>.
<xsd:gMonthYear> <rdf:type> <rdfs:Datatype> .
<xsd:gMonthYear> <rdfs:subClassOf> <xsd:AnyType>.
<xsd:anyURI> <rdf:type> <rdfs:Datatype> .
<xsd:anyURI> <rdfs:subClassOf> <xsd:AnyType>.
<xsd:monetary> <rdf:type> <rdfs:Datatype> .
<xsd:monetary> <rdfs:subClassOf> <xsd:AnyType>.
<xsd:uDateTime> <rdf:type> <rdfs:Datatype> .
<xsd:uDateTime> <rdfs:subClassOf> <xsd:AnyType>.

# Comments owl:Nothing
<owl:Nothing> <rdfs:label> "Nothing"^^<xsd:string> .
<owl:Nothing> <rdfs:comment> "This_is_the_empty_class."^^<xsd:string> .
<owl:Nothing> <rdfs:isDefinedBy> "<http://www.w3.org/2002/07/owl#>"^^<xsd:string> .
# Comments owl:Thing
<owl:Thing> <rdfs:label> "Thing"^^<xsd:string> .
<owl:Thing> <rdfs:comment> "The_class_of_OWL_individuals."^^<xsd:string> .
<owl:Thing> <rdfs:isDefinedBy> "<http://www.w3.org/2002/07/owl#>"^^<xsd:string> .

#<rdfs:Datatype> <rdfs:subClassOf> <rdfs:Class> .
#<owl:Class> <rdfs:subClassOf> <rdfs:Class> .
#<owl:Class> <owl:disjointWith> <rdfs:Datatype> .
