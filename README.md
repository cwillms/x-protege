# X-Protege Repository #

X-Protege is an ontology development environment supporting n-ary relations and extra arguments for relations.
It  comes up with all features needed to efficiently develop an ontology. Some of those features are:

- support of rdf, owl as well as n-ary relations and extra arguments for relations
- workspaces for the maintaining of classes, properties and individuals
- several in and output formats
- adaptable and flexible layout
- short cuts
- drag and drop

***
## Authors ##
name | role 
---|---
Christian Willms |  developer 
Hans-Ulrich Krieger | adviser

## Scientific Background ##

This tool is based on the work presented in the following paper:

[X-Protege: An Ontology Editor for Defining Cartesian Types to Represent n-ary Relations
](https://www.dfki.de/web/forschung/publikationen?pubid=8283)

[Extending OWL Ontologies by Cartesian Types to Represent N-ary Relations in Natural Language](https://www.dfki.de/web/forschung/publikationen?pubid=7731)

## Acknowledgements ##
This work was conducted using the Protégé resource, which is supported by grant GM10331601 from the National Institute of General Medical Sciences of the United States National Institutes of Health. 

[Protege ]( http://protege.stanford.edu)

This work is also based on HFC, a rule-based forward chainer implemented in the LT Lab at DFKI for reasoning and querying with
OWL-encoded ontologies, but it supports arbitrary n-tuples.

[An Efficient Implementation of Equivalence Relations in OWL via Rule and Query Rewriting](http://www.dfki.de/lt/publication_show.php?id=6936)

## Example Output ##

The commented example output for one a simple ontology can be found in the `examples` folder.

## Prerequisites ##

To run the compiled X-Protege located in the dolwnload section (link: [X-Protege_v1.2](https://bitbucket.org/cwillms/x-protege/downloads/x-protege_v1.2.zip) only [Java version 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html) is required.   

If you want compile X-Protege from source please follow the introductions in the user [documentation](https://bitbucket.org/cwillms/x-protege/downloads/Documentation.pdf) or the corresponding section in the [wiki](https://bitbucket.org/cwillms/x-protege/wiki/Compile_X-Protege_from_source)(WIP).

## Javadoc ##

The javadoc of the Java portion is located at `docs/`.